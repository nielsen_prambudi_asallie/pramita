import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
import { RouterModule } from '@angular/router';
import { FuseAngularMaterialModule } from '../components/angular-material/angular-material.module';
import { HttpModule } from '@angular/http';


const routes = [
    {
        path        : 'dashboards/project',
        loadChildren: './dashboards/project/project.module#ProjectModule'
    },
    {
      path        : 'master',
      loadChildren: './master/master.module#MasterModule'
    },
    {
        path        : 'cust',
        loadChildren: './cust/registration.module#RegistrationModule'
    },
    {
        path        : 'specimen',
        loadChildren: './specimen/specimen.module#SpecimenModule'
    },
    {
      path        : 'specimen_non_lab',
      loadChildren: './specimen_non_lab/specimen_nl.module#SpecimenNonLabModule'
    },
    {
        path        : 'analysis',
        loadChildren: './analysis/analysis.module#AnalysisModule'
    },
    {
      path        : 'analysis_non_lab',
      loadChildren: './analysis_non_lab/analysis_nl.module#AnalysisNonLabModule'
    },
    {
        path        : 'result',
        loadChildren: './result/result.module#ResultModule'
    }
];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseAngularMaterialModule,
        HttpModule
    ],
    declarations: []
})
export class FuseAppsModule
{
}
