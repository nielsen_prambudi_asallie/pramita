import { Component, OnDestroy, ElementRef, OnInit, ViewEncapsulation, ViewChild, Inject } from '@angular/core';
import { ResultDetailService } from './result_detail.service';
import { fuseAnimations } from '../../../../../core/animations';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Product } from './result_detail.model';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar, MatSnackBarVerticalPosition } from '@angular/material';
import { Location } from '@angular/common';
import { MatPaginator, MatSort,  MatTableDataSource  } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ActivatedRoute, Router } from '@angular/router';
import {of} from "rxjs/observable/of";
import {catchError, finalize} from "rxjs/operators";
import { Options } from 'selenium-webdriver';
  

export interface Detail {
    nama?;
    estVol?;
    no?;
    bodySite?;
    loCSite?;
    drawSite?;
    drawUser?;
    draw?: boolean;
    condition?;
    specimenId?;
    note?;
    received?: boolean;
    receiveDate?: "yyyy-MM-dd";
    receiveUser?: "yyyy-MM-dd";
}

export interface Detail2 {
    registerTestId?;
    testName?;
    cito?: boolean;
    scheduleName?;
}

export interface Detail3 {
    id?;
    name?;
    result?;
    result_comment?;
    auth?: boolean;
    authDate?;
    regno?;
    regdate?;
}

@Component({
    selector     : 'result_detail',
    templateUrl  : './result_detail.component.html',
    styleUrls    : ['./result_detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ResultDetailComponent implements OnInit
{
  bgColor: string;
  largerValue: any = [];
  lessValue: any = [];
    process_sample_detail: {insid: any, methodId:any, insname: any, age:any, pasid: any }
    insname: any;
    insid: any;
    pasid: any;
    methodId: any;
    age: any;
    regdate: any;
    user: any;
    doctor: any;
    tampungItemProcess: any = [];
    tampungSampleProcess: any = [];
    tampungSampleReceived: any = [];
    tampungResultAuth: any = [];
    createData : any = [];
    data: any;
    createRes: any = [];
    detail: Detail[];
    detail2: Detail2[];
    detail3: Detail3[];
    drawReceive: FormGroup;
    patientNoteGroup: FormGroup;
    physicianNoteGroup: FormGroup;
    referralNoteGroup: FormGroup;
    labNoteGroup: FormGroup;
    samples: FormGroup;
    verticalPosition: MatSnackBarVerticalPosition;
    errorMessage: boolean;
    successMessage: boolean;
    editor: string;
    dataSourceItem: FileDetailSourceItem;
    dataSourceTest: FileDetailSourceTest;
    dataSourceSample: FileDetailSourceSample;
    dataSourceSampleHandling: FileDetailSourceSampleHandling;
    dataSourceSpecimenHandling: FileDetailSourceSpecimenHandling;
    dataSourceSpecimenCollection: FileDetailSourceSpecimenCollection;
    dataSourceLabNote: FileDetailSourceLabNote;
    dataSource: FileDetailSource;
    dataSourceSch : FileDetailSourceSch;
    dataSourceStatus: FileDetailSourceStatus;
    dataSourceStatusTest: FileDetailSourceStatusTest;
    dataSourceList: FileDetailSourceList;
    dataSourceImg: FileDetailSourceImg;
    dataSourceRun: FileDetailSourceRun;
    drawer: string;
    clickDraw: boolean = false;
    clickRe: boolean = false;
    clickCito: boolean = false;

    displayedColumns = ['question', 'answer', 'note']
    displayedColumnsNoteLab = ['no', 'note', 'user', 'date'];
    displayedColumns2= ['schedule', 'test', 'day', 'time'];
    displayedColumnsItem = ['testName', 'flag', 'interfaceFlag', 'result', 'unit', 'resultComment', 'INP', 'reference', 'printGroup', 'rawResult', 'PRN', 'process', 'inputDate', 'procDate', 'cito', 'schedule', 'scheduleDateTime', 'resultSI', 'interfacingFlag', 'testid'];
    displayedColumnsRun = ['id', 'name', 'result', 'result_comment', 'status', 'analyzer'];
    displayedColumnsTest = ['testid', 'testname', 'testmethod'];
    displayedColumnsImg = ['regno', 'id', 'name'];
    displayedColumnsStatusTest = ['id', 'name', 'result', 'result_comment', 'auth', 'authdate'];
    displayedColumnsStatus = ['id', 'name', 'result', 'result_comment', 'auth', 'auth_date'];
    displayedColumnsList = ['regno', 'result', 'resultinputdate', 'p', 'interfacingresult', 'interfacingdate'];
    //displayedColumnsSampleHandling = ['name', 'no', 'estvol', 'vol', 'condition', 'note', 'process', 'processdate', 'processuser', 'samplehandlingid'];
    //displayedColumnsSpecimenHandling = ['nama', 'no', 'estvol', 'vol', 'conditionid', 'condition', 'note', 'received', 'receiveddate', 'process', 'processdate', 'receiveuser', 'processuser'];
    displayedColumnsSampleHandling = ['name', 'estvol', 'condition', 'note', 'received', 'receiveddate', 'receiveduser','process', 'processdate', 'processuser', 'samplehandlingid'];
    displayedColumnsSpecimenHandling = ['nama',  'estvol', 'conditionid', 'condition', 'note', 'send', 'senddate', 'process', 'processdate', 'senduser', 'processuser'];
    displayedColumnsSample = ['specimenName', 'no', 'estvol', 'condition', 'note', 'process', 'processdate','send', 'senddate'];
    //displayedColumnsSpecimenCollection = ['nama', 'estvol', 'no', 'bodysite','locsite', 'drawsite', 'drawuser','draw','specimenid', 'conditionid', 'note', 'received', 'receiveddate', 'receiveuser'];
    displayedColumnsSpecimenCollection = ['nama', 'estvol',  'receiveuser', 'receive', 'specimenid', 'conditionid', 'note', 'send', 'senddate', 'senduser'];



    initNote() {
        return this.formBuilder.group({
          LabNote: [''],
          user: 'admin'
          // date: [''],
        //   RegisterId: this.route.snapshot.params['pasid']
        })
    }

    

    addNoteValue() {
        const control = <FormArray>this.labNoteGroup.controls['labNotes'];
        control.push(this.initNote())
    }

    deleteNoteValue(index) {
        if(index > 0){
            const control = <FormArray>this.labNoteGroup.controls['labNotes'];
            control.removeAt(index);
        }
    }

    passinp(index, value) {
      this.tampungItemProcess[index].resultINP = value.controls.resultINP.value, value.controls.testId.value;
    }



     passprocesssample(index, value) {
         this.tampungSampleProcess[index].process = value.controls.process.value, value.controls.specimenId.value;
     }

     passreceived(index, value) {
         this.tampungSampleReceived[index].received = value.controls.received.value, value.controls.specimenId.value;
     }

    passauth(index, value) {
        this.tampungResultAuth[index].auth = value.controls.auth.value, value.controls.id.value;
    }

    constructor(private route: ActivatedRoute, private resultDetailService: ResultDetailService,
         private formBuilder: FormBuilder, private router: Router, private snackbar: MatSnackBar, private dialog: MatDialog) {

      this.bgColor = 'FFFFFF';

      this.samples = this.formBuilder.group({
            sampleArray: this.formBuilder.array([])
        })

        // console.log(this.drawReceive.value['drawUser'] = "admin")

        this.labNoteGroup = this.formBuilder.group({
            labNotes: this.formBuilder.array([this.initNote()])
        })


    }

    ngOnInit() {
        // private loadingSpecimensDetail = new BehaviorSubject<boolean>(false);
        // private specimensDetailSubject = new BehaviorSubject<any>([]);

        this.process_sample_detail = {
            insid: this.route.snapshot.params['insid'],
            methodId: this.route.snapshot.params['methodId'],
            insname: this.route.snapshot.params['insname'],
            age: this.route.snapshot.params['age'],
            pasid: this.route.snapshot.params['pasid']
        }
        this.insid = this.process_sample_detail['insid'];
        this.methodId = this.process_sample_detail['methodId'];
        this.age = this.process_sample_detail['age'];
        this.pasid = this.process_sample_detail['pasid'];
        this.insname = this.process_sample_detail['insname'];

        

        this.resultDetailService.createAnalysisResultDetail(
          this.insid, this.methodId, this.age, this.pasid).subscribe(create => {
            this.createData = create;

        })

        this.resultDetailService.getDoctor(this.pasid).then(doc => this.doctor = doc)


        


        this.dataSourceItem = new FileDetailSourceItem(this.resultDetailService)

        this.dataSourceItem.loadDetailItem(this.insid, this.methodId, this.age, this.pasid)

        this.dataSourceTest = new FileDetailSourceTest(this.resultDetailService)

        this.dataSourceTest.loadDetailTest(this.insid, this.methodId, this.age, this.pasid)

        console.log(this.dataSourceTest.loadDetailTest(this.insid, this.methodId, this.age, this.pasid))

        this.dataSourceRun = new FileDetailSourceRun(this.resultDetailService)

        this.dataSourceRun.loadDetailRun(this.insid, this.methodId, this.age, this.pasid)

        this.dataSourceImg = new FileDetailSourceImg(this.resultDetailService)

        this.dataSourceImg.loadDetailImg(this.pasid)

        this.dataSourceStatus = new FileDetailSourceStatus(this.resultDetailService)

        this.dataSourceStatus.loadDetailStatus(this.insid, this.methodId, this.age, this.pasid)

        this.dataSourceList = new FileDetailSourceList(this.resultDetailService)

        this.dataSourceList.loadDetailList(this.pasid)

        this.dataSourceStatusTest = new FileDetailSourceStatusTest(this.resultDetailService)

        this.dataSourceStatusTest.loadDetailStatusTest(this.insid, this.methodId, this.age, this.pasid)

        this.dataSourceSampleHandling = new FileDetailSourceSampleHandling(this.resultDetailService)

        this.dataSourceSampleHandling.loadDetailSampleHandling(this.pasid)

        this.dataSourceSpecimenHandling = new FileDetailSourceSpecimenHandling(this.resultDetailService)

        this.dataSourceSpecimenHandling.loadDetailSpecimenHandling(this.pasid)

        this.dataSourceSpecimenCollection = new FileDetailSourceSpecimenCollection(this.resultDetailService)

        this.dataSourceSpecimenCollection.loadDetailSpecimenCollection(this.pasid)

        this.dataSource = new FileDetailSource(this.resultDetailService);

        this.dataSource.loadDetail(this.pasid)

        this.dataSourceSch = new FileDetailSourceSch(this.resultDetailService);

        this.dataSourceSch.loadDetailSch(this.pasid)

        this.dataSourceLabNote = new FileDetailSourceLabNote(this.resultDetailService);

        this.dataSourceLabNote.loadDetailLabNote(this.pasid)

        this.dataSourceSample = new FileDetailSourceSample(this.resultDetailService);

        this.dataSourceSample.loadDetailSample(this.insid, this.methodId, this.age, this.pasid)


    }

    comment(event, index) {
        this.dataSourceItem.saveComment(event, index)
    }

    result(event, index) {
        // console.log(event)
        // let color = document.getElementById('color');
        // if (event > 10) {
        //     // color.style.cssText = "#ff0000";
        //     event[index]="#ff0000";
        // }
        // else {
        //     // color.style.cssText = "#008000";
        //     event[index]="#008000"
        // }
        this.dataSourceItem.saveResult(event, index)
    }

    clickAllSampleProcess(event) {
        this.dataSourceSample.AllProcess(event)
    }

    processSample(event, index) {
        this.dataSourceSample.saveProSamp(event, index)
    }

    clickAllReceivedSample(event) {
        this.dataSourceSample.AllReceived(event)
    }

    receivedSample(event, index) {
        this.dataSourceSample.saveReSamp(event, index)
    }


    clickAllPrn(event) {
        this.dataSourceItem.AllPrn(event)
    }

    prn(event, index) {
        this.dataSourceItem.savePrn(event, index)
    }

    clickAllAuthStat(event) {
        this.dataSourceStatusTest.AllAuth(event)
    }

    auth(event, index) {
        this.dataSourceStatusTest.saveAuth(event, index)
    }

    clickAllInp(event){
        this.dataSourceItem.AllInp(event)
    }

    inp(event, index) {
        this.dataSourceItem.saveInp(event, index)
    }

    cekColor(x, value){
        
      console.log("x: ", x)
      console.log("value x: ", value)
      console.log("largerValue x: ", this.largerValue[x])
      console.log("lessValue x: ", this.lessValue[x])
      var valueInt = +value;
      var color = document.getElementById('color');
      if(value[x] < 10){
    //    this.bgColor = '24CBE5'
        color.style.cssText = "#008000";
      } else if(value[x] > 10){
       color.style.cssText = '#FF0000'
      } else {
        color.style.cssText = '#FFFFFF'
      }


    }

    // sendData() {
    //     console.log(this.drawReceive.value["drawReceiveArray"])
    //     console.log(this.regid);
    //     // console.log(this.dataSource['value'])
    // }
    backToPasien(insid, testMethodId){
        this.router.navigate(['/apps/analysis/result_instrument/' + this.insid + '/' + this.methodId + '/' + this.insname])
    }

    submitRes(data) {
        this.createRes = {
            registerId: this.createData.registerId,
            patientNote: this.createData.patientNote,
            referalNote: this.createData.referalNote,
            doctornote: this.createData.doctornote,
            regNo: this.createData.regNo,
            regDate: this.createData.regDate,
            labNote: this.labNoteGroup.value['labNotes'],
            // process: this.tampungItemProcess,
            // specimenCollectionForm : this.drawReceive.value['drawReceiveArray'],
            // specimenHandlingForm : null,
            // sampleHandlingForm : null,
            // labNote : this.labNoteGroup.value['labNote']
            analysisResultTest : this.createData.analysisResultTest,
            analysisResultForm : this.dataSourceSample.loadSample(),
            analysisResultItem : this.dataSourceItem.loadItem()
        }
        // console.log(this.createRes)
        data = this.createRes
        // this.resultDetailService.postRes(data)
        // console.log(data);
        // alert('Result Berhasil!');
        // this.router.navigate(['apps/analysis/verification_pasien'])
        this.resultDetailService.postRes(data).subscribe(res => {
            if(res == 'Submited'){
                this.successMessage = true;
                setTimeout(function () {
                        this.successMessage = false;
                        this.router.navigate(['apps/analysis/result_instrument'])
                    }.bind(this), 2000)

                // alert('Result Berhasil!');
            }else{
                this.errorMessage = true;
                setTimeout(function (){
                    this.errorMessage = false;
                }.bind(this), 2000)
            }
        },error => {
            this.errorMessage = true;
            setTimeout(function (){
                this.errorMessage = false;
            }.bind(this), 2000)
        });
        
        //  .then(success => this.successMessage = <any>success)
        //  .catch(error => this.errorMessage = <any>error)
    }

   
}

export class FileDetailSourceSample extends DataSource<any>
{
    private detailSampleSubject = new BehaviorSubject<any>([]);

    private loadingDetailSample = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSample.asObservable();

    public clickpro: boolean;

    public clickre: boolean;
    
    constructor(private resultDetailService: ResultDetailService) {
        super()
    }


    AllProcess(process:any) {
        // if (this.clickpro) { this.clickpro = false; } else { this.clickpro = true; }
      for (var i = 0; i < this.detailSampleSubject.value.length; i++) {
        this.detailSampleSubject.value[i].process = process;
      }
      return this.detailSampleSubject.value;
    }

    saveProSamp(pro: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailSampleSubject.value[index].process = pro;
        return this.detailSampleSubject.value;
    }

    AllReceived(receive:any) {
        // if (this.clickre) { this.clickre = false; } else { this.clickre = true; }
      for (var i = 0; i < this.detailSampleSubject.value.length; i++) {
        this.detailSampleSubject.value[i].received = receive;
      }
      return this.detailSampleSubject.value;
    }

    saveReSamp(re: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailSampleSubject.value[index].received = re;
        return this.detailSampleSubject.value;
    }



    loadSample(){
        return this.detailSampleSubject.value;
    }

    loadDetailSample(insid: any, methodId: any, age: any, pasid: any){
        this.loadingDetailSample.next(true);

        this.resultDetailService.getSample(insid, methodId, age, pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSample.next(false))
        )
        .subscribe(sample => this.detailSampleSubject.next(sample))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSampleSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSampleSubject.complete();
        this.loadingDetailSample.complete();
    }
}

export class FileDetailSourceItem extends DataSource<any>
{
    private detailItemSubject = new BehaviorSubject<any>([]);

    private loadingDetailItem = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailItem.asObservable();

    public clickprn: boolean;

    public clickinp: boolean;
    
    constructor(private resultDetailService: ResultDetailService) {
        super()
    }

    saveComment(resComment: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailItemSubject.value[index].resultComment = resComment;
        return this.detailItemSubject.value;
    }

    saveResult(res: any, index) {
        console.log(this.detailItemSubject.value[index].result)
        this.detailItemSubject.value[index].result = res;
        return this.detailItemSubject.value;
    }

    AllPrn(prn:any) {
        // if (this.clickprn) { this.clickprn = false; } else { this.clickprn = true; }
      for (var i = 0; i < this.detailItemSubject.value.length; i++) {
        this.detailItemSubject.value[i].PRN = prn;
      }
      return this.detailItemSubject.value;
    }

    savePrn(prn: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailItemSubject.value[index].prn = true;
        return this.detailItemSubject.value;
    }

    AllInp(inp:any) {
        // if (this.clickinp) { this.clickinp = false; } else { this.clickinp = true; }
      for (var i = 0; i < this.detailItemSubject.value.length; i++) {
        this.detailItemSubject.value[i].resultINP = inp;
      }
      return this.detailItemSubject.value;
    }

    saveInp(inp: any, index) {
        // console.log(this.detailItemSubject.value[index])
      this.detailItemSubject.value[index].resultINP = inp;
        return this.detailItemSubject.value;
    }


    loadItem(){
        return this.detailItemSubject.value;
    }

    loadDetailItem(insid: any, methodId: any, age: any, pasid: any){
        this.loadingDetailItem.next(true);

        this.resultDetailService.getItem(insid, methodId, age, pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailItem.next(false))
        )
        .subscribe(item => this.detailItemSubject.next(item))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailItemSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailItemSubject.complete();
        this.loadingDetailItem.complete();
    }
}

export class FileDetailSourceLabNote extends DataSource<any>
{
    private detailLabNoteSubject = new BehaviorSubject<any>([]);

    private loadingDetailLabNote = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailLabNote.asObservable();

    constructor(private resultDetailService: ResultDetailService) {
        super()
    }

    loadDetailLabNote(pasid: any){
        this.loadingDetailLabNote.next(true);

        this.resultDetailService.getLabNote(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailLabNote.next(false))
        )
        .subscribe(test => this.detailLabNoteSubject.next(test))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailLabNoteSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailLabNoteSubject.complete();
        this.loadingDetailLabNote.complete();
    }
}

export class FileDetailSourceTest extends DataSource<any>
{
    private detailTestSubject = new BehaviorSubject<any>([]);

    private loadingDetailTest = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailTest.asObservable();

    constructor(private resultDetailService: ResultDetailService) {
        super()
    }

    loadDetailTest(insid: any, testMethodId: any, pasienAge:any, pasid: any){
        this.loadingDetailTest.next(true);

        this.resultDetailService.getTest(insid, testMethodId, pasienAge, pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailTest.next(false))
        )
        .subscribe(test => this.detailTestSubject.next(test))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailTestSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailTestSubject.complete();
        this.loadingDetailTest.complete();
    }
}

export class FileDetailSourceRun extends DataSource<any>
{
    private detailRunSubject = new BehaviorSubject<any>([]);

    private loadingDetailRun = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailRun.asObservable();

    constructor(private resultDetailService: ResultDetailService) {
        super()
    }

    loadDetailRun(insid: any, testMethodId: any, pasienAge:any, pasid: any){
        this.loadingDetailRun.next(true);

        this.resultDetailService.getTest(insid, testMethodId, pasienAge, pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailRun.next(false))
        )
        .subscribe(run => this.detailRunSubject.next(run))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailRunSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailRunSubject.complete();
        this.loadingDetailRun.complete();
    }
}

export class FileDetailSourceStatusTest extends DataSource<any>
{
    private detailStatusTestSubject = new BehaviorSubject<any>([]);

    private loadingDetailStatusTest = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailStatusTest.asObservable();

    public clickauth: boolean;

    constructor(private resultDetailService: ResultDetailService) {
        super()
    }

    AllAuth(auth:any) {
        // if (this.clickauth) { this.clickauth = false; } else { this.clickauth = true; }
      for (var i = 0; i < this.detailStatusTestSubject.value.length; i++) {
        this.detailStatusTestSubject.value[i].auth = auth;
      }
      return this.detailStatusTestSubject.value;
    }

    saveAuth(auth: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailStatusTestSubject.value[index].auth = auth;
        return this.detailStatusTestSubject.value;
    }

    loadDetailStatusTest(insid: any, testMethodId: any, pasienAge:any, pasid: any){
        this.loadingDetailStatusTest.next(true);

        this.resultDetailService.getstatustest(insid, testMethodId, pasienAge, pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailStatusTest.next(false))
        )
        .subscribe(statusTest => this.detailStatusTestSubject.next(statusTest))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailStatusTestSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailStatusTestSubject.complete();
        this.loadingDetailStatusTest.complete();
    }
}

export class FileDetailSourceStatus extends DataSource<any>
{
    private detailStatusSubject = new BehaviorSubject<any>([]);

    private loadingDetailStatus = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailStatus.asObservable();

    constructor(private resultDetailService: ResultDetailService) {
        super()
    }

    loadDetailStatus(insid: any, testMethodId: any, pasienAge:any, pasid: any){
        this.loadingDetailStatus.next(true);

        this.resultDetailService.getResultStatus(insid, testMethodId, pasienAge, pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailStatus.next(false))
        )
        .subscribe(status => this.detailStatusSubject.next(status))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailStatusSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailStatusSubject.complete();
        this.loadingDetailStatus.complete();
    }
}

export class FileDetailSourceList extends DataSource<any>
{
    private detailListSubject = new BehaviorSubject<any>([]);

    private loadingDetailList = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailList.asObservable();

    constructor(private resultDetailService: ResultDetailService) {
        super()
    }

    loadDetailList(pasid: any){
        this.loadingDetailList.next(true);

        this.resultDetailService.getSpecimensDetailCond(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailList.next(false))
        )
        .subscribe(status => this.detailListSubject.next(status))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailListSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailListSubject.complete();
        this.loadingDetailList.complete();
    }
}

export class FileDetailSourceImg extends DataSource<any>
{
    private detailImgSubject = new BehaviorSubject<any>([]);

    private loadingDetailImg = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailImg.asObservable();

    constructor(private resultDetailService: ResultDetailService) {
        super()
    }

    loadDetailImg(pasid: any){
        this.loadingDetailImg.next(true);

        this.resultDetailService.getSpecimensDetailCond(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailImg.next(false))
        )
        .subscribe(status => this.detailImgSubject.next(status))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailImgSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailImgSubject.complete();
        this.loadingDetailImg.complete();
    }
}

//export class FileDetailSourceSample extends DataSource<any>
//{
//    private detailSampleSubject = new BehaviorSubject<any>([]);

//    private loadingDetailSample = new BehaviorSubject<boolean>(false);

//    public loading$ = this.loadingDetailSample.asObservable();

//    constructor(private resultDetailService: ResultDetailService) {
//        super()
//    }

//    loadDetailSample(insid: any,pasid: any){
//        this.loadingDetailSample.next(true);

//        this.resultDetailService.getSample(insid,pasid).pipe(
//             catchError(() => of([])),
//             finalize(() => this.loadingDetailSample.next(false))
//        )
//        .subscribe(sample => this.detailSampleSubject.next(sample))
//    }

//    connect(collectionViewer: CollectionViewer): Observable<any>
//    {
//        console.log("Connecting data source");
//        return this.detailSampleSubject.asObservable();
//    }


//    disconnect(collectionViewer: CollectionViewer): void
//    {
//        this.detailSampleSubject.complete();
//        this.loadingDetailSample.complete();
//    }
//}

export class FileDetailSourceSampleHandling extends DataSource<any>
{
    private detailSampleHandlingSubject = new BehaviorSubject<any>([]);

    private loadingDetailSampleHandling = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSampleHandling.asObservable();

    constructor(private resultDetailService: ResultDetailService) {
        super()
    }

    loadDetailSampleHandling(pasid: any){
        this.loadingDetailSampleHandling.next(true);

        this.resultDetailService.getSampleHandling(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSampleHandling.next(false))
        )
        .subscribe(sample => this.detailSampleHandlingSubject.next(sample))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSampleHandlingSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSampleHandlingSubject.complete();
        this.loadingDetailSampleHandling.complete();
    }
}

export class FileDetailSourceSpecimenHandling extends DataSource<any>
{
    private detailSpecimenHandlingSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimenHandling = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimenHandling.asObservable();

    constructor(private resultDetailService: ResultDetailService) {
        super()
    }

    loadDetailSpecimenHandling(pasid: any){
        this.loadingDetailSpecimenHandling.next(true);

        this.resultDetailService.getSpecimenHandling(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimenHandling.next(false))
        )
        .subscribe(specimen => this.detailSpecimenHandlingSubject.next(specimen))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimenHandlingSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimenHandlingSubject.complete();
        this.loadingDetailSpecimenHandling.complete();
    }
}

export class FileDetailSourceSpecimenCollection extends DataSource<any>
{
    private detailSpecimenCollectionSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimenCollection = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimenCollection.asObservable();

    constructor(private resultDetailService: ResultDetailService) {
        super()
    }

    loadDetailSpecimenCollection(pasid: any){
        this.loadingDetailSpecimenCollection.next(true);

        this.resultDetailService.getSpecimenCollection(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimenCollection.next(false))
        )
        .subscribe(specimen => this.detailSpecimenCollectionSubject.next(specimen))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimenCollectionSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimenCollectionSubject.complete();
        this.loadingDetailSpecimenCollection.complete();
    }
}

export class FileDetailSource extends DataSource<any>
{
    private detailSpecimensSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimens = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimens.asObservable();

    constructor(private resultDetailService: ResultDetailService) {
        super()
    }

    loadDetail(regid: any){
        this.loadingDetailSpecimens.next(true);

        this.resultDetailService.getSpecimensDetailCond(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimens.next(false))
        )
        .subscribe(detspec => this.detailSpecimensSubject.next(detspec))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimensSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimensSubject.complete();
        this.loadingDetailSpecimens.complete();
    }
}



export class FileDetailSourceSch extends DataSource<any>
{
    private detailSpecimensSubjectSch = new BehaviorSubject<any>([]);

    private loadingDetailSpecimensSch = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimensSch.asObservable();

    constructor(private resultDetailService: ResultDetailService) {
        super()
    }

    loadDetailSch(regid: any){
        this.loadingDetailSpecimensSch.next(true);

        this.resultDetailService.getSpecimensDetailSch(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimensSch.next(false))
        )
        .subscribe(detspec => this.detailSpecimensSubjectSch.next(detspec))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimensSubjectSch.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimensSubjectSch.complete();
        this.loadingDetailSpecimensSch.complete();
    }
}

// export class FileDetailSourceSample extends DataSource<any>
// {
//     private detailAnalysisSubjectSample = new BehaviorSubject<any>([]);

//     private loadingDetailAnalysisSample = new BehaviorSubject<boolean>(false);

//     public loading$ = this.loadingDetailAnalysisSample.asObservable();

//     constructor(private processSampleDetailService: ProcessSampleDetailService) {
//         super()
//     }

//     loadDetailSample(regid: any){
//         this.loadingDetailAnalysisSample.next(true);

//         this.processSampleDetailService.getAnalysisDetailSample(regid).pipe(
//              catchError(() => of([])),
//              finalize(() => this.loadingDetailAnalysisSample.next(false))
//         )
//         .subscribe(detspec => this.detailAnalysisSubjectSample.next(detspec))
//     }

//     connect(collectionViewer: CollectionViewer): Observable<any>
//     {
//         console.log("Connecting data source");
//         return this.detailAnalysisSubjectSample.asObservable();
//     }


//     disconnect(collectionViewer: CollectionViewer): void
//     {
//         this.detailAnalysisSubjectSample.complete();
//         this.loadingDetailAnalysisSample.complete();
//     }
// }
