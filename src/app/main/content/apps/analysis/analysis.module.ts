import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { ProcessSampleInstrumentComponent } from './process_sample_instrument/process_sample_instrument.component';
import { ProcessSampleInstrumentService } from './process_sample_instrument/process_sample_instrument.service';
import { ProcessSamplePasienComponent } from './process_sample_pasien/process_sample_pasien.component';
import { ProcessSamplePasienService } from './process_sample_pasien/process_sample_pasien.service';
import { ProcessSampleDetailComponent } from './process_sample_detail/process_sample_detail.component';
import { ProcessSampleDetailService } from './process_sample_detail/process_sample_detail.service';
import { ResultInstrumentComponent } from './result_instrument/result_instrument.component';
import { ResultPasienComponent } from './result_pasien/result_pasien.component';
import { ResultInstrumentService } from './result_instrument/result_instrument.service';
import { ResultPasienService } from './result_pasien/result_pasien.service';
import { ResultDetailComponent } from './result_detail/result_detail.component';
import { ResultDetailService } from './result_detail/result_detail.service';
import { VerificationPasienComponent } from './verification_pasien/verification_pasien.component';
import { VerificationPasienService } from './verification_pasien/verification_pasien.service';
import { VerificationDetailComponent } from './verification_detail/verification_detail.component';
import { VerificationDetailService } from './verification_detail/verification_detail.service';
import { AuthorizationPasienComponent } from './authorization_pasien/authorization_pasien.component';
import { AuthorizationPasienService } from './authorization_pasien/authorization_pasien.service';
import { AuthorizationDetailComponent } from './authorization_detail/authorization_detail.component';
import { AuthorizationDetailService } from './authorization_detail/authorization_detail.service';
import { AgmCoreModule } from '@agm/core';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

const routes: Routes = [
    {
        path     : 'process_sample_instrument',
        component: ProcessSampleInstrumentComponent
    },
    {
        path     : 'process_sample_instrument/:insid/:name',
        component: ProcessSamplePasienComponent
    },
    {
        path     : 'process_sample_pasien/:insid/:name/:pasid/:age',
        component: ProcessSampleDetailComponent
    },
    {
        path     : 'result_instrument',
        component: ResultInstrumentComponent
    },
    {
        path     : 'result_instrument/:insid/:methodId/:insname',
        component: ResultPasienComponent
    },
    {
        path     : 'result_pasien/:insid/:methodId/:insname/:age/:pasid',
        component: ResultDetailComponent
    },
    {
        path: 'verification_pasien',
        component: VerificationPasienComponent
    },
    {
        path: 'verification_pasien/:regid/:age',
        component: VerificationDetailComponent
    },
    {
        path     : 'authorization_pasien',
        component: AuthorizationPasienComponent
    },
    {
      path: 'authorization_pasien/:regid/:age',
        component: AuthorizationDetailComponent
    }



];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
        FuseWidgetModule,
        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    declarations: [
        ProcessSampleInstrumentComponent,
        ProcessSamplePasienComponent,
        ProcessSampleDetailComponent,
        ResultInstrumentComponent,
        ResultPasienComponent,
        ResultDetailComponent,
        VerificationPasienComponent,
        VerificationDetailComponent,
        AuthorizationDetailComponent,
        AuthorizationPasienComponent
    ],
    providers   : [
        ProcessSampleInstrumentService,
        ProcessSamplePasienService,
        ProcessSampleDetailService,
        ResultInstrumentService,
        ResultPasienService,
        ResultDetailService,
        VerificationPasienService,
        VerificationDetailService,
        AuthorizationPasienService,
        AuthorizationDetailService
    ]
})
export class AnalysisModule
{
}
