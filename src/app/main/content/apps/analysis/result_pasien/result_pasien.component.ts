import { Component, ElementRef, OnInit, ViewChild, AfterViewInit  } from '@angular/core';
import { ResultPasienService } from './result_pasien.service';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import {ActivatedRoute, Router} from "@angular/router";
import { fuseAnimations } from '../../../../../core/animations';
import { MatPaginator, MatSort, PageEvent, MatTableDataSource } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import {debounceTime, distinctUntilChanged, startWith, tap, delay} from 'rxjs/operators';
import {merge} from "rxjs/observable/merge";
import {fromEvent} from 'rxjs/observable/fromEvent';
import { FuseUtils } from '../../../../../core/fuseUtils';
import {of} from "rxjs/observable/of";
import {catchError, finalize} from "rxjs/operators";

@Component({
    selector   : 'result_pasien',
    templateUrl: './result_pasien.component.html',
    styleUrls  : ['./result_pasien.component.scss'],
    animations : fuseAnimations
})
export class ResultPasienComponent implements OnInit, AfterViewInit
{
    dataSourcePasien: FilesDataSourcePasien;
    displayedColumnsPasien = [ 'no', 'regno', 'patientno', 'patientname', 'patientgender', 'patientage', 'patientdob', 'patientresult', 'regdate', 'company', 'cito', 'status'];
    selectedRowIndex: any;
    pasien: {insid: any, methodId:any, insname: any}
    insid: any;
    methodId: any;
    insname: any;
    pasid: any;
    pasAge: any;
    pasName: any;
    length: number;
    pageEvent: PageEvent;
    pageIndex: any = this.pageEvent;
    pageSize: any = this.pageEvent;
    pageSizeOptions = [10, 25, 100];


    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private resultPasienService: ResultPasienService,
        private route : ActivatedRoute,
        private router : Router
    )
    {
    }



    ngOnInit()
    {
        this.pasien = {
            insid: this.route.snapshot.params["insid"],
            methodId: this.route.snapshot.params["methodId"],
            insname: this.route.snapshot.params["insname"]
        }
        this.insid = this.pasien['insid'];
        this.methodId = this.pasien['methodId'];
        this.insname = this.pasien["insname"];
        console.log(this.insid)
        console.log(this.methodId)

        this.dataSourcePasien = new FilesDataSourcePasien(this.resultPasienService);

        this.dataSourcePasien.loadPasien(this.length, this.insid, 0, 10, '');
    }

    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        fromEvent(this.filter.nativeElement,'keyup')
            .pipe(
                debounceTime(150),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;

                    this.loadPasienPage();
                })
            )
            .subscribe();

        // merge(this.sort.sortChange, this.paginator.page)
        this.paginator.page
        .pipe(
            tap(() => this.loadPasienPage())
        )
        .subscribe();

    }

    loadPasienPage() {
        this.dataSourcePasien.loadPasien(
            this.length,
            this.insid,
            this.paginator.pageIndex,
            this.paginator.pageSize,
            this.filter.nativeElement.value);
    }

   onRowClicked(dataSourcePasien, insid) {
        console.log('Row clicked: ', dataSourcePasien);
        console.log('Row clicked id', dataSourcePasien.regid);
        this.pasid = dataSourcePasien.regid;
        this.pasAge = dataSourcePasien.patientAge;
        this.router.navigate(['/apps/analysis/result_pasien/' + this.insid + '/' + this.methodId + '/' + this.insname + '/' + this.pasAge + '/' + this.pasid ])
    }
}

export class FilesDataSourcePasien extends DataSource<any>
{
    private pasienSubject = new BehaviorSubject<any>([]);
    public totalData : any;

    private loadingPasien = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingPasien.asObservable();

    constructor(
        private resultPasienService: ResultPasienService,
    )
    {
        super()

    }

    loadPasien(
        length: number,
        insid: any,
        pageIndex:number,
        pageSize:number,
        filter:string) {

        this.loadingPasien.next(true);

        this.resultPasienService.getAnalysisResultPasien(insid, pageIndex, pageSize, filter).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingPasien.next(false))
            )
            .subscribe(pasien => {
               this.pasienSubject.next(pasien.data)
               this.totalData = pasien.totalData
            });



        }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.pasienSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.pasienSubject.complete();
        this.loadingPasien.complete();
    }
}
