import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from "@angular/forms"
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { SpecimenCollectionNonLabComponent } from './specimen_collection_nl/specimen_collection_nl.component';
import { SpecimenCollectionNonLabService } from './specimen_collection_nl/specimen_collection_nl.service';
import { SpecimenDetailNonLabComponent } from './specimen_detail_nl/specimen_detail_nl.component';
import { SpecimenDetailNonLabService } from './specimen_detail_nl/specimen_detail_nl.service';
import { SpecimenHandlingNonLabComponent } from './specimen_handling_nl/specimen_handling_nl.component';
import { SpecimenHandlingNonLabService } from './specimen_handling_nl/specimen_handling_nl.service';
import { SpecimenHandlingDetailNonLabComponent } from './specimen_handling_detail_nl/specimen_handling_detail_nl.component';
import { SpecimenHandlingDetailNonLabService } from './specimen_handling_detail_nl/specimen_handling_detail_nl.service';
import { SampleHandlingNonLabComponent } from './sample_handling_nl/sample_handling_nl.component';
import { SampleHandlingNonLabService } from './sample_handling_nl/sample_handling_nl.service';
import { SampleHandlingDetailNonLabComponent } from './sample_handling_detail_nl/sample_handling_detail_nl.component';
import { SampleHandlingDetailNonLabService } from './sample_handling_detail_nl/sample_handling_detail_nl.service';
import { AgmCoreModule } from '@agm/core';
import { AccordionModule } from 'primeng/components/accordion/accordion';

const routes: Routes = [

    {
        path     : 'specimen_collection_nl',
        component: SpecimenCollectionNonLabComponent,
    },
    {
        path     : 'specimen_collection_nl/:regid/:regno/:regdate/:name',
        component: SpecimenDetailNonLabComponent,
    },
    {
        path     : 'specimen_handling_nl',
        component: SpecimenHandlingNonLabComponent,

    },
    {
        path: 'specimen_handling_nl/:regid/:regno/:regdate/:name',
        component: SpecimenHandlingDetailNonLabComponent,

    },
    {
        path     : 'sample_handling_nl',
        component: SampleHandlingNonLabComponent,

    },
    {
        path: 'sample_handling_nl/:regid/:regno/:regdate/:name',
        component: SampleHandlingDetailNonLabComponent,

    },


];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        FormsModule,
        AccordionModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    declarations: [
        SpecimenCollectionNonLabComponent,
        SpecimenDetailNonLabComponent,
        SpecimenHandlingNonLabComponent,
        SpecimenHandlingDetailNonLabComponent,
        SampleHandlingNonLabComponent,
        SampleHandlingDetailNonLabComponent,
    ],
    providers   : [
        SpecimenCollectionNonLabService,
        SpecimenDetailNonLabService,
        SpecimenHandlingNonLabService,
        SpecimenHandlingDetailNonLabService,
        SampleHandlingDetailNonLabService,
        SampleHandlingNonLabService,
    ]
})
export class SpecimenNonLabModule
{
}
