import { Component, OnDestroy, ElementRef, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { SpecimenDetailService } from './specimen_detail.service';
import { fuseAnimations } from '../../../../../core/animations';
import {CollectionViewer, DataSource } from '@angular/cdk/collections';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar } from '@angular/material';
import { Location } from '@angular/common';
import { MatPaginator, MatSort,  MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ActivatedRoute, Router } from '@angular/router';
import {of} from "rxjs/observable/of";
import {catchError, finalize} from "rxjs/operators";
import { Options } from 'selenium-webdriver';

export interface Detail {
    nama?;
    estVol?;
    no?;
    bodySite?;
    loCSite?;
    drawSite?;
    drawUser?;
    draw?: boolean;
    drawDate?: "yyyy-MM-dd";
    condition?;
    specimenId?;
    note?;
    received?: boolean;
    receiveDate?: "yyyy-MM-dd";
    receiveUser?;
}

export interface Detail2 {
    registerTestId?;
    testName?;
    cito?: boolean;
    scheduleName?;
}

@Component({
    selector     : 'specimen_detail',
    templateUrl  : './specimen_detail.component.html',
    styleUrls    : ['./specimen_detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})

export class SpecimenDetailComponent implements OnInit
{
    specimen_detail: {regid: number, regno: any, regdate: any, name: any }
    regid: any;
    regno: any;
    regdate: any;
    name: any;
    user: any;
    errorMessage: boolean;
    successMessage: boolean;
    tampungTestCito: any = [];
    createData : any = [];
    data: any;
    createSpec: any = [];
    detail: Detail[];
    detail2: Detail2[];
    drawReceive: FormGroup;
    patientNoteGroup: FormGroup;
    physicianNoteGroup: FormGroup;
    referralNoteGroup: FormGroup;
    labNoteGroup: FormGroup;
    manyTest: FormGroup;
    physicianGroup : FormGroup;
    dataSource: FileDetailSource;
    dataSourceSch : FileDetailSourceSch;
    dataSourceLabNote: FileDetailSourceLabNote;
    dataSourceItem : FileDetailSourceItem;
    dataSourceSpc : FileDetailSourceSpc;
    drawer: string;
    clickDraw = [];
    clickRe = [];
    clickCito: boolean = false;
    clickPrint: boolean;
    specCond: any;
    condexs = [];

    displayedColumns= ['question', 'answer', 'note'];
    displayedColumns2= ['schedule', 'test', 'day', 'time'];
    displayedColumnsNoteLab = ['no', 'note', 'user', 'date'];
    displayedColumnsItem= ['no', 'testName', 'cito', 'scheduleName'];
    displayedColumnsSpc = ['action', 'nama', 'estVol', 'receive', 'send', 'condition',
      'note', 'receiveDate', 'receiveUser', 'sendDate', 'sendUser'];

    initDraRe(dra) {
        const group = this.formBuilder.group({
            nama: dra.nama,
            estVol: dra.estVol,
            no: dra.no,
            bodySite: dra.bodySite,
            loCSite: dra.loCSite,
            drawSite: dra.drawSite,
            drawUser: (dra.draw == true ? "admin" : ""),
            draw: dra.draw,
            drawDate: dra.drawDate,
            specimenId: dra.specimenId,
            conditionId: dra.condition,
            note: dra.note,
            received: dra.received,
            receiveDate: dra.receiveDate,
            receiveUser: (dra.received == true ? "admin" : ""),
            registerSpecimenId: dra.registerSpecimenId
        })
        return group;
    }


    getClickDraw(dr, index) {
      console.log("idx: ", index)
      console.log("clickdraw: ", this.clickDraw[index])
        if (this.clickDraw[index] != true) {
            dr.controls.drawUser.value = "admin";
            this.clickDraw[index] = true;
            // dr.controls.condition.value = "berhasil";
        } else {
            dr.controls.drawUser.value = "";
            this.clickDraw[index] = false;
            // dr.controls.condition.value = "";
        }
    }

    getClickRec(dr, index) {
        if (this.clickRe[index] != true) {
            dr.controls.receiveUser.value = "admin";
            this.clickRe[index] = true
        } else {
            dr.controls.receiveUser.value = "";
            this.clickRe[index] = false
        }
    }

    initTest(tes) {
        const group = this.formBuilder.group({
            registerTestId: tes.registerTestId,
            testName: tes.testName,
            cito: tes.cito,
            scheduleName: tes.scheduleName
        })
        //if (tes.cito = 0) {
        //    tes.cito = true
        //} else {
        //    tes.cito = false
        //}
        return group;

    }

    initNote() {
      return this.formBuilder.group({
          LabNote: [''],
          user: 'admin',
          // date: [''],
          RegisterId: this.route.snapshot.params['regid']
      })
    }

    addNoteValue() {
        const control = <FormArray>this.labNoteGroup.controls['labNotes'];
        control.push(this.initNote())
    }

    deleteNoteValue(index) {
        if(index > 0){
            const control = <FormArray>this.labNoteGroup.controls['labNotes'];
            control.removeAt(index);
        }
    }

    passcito(index, value) {
      console.log("aloha", value);
        this.tampungTestCito[index].cito = value.controls.cito.value, value.controls.registerTestId.value;
    }

    constructor(private route: ActivatedRoute,
      private router: Router,
      private specimenDetailService: SpecimenDetailService,
      private formBuilder: FormBuilder) {
        this.drawReceive = this.formBuilder.group({
            drawReceiveArray: this.formBuilder.array([])
        })

        // console.log(this.drawReceive.value['drawUser'] = "admin")

        this.patientNoteGroup = this.formBuilder.group({
            patientNote: [''],
            patientName: [''],
            regDate: [''],
            patientId: ['']
        })

        this.physicianNoteGroup = this.formBuilder.group({
            physicianNote: [''],
            physicianName: [''],
            regDate: ['']
        })

        this.referralNoteGroup = this.formBuilder.group({
            referralNote: [''],
            companyName: [''],
            regDate: ['']
        })

        this.labNoteGroup = this.formBuilder.group({
            // labNote: ['']
            labNotes: this.formBuilder.array([this.initNote()])
        })

        this.manyTest = this.formBuilder.group({
            testArray: this.formBuilder.array([])
        })

        this.physicianGroup = this.formBuilder.group({
          frontTitle    : [''],
          doctorName : [''],
          endTitle : [''],
          addressName : [''],
          addressLocation : [''],
          cityName : [''],
          formTypeName : [''],
          specialtyName : ['']
        })
    }

    ngOnInit() {
        // private loadingSpecimensDetail = new BehaviorSubject<boolean>(false);
        // private specimensDetailSubject = new BehaviorSubject<any>([]);

        this.specimen_detail = {
            regid: this.route.snapshot.params['regid'],
            regno: this.route.snapshot.params['regno'],
            regdate: this.route.snapshot.params['regdate'],
            name: this.route.snapshot.params['name']
        }
        this.regid = this.specimen_detail['regid'];
        this.regno = this.specimen_detail['regno'];
        this.regdate = this.specimen_detail['regdate'];
        this.name = this.specimen_detail['name'];

        this.specimenDetailService.createSpecimenCol(this.regid).then(create => {
            this.createData = create;
            console.log("create: ", create);
            this.physicianNoteGroup.patchValue({
                physicianNote: create[0].doctornote,
                physicianName: create[0].doctorName,
                regDate: create[0].regDate
             });
            this.patientNoteGroup.patchValue({
                patientNote: create[0].patientNote,
                patientName: create[0].patientName,
                regDate: create[0].regDate,
                patientId: create[0].patientID
            });
            this.referralNoteGroup.patchValue({
                referralNote: create[0].referalNote,
                companyName: create[0].referalCompanyName,
                regDate: create[0].regDate
            });
        })

        this.specimenDetailService.getSpecimensCondition().subscribe(
          specCond => this.specCond = specCond);

          // this.clickDraw = [];
        const control = <FormArray>this.drawReceive.controls['drawReceiveArray'];
        this.specimenDetailService.getSpecimensDetail(this.regid).then(dra => {
            this.detail = dra
            dra.forEach(x => {
                this.clickDraw.push(false)
                console.log("clickdraw push", this.clickDraw)
                this.clickRe.push(false)
                control.push(this.initDraRe(x))
                console.log("init Dra re", x)
                this.condexs.push(false);
            });
            // console.log("note: ", this.patientNoteGroup.value);
        })

        this.labNoteGroup.patchValue({
          RegisterId:this.regid
        })
        const control1 = <FormArray>this.manyTest.controls['testArray'];
        this.specimenDetailService.getSpecimensDetailTest(this.regid).then(tes => {
            this.detail2 = tes
            tes.forEach(y => {
                control1.push(this.initTest(y))
                console.log("init test", y.cito)
                this.tampungTestCito.push({
                    "testId" : y.registerTestId,
                    "testName": y.testName,
                    "cito": y.cito,
                    "scheduleName": y.scheduleName
                });
                console.log("tampungTestCito", this.tampungTestCito)
            });
        })

        this.specimenDetailService.getSpecimensDetailPhy(this.regid)
        .then(phys => {
          this.physicianGroup.patchValue({
            frontTitle: phys.frontTitle,
            doctorName : phys.doctorName,
            endTitle: phys.endTitle,
            addressName : phys.addressName,
            addressLocation : phys.addressLocation,
            cityName : phys.cityName,
            formTypeName : phys.formTypeName,
            specialtyName : phys.specialtyName
          })
        });

        this.dataSource = new FileDetailSource(this.specimenDetailService);
        this.dataSource.loadDetail(this.regid)

        this.dataSourceSch = new FileDetailSourceSch(this.specimenDetailService);
        this.dataSourceSch.loadDetailSch(this.regid)

        this.dataSourceLabNote = new FileDetailSourceLabNote(this.specimenDetailService);
        this.dataSourceLabNote.loadDetailLabNote(this.regid)

        this.dataSourceItem = new FileDetailSourceItem(this.specimenDetailService);
        this.dataSourceItem.loadDetailItem(this.regid)

        this.dataSourceSpc = new FileDetailSourceSpc(this.specimenDetailService);
        this.dataSourceSpc.loadSpc(this.regid)
    }

    changeDisabled(){
        this.clickPrint != this.clickPrint
    }

    sendData(dr) {
      // console.log(dr)
      alert("specimen: " + dr.value.nama)
        // console.log(this.drawReceive.value["drawReceiveArray"])
        // console.log(this.regid);
        // console.log(this.dataSource['value'])
    }

    cito(event, index) {
      this.dataSourceItem.saveCito(event, index)
    }

    // clickcitos() {
    //   this.dataSourceItem.CitoAll();
    // }

    allcitos(event) {
        this.dataSourceItem.CitoAlls(event);
      }

    // clickdraws() {
    //   this.dataSourceSpc.DrawAll();
    // }

    alldraws(event) {
        this.dataSourceSpc.DrawAlls(event);
      }

    // clickreceives() {
    //   this.dataSourceSpc.ReceiveAll();
    // }

    allreceives(event) {
        this.dataSourceSpc.ReceiveAlls(event);
      }

    draw(event, index){
      // console.log("draw: ", event)
      this.dataSourceSpc.saveDraw(event, index)
    }
    receive(event, index){
      this.dataSourceSpc.saveReceive(event, index)
    }
    note(event, index){
      this.dataSourceSpc.saveNote(event, index)
    }
    cond(event, index){
      // console.log("cond: ", event)
      event ? this.condexs[index] = true : this.condexs[index] = false;
      this.dataSourceSpc.saveCondID(event, index)
    }
    cekcond(index){
        let color = "#fff";
        if(this.condexs[index])
        color = "#ff4242";
        return color;
    }

    submitSpec(data) {
      // console.log("detSpc: ", this.dataSourceSpc.dataSpc())
        this.createSpec = {
            registerId : this.regid,
            patientNote : this.patientNoteGroup.value['patientNote'],
            referalNote : this.referralNoteGroup.value['referralNote'],
            doctornote : this.physicianNoteGroup.value['physicianNote'],
            registerNo : this.regno,
            regDate : this.regdate,
            labNote : this.labNoteGroup.value['labNotes'],
            // cito: this.tampungTestCito,
            cito: this.dataSourceItem.dataItem(),
            // specimenCollectionForm : this.drawReceive.value['drawReceiveArray'],
            specimenCollectionForm : this.dataSourceSpc.dataSpc(),
            specimenHandlingForm : null,
            sampleHandlingForm : null
        }
        // console.log(this.createSpec)
        data = this.createSpec
        this.specimenDetailService.postSpec(data).subscribe(res => {
            if(res == 'Submited'){
                this.successMessage = true;
                setTimeout(function () {
                        this.successMessage = false;
                        this.router.navigate(['/apps/specimen/specimen_collection']) ;
                    }.bind(this), 2000)

                // alert('Result Berhasil!');
            }else{
                this.errorMessage = true;
                setTimeout(function (){
                    this.errorMessage = false;
                }.bind(this), 2000)
            }
        },error => {
            this.errorMessage = true;
            setTimeout(function (){
                this.errorMessage = false;
            }.bind(this), 2000)
        });
        
    }



}

export class FileDetailSource extends DataSource<any>
{
    private detailSpecimensSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimens = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimens.asObservable();

    constructor(private specimenDetailService: SpecimenDetailService) {
        super()
    }

    loadDetail(regid: any){
        this.loadingDetailSpecimens.next(true);

        this.specimenDetailService.getSpecimensDetailCond(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimens.next(false))
        )
        .subscribe(detspec => this.detailSpecimensSubject.next(detspec))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimensSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimensSubject.complete();
        this.loadingDetailSpecimens.complete();
    }
}

export class FileDetailSourceSch extends DataSource<any>
{
    private detailSpecimensSubjectSch = new BehaviorSubject<any>([]);

    private loadingDetailSpecimensSch = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimensSch.asObservable();

    constructor(private specimenDetailService: SpecimenDetailService) {
        super()
    }

    loadDetailSch(regid: any){
        this.loadingDetailSpecimensSch.next(true);

        this.specimenDetailService.getSpecimensDetailSch(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimensSch.next(false))
        )
        .subscribe(detspec => this.detailSpecimensSubjectSch.next(detspec))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimensSubjectSch.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimensSubjectSch.complete();
        this.loadingDetailSpecimensSch.complete();
    }
}

export class FileDetailSourceLabNote extends DataSource<any>
{
    private detailLabNoteSubject = new BehaviorSubject<any>([]);

    private loadingDetailLabNote = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailLabNote.asObservable();

    constructor(private specimenDetailService: SpecimenDetailService) {
        super()
    }

    loadDetailLabNote(regid: any){
        this.loadingDetailLabNote.next(true);

        this.specimenDetailService.getLabNote(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailLabNote.next(false))
        )
        .subscribe(test => this.detailLabNoteSubject.next(test))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailLabNoteSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailLabNoteSubject.complete();
        this.loadingDetailLabNote.complete();
    }
}

export class FileDetailSourceItem extends DataSource<any>
{
    private detailItem = new BehaviorSubject<any>([]);

    private loadingItem = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingItem.asObservable();
    public clickCit: boolean;

    constructor(private specimenDetailService: SpecimenDetailService) {
      super()
      this.clickCit = false;
    }

    saveCito(cito: any, index) {
      // console.log(this.detailItem.value[index])
        this.detailItem.value[index].cito = cito;
        return this.detailItem.value;
    }

    // CitoAll() {
    //   // console.log(this.detailItem.value[index])
    //   if (this.clickCit) { this.clickCit = false; } else { this.clickCit = true; }
    //   for (var i = 0; i < this.detailItem.value.length; i++) {
    //     this.detailItem.value[i].cito = this.clickCit;
    //   }
    //   return this.detailItem.value;
    // }

    CitoAlls(cito: any) {
        // console.log(this.detailItem.value[index])
        for (var i = 0; i < this.detailItem.value.length; i++) {
          this.detailItem.value[i].cito = cito;
        }
        return this.detailItem.value;
      }

    dataItem(){
      return this.detailItem.value;
    }

    loadDetailItem(regid: any){
        this.loadingItem.next(true);

        this.specimenDetailService.getSpecimensItem(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingItem.next(false))
        )
        .subscribe(detItem => {
          // console.log("detItem: ", detItem)
           this.detailItem.next(detItem)
        })
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailItem.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailItem.complete();
        this.loadingItem.complete();
    }
}

export class FileDetailSourceSpc extends DataSource<any>
{
    private detailSpc = new BehaviorSubject<any>([]);

    private loadingSpc = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSpc.asObservable();

    public clickdra: boolean;

    public clickrec: boolean;

    constructor(private specimenDetailService: SpecimenDetailService) {
      super()
      this.clickdra = false;
      this.clickrec = false;
    }

    saveDraw(draw:any, index) {
      // console.log(this.detailItem.value[index])
        this.detailSpc.value[index].draw = draw;
        if (this.detailSpc.value[index].draw == true) {
          this.detailSpc.value[index].drawUser = 'admin';
        } else {
          this.detailSpc.value[index].drawUser = '';
        }
        return this.detailSpc.value;
    }

    saveReceive(rec: any, index) {
      // console.log(this.detailItem.value[index])
        this.detailSpc.value[index].received = rec;
        if (this.detailSpc.value[index].received == true) {
          this.detailSpc.value[index].receiveUser = 'admin';
        } else {
          this.detailSpc.value[index].receiveUser = '';
        }
        return this.detailSpc.value;
    }

    // DrawAll() {
    //   // console.log(this.detailItem.value[index])
    //   if (this.clickdra) { this.clickdra = false; } else { this.clickdra = true; }
    //   for (var i = 0; i < this.detailSpc.value.length; i++) {
    //     this.detailSpc.value[i].draw = this.clickdra;
    //   }
    //   return this.detailSpc.value;
    // }

    DrawAlls(draw:any) {
        for (var i = 0; i < this.detailSpc.value.length; i++) {
          this.detailSpc.value[i].draw = draw;
          if (this.detailSpc.value[i].draw == true) {
            this.detailSpc.value[i].drawUser = 'admin';
          } else {
            this.detailSpc.value[i].drawUser = '';
          }
        }
        return this.detailSpc.value;
      }

    // ReceiveAll() {
    //   // console.log(this.detailItem.value[index])
    //   if (this.clickrec) { this.clickrec = false; } else { this.clickrec = true; }
    //   for (var i = 0; i < this.detailSpc.value.length; i++) {
    //     this.detailSpc.value[i].received = this.clickrec;
    //   }
    //   return this.detailSpc.value;
    // }

    ReceiveAlls(receive:any) {
        for (var i = 0; i < this.detailSpc.value.length; i++) {
          this.detailSpc.value[i].received = receive;
          if (this.detailSpc.value[i].received == true) {
            this.detailSpc.value[i].receiveUser = 'admin';
          } else {
            this.detailSpc.value[i].receiveUser = '';
          }
        }
        return this.detailSpc.value;
      }

    saveCondID(condId: any, index) {
      // console.log(this.detailItem.value[index])
        this.detailSpc.value[index].conditionId = condId;
        return this.detailSpc.value;
    }

    saveNote(note: any, index) {
      // console.log(this.detailItem.value[index])
        this.detailSpc.value[index].note = note;
        return this.detailSpc.value;
    }

    dataSpc(){
      return this.detailSpc.value;
    }

    loadSpc(regid: any){
        this.loadingSpc.next(true);

        this.specimenDetailService.getSpecimens(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingSpc.next(false))
        )
        .subscribe(detSpc => {
          // console.log("detSpc: ", detSpc)
           this.detailSpc.next(detSpc)
        })
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpc.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpc.complete();
        this.loadingSpc.complete();
    }
}
