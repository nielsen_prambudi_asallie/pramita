export const locale = {
  lang: 'id',
  data: {
      'NAV': {
          'APPLICATIONS': 'Aplikasi',
          'WELCOME': 'Selamat Datang',
          'DASHBOARDS'  : 'Dasbor',
          'MASTER' : 'Master',
          'AGREEMENT' : 'Persetujuan',
          'COMPANY' : 'Perusahaan',
          'PHYSICIAN' : 'Dokter',
          'PRICE_LIST' : 'Daftar Harga',
          'REWARD' : 'Bonus',
          'SCHEDULE' : 'Jadwal',
          'SCHEDULE_RESULT' : 'Jadwal Hasil',
          'SCHEDULE_TEST' : 'Jadwal Tes',
          'TEST' : 'Test',
          'TEST_REFERENCE' : 'Test Reference',
          'CUSTOMER_RELATIONSHIP': 'Hubungan Pelanggan',
          'REGISTRATIONS_LIST': 'Daftar Registrasi',
          'REGISTRATIONS': 'Registrasi',
          'SPECIMEN_LAB' : 'Specimen Lab',
          'SPECIMEN_NON_LAB' : 'Specimen Non Lab',
          'ANALYSIS' : 'Analysis Lab',
          'ANALYSIS_NON_LAB' : 'Analysis Non Lab',
          'RESULT' : 'Result',
          'CALENDAR'    : 'Kalender',
          'ECOMMERCE'   : 'E-Commerce',
          'MAIL'        : {
              'TITLE': 'Surat',
              'BADGE': '25'
          },
          'MAIL_NGRX'        : {
              'TITLE': 'Surat Ngrx',
              'BADGE': '13'
          },
          'CHAT'        : 'Pesan',
          'FILE_MANAGER': 'Pengelola File',
          'CONTACTS'    : 'Kontak',
          'TODO'        : 'Daftar Catatan',
          'SCRUMBOARD'  : 'Scrumboard'
      }
  }
};
