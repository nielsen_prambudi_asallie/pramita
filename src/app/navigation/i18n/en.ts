export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'APPLICATIONS': 'Applications',
            'WELCOME': 'Welcome',
            'DASHBOARDS'  : 'Dashboards',
            'MASTER' : 'Master',
            'AGREEMENT' : 'Agreement',
            'COMPANY' : 'Company',
            'PHYSICIAN' : 'Physician',
            'PRICE_LIST' : 'Price List',
            'REWARD' : 'Reward',
            'SCHEDULE' : 'Schedule',
            'SCHEDULE_RESULT' : 'Schedule Result',
            'SCHEDULE_TEST' : 'Schedule Test',
            'TEST' : 'Test',
            'TEST_REFERENCE' : 'Test Reference',
            'CUSTOMER_RELATIONSHIP' : 'Customer Relationship',
            'REGISTRATIONS': 'Registration',
            'REGISTRATIONS_LIST': 'Registration List',
            'SPECIMEN_LAB' : 'Specimen Lab',
            'SPECIMEN_NON_LAB' : 'Specimen Non Lab',
            'ANALYSIS' : 'Analysis Lab',
            'ANALYSIS_NON_LAB' : 'Analysis Non Lab',
            'RESULT' : 'Result',
            'CALENDAR'    : 'Calendar',
            'ECOMMERCE'   : 'E-Commerce',
            'MAIL'        : {
                'TITLE': 'Mail',
                'BADGE': '25'
            },
            'MAIL_NGRX'        : {
                'TITLE': 'Mail Ngrx',
                'BADGE': '13'
            },
            'CHAT'        : 'Chat',
            'FILE_MANAGER': 'File Manager',
            'CONTACTS'    : 'Contacts',
            'TODO'        : 'To-Do',
            'SCRUMBOARD'  : 'Scrumboard'
        }
    }
};
