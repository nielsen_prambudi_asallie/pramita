import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { RequestOptions } from '@angular/http/src/base_request_options';
import {HttpModule, Http, Headers} from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import {catchError, map, debounceTime, switchMap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';



@Injectable()
export class RegistrationListDetailService
{
    private _options ={ headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    constructor(private http:HttpClient) {

    }

    getPaymentBank(searchPayBank : any): Observable<any[]> {
        return this.http.get<any>('https://202.146.232.86:4000/Api/payto/'+ searchPayBank).pipe(
          catchError(() => of(({}))),
          map(res => res)
        )
    }
  
    getPaymentCard(searchPayCard : any): Observable<any[]> {
        return this.http.get<any>('https://202.146.232.86:4000/Api/PayCard/'+ searchPayCard).pipe(
        catchError(() => of(({}))),
        map(res => res)
    )

    }

    getRegListDetail(regId):  Observable<any> {
        // regId = '5B5CA7A6-35E7-421B-A4E8-92A9DA132EDD';

        return this.http.get<any>('https://202.146.232.86:4000/Api/Register/' + regId + '/Payment')
        .pipe(
            map(res => res)
        );
    }

    getRegListDetailPayment(regId) {
        // regId = '5B5CA7A6-35E7-421B-A4E8-92A9DA132EDD';

        return this.http.get<any>('https://202.146.232.86:4000/Api/Register/' + regId + '/Payment')
        // .pipe(
        //     map(res => res)
        // );
        .toPromise()
        .then(res => res);
    }

    postPayment(specData) {
        let body = JSON.stringify(
          specData
        );
        return this.http.post('https://202.146.232.86:4000/Api/Register/Payment/submit', body, this._options)
          .toPromise()
          .then(response => response, this.handleError)

      }

      handleError(error) {
        console.log(error);
        return error.message || 'Server error, please try again later';
      }

}
