export interface Tes {
  testID?;
  testName?: string;
  TestGroup?: string;
  testPrice?;
  schedule_name?: string;
  condition?: Condition[];
  schedule?: Schedule[];
  price?: Price[];
  children?: Tes[];
  parent?: string;
  label?;
  testCategory?: Tes[];
  testCategoryName?;
  expanded?: boolean;

  structuredItemDetail?: [
    {
      testMasterId?;
      testDetailId?;
      testDetailName?;
      testPrice?;
      testDiscount?;
      testPayable?;
      structuredItemDetail?: [{}];
    }]
}

export interface Condition {
  questionID?;
  question?: string;
  answer?: Answer[];
  note?: string;
}

export interface Answer {
  AnswerID?;
  AnswerName?: string;
}

export interface Schedule {
  scheduleTimeId?;
  ScheduleName?: string;
  Date?: "yyyy-MM-dd";
  Time?: "HH-mm";
}

export interface Price {
  TestID?;
  TestName?: string;
  Price?;
  Discount?;
  Payable?;
}
