import { Component, OnDestroy, ElementRef, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { VerificationDetailService } from './verification_detail.service';
import { fuseAnimations } from '../../../../../core/animations';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar } from '@angular/material';
import { Location } from '@angular/common';
import { MatPaginator, MatSort,  MatTableDataSource  } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ActivatedRoute, Router } from '@angular/router';
import {of} from "rxjs/observable/of";
import {catchError, finalize} from "rxjs/operators";
import { Options } from 'selenium-webdriver';

export interface Detail {
    nama?;
    estVol?;
    no?;
    bodySite?;
    loCSite?;
    drawSite?;
    drawUser?;
    draw?: boolean;
    condition?;
    specimenId?;
    note?;
    received?: boolean;
    receiveDate?: "yyyy-MM-dd";
    receiveUser?: "yyyy-MM-dd";
}

export interface Detail2 {
    registerTestId?;
    testName?;
    cito?: boolean;
    scheduleName?;
}

export interface Detail3 {
    id?;
    name?;
    result?;
    result_comment?;
    auth?: boolean;
    authDate?;
    regno?;
    regdate?;
}

@Component({
    selector     : 'verification_detail',
    templateUrl  : './verification_detail.component.html',
    styleUrls    : ['./verification_detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class VerificationDetailComponent implements OnInit
{
    verifikasi_detail: {regid: any, name: any, age: any }
    name: any;
    age: any;
    regid: any;
    regdate: any;
    user: any;
    doctor: any;
    errorMessage: boolean;
    successMessage: boolean;
    tampungItemVer: any = [];
    // tampungSampleProcess: any = [];
    // tampungSampleReceived: any = [];
    // tampungResultAuth: any = [];
    createData : any = [];
    data: any;
    createVer: any = [];
    detail: Detail[];
    detail2: Detail2[];
    detail3: Detail3[];
    drawReceive: FormGroup;
    patientNoteGroup: FormGroup;
    physicianNoteGroup: FormGroup;
    referralNoteGroup: FormGroup;
    labNoteGroup: FormGroup;
    statusTest: FormGroup;
    manyItem: FormGroup;
    samples: FormGroup;
    dataSourceItem: FileDetailSourceItem;
    // dataSourceSample: FileDetailSourceSample;
    dataSourceSampleHandling: FileDetailSourceSampleHandling;
    dataSourceSpecimenHandling: FileDetailSourceSpecimenHandling;
    dataSourceSpecimenCollection: FileDetailSourceSpecimenCollection;
    dataSourceLabNote: FileDetailSourceLabNote;
    dataSource: FileDetailSource;
    dataSourceSch : FileDetailSourceSch;
    // dataSourceStatus: FileDetailSourceStatus;
    // dataSourceRun: FileDetailSourceRun;
    drawer: string;
    clickDraw: boolean = false;
    clickRe: boolean = false;
    clickCito: boolean = false;

    displayedColumns = ['question', 'answer', 'note']
    displayedColumnsNoteLab = ['no', 'note', 'user', 'date', 'regno'];
    displayedColumns2= ['schedule', 'test', 'day', 'time'];
    displayedColumnsItem = ['testname', 'flag', 'interfaceFlag', 'result', 'resultComment', 'verification', 'last1', 'last2', 'reference', 'INP', 'PRN', 'process', 'verificationDate', 'verificationUser', 'resultINPDate', 'resultINPUser', 'processDate', 'processUser','cito', 'schedule', 'scheduleDateTime', 'resultSI', 'format', 'formatSI', 'rawResult', 'unit', 'testid', 'testGroupName', 'instrumentName', 'microbiology'];
    displayedColumnsRun = ['id', 'name', 'result', 'result_comment', 'status', 'analyzer'];
    displayedColumnsTest = ['testid', 'testname', 'testmethod'];
    displayedColumnsStatus = ['id', 'name', 'result', 'result_comment', 'auth', 'auth_date'];
    displayedColumnsSampleHandling = ['name', 'estvol', 'condition', 'note', 'received', 'receiveddate', 'receiveduser','process', 'processdate', 'processuser', 'samplehandlingid'];
    displayedColumnsSpecimenHandling = ['nama', 'estvol', 'conditionid', 'condition', 'note', 'send', 'senddate', 'process', 'processdate', 'senduser', 'processuser'];
    // displayedColumnsSample = ['specimenName', 'no', 'estvol', 'vol', 'condition', 'note', , 'process', 'processdate','received', 'receiveddate'];
    displayedColumnsSpecimenCollection = ['nama', 'estvol', 'receiveuser', 'receive', 'specimenid', 'conditionid', 'note', 'send', 'senddate', 'senduser'];

    initSample(sample) {
      const group = this.formBuilder.group({
        specimenId: sample.specimenId,
        specimenName: sample.specimenName,
        no: sample.no,
        estvol: sample.estvol,
        vol: sample.vol,
        condition: sample.condition,
        note: sample.note,
        process: sample.process,
        processDate: sample.processDate,
        received: sample.received,
        receivedDate: sample.receiveDate
      })
      return group;
    }

    initStatusTest (stat) {
        const group = this.formBuilder.group({
            id: stat.id,
            name: stat.name,
            result: stat.result,
            result_comment: stat.result_comment,
            auth: stat.auth,
            authDate: stat.authDate
        })
        return group;
    }


    initNote() {
        return this.formBuilder.group({
            LabNote: [''],
            user: 'admin'
            // date: [''],
            // RegisterId: this.route.snapshot.params['regid']
        })
    }

    addNoteValue() {
        const control = <FormArray>this.labNoteGroup.controls['labNotes'];
        control.push(this.initNote())
    }

    deleteNoteValue(index) {
        if(index > 0){
            const control = <FormArray>this.labNoteGroup.controls['labNotes'];
            control.removeAt(index);
        }
    }

    passver(index, value) {
      this.tampungItemVer[index].resultINP = value.controls.process.value;
    }



    // passprocesssample(index, value) {
    //     this.tampungSampleProcess[index].process = value.controls.process.value, value.controls.specimenId.value;
    // }

    // passreceived(index, value) {
    //     this.tampungSampleReceived[index].received = value.controls.received.value, value.controls.specimenId.value;
    // }

    // passauth(index, value) {
    //     this.tampungResultAuth[index].auth = value.controls.auth.value, value.controls.id.value;
    // }

    constructor(private route: ActivatedRoute, private verificationDetailService: VerificationDetailService, private formBuilder: FormBuilder, private router: Router) {
        // this.samples = this.formBuilder.group({
        //     sampleArray: this.formBuilder.array([])
        // })

        // this.statusTest = this.formBuilder.group({
        //     statusTestArray: this.formBuilder.array([])
        // })

        // // console.log(this.drawReceive.value['drawUser'] = "admin")

        this.labNoteGroup = this.formBuilder.group({
            labNotes: this.formBuilder.array([this.initNote()])
        })

        this.manyItem = this.formBuilder.group({
            itemArray: this.formBuilder.array([])
        })


    }

    ngOnInit() {


        this.verifikasi_detail = {
          regid: this.route.snapshot.params['regid'],
          name: this.route.snapshot.params['name'],
          age: this.route.snapshot.params['age']
        }
        this.regid = this.verifikasi_detail['regid'];
        this.name = this.verifikasi_detail['name'];
        this.age = this.verifikasi_detail['age'];


        this.verificationDetailService.createVerificationDetail(this.regid).subscribe(create => {
            this.createData = create;

        })

        this.verificationDetailService.getDoctor(this.regid).then(doc => this.doctor = doc)


        // const control2 = <FormArray>this.statusTest.controls['statusTestArray'];
        // this.verificationDetailService.getstatustest(this.insid, this.pasid).then(statTest => {
        //   this.detail3 = statTest
        //   statTest.forEach(b => {
        //     control2.push(this.initStatusTest(b))
        //     console.log("init Status Text", b)
        //     this.tampungResultAuth.push({
        //       "id": b.id,
        //       "auth": b.auth
        //     })
        //   });
        // })

        // const control = <FormArray>this.samples.controls['sampleArray'];
        // this.verificationDetailService.getSample(this.insid, this.pasid).then(sample => {
        //   this.detail = sample
        //   sample.forEach(x => {
        //     control.push(this.initSample(x))
        //     console.log("init Dra re", x)
        //     this.tampungSampleProcess.push({
        //       "specimenId": x.specimenId,
        //       "process": x.process
        //     })
        //     this.tampungSampleReceived.push({
        //       "specimenId": x.specimenId,
        //       "received": x.received
        //     })
        //   });
        // })

        // const control1 = <FormArray>this.manyItem.controls['itemArray'];
        // this.verificationDetailService.getItem(this.regid).then(item => {
        //   this.detail2 = item
        //   console.log("item item", item)
        //   item.forEach(y => {
        //     control1.push(this.initItem(y))
        //     console.log("init item", y)
        //     this.tampungItemVer.push({
        //       "process": y.process
        //     });
        //   });
        // })

        this.dataSourceItem = new FileDetailSourceItem(this.verificationDetailService)

        this.dataSourceItem.loadDetailItem(this.regid)

        // this.dataSourceTest = new FileDetailSourceTest(this.verificationDetailService)

        // this.dataSourceTest.loadDetailTest(this.regid)

        // this.dataSourceRun = new FileDetailSourceRun(this.verificationDetailService)

        // this.dataSourceRun.loadDetailRun(this.insid,this.pasid)

        // this.dataSourceStatus = new FileDetailSourceStatus(this.verificationDetailService)

        // this.dataSourceStatus.loadDetailStatus(this.insid, this.pasid)

        // this.dataSourceSample = new FileDetailSourceSample(this.verificationDetailService)

        // this.dataSourceSample.loadDetailSample(this.regid)

        this.dataSourceSampleHandling = new FileDetailSourceSampleHandling(this.verificationDetailService)

        this.dataSourceSampleHandling.loadDetailSampleHandling(this.regid)

        this.dataSourceSpecimenHandling = new FileDetailSourceSpecimenHandling(this.verificationDetailService)

        this.dataSourceSpecimenHandling.loadDetailSpecimenHandling(this.regid)

        this.dataSourceSpecimenCollection = new FileDetailSourceSpecimenCollection(this.verificationDetailService)

        this.dataSourceSpecimenCollection.loadDetailSpecimenCollection(this.regid)

        this.dataSource = new FileDetailSource(this.verificationDetailService);

        this.dataSource.loadDetail(this.regid)

        this.dataSourceSch = new FileDetailSourceSch(this.verificationDetailService);

        this.dataSourceSch.loadDetailSch(this.regid)

        this.dataSourceLabNote = new FileDetailSourceLabNote(this.verificationDetailService);

        this.dataSourceLabNote.loadDetailLabNote(this.regid)

        // this.dataSourceSample = new FileDetailSourceSample(this.processSampleDetailService);

        // this.dataSourceSample.loadDetailSample(this.regid)


    }

    // sendData() {
    //     console.log(this.drawReceive.value["drawReceiveArray"])
    //     console.log(this.regid);
    //     // console.log(this.dataSource['value'])
    // }

    verify(event, index) {
        this.dataSourceItem.saveVerify(index)
    }

    comment(event, index) {
        this.dataSourceItem.saveComment(event, index)
    }

    result(event, index) {
        this.dataSourceItem.saveResult(event, index)
    }

    prn(event, index) {
        this.dataSourceItem.savePrn(event, index)
    }



    submitVerifikasi(data) {
        this.createVer = {
            registerId : this.regid,
            patientNote : this.createData.patientNote,
            referalNote: this.createData.referalNote,
            doctornote : this.createData.doctornote,
            regNo : this.createData.regNo,
            regDate : this.createData.regDate,
            verificationAnalystForm : null,
            verificationAnalystItem : this.dataSourceItem.loadItem(),
            // process: this.tampungItemProcess,
            // specimenCollectionForm : this.drawReceive.value['drawReceiveArray'],
            // specimenHandlingForm : null,
            // sampleHandlingForm : null,
            labNote : this.labNoteGroup.value['labNote']
        }
        // console.log(this.dataSourceItem.loadItem())
        console.log(this.createVer)
        data = this.createVer
        this.verificationDetailService.postVer(data).subscribe(res => {
            if(res == 'Submited'){
                this.successMessage = true;
                setTimeout(function () {
                        this.successMessage = false;
                        this.router.navigate(['apps/analysis/verification_pasien'])
                    }.bind(this), 2000)
            }else{
                this.errorMessage = true;
                setTimeout(function (){
                    this.errorMessage = false;
                }.bind(this), 2000)
            }
        },error => {
            this.errorMessage = true;
            setTimeout(function (){
                this.errorMessage = false;
            }.bind(this), 2000)
        });
        console.log(data);
        
    }
}

export class FileDetailSourceItem extends DataSource<any>
{
    private detailItemSubject = new BehaviorSubject<any>([]);

    private loadingDetailItem = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailItem.asObservable();

    constructor(private verificationDetailService: VerificationDetailService) {
        super()
    }

    saveVerify(index) {
        console.log(this.detailItemSubject.value[index])
        this.detailItemSubject.value[index].verification = true;
        return this.detailItemSubject.value;
    }

    saveComment(resComment: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailItemSubject.value[index].resultComment = resComment;
        return this.detailItemSubject.value;
    }

    saveResult(res: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailItemSubject.value[index].result = res;
        return this.detailItemSubject.value;
    }

    savePrn(prn: any, index) {
        // console.log(this.detailItemSubject.value[index])
      this.detailItemSubject.value[index].PRN = prn;
        return this.detailItemSubject.value;
    }

    loadItem(){
        return this.detailItemSubject.value;
    }

    loadDetailItem(regid: any){
        this.loadingDetailItem.next(true);

        this.verificationDetailService.createVerificationDetail(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailItem.next(false))
        )
        .subscribe(item => {
            this.detailItemSubject.next(item.verificationAnalystItem)
        })
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailItemSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailItemSubject.complete();
        this.loadingDetailItem.complete();
    }
}

export class FileDetailSourceLabNote extends DataSource<any>
{
    private detailLabNoteSubject = new BehaviorSubject<any>([]);

    private loadingDetailLabNote = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailLabNote.asObservable();

    constructor(private verificationDetailService: VerificationDetailService) {
        super()
    }

    loadDetailLabNote(regid: any){
        this.loadingDetailLabNote.next(true);

        this.verificationDetailService.getLabNote(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailLabNote.next(false))
        )
        .subscribe(test => this.detailLabNoteSubject.next(test))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailLabNoteSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailLabNoteSubject.complete();
        this.loadingDetailLabNote.complete();
    }
}

// export class FileDetailSourceTest extends DataSource<any>
// {
//     private detailTestSubject = new BehaviorSubject<any>([]);

//     private loadingDetailTest = new BehaviorSubject<boolean>(false);

//     public loading$ = this.loadingDetailTest.asObservable();

//     constructor(private verificationDetailService: VerificationDetailService) {
//         super()
//     }

//     loadDetailTest(regid: any){
//         this.loadingDetailTest.next(true);

//         this.verificationDetailService.getTest(regid).pipe(
//              catchError(() => of([])),
//              finalize(() => this.loadingDetailTest.next(false))
//         )
//         .subscribe(test => this.detailTestSubject.next(test))
//     }

//     connect(collectionViewer: CollectionViewer): Observable<any>
//     {
//         console.log("Connecting data source");
//         return this.detailTestSubject.asObservable();
//     }


//     disconnect(collectionViewer: CollectionViewer): void
//     {
//         this.detailTestSubject.complete();
//         this.loadingDetailTest.complete();
//     }
// }

// export class FileDetailSourceRun extends DataSource<any>
// {
//     private detailRunSubject = new BehaviorSubject<any>([]);

//     private loadingDetailRun = new BehaviorSubject<boolean>(false);

//     public loading$ = this.loadingDetailRun.asObservable();

//     constructor(private verificationDetailService: VerificationDetailService) {
//         super()
//     }

//     loadDetailRun(insid: any, pasid: any){
//         this.loadingDetailRun.next(true);

//         this.verificationDetailService.getRun(insid,pasid).pipe(
//              catchError(() => of([])),
//              finalize(() => this.loadingDetailRun.next(false))
//         )
//         .subscribe(run => this.detailRunSubject.next(run))
//     }

//     connect(collectionViewer: CollectionViewer): Observable<any>
//     {
//         console.log("Connecting data source");
//         return this.detailRunSubject.asObservable();
//     }


//     disconnect(collectionViewer: CollectionViewer): void
//     {
//         this.detailRunSubject.complete();
//         this.loadingDetailRun.complete();
//     }
// }

// export class FileDetailSourceStatus extends DataSource<any>
// {
//     private detailStatusSubject = new BehaviorSubject<any>([]);

//     private loadingDetailStatus = new BehaviorSubject<boolean>(false);

//     public loading$ = this.loadingDetailStatus.asObservable();

//     constructor(private verificationDetailService: VerificationDetailService) {
//         super()
//     }

//     loadDetailStatus(insid: any, pasid: any){
//         this.loadingDetailStatus.next(true);

//         this.verificationDetailService.getResultStatus(insid, pasid).pipe(
//              catchError(() => of([])),
//              finalize(() => this.loadingDetailStatus.next(false))
//         )
//         .subscribe(status => this.detailStatusSubject.next(status))
//     }

//     connect(collectionViewer: CollectionViewer): Observable<any>
//     {
//         console.log("Connecting data source");
//         return this.detailStatusSubject.asObservable();
//     }


//     disconnect(collectionViewer: CollectionViewer): void
//     {
//         this.detailStatusSubject.complete();
//         this.loadingDetailStatus.complete();
//     }
// }

// export class FileDetailSourceSample extends DataSource<any>
// {
//    private detailSampleSubject = new BehaviorSubject<any>([]);

//    private loadingDetailSample = new BehaviorSubject<boolean>(false);

//    public loading$ = this.loadingDetailSample.asObservable();

//    constructor(private verificationDetailService: VerificationDetailService) {
//        super()
//    }

//    loadDetailSample(pasid: any){
//        this.loadingDetailSample.next(true);

//        this.verificationDetailService.getSample(pasid).pipe(
//             catchError(() => of([])),
//             finalize(() => this.loadingDetailSample.next(false))
//        )
//        .subscribe(sample => this.detailSampleSubject.next(sample))
//    }

//    connect(collectionViewer: CollectionViewer): Observable<any>
//    {
//        console.log("Connecting data source");
//        return this.detailSampleSubject.asObservable();
//    }


//    disconnect(collectionViewer: CollectionViewer): void
//    {
//        this.detailSampleSubject.complete();
//        this.loadingDetailSample.complete();
//    }
// }

export class FileDetailSourceSampleHandling extends DataSource<any>
{
    private detailSampleHandlingSubject = new BehaviorSubject<any>([]);

    private loadingDetailSampleHandling = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSampleHandling.asObservable();

    constructor(private verificationDetailService: VerificationDetailService) {
        super()
    }

    loadDetailSampleHandling(regid: any){
        this.loadingDetailSampleHandling.next(true);

        this.verificationDetailService.getSampleHandling(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSampleHandling.next(false))
        )
        .subscribe(sample => this.detailSampleHandlingSubject.next(sample))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSampleHandlingSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSampleHandlingSubject.complete();
        this.loadingDetailSampleHandling.complete();
    }
}

export class FileDetailSourceSpecimenHandling extends DataSource<any>
{
    private detailSpecimenHandlingSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimenHandling = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimenHandling.asObservable();

    constructor(private verificationDetailService: VerificationDetailService) {
        super()
    }

    loadDetailSpecimenHandling(regid: any){
        this.loadingDetailSpecimenHandling.next(true);

        this.verificationDetailService.getSpecimenHandling(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimenHandling.next(false))
        )
        .subscribe(specimen => this.detailSpecimenHandlingSubject.next(specimen))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimenHandlingSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimenHandlingSubject.complete();
        this.loadingDetailSpecimenHandling.complete();
    }
}

export class FileDetailSourceSpecimenCollection extends DataSource<any>
{
    private detailSpecimenCollectionSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimenCollection = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimenCollection.asObservable();

    constructor(private verificationDetailService: VerificationDetailService) {
        super()
    }

    loadDetailSpecimenCollection(regid: any){
        this.loadingDetailSpecimenCollection.next(true);

        this.verificationDetailService.getSpecimenCollection(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimenCollection.next(false))
        )
        .subscribe(specimen => this.detailSpecimenCollectionSubject.next(specimen))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimenCollectionSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimenCollectionSubject.complete();
        this.loadingDetailSpecimenCollection.complete();
    }
}

export class FileDetailSource extends DataSource<any>
{
    private detailSpecimensSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimens = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimens.asObservable();

    constructor(private verificationDetailService: VerificationDetailService) {
        super()
    }

    loadDetail(regid: any){
        this.loadingDetailSpecimens.next(true);

        this.verificationDetailService.getSpecimensDetailCond(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimens.next(false))
        )
        .subscribe(detspec => this.detailSpecimensSubject.next(detspec))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimensSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimensSubject.complete();
        this.loadingDetailSpecimens.complete();
    }
}



export class FileDetailSourceSch extends DataSource<any>
{
    private detailSpecimensSubjectSch = new BehaviorSubject<any>([]);

    private loadingDetailSpecimensSch = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimensSch.asObservable();

    constructor(private verificationDetailService: VerificationDetailService) {
        super()
    }

    loadDetailSch(regid: any){
        this.loadingDetailSpecimensSch.next(true);

        this.verificationDetailService.getSpecimensDetailSch(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimensSch.next(false))
        )
        .subscribe(detspec => this.detailSpecimensSubjectSch.next(detspec))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimensSubjectSch.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimensSubjectSch.complete();
        this.loadingDetailSpecimensSch.complete();
    }
}

// export class FileDetailSourceSample extends DataSource<any>
// {
//     private detailAnalysisSubjectSample = new BehaviorSubject<any>([]);

//     private loadingDetailAnalysisSample = new BehaviorSubject<boolean>(false);

//     public loading$ = this.loadingDetailAnalysisSample.asObservable();

//     constructor(private processSampleDetailService: ProcessSampleDetailService) {
//         super()
//     }

//     loadDetailSample(regid: any){
//         this.loadingDetailAnalysisSample.next(true);

//         this.processSampleDetailService.getAnalysisDetailSample(regid).pipe(
//              catchError(() => of([])),
//              finalize(() => this.loadingDetailAnalysisSample.next(false))
//         )
//         .subscribe(detspec => this.detailAnalysisSubjectSample.next(detspec))
//     }

//     connect(collectionViewer: CollectionViewer): Observable<any>
//     {
//         console.log("Connecting data source");
//         return this.detailAnalysisSubjectSample.asObservable();
//     }


//     disconnect(collectionViewer: CollectionViewer): void
//     {
//         this.detailAnalysisSubjectSample.complete();
//         this.loadingDetailAnalysisSample.complete();
//     }
// }
