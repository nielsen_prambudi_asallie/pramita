import { MatChipInputEvent } from '@angular/material';


export class TesModel
{
  TestID: string;
    TestName: string;
    TestGroup: string;
    TestPrice: string;
    schedule_name: string;
    Schedule: string;
    jamJadwal: Date;

    constructor(tes?)
    {
        tes = tes || {};
        this.TestID = tes.TestID || '';
        this.TestName = tes.TestName || '';
        this.TestGroup = tes.TestGroup || '';
        this.TestPrice = tes.TestPrice || '';
        this.schedule_name = tes.schedule_name || '';
        this.Schedule = tes.Schedule || '';
    }
}
