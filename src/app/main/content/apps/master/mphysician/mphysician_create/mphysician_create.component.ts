import { Component, ElementRef, OnInit, ViewChild, AfterViewInit, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { MasterPhysicianCreateService } from './mphysician_create.service';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import {ActivatedRoute} from "@angular/router";
import { fuseAnimations } from '../../../../../../core/animations';
import { MatPaginator, MatSort, PageEvent, MatTableDataSource } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import {debounceTime, map, distinctUntilChanged, switchMap, startWith, tap, delay} from 'rxjs/operators';
import {merge} from "rxjs/observable/merge";
import {fromEvent} from 'rxjs/observable/fromEvent';
import { FuseUtils } from '../../../../../../core/fuseUtils';
import { ErrorStateMatcher } from '@angular/material/core';
import {of} from "rxjs/observable/of";
import {catchError, finalize} from "rxjs/operators";
import { NgOption } from '@ng-select/ng-select';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  FormGroupDirective,
  NgForm,
  ValidatorFn,
  AbstractControl,
  Validators,
  NG_VALUE_ACCESSOR,
  FormArray
} from '@angular/forms';

@Component({
    selector   : 'mphysician_create',
    templateUrl: './mphysician_create.component.html',
    styleUrls  : ['./mphysician_create.component.scss'],
    animations : fuseAnimations
})
export class MasterPhysicianCreateComponent implements OnInit, AfterViewInit
{
  doctorForm: FormGroup;

  createDoctor: any = [];

  doctorNameCtrl = new FormControl();
  frontTitleCtrl = new FormControl();
  endTitleCtrl = new FormControl();
  nipCtrl = new FormControl();

  chooseDoctorSpec = new FormControl();
  filteredDoctorSpec: Observable<any[]>;
  docSpec:any;

  chooseSex = new FormControl();
  filteredDoctorSex: Observable<any[]>;
  docSex:any;

  chooseMarketer = new FormControl();
  filteredMarks: Observable<any[]>;
  marketer:any;

  activeCtrl = new FormControl();
  active:boolean;

  typeaheadType = new EventEmitter<any>();
  doctorAddType = [];
  hid_type = [];
  
  typeaheadCity = new EventEmitter<any>();
  doctorCity = [];
  hid_city = [];
  
  typeaheadContactType = new EventEmitter<any>();
  doctorContact = [];
  hid_contact = [];

  doctorName: any;
  doctorSub: any;
  matcher: any;
  doctorType: any;
  doctorSpec: any;
  frontTitle: any;
  nip: any;
  doctorSex: any;
  textChanged: any;
  endTitle: any;
  successMessage: boolean;
  errorMessage: boolean;

  constructor(
      private masterPhysicianCreateService: MasterPhysicianCreateService,
      private route : ActivatedRoute,
      private formBuilder: FormBuilder,
      private ref: ChangeDetectorRef
    )
  {
    this.constructAll()
  }

  constructAll(){
    this.activeCtrl.setValue(true);

    this.filteredDoctorSpec = this.chooseSex.valueChanges
      .pipe(
        startWith(null),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(valref => {
          // console.log("valref: ", valref)
          return this.filterDoctorSpec(valref || '')
        })
      );

      this.filteredDoctorSex = this.chooseSex.valueChanges
      .pipe(
        startWith(null),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(valref => {
          // console.log("valref: ", valref)
          return this.filterDoctorSex(valref || '')
        })
      );

      this.filteredMarks = this.chooseMarketer.valueChanges
      .pipe(
        startWith(null),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(valref => {
          // console.log("valref: ", valref)
          return this.filterMarketer(valref || '')
        })
      );
  }

  ngOnInit()
  {
    this.hid_contact.push(false);
    this.hid_type.push(false);
    this.hid_city.push(false);
    this.resetDoctor();
  }

  ngAfterViewInit() {

  }

  //Specialities
  displayDoctorSpec(docSpec): any {
    return docSpec ? docSpec.specialtyName : docSpec;
  }

  filterDoctorSpec(doctorSpecVal: string): Observable<any[]> {
    return this.masterPhysicianCreateService.getSpecialities(doctorSpecVal)
    .pipe(
      map(response => response.filter(option => {
        return (option.specialtyName) != null ?
        option.specialtyName :
        option.specialtyId
      }))
    )
  }

  onDoctorSpecSelected(docSpec){
    if(docSpec !== null){
      console.log(docSpec.specialtyId);
      this.doctorForm.patchValue({
        chooseDoctorSpec : docSpec.specialtyId
      })
    }
  }

  //Sex
  displayDoctorSex(docSex): any {
    return docSex ? docSex.sexName : docSex;
  }

  filterDoctorSex(doctorSexVal: string): Observable<any[]> {
    return this.masterPhysicianCreateService.getSex(doctorSexVal)
    .pipe(
      map(response => response.filter(option => {
        return (option.sexName) != null ?
        option.sexName :
        option.sexId
      }))
    )
  }

  onDoctorSexSelected(docSex){
    if(docSex !== null){
      console.log(docSex.sexId);
      this.doctorForm.patchValue({
        chooseSex : docSex.sexId
      })
    }
  }
  
  //Marketer
  displayMarketer(marketer): any {
    return marketer ? marketer.marketerName : marketer;
  }

  filterMarketer(marketerVal: string): Observable<any[]> {
    return this.masterPhysicianCreateService.getMarketer(marketerVal)
    .pipe(
      map(response => response.filter(option => {
        return (option.marketerName) != null ?
        option.marketerName :
        option.marketerId
      }))
    )
  }

  onMarketerSelected(marketer){
    if(marketer !== null){
      console.log(marketer.marketerId);
      this.doctorForm.patchValue({
        chooseMarketer : marketer.marketerId
      })
    }
  }

  //Doctor
  resetDoctor(){
    this.doctorNameCtrl.reset();
    this.frontTitleCtrl.reset();
    this.endTitleCtrl.reset();
    this.nipCtrl.reset();
    this.chooseDoctorSpec.reset();
    this.chooseSex.reset();
    this.chooseMarketer.reset();
    this.doctorForm = this.formBuilder.group({
      doctorName: this.doctorNameCtrl,
      doctorSpec: this.chooseDoctorSpec,
      sex: this.chooseSex,
      marketer: this.chooseMarketer,
      doctorAddress: this.formBuilder.array([this.initAddress(1)]),
      doctorContact: this.formBuilder.array([this.initContact()])
    });
  }

  submitNewDoctor(){
    this.createDoctor = {
      doctorName: this.doctorNameCtrl.value,
      frontTitle: this.frontTitleCtrl.value,
      endTitle: this.endTitleCtrl.value,
      nip: this.nipCtrl.value,
      specialtyId: this.chooseDoctorSpec.value.specialtyId,
      sexId: this.chooseSex.value.sexId,
      marketerId: this.chooseMarketer.value.marketerId,
      active: this.activeCtrl.value,
      strDoctorAddress: this.doctorForm.value['doctorAddress'],
      strDoctorMessenger: this.doctorForm.value['doctorContact']
    }
    console.log(this.createDoctor);
    this.masterPhysicianCreateService.postNewDoctor(this.createDoctor)
      .subscribe(res => {
        if(res == 'true'){
            this.successMessage = true;
            setTimeout(function () {
                    this.successMessage = false;
                
                }.bind(this), 2000)
        }else{
            this.errorMessage = true;
            setTimeout(function (){
                this.errorMessage = false;
            }.bind(this), 2000)
        }
      },error => {
          this.errorMessage = true;
          setTimeout(function (){
              this.errorMessage = false;
          }.bind(this), 2000)
      });
    this.resetDoctor();
  }
  
  //Address
  initAddress(addressIdVal): FormGroup {
    return this.formBuilder.group({
      addressId: [addressIdVal],
      addressLocation: [''],
      addressName: [''],
      addressType: [''],
      addressTypeId: [''],
      addressCity: [''],
      addressCityId: [''],
      addressPostCode: ['', [Validators.required, Validators.pattern]],
      addressPhone: [''],
      addressFax: ['']
    });
  }

  addAddressValue() {
    const control = <FormArray>this.doctorForm.controls['doctorAddress'];
    control.push(this.initAddress(this.doctorForm.value['doctorAddress'].length+1));
  }

  deleteAddressValue(index) {
    if(index > 0){
      const control = <FormArray>this.doctorForm.controls['doctorAddress'];
      control.removeAt(index);
    }
  }

  //Address Type
  onchangeTipe(event, index) {
    console.log('Tipe set', this.doctorForm.controls.doctorAddress['controls'][index]);
    let Tipe = this.doctorForm.controls.doctorAddress['controls'][index].controls["addressType"];
    let Tipeid = this.doctorForm.controls.doctorAddress['controls'][index].controls["addressTypeId"];
    Tipeid.setValue(event.addressTypeId);
    Tipe.setValue(event.addressType);
  }

  onfilledType(event) {
    this.typeaheadType
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.masterPhysicianCreateService.getAddressType(data, ""))
      )
      .subscribe(Type => {
        console.log("Type: ", Type)
        this.doctorAddType = Type;
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienType error : ', err);
        this.doctorAddType = [];
        this.ref.markForCheck();
      })
  }

  inputType(event) {
    console.log('event inputtype : ', event);
    this.typeaheadType
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.masterPhysicianCreateService.getAddressType(data, event.target.value))
      )
      .subscribe(Type => {
        console.log("Type: ", Type)
        this.doctorAddType = Type;
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienType error : ', err);
        this.doctorAddType = [];
        this.ref.markForCheck();
      })
  }

  hidtype(event, action, index) {
    console.log("action hidtype", action[index].value.addressType)
    this.hid_type[index] = true;
  }

  //Address City
  onfilledCity(event) {
    this.typeaheadCity
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.masterPhysicianCreateService.getCity(data,""))
      )
      .subscribe(city => {
        console.log("city: ", city)
        this.doctorCity = city;
        console.log("this.pasienCity : ", this.doctorCity)
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienCity error : ', err);
        this.doctorCity = [];
        this.ref.markForCheck();
      })
  }

  inputcity(event) {
    console.log('event inputcity : ', event);
    this.typeaheadCity
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.masterPhysicianCreateService.getCity(data, event.target.value))
      )
      .subscribe(city => {
        console.log("city: ", city)
        this.doctorCity = city;
        console.log("this.doctorCity : ", this.doctorCity )
        this.ref.markForCheck();
      }, (err) => {
        console.log('doctorCity error : ', err);
        this.doctorCity = [];
        this.ref.markForCheck();
      })
  }

  onchangeCity(event, index) {
    let city = this.doctorForm.controls.doctorAddress['controls'][index].controls["addressCity"];
    let cityid = this.doctorForm.controls.doctorAddress['controls'][index].controls["addressCityId"];
    cityid.setValue(event.cityId);
    city.setValue(event.cityName);
    console.log('city set',event);
  }

  hidCity(event, action, index) {
    console.log("action hidCity", action[index].value.addressCity)
    this.hid_city[index]=true;
  }

  //Contact
  initContact() : FormGroup {
    return this.formBuilder.group({
      messangerTypeId: [''],
      messangerType: [''],
      messangerName: ['']
    });
  }

  addContactValue() {
    const control = <FormArray>this.doctorForm.controls['doctorContact'];
    control.push(this.initContact())
  }

  deleteContactValue(index) {
    if(index > 0){
      const control = <FormArray>this.doctorForm.controls['doctorContact'];
      control.removeAt(index);
    }
  }

  //Messenger Type
  onfilledContactType(event) {
    this.typeaheadContactType
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.masterPhysicianCreateService.getContactType(data, ""))
      )
      .subscribe(Type => {
        console.log("Type: ", Type)
        this.doctorContact = Type;
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienType error : ', err);
        this.doctorContact = [];
        this.ref.markForCheck();
      })
  }

  onchangeContactTipe(event, index) {
    console.log('ContactTipe set', event);
    let contactTipe = this.doctorForm.controls.doctorContact['controls'][index].controls["messangerType"];
    let contactTipeid = this.doctorForm.controls.doctorContact['controls'][index].controls["messangerTypeId"];
    contactTipeid.setValue(event.messengerTypeId);
    contactTipe.setValue(event.messangerType);
    console.log('contactTipeid', contactTipeid);
      console.log('contactTipe', contactTipe);
  }

  inputContactType(event) {
    console.log('event inputContactType : ', event);
    this.typeaheadContactType
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.masterPhysicianCreateService.getContactType(data, event.target.value))
      )
      .subscribe(Type => {
        console.log("Type: ", Type)
        this.doctorContact = Type;
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienType error : ', err);
        this.doctorContact = [];
        this.ref.markForCheck();
      })
  }

  hidContacttype(event, action, index) {
    this.hid_contact[index] = true;
  }

}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

