import { Component, OnDestroy, ElementRef, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { ProcessSampleDetailService } from './process_sample_detail.service';
import { fuseAnimations } from '../../../../../core/animations';
import {CollectionViewer, DataSource } from '@angular/cdk/collections';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Product } from './process_sample_detail.model';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar } from '@angular/material';
import { Location, UpperCasePipe } from '@angular/common';
import { MatPaginator, MatSort,  MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ActivatedRoute, Router } from '@angular/router';
import {of} from "rxjs/observable/of";
import {catchError, finalize} from "rxjs/operators";
import { Options } from 'selenium-webdriver';

export interface Detail {
    nama?;
    estVol?;
    no?;
    bodySite?;
    loCSite?;
    drawSite?;
    drawUser?;
    draw?: boolean;
    condition?;
    specimenId?;
    note?;
    received?: boolean;
    receiveDate?: "yyyy-MM-dd";
    receiveUser?: "yyyy-MM-dd";
}

export interface Detail2 {
    registerTestId?;
    testName?;
    cito?: boolean;
    scheduleName?;
}

@Component({
    selector     : 'process_sample_detail',
    templateUrl  : './process_sample_detail.component.html',
    styleUrls    : ['./process_sample_detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProcessSampleDetailComponent implements OnInit
{
    process_sample_detail: {insid: any, pasid: any, name: any, age: any }
    age: any;
    name: any;
    insid: any;
    pasid: any;
    regdate: any;
    user: any;
    doctor: any;
    tampungItemProcess: any = [];
    tampungSampleProcess: any = [];
    tampungSampleReceived: any = [];
    createData : any = [];
    data: any;
    createProcSamp: any = [];
    detail: Detail[];
    detail2: Detail2[];
    drawReceive: FormGroup;
    patientNoteGroup: FormGroup;
    physicianNoteGroup: FormGroup;
    referralNoteGroup: FormGroup;
    labNoteGroup: FormGroup;
    samples: FormGroup;
    errorMessage: boolean;
    successMessage: boolean;
    dataSourceItem: FileDetailSourceItem;
    dataSourceTest: FileDetailSourceTest;
    dataSourceSample: FileDetailSourceSample;
    dataSourceSampleHandling: FileDetailSourceSampleHandling;
    dataSourceSpecimenHandling: FileDetailSourceSpecimenHandling;
    dataSourceSpecimenCollection: FileDetailSourceSpecimenCollection;
    dataSourceLabNote: FileDetailSourceLabNote;
    dataSource: FileDetailSource;
    dataSourceSch : FileDetailSourceSch;
    drawer: string;
    clickDraw: boolean = false;
    clickRe: boolean = false;
    clickCito: boolean = false;

    displayedColumns = ['question', 'answer', 'note']
    displayedColumnsNoteLab = ['no', 'note', 'user', 'date'];
    displayedColumns2= ['schedule', 'test', 'day', 'time'];
    displayedColumnsItem = ['testid', 'testname', 'process', 'processdate', 'priority', 'schedule', 'scheduleDateTime'];
    displayedColumnsTest = ['testid', 'testname', 'testmethod'];
    displayedColumnsSample = ['name', 'no', 'estvol', 'condition', 'note', 'process', 'processdate', 'send', 'sendDate'];
    //displayedColumnsSampleHandling = ['name', 'no', 'estvol', 'vol', 'condition', 'note', 'process', 'processdate', 'processuser', 'samplehandlingid'];
    displayedColumnsSampleHandling = ['name', 'estvol', 'condition', 'note', 'received', 'receiveddate', 'receiveduser','process', 'processdate', 'processuser', 'samplehandlingid'];
   // displayedColumnsSpecimenHandling = ['nama', 'no', 'estvol', 'vol', 'conditionid', 'condition', 'note', 'received', 'receiveddate', 'process', 'processdate', 'receiveuser', 'processuser'];
    displayedColumnsSpecimenHandling = ['nama', 'estvol', 'conditionid', 'condition', 'note', 'send', 'senddate', 'process', 'processdate', 'senduser', 'processuser'];
    //displayedColumnsSpecimenCollection = ['nama', 'estvol', 'no', 'bodysite','locsite', 'drawsite', 'drawuser','draw','specimenid', 'conditionid', 'note', 'received', 'receiveddate', 'receiveuser'];
    displayedColumnsSpecimenCollection = ['nama', 'estvol', 'receiveuser', 'receive', 'specimenid', 'conditionid', 'note', 'send', 'senddate', 'senduser'];



    initNote() {
      return this.formBuilder.group({
        LabNote: [''],
        user: 'admin'
        // date: [''],
        // RegisterId: this.route.snapshot.params['pasid']
      })
    }

    addNoteValue() {
        const control = <FormArray>this.labNoteGroup.controls['labNotes'];
        control.push(this.initNote())
    }
    
    deleteNoteValue(index) {
        if(index > 0){
            const control = <FormArray>this.labNoteGroup.controls['labNotes'];
            control.removeAt(index);
        }
    }


    passprocesssample(index, value) {
      this.tampungSampleProcess[index].process = value.controls.process.value, value.controls.specimenId.value;
      console.log("this.samples", this.samples)
    }

    passreceived(index, value) {
        this.tampungSampleReceived[index].received = value.controls.received.value, value.controls.specimenId.value;
    }

    constructor(private route: ActivatedRoute, private processSampleDetailService: ProcessSampleDetailService, private formBuilder: FormBuilder, private router: Router) {
        this.samples = this.formBuilder.group({
            sampleArray: this.formBuilder.array([])
        })

        // console.log(this.drawReceive.value['drawUser'] = "admin")

        this.labNoteGroup = this.formBuilder.group({
            labNotes: this.formBuilder.array([this.initNote()])
        })


        
    }

    ngOnInit() {
        // private loadingSpecimensDetail = new BehaviorSubject<boolean>(false);
        // private specimensDetailSubject = new BehaviorSubject<any>([]);

        

        this.process_sample_detail = {
            insid: this.route.snapshot.params['insid'],
            pasid: this.route.snapshot.params['pasid'],
            age: this.route.snapshot.params['age'],
            name: this.route.snapshot.params['name']
        }
        this.insid = this.process_sample_detail['insid'];
        this.pasid = this.process_sample_detail['pasid'];
        this.age = this.process_sample_detail['age'];
        this.name = this.process_sample_detail['name'];

        this.processSampleDetailService.createProcessSampleDetail(this.insid,this.pasid).subscribe(create => {
            this.createData = create;
            console.log("this.createData",this.createData.registerId)
        })

        this.processSampleDetailService.getDoctor(this.pasid).then(doc => this.doctor = doc)

        

        this.dataSourceItem = new FileDetailSourceItem(this.processSampleDetailService)

        this.dataSourceItem.loadDetailItem(this.insid, this.pasid)

        this.dataSourceTest = new FileDetailSourceTest(this.processSampleDetailService)

        this.dataSourceTest.loadDetailTest(this.insid,this.pasid)

        console.log(this.dataSourceTest.loadDetailTest(this.insid,this.pasid))

        this.dataSourceSample = new FileDetailSourceSample(this.processSampleDetailService)

        this.dataSourceSample.loadDetailSample(this.insid, this.pasid)

        this.dataSourceSampleHandling = new FileDetailSourceSampleHandling(this.processSampleDetailService)

        this.dataSourceSampleHandling.loadDetailSampleHandling(this.pasid)

        this.dataSourceSpecimenHandling = new FileDetailSourceSpecimenHandling(this.processSampleDetailService)

        this.dataSourceSpecimenHandling.loadDetailSpecimenHandling(this.pasid)

        this.dataSourceSpecimenCollection = new FileDetailSourceSpecimenCollection(this.processSampleDetailService)

        this.dataSourceSpecimenCollection.loadDetailSpecimenCollection(this.pasid)

        this.dataSource = new FileDetailSource(this.processSampleDetailService);

        this.dataSource.loadDetail(this.pasid)

        this.dataSourceSch = new FileDetailSourceSch(this.processSampleDetailService);

        this.dataSourceSch.loadDetailSch(this.pasid)

        this.dataSourceLabNote = new FileDetailSourceLabNote(this.processSampleDetailService);

        this.dataSourceLabNote.loadDetailLabNote(this.pasid)

        // this.dataSourceSample = new FileDetailSourceSample(this.processSampleDetailService);

        // this.dataSourceSample.loadDetailSample(this.regid)
        

    }

    // sendData() {
    //     console.log(this.drawReceive.value["drawReceiveArray"])
    //     console.log(this.regid);
    //     // console.log(this.dataSource['value'])
    // }

    clickAllProc(event){
        this.dataSourceItem.ProcessAll(event)
    }

    process(event, index) {
        this.dataSourceItem.saveProcess(event, index)
    }

    clickAllProcSamp(event){
        this.dataSourceSample.ProcessAll(event)
    }

    processSamp(event, index) {
        this.dataSourceSample.saveProcess(event, index)
    }

    clickAllReSamp(event){
        this.dataSourceSample.ReceivedAll(event)
    }

    receivedSamp(event, index) {
        this.dataSourceSample.saveReceived(event, index)
    }

    backToPasien(insid){
        this.router.navigate(['/apps/analysis/process_sample_instrument/' + this.insid + '/' + this.name])
    }

    submitSample(data) {
         this.createProcSamp = {
           registerId: this.createData.registerId,
           patientNote: this.createData.patientNote,
           referalNote: this.createData.referalNote,
           doctornote: this.createData.doctornote,
           regNo: this.createData.regNo,
           regDate: this.createData.regDate,
           labNote: this.labNoteGroup.value['labNotes'],
           processSampleForm: this.dataSourceSample.loadSamples(),
           processSampleItem: this.dataSourceItem.loadItem()
         }
         console.log(this.createProcSamp)
         data = this.createProcSamp 
        //  this.processSampleDetailService.postSampProc(data)
        //  console.log(data);
        //  alert('Sample berhasil!');
        //  this.router.navigate(['apps/analysis/result_instrument'])
        this.processSampleDetailService.postSampProc(data)
         .subscribe(res => {
            if(res == 'Submited'){
                this.successMessage = true;
                setTimeout(function () {
                        this.successMessage = false;
                        this.router.navigate(['apps/analysis/process_sample_instrument']);
                    
                    }.bind(this), 2000)
                }else{
                    this.errorMessage = true;
                    setTimeout(function (){
                        this.errorMessage = false;
                    }.bind(this), 2000)
                }
            },error => {
                this.errorMessage = true;
                setTimeout(function (){
                    this.errorMessage = false;
                }.bind(this), 2000)
            });
        
    }
}

export class FileDetailSourceItem extends DataSource<any> 
{
    private detailItemSubject = new BehaviorSubject<any>([]);

    private loadingDetailItem = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailItem.asObservable();

    public clickproses: boolean;

    constructor(private processSampleDetailService: ProcessSampleDetailService) {
        super()
    }

    ProcessAll(process:any) {
        // console.log(this.detailItem.value[index])
        // if (this.clickproses) { this.clickproses = false; } else { this.clickproses = true; }
        for (var i = 0; i < this.detailItemSubject.value.length; i++) {
            if(!this.detailItemSubject.value[i].processDisabled)
                this.detailItemSubject.value[i].process = process;
        }
        return this.detailItemSubject.value;
      }
  
    saveProcess(process:any, index) {
    // console.log(this.detailItem.value[index])
        this.detailItemSubject.value[index].process = process;
        // this.detailSpc.value[index].processUser = (process == true ? 'admin' : '');
        return this.detailItemSubject.value;
    }

    loadItem(){
        return this.detailItemSubject.value;
      }

    loadDetailItem(insid: any, pasid: any){
        this.loadingDetailItem.next(true);

        this.processSampleDetailService.getItem(insid, pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailItem.next(false))
        )
        .subscribe(item => {
            for (var i = 0; i < item.length; i++) {
                item[i].processDisabled = item[i].process;
              }
            this.detailItemSubject.next(item);
        })
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailItemSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailItemSubject.complete();
        this.loadingDetailItem.complete();
    }
}

export class FileDetailSourceLabNote extends DataSource<any> 
{
    private detailLabNoteSubject = new BehaviorSubject<any>([]);

    private loadingDetailLabNote = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailLabNote.asObservable();

    constructor(private processSampleDetailService: ProcessSampleDetailService) {
        super()
    }

    loadDetailLabNote(pasid: any){
        this.loadingDetailLabNote.next(true);

        this.processSampleDetailService.getLabNote(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailLabNote.next(false))
        )
        .subscribe(test => this.detailLabNoteSubject.next(test))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailLabNoteSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailLabNoteSubject.complete();
        this.loadingDetailLabNote.complete();
    }
}

export class FileDetailSourceTest extends DataSource<any> 
{
    private detailTestSubject = new BehaviorSubject<any>([]);

    private loadingDetailTest = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailTest.asObservable();

    constructor(private processSampleDetailService: ProcessSampleDetailService) {
        super()
    }

    loadDetailTest(insid: any,pasid: any){
        this.loadingDetailTest.next(true);

        this.processSampleDetailService.getTest(insid,pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailTest.next(false))
        )
        .subscribe(test => this.detailTestSubject.next(test))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailTestSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailTestSubject.complete();
        this.loadingDetailTest.complete();
    }
}

export class FileDetailSourceSample extends DataSource<any> 
{
    private detailSampleSubject = new BehaviorSubject<any>([]);

    private loadingDetailSample = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSample.asObservable();

    public clickproses: boolean;

    public clickreceived: boolean;

    constructor(private processSampleDetailService: ProcessSampleDetailService) {
        super()
    }

    ReceivedAll(receive:any) {
        // console.log(this.detailItem.value[index])
        // if (this.clickreceived) { this.clickreceived = false; } else { this.clickreceived = true; }
        for (var i = 0; i < this.detailSampleSubject.value.length; i++) {
          this.detailSampleSubject.value[i].received = receive;
        }
        return this.detailSampleSubject.value;
      }
  
    saveReceived(received:any, index) {
    // console.log(this.detailItem.value[index])
        this.detailSampleSubject.value[index].received = received;
        // this.detailSpc.value[index].processUser = (process == true ? 'admin' : '');
        return this.detailSampleSubject.value;
    }

    ProcessAll(process:any) {
        // console.log(this.detailItem.value[index])
        // if (this.clickproses) { this.clickproses = false; } else { this.clickproses = true; }
        for (var i = 0; i < this.detailSampleSubject.value.length; i++) {
          this.detailSampleSubject.value[i].process = process;
        }
        return this.detailSampleSubject.value;
      }
  
    saveProcess(process:any, index) {
    // console.log(this.detailItem.value[index])
        this.detailSampleSubject.value[index].process = process;
        // this.detailSpc.value[index].processUser = (process == true ? 'admin' : '');
        return this.detailSampleSubject.value;
    }

    loadSamples(){
        return this.detailSampleSubject.value;
    }

    loadDetailSample(insid: any, pasid: any){
        this.loadingDetailSample.next(true);

        this.processSampleDetailService.getSample(insid, pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSample.next(false))
        )
        .subscribe(sample => this.detailSampleSubject.next(sample))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSampleSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSampleSubject.complete();
        this.loadingDetailSample.complete();
    }
}

export class FileDetailSourceSampleHandling extends DataSource<any> 
{
    private detailSampleHandlingSubject = new BehaviorSubject<any>([]);

    private loadingDetailSampleHandling = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSampleHandling.asObservable();

    constructor(private processSampleDetailService: ProcessSampleDetailService) {
        super()
    }

    loadDetailSampleHandling(pasid: any){
        this.loadingDetailSampleHandling.next(true);

        this.processSampleDetailService.getSampleHandling(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSampleHandling.next(false))
        )
        .subscribe(sample => this.detailSampleHandlingSubject.next(sample))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSampleHandlingSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSampleHandlingSubject.complete();
        this.loadingDetailSampleHandling.complete();
    }
}

export class FileDetailSourceSpecimenHandling extends DataSource<any> 
{
    private detailSpecimenHandlingSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimenHandling = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimenHandling.asObservable();

    constructor(private processSampleDetailService: ProcessSampleDetailService) {
        super()
    }

    loadDetailSpecimenHandling(pasid: any){
        this.loadingDetailSpecimenHandling.next(true);

        this.processSampleDetailService.getSpecimenHandling(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimenHandling.next(false))
        )
        .subscribe(specimen => this.detailSpecimenHandlingSubject.next(specimen))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimenHandlingSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimenHandlingSubject.complete();
        this.loadingDetailSpecimenHandling.complete();
    }
}

export class FileDetailSourceSpecimenCollection extends DataSource<any> 
{
    private detailSpecimenCollectionSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimenCollection = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimenCollection.asObservable();

    constructor(private processSampleDetailService: ProcessSampleDetailService) {
        super()
    }

    loadDetailSpecimenCollection(pasid: any){
        this.loadingDetailSpecimenCollection.next(true);

        this.processSampleDetailService.getSpecimenCollection(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimenCollection.next(false))
        )
        .subscribe(specimen => this.detailSpecimenCollectionSubject.next(specimen))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimenCollectionSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimenCollectionSubject.complete();
        this.loadingDetailSpecimenCollection.complete();
    }
}

export class FileDetailSource extends DataSource<any> 
{
    private detailSpecimensSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimens = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimens.asObservable();

    constructor(private processSampleDetailService: ProcessSampleDetailService) {
        super()
    }

    loadDetail(regid: any){
        this.loadingDetailSpecimens.next(true);

        this.processSampleDetailService.getSpecimensDetailCond(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimens.next(false))
        )
        .subscribe(detspec => this.detailSpecimensSubject.next(detspec))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimensSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimensSubject.complete();
        this.loadingDetailSpecimens.complete();
    }
}



export class FileDetailSourceSch extends DataSource<any> 
{
    private detailSpecimensSubjectSch = new BehaviorSubject<any>([]);

    private loadingDetailSpecimensSch = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimensSch.asObservable();

    constructor(private processSampleDetailService: ProcessSampleDetailService) {
        super()
    }

    loadDetailSch(regid: any){
        this.loadingDetailSpecimensSch.next(true);

        this.processSampleDetailService.getSpecimensDetailSch(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimensSch.next(false))
        )
        .subscribe(detspec => this.detailSpecimensSubjectSch.next(detspec))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimensSubjectSch.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimensSubjectSch.complete();
        this.loadingDetailSpecimensSch.complete();
    }
}

// export class FileDetailSourceSample extends DataSource<any> 
// {
//     private detailAnalysisSubjectSample = new BehaviorSubject<any>([]);

//     private loadingDetailAnalysisSample = new BehaviorSubject<boolean>(false);

//     public loading$ = this.loadingDetailAnalysisSample.asObservable();

//     constructor(private processSampleDetailService: ProcessSampleDetailService) {
//         super()
//     }

//     loadDetailSample(regid: any){
//         this.loadingDetailAnalysisSample.next(true);

//         this.processSampleDetailService.getAnalysisDetailSample(regid).pipe(
//              catchError(() => of([])),
//              finalize(() => this.loadingDetailAnalysisSample.next(false))
//         )
//         .subscribe(detspec => this.detailAnalysisSubjectSample.next(detspec))
//     }

//     connect(collectionViewer: CollectionViewer): Observable<any>
//     {
//         console.log("Connecting data source");
//         return this.detailAnalysisSubjectSample.asObservable();   
//     }


//     disconnect(collectionViewer: CollectionViewer): void
//     {
//         this.detailAnalysisSubjectSample.complete();
//         this.loadingDetailAnalysisSample.complete();
//     }
// }
