import { Component, OnDestroy, ElementRef, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { ResultHandlingDetailService } from './result_handling_detail.service';
import { fuseAnimations } from '../../../../../core/animations';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar } from '@angular/material';
import { Location } from '@angular/common';
import { MatPaginator, MatSort,  MatTableDataSource  } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ActivatedRoute, Router } from '@angular/router';
import {of} from "rxjs/observable/of";
import {catchError, finalize} from "rxjs/operators";
import { Options } from 'selenium-webdriver';



@Component({
    selector     : 'result_handling_detail',
    templateUrl  : './result_handling_detail.component.html',
    styleUrls    : ['./result_handling_detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ResultHandlingDetailComponent implements OnInit
{
    result_handling_detail: {regid: any, age: any }
    regid: any;
    age: any;
    regdate: any;
    user: any;
    doctor: any;
    errorMessage: boolean;
    successMessage: boolean;
    tampungItemProcess: any = [];
    tampungSampleProcess: any = [];
    tampungSampleReceived: any = [];
    tampungResultAuth: any = [];
    createData : any = [];
    createRh : any = [];
    data: any;
    createSpec: any = [];
    drawReceive: FormGroup;
    patientNoteGroup: FormGroup;
    physicianNoteGroup: FormGroup;
    referralNoteGroup: FormGroup;
    labNoteGroup: FormGroup;
    statusTest: FormGroup;
    samples: FormGroup;
    dataSourceItem: FileDetailSourceItem;
    dataSourceRh: FileDetailRh;
    dataSourceLabNote: FileDetailSourceLabNote;
    dataSource: FileDetailSource;
    dataSourceSch : FileDetailSourceSch;
    drawer: string;
    urlResult = "http://202.146.232.86:8081/jasperserver/flow.html?_flowId=viewReportFlow&_flowId=viewReportFlow&ParentFolderUri=/reports/Pramita&reportUnit=/reports/Pramita/ResultLabIndKonv&standAlone=true&&j_username=joeuser&j_password=joeuser&RegNo=";
  
    displayedColumns = ['question', 'answer', 'note']
    displayedColumnsNoteLab = ['no', 'note', 'user', 'date'];
    displayedColumns2= ['schedule', 'test', 'day', 'time'];
    displayedColumnsItem = ['testname','cito', 'result','unit', 'resultComment', 'verification','verificationDate', 'verificationUser', 'resultINP','resultINPDate','resultINPUser','process','processDate', 'processUser', 'auth','authDate', 'authUser','schedule','scheduleDateTime','resultSI', 'formatSI','format','rawResult','testGroupName', 'instrumentName', 'reference', 'PRN', 'analysisResultReference'];
    displayedColumnsRh = ['doctor_id', 'n', 'action', 'to', 'format', 'auth_date', 'rh_id' , 'rh_nam', 'officer','customer_name', 'custom', 'received', 'submitted', 'doctor', 'submitted_date', 'submitted_user', 'received_date', 'received_user', 'officer_date', 'officer_user', 'customer_date', 'customer_user'];


    

    

    

    initNote() {
        return this.formBuilder.group({
            LabNote: [''],
            user: 'admin'
            // date: [''],
            // RegisterId: this.route.snapshot.params['regid']
        })
    }

    addNoteValue() {
        const control = <FormArray>this.labNoteGroup.controls['labNotes'];
        control.push(this.initNote())
    }
    
    deleteNoteValue(index) {
        if(index > 0){
            const control = <FormArray>this.labNoteGroup.controls['labNotes'];
            control.removeAt(index);
        }
    }

    

    constructor(private route: ActivatedRoute, private resultHandlingDetailService: ResultHandlingDetailService, private formBuilder: FormBuilder, private router: Router) {
        
        
        this.labNoteGroup = this.formBuilder.group({
            labNotes: this.formBuilder.array([this.initNote()])
        })

        
    }

    ngOnInit() {
       

        this.result_handling_detail = {
            regid: this.route.snapshot.params['regid'],
            age: this.route.snapshot.params['age']
        }
        this.regid = this.result_handling_detail['regid'];
        this.age = this.result_handling_detail['age'];

        this.resultHandlingDetailService.createResultHandlingDetail(this.regid).subscribe(create => {
            this.createData = create;
            
        })

        this.resultHandlingDetailService.getDoctor(this.regid).then(doc => this.doctor = doc)

        // const control4 = <FormArray>this.manyMsg.controls['msgArray'];
        // this.resultDetailService.getSample(this.pasid).then(msg => {
        //     this.detail4 = msg
        //     msg.forEach(b => {
        //         control.push(this.initStatusTest(b))
        //         console.log("init Status Text", b)
        //         this.tampungResultAuth.push({
        //             "id": b.id,
        //             "auth": b.auth
        //         })
        //     });
        // })

        // const control3 = <FormArray>this.manyRh.controls['rhArray'];
        // this.resultDetailService.getSample(this.pasid).then(rh => {
        //     this.detail4 = rh
        //     rh.forEach(b => {
        //         control.push(this.initStatusTest(b))
        //         console.log("init Status Text", b)
        //         this.tampungResultAuth.push({
        //             "id": b.id,
        //             "auth": b.auth
        //         })
        //     });
        // })

        // const control2 = <FormArray>this.statusTest.controls['statusTestArray'];
        // this.resultDetailService.getSample(this.pasid).then(statTest => {
        //     this.detail3 = statTest
        //     statTest.forEach(b => {
        //         control.push(this.initStatusTest(b))
        //         console.log("init Status Text", b)
        //         this.tampungResultAuth.push({
        //             "id": b.id,
        //             "auth": b.auth
        //         })
        //     });
        // })


        this.dataSourceItem = new FileDetailSourceItem(this.resultHandlingDetailService)

        this.dataSourceItem.loadDetailItem(this.regid)

        this.dataSourceRh = new FileDetailRh(this.resultHandlingDetailService)

        this.dataSourceRh.loadDetailRh(this.regid)

        this.dataSource = new FileDetailSource(this.resultHandlingDetailService);

        this.dataSource.loadDetail(this.regid)

        this.dataSourceSch = new FileDetailSourceSch(this.resultHandlingDetailService);

        this.dataSourceSch.loadDetailSch(this.regid)

        this.dataSourceLabNote = new FileDetailSourceLabNote(this.resultHandlingDetailService);

        this.dataSourceLabNote.loadDetailLabNote(this.regid)

        // this.dataSourceSample = new FileDetailSourceSample(this.processSampleDetailService);

        // this.dataSourceSample.loadDetailSample(this.regid)
        

    }

    // sendData() {
    //     console.log(this.drawReceive.value["drawReceiveArray"])
    //     console.log(this.regid);
    //     // console.log(this.dataSource['value'])
    // }
    
    clickAllOfficer(){
        this.dataSourceRh.allOfficer()
    }
    
    clickOfficer(event, index) {
        this.dataSourceRh.saveOfficer(event, index)
    }

    clickAllCustom(){
        this.dataSourceRh.allCustom()
    }
    
    clickCustom(event, index) {
        this.dataSourceRh.saveCustom(event, index)
    }

    clickAllRe(){
        this.dataSourceRh.allRe()
    }
    
    clickRe(event, index) {
        this.dataSourceRh.saveRe(event, index)
    }

    clickAllSub(){
        this.dataSourceRh.allSub()
    }
    
    clickSub(event, index) {
        this.dataSourceRh.saveSub(event, index)
    }

    clickAllVerify(){
        this.dataSourceItem.allVerify()
    }
    
    verify(event, index) {
        this.dataSourceItem.saveVerify(event, index)
    }

    comment(event, index) {
        this.dataSourceItem.saveComment(event, index)
    }

    result(event, index) {
        this.dataSourceItem.saveResult(event, index)
    }

    clickAllPrn() {
        this.dataSourceItem.allPrn()
    }

    prn(event, index) {
        this.dataSourceItem.savePrn(event, index)
    }

    clickAllAuth() {
        this.dataSourceItem.allAuth()
    }

    auth(event, index) {
        this.dataSourceItem.saveAuth(event, index)
    }

    submitRh(data) {
        this.createRh = {
            registerId : this.regid,
            patientNote : this.createData.patientNote,
            referalNote: this.createData.referalNote,
            doctornote : this.createData.doctornote,
            regNo : this.createData.regNo,
            regDate : this.createData.regDate,
            resultHandlingForm : this.dataSourceRh.loadRh(),
            resultHandlingItem : this.dataSourceItem.loadItem(),
            labNote : this.labNoteGroup.value['labNote']
        }
        console.log(this.createRh)
        data = this.createRh
        this.resultHandlingDetailService.postRh(data).subscribe(res => {
            if(res == 'Submited'){
                this.successMessage = true;
                this.router.navigate(['apps/result/result_handling_pasien'])
                setTimeout(function () {
                        this.successMessage = false;
                    
                    }.bind(this), 2000)
            }else{
                this.errorMessage = true;
                setTimeout(function (){
                    this.errorMessage = false;
                }.bind(this), 2000)
            }
        },error => {
            this.errorMessage = true;
            setTimeout(function (){
                this.errorMessage = false;
            }.bind(this), 2000)
        });
        console.log(data);
    }
    
    gotoResult(){
        window.open(this.urlResult + this.createData.regNo);
    }
}

export class FileDetailSourceItem extends DataSource<any> 
{
    private detailItemSubject = new BehaviorSubject<any>([]);

    private loadingDetailItem = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailItem.asObservable();

    public clickauth: boolean;

    public clickverify: boolean;

    public clickprn: boolean;

    constructor(private resultHandlingDetailService: ResultHandlingDetailService) {
        super()
    }

    allVerify() {
        if (this.clickverify) { this.clickverify = false; } else { this.clickverify = true; }
      for (var i = 0; i < this.detailItemSubject.value.length; i++) {
        this.detailItemSubject.value[i].verification = this.clickverify;
      }
      return this.detailItemSubject.value;
    }

    saveVerify(verify: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailItemSubject.value[index].verification = verify;
        return this.detailItemSubject.value;
    }

    saveComment(resComment: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailItemSubject.value[index].resultComment = resComment;
        return this.detailItemSubject.value;
    }

    saveResult(res: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailItemSubject.value[index].result = res;
        return this.detailItemSubject.value;
    }

    allPrn() {
        if (this.clickprn) { this.clickprn = false; } else { this.clickprn = true; }
      for (var i = 0; i < this.detailItemSubject.value.length; i++) {
        this.detailItemSubject.value[i].PRN = this.clickprn;
      }
      return this.detailItemSubject.value;
    }

    savePrn(prn: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailItemSubject.value[index].PRN = prn;
        return this.detailItemSubject.value;
    }

    allAuth() {
        if (this.clickauth) { this.clickauth = false; } else { this.clickauth = true; }
      for (var i = 0; i < this.detailItemSubject.value.length; i++) {
        this.detailItemSubject.value[i].auth = this.clickauth;
      }
      return this.detailItemSubject.value;
    }

    saveAuth(auth: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailItemSubject.value[index].auth = auth;
        return this.detailItemSubject.value;
    }

    loadItem(){
        return this.detailItemSubject.value;
    }

    loadDetailItem(regid: any){
        this.loadingDetailItem.next(true);

        this.resultHandlingDetailService.getItem(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailItem.next(false))
        )
        .subscribe(item => this.detailItemSubject.next(item))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailItemSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailItemSubject.complete();
        this.loadingDetailItem.complete();
    }
}


export class FileDetailSourceLabNote extends DataSource<any> 
{
    private detailLabNoteSubject = new BehaviorSubject<any>([]);

    private loadingDetailLabNote = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailLabNote.asObservable();

    constructor(private resultHandlingDetailService: ResultHandlingDetailService) {
        super()
    }

    loadDetailLabNote(regid: any){
        this.loadingDetailLabNote.next(true);

        this.resultHandlingDetailService.getLabNote(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailLabNote.next(false))
        )
        .subscribe(test => this.detailLabNoteSubject.next(test))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailLabNoteSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailLabNoteSubject.complete();
        this.loadingDetailLabNote.complete();
    }
}

export class FileDetailSource extends DataSource<any> 
{
    private detailSpecimensSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimens = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimens.asObservable();

    constructor(private resultHandlingDetailService: ResultHandlingDetailService) {
        super()
    }

    loadDetail(regid: any){
        this.loadingDetailSpecimens.next(true);

        this.resultHandlingDetailService.getSpecimensDetailCond(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimens.next(false))
        )
        .subscribe(detspec => this.detailSpecimensSubject.next(detspec))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimensSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimensSubject.complete();
        this.loadingDetailSpecimens.complete();
    }
}



export class FileDetailSourceSch extends DataSource<any> 
{
    private detailSpecimensSubjectSch = new BehaviorSubject<any>([]);

    private loadingDetailSpecimensSch = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimensSch.asObservable();

    constructor(private resultHandlingDetailService: ResultHandlingDetailService) {
        super()
    }

    loadDetailSch(regid: any){
        this.loadingDetailSpecimensSch.next(true);

        this.resultHandlingDetailService.getSpecimensDetailSch(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimensSch.next(false))
        )
        .subscribe(detspec => this.detailSpecimensSubjectSch.next(detspec))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimensSubjectSch.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimensSubjectSch.complete();
        this.loadingDetailSpecimensSch.complete();
    }
}

export class FileDetailRh extends DataSource<any> 
{
    private detailResultSubjectRh = new BehaviorSubject<any>([]);

    private loadingDetailResultRh = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailResultRh.asObservable();

    clickofficer: boolean;

    clickcustom: boolean;

    clickre: boolean;

    clicksub: boolean;

    constructor(private resultHandlingDetailService: ResultHandlingDetailService) {
        super()
    }

    allOfficer() {
        if (this.clickofficer) { this.clickofficer = false; } else { this.clickofficer = true; }
      for (var i = 0; i < this.detailResultSubjectRh.value.length; i++) {
        this.detailResultSubjectRh.value[i].officer = this.clickofficer;
      }
      return this.detailResultSubjectRh.value;
    }

    saveOfficer(officer: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailResultSubjectRh.value[index].officer = officer;
        return this.detailResultSubjectRh.value;
    }

    allCustom() {
        if (this.clickcustom) { this.clickcustom = false; } else { this.clickcustom = true; }
      for (var i = 0; i < this.detailResultSubjectRh.value.length; i++) {
        this.detailResultSubjectRh.value[i].custom = this.clickcustom;
      }
      return this.detailResultSubjectRh.value;
    }

    saveCustom(custom: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailResultSubjectRh.value[index].custom = custom;
        return this.detailResultSubjectRh.value;
    }

    allRe() {
        if (this.clickre) { this.clickre = false; } else { this.clickre = true; }
      for (var i = 0; i < this.detailResultSubjectRh.value.length; i++) {
        this.detailResultSubjectRh.value[i].received = this.clickre;
      }
      return this.detailResultSubjectRh.value;
    }

    saveRe(re: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailResultSubjectRh.value[index].received = re;
        return this.detailResultSubjectRh.value;
    }

    allSub() {
        if (this.clicksub) { this.clicksub = false; } else { this.clicksub = true; }
      for (var i = 0; i < this.detailResultSubjectRh.value.length; i++) {
        this.detailResultSubjectRh.value[i].submitted = this.clicksub;
      }
      return this.detailResultSubjectRh.value;
    }

    saveSub(sub: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailResultSubjectRh.value[index].submitted = sub;
        return this.detailResultSubjectRh.value;
    }

    loadRh(){
        return this.detailResultSubjectRh.value;
    }

    loadDetailRh(regid: any){
        this.loadingDetailResultRh.next(true);

        this.resultHandlingDetailService.getResultRh(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailResultRh.next(false))
        )
        .subscribe(resRh => this.detailResultSubjectRh.next(resRh))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailResultSubjectRh.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailResultSubjectRh.complete();
        this.loadingDetailResultRh.complete();
    }
}
