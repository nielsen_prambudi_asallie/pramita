import { Component, ElementRef, OnInit, ViewChild, AfterViewInit, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { MasterCompanyCreateService } from './mcompany_create.service';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import {ActivatedRoute} from "@angular/router";
import { fuseAnimations } from '../../../../../../core/animations';
import { MatPaginator, MatSort, PageEvent, MatTableDataSource } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import {debounceTime, map, distinctUntilChanged, switchMap, startWith, tap, delay} from 'rxjs/operators';
import {merge} from "rxjs/observable/merge";
import {fromEvent} from 'rxjs/observable/fromEvent';
import { FuseUtils } from '../../../../../../core/fuseUtils';
import { ErrorStateMatcher } from '@angular/material/core';
import {of} from "rxjs/observable/of";
import {catchError, finalize} from "rxjs/operators";
import { NgOption } from '@ng-select/ng-select';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  FormGroupDirective,
  NgForm,
  ValidatorFn,
  AbstractControl,
  Validators,
  NG_VALUE_ACCESSOR,
  FormArray
} from '@angular/forms';

@Component({
    selector   : 'mcompany_create',
    templateUrl: './mcompany_create.component.html',
    styleUrls  : ['./mcompany_create.component.scss'],
    animations : fuseAnimations
})
export class MasterCompanyCreateComponent implements OnInit, AfterViewInit
{
  companyForm: FormGroup;

  createCompany: any = [];

  companyNameCtrl = new FormControl();

  chooseCompSubs = new FormControl();
  filteredCompSubs: Observable<any[]>;
  compSubs:any;

  chooseCompType = new FormControl();
  filteredCompTypes: Observable<any[]>;
  compType:any;

  chooseMarketer = new FormControl();
  filteredMarks: Observable<any[]>;
  marketer:any;

  activeCtrl = new FormControl();
  active:boolean;

  typeaheadType = new EventEmitter<any>();
  companyAddType = [];
  hid_type = [];
  
  typeaheadCity = new EventEmitter<any>();
  companyCity = [];
  hid_city = [];
  
  typeaheadContactType = new EventEmitter<any>();
  companyContact = [];
  hid_contact = [];

  companyName: any;
  companySub: any;
  matcher: any;
  companyType: any;
  successMessage: boolean;
  errorMessage: boolean;
  

  
  constructor(
      private masterCompanyCreateService: MasterCompanyCreateService,
      private route : ActivatedRoute,
      private formBuilder: FormBuilder,
      private ref: ChangeDetectorRef
    )
  {
    this.constructAll()
  }

  constructAll(){
    this.activeCtrl.setValue(true);

    this.filteredCompSubs = this.chooseCompSubs.valueChanges
      .pipe(
        startWith(null),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(valref => {
          // console.log("valref: ", valref)
          return this.filterCompSub(valref || '')
        })
      );

      this.filteredCompTypes = this.chooseCompType.valueChanges
      .pipe(
        startWith(null),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(valref => {
          // console.log("valref: ", valref)
          return this.filterCompType(valref || '')
        })
      );

      this.filteredMarks = this.chooseMarketer.valueChanges
      .pipe(
        startWith(null),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(valref => {
          // console.log("valref: ", valref)
          return this.filterMarketer(valref || '')
        })
      );
  }

  ngOnInit()
  {
    this.hid_contact.push(false);
    this.hid_type.push(false);
    this.hid_city.push(false);
    this.resetCompany();
  }

  ngAfterViewInit() {

  }

  //Sub Group
  textChanged(x){
    this.filterCompSub(x)
  }

  displayCompSubs(compSubs): any {
    return compSubs ? compSubs.companySubgroupName : compSubs;
  }

  filterCompSub(compSubVal: string): Observable<any[]> {
    return this.masterCompanyCreateService.getCompSub(compSubVal)
    .pipe(
      map(response => response.filter(option => {
        return (option.companySubgroupName) != null ?
        option.companySubgroupName :
        option.companySubgroupId
      }))
    )
  }

   onSubGroupSelected(compSubs){
    if(compSubs !== null){
      console.log(compSubs.companySubgroupId);
      this.companyForm.patchValue({
        chooseCompSubs : compSubs.companySubgroupId
      })
    }
  }

  //Company Type
  displayCompType(compType): any {
    return compType ? compType.companyTypeName : compType;
  }

  filterCompType(compTypeVal: string): Observable<any[]> {
    return this.masterCompanyCreateService.getCompType(compTypeVal)
    .pipe(
      map(response => response.filter(option => {
        return (option.companyTypeName) != null ?
        option.companyTypeName :
        option.companyTypeId
      }))
    )
  }

  onCompTypeSelected(compType){
    if(compType !== null){
      console.log(compType.companyTypeId);
      this.companyForm.patchValue({
        chooseCompType : compType.companyTypeId
      })
    }
  }

  //Marketer
  displayMarketer(marketer): any {
    return marketer ? marketer.marketerName : marketer;
  }

  filterMarketer(marketerVal: string): Observable<any[]> {
    return this.masterCompanyCreateService.getMarketer(marketerVal)
    .pipe(
      map(response => response.filter(option => {
        return (option.marketerName) != null ?
        option.marketerName :
        option.marketerId
      }))
    )
  }

  onMarketerSelected(marketer){
    if(marketer !== null){
      console.log(marketer.marketerId);
      this.companyForm.patchValue({
        chooseMarketer : marketer.marketerId
      })
    }
  }

  //Company
  resetCompany(){
    this.companyNameCtrl.reset();
    this.chooseCompSubs.reset();
    this.chooseCompType.reset();
    this.chooseMarketer.reset();
    this.companyForm = this.formBuilder.group({
      companyName: this.companyNameCtrl,
      companySubs: this.chooseCompSubs,
      companyType: this.chooseCompType,
      marketer: this.chooseMarketer,
      companyAddress: this.formBuilder.array([this.initAddress(1)]),
      companyContact: this.formBuilder.array([this.initContact()])
    });
  }

  submitNewCompany(){
    this.createCompany = {
      companyName: this.companyNameCtrl.value,
      companyGroupId: this.chooseCompSubs.value.companyGroupId,
      companySubgroupId: this.chooseCompSubs.value.companySubgroupId,
      companyTypeId: this.chooseCompType.value.companyTypeId,
      marketerId: this.chooseMarketer.value.marketerId,
      active: this.activeCtrl.value,
      strCompanyAddress: this.companyForm.value['companyAddress'],
      strCompanyMessenger: this.companyForm.value['companyContact']
    }
    console.log(this.createCompany);
    this.masterCompanyCreateService.postNewCompany(this.createCompany)
      .subscribe(res => {
        if(res == 'true'){
            this.successMessage = true;
            setTimeout(function () {
                    this.successMessage = false;
                
                }.bind(this), 2000)
        }else{
            this.errorMessage = true;
            setTimeout(function (){
                this.errorMessage = false;
            }.bind(this), 2000)
        }
    },error => {
        this.errorMessage = true;
        setTimeout(function (){
            this.errorMessage = false;
        }.bind(this), 2000)
    });
    this.resetCompany();
  }

  //Address
  initAddress(addressIdVal): FormGroup {
    return this.formBuilder.group({
      addressId: [addressIdVal],
      addressLocation: [''],
      addressName: [''],
      addressType: [''],
      addressTypeId: [''],
      addressCity: [''],
      addressCityId: [''],
      addressPostCode: ['', [Validators.required, Validators.pattern]],
      addressPhone: [''],
      addressFax: ['']
    });
  }

  addAddressValue() {
    const control = <FormArray>this.companyForm.controls['companyAddress'];
    control.push(this.initAddress(this.companyForm.value['companyAddress'].length+1));
  }

  deleteAddressValue(index) {
    if(index > 0){
      const control = <FormArray>this.companyForm.controls['companyAddress'];
      control.removeAt(index);
    }
  }

  //Address Type
  onchangeTipe(event, index) {
    console.log('Tipe set', this.companyForm.controls.companyAddress['controls'][index]);
    let Tipe = this.companyForm.controls.companyAddress['controls'][index].controls["addressType"];
    let Tipeid = this.companyForm.controls.companyAddress['controls'][index].controls["addressTypeId"];
    Tipeid.setValue(event.addressTypeId);
    Tipe.setValue(event.addressType);
  }

  onfilledType(event) {
    this.typeaheadType
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.masterCompanyCreateService.getAddressType(data, ""))
      )
      .subscribe(Type => {
        console.log("Type: ", Type)
        this.companyAddType = Type;
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienType error : ', err);
        this.companyAddType = [];
        this.ref.markForCheck();
      })
  }

  inputType(event) {
    console.log('event inputtype : ', event);
    this.typeaheadType
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.masterCompanyCreateService.getAddressType(data, event.target.value))
      )
      .subscribe(Type => {
        console.log("Type: ", Type)
        this.companyAddType = Type;
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienType error : ', err);
        this.companyAddType = [];
        this.ref.markForCheck();
      })
  }

  hidtype(event, action, index) {
    console.log("action hidtype", action[index].value.addressType)
    this.hid_type[index] = true;
  }

  //Address City
  onfilledCity(event) {
    this.typeaheadCity
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.masterCompanyCreateService.getCity(data,""))
      )
      .subscribe(city => {
        console.log("city: ", city)
        this.companyCity = city;
        console.log("this.pasienCity : ", this.companyCity)
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienCity error : ', err);
        this.companyCity = [];
        this.ref.markForCheck();
      })
  }

  inputcity(event) {
    console.log('event inputcity : ', event);
    this.typeaheadCity
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.masterCompanyCreateService.getCity(data, event.target.value))
      )
      .subscribe(city => {
        console.log("city: ", city)
        this.companyCity = city;
        console.log("this.companyCity : ", this.companyCity )
        this.ref.markForCheck();
      }, (err) => {
        console.log('companyCity error : ', err);
        this.companyCity = [];
        this.ref.markForCheck();
      })
  }

  onchangeCity(event, index) {
    let city = this.companyForm.controls.companyAddress['controls'][index].controls["addressCity"];
    let cityid = this.companyForm.controls.companyAddress['controls'][index].controls["addressCityId"];
    cityid.setValue(event.cityId);
    city.setValue(event.cityName);
    console.log('city set',event);
  }

  hidCity(event, action, index) {
    console.log("action hidCity", action[index].value.addressCity)
    this.hid_city[index]=true;
  }

  //Contact
  initContact() : FormGroup {
    return this.formBuilder.group({
      messangerTypeId: [''],
      messangerType: [''],
      messangerName: ['']
    });
  }

  addContactValue() {
    const control = <FormArray>this.companyForm.controls['companyContact'];
    control.push(this.initContact())
  }

  deleteContactValue(index) {
    if(index > 0){
      const control = <FormArray>this.companyForm.controls['companyContact'];
      control.removeAt(index);
    }
  }

  //Messenger Type
  onfilledContactType(event) {
    this.typeaheadContactType
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.masterCompanyCreateService.getCompanyContactType(data, ""))
      )
      .subscribe(Type => {
        console.log("Type: ", Type)
        this.companyContact = Type;
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienType error : ', err);
        this.companyContact = [];
        this.ref.markForCheck();
      })
  }

  onchangeContactTipe(event, index) {
    console.log('ContactTipe set', event);
    let contactTipe = this.companyForm.controls.companyContact['controls'][index].controls["messangerType"];
    let contactTipeid = this.companyForm.controls.companyContact['controls'][index].controls["messangerTypeId"];
    contactTipeid.setValue(event.messengerTypeId);
    contactTipe.setValue(event.messangerType);
    console.log('contactTipeid', contactTipeid);
      console.log('contactTipe', contactTipe);
  }

  inputContactType(event) {
    console.log('event inputContactType : ', event);
    this.typeaheadContactType
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.masterCompanyCreateService.getCompanyContactType(data, event.target.value))
      )
      .subscribe(Type => {
        console.log("Type: ", Type)
        this.companyContact = Type;
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienType error : ', err);
        this.companyContact = [];
        this.ref.markForCheck();
      })
  }

  hidContacttype(event, action, index) {
    this.hid_contact[index] = true;
  }

}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

