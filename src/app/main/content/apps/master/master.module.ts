import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from "@angular/forms"
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { MasterAgreementComponent } from './magreement/magreement.component';
import { MasterAgreementService } from './magreement/magreement.service';
import { MasterAgreementCreateComponent } from './magreement/magreement_create/magreement_create.component';
import { MasterAgreementCreateService } from './magreement/magreement_create/magreement_create.service';
import { MasterCompanyComponent } from './mcompany/mcompany.component';
import { MasterCompanyService } from './mcompany/mcompany.service';
import { MasterCompanyCreateComponent } from './mcompany/mcompany_create/mcompany_create.component';
import { MasterCompanyCreateService } from './mcompany/mcompany_create/mcompany_create.service';
import { MasterPhysicianComponent } from './mphysician/mphysician.component';
import { MasterPhysicianService } from './mphysician/mphysician.service';
import { MasterPhysicianCreateComponent } from './mphysician/mphysician_create/mphysician_create.component';
import { MasterPhysicianCreateService } from './mphysician/mphysician_create/mphysician_create.service';
import { MasterPriceListComponent } from './mprice_list/mprice_list.component';
import { MasterPriceListService } from './mprice_list/mprice_list.service';
import { MasterPriceListCreateComponent } from './mprice_list/mprice_list_create/mprice_list_create.component';
import { MasterPriceListCreateService } from './mprice_list/mprice_list_create/mprice_list_create.service';
import { MasterRewardComponent } from './mreward/mreward.component';
import { MasterRewardService } from './mreward/mreward.service';
import { MasterRewardCreateComponent } from './mreward/mreward_create/mreward_create.component';
import { MasterRewardCreateService } from './mreward/mreward_create/mreward_create.service';
import { MasterScheduleComponent } from './mschedule/mschedule.component';
import { MasterScheduleService } from './mschedule/mschedule.service';
import { MasterScheduleCreateComponent } from './mschedule/mschedule_create/mschedule_create.component';
import { MasterScheduleCreateService } from './mschedule/mschedule_create/mschedule_create.service';
import { MasterScheduleResultComponent } from './mschedule_result/mschedule_result.component';
import { MasterScheduleResultService } from './mschedule_result/mschedule_result.service';
import { MasterScheduleResultCreateComponent } from './mschedule_result/mschedule_result_create/mschedule_result_create.component';
import { MasterScheduleResultCreateService } from './mschedule_result/mschedule_result_create/mschedule_result_create.service';
import { MasterScheduleTestComponent } from './mschedule_test/mschedule_test.component';
import { MasterScheduleTestService } from './mschedule_test/mschedule_test.service';
import { MasterScheduleTestCreateComponent } from './mschedule_test/mschedule_test_create/mschedule_test_create.component';
import { MasterScheduleTestCreateService } from './mschedule_test/mschedule_test_create/mschedule_test_create.service';
import { MasterTestReferenceComponent } from './mtest_reference/mtest_reference.component';
import { MasterTestReferenceService } from './mtest_reference/mtest_reference.service';
import { MasterTestReferenceCreateComponent } from './mtest_reference/mtest_reference_create/mtest_reference_create.component';
import { MasterTestReferenceCreateService } from './mtest_reference/mtest_reference_create/mtest_reference_create.service';
import { MasterTestComponent } from './mtest/mtest.component';
import { MasterTestService } from './mtest/mtest.service';
import { MasterTestCreateComponent } from './mtest/mtest_create/mtest_create.component';
import { MasterTestCreateService } from './mtest/mtest_create/mtest_create.service';
import { AgmCoreModule } from '@agm/core';
import { AccordionModule } from 'primeng/components/accordion/accordion';
import { NgSelectModule } from '@ng-select/ng-select';

const routes: Routes = [

    {
        path     : 'magreement',
        component: MasterAgreementComponent,
    },
    {
        path     : 'magreement/create',
        component: MasterAgreementCreateComponent,
    },
    {
      path     : 'magreement/:id/edit',
      component: MasterAgreementCreateComponent,
    },
    {
        path     : 'mcompany',
        component: MasterCompanyComponent,
    },
    {
      path     : 'mcompany/create',
      component: MasterCompanyCreateComponent,
    },
    {
      path     : 'mcompany/:id/edit',
      component: MasterCompanyCreateComponent,
    },
    {
        path     : 'mphysician',
        component: MasterPhysicianComponent,
    },
    {
      path     : 'mphysician/create',
      component: MasterPhysicianCreateComponent,
    },
    {
      path     : 'mphysician/:id/edit',
      component: MasterPhysicianCreateComponent,
    },
    {
      path     : 'mprice_list',
      component: MasterPriceListComponent,
    },
    {
      path     : 'mprice_list/create',
      component: MasterPriceListCreateComponent,
    },
    {
      path     : 'mprice_list/:id/edit',
      component: MasterPriceListCreateComponent,
    },
    {
      path     : 'mreward',
      component: MasterRewardComponent,
    },
    {
      path     : 'mreward/create',
      component: MasterRewardCreateComponent,
    },
    {
      path     : 'mreward/:id/edit',
      component: MasterRewardCreateComponent,
    },
    {
      path     : 'mschedule',
      component: MasterScheduleComponent,
    },
    {
      path     : 'mschedule/create',
      component: MasterScheduleCreateComponent,
    },
    {
      path     : 'mschedule/:id/edit',
      component: MasterScheduleCreateComponent,
    },
    {
      path     : 'mschedule_result',
      component: MasterScheduleResultComponent,
    },
    {
      path     : 'mschedule_result/create',
      component: MasterScheduleResultCreateComponent,
    },
    {
      path     : 'mschedule_result/:id/edit',
      component: MasterScheduleResultCreateComponent,
    },
    {
      path     : 'mschedule_test',
      component: MasterScheduleTestComponent,
    },
    {
      path     : 'mschedule_test/create',
      component: MasterScheduleTestCreateComponent,
    },
    {
      path     : 'mschedule_test/:id/edit',
      component: MasterScheduleTestCreateComponent,
    },
    {
      path     : 'mtest_reference',
      component: MasterTestReferenceComponent,
    },
    {
      path     : 'mtest_reference/create',
      component: MasterTestReferenceCreateComponent,
    },
    {
      path     : 'mtest_reference/:id/edit',
      component: MasterTestReferenceCreateComponent,
    },
    {
      path     : 'mtest',
      component: MasterTestComponent,
    },
    {
      path     : 'mtest/create',
      component: MasterTestCreateComponent,
    },
    {
      path     : 'mtest/:id/edit',
      component: MasterTestCreateComponent,
    }
];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        FormsModule,
        AccordionModule,
        NgSelectModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    declarations: [
      MasterAgreementComponent,
      MasterAgreementCreateComponent,
      MasterCompanyComponent,
      MasterCompanyCreateComponent,
      MasterPhysicianComponent,
      MasterPhysicianCreateComponent,
      MasterPriceListComponent,
      MasterPriceListCreateComponent,
      MasterRewardComponent,
      MasterRewardCreateComponent,
      MasterScheduleComponent,
      MasterScheduleCreateComponent,
      MasterScheduleResultComponent,
      MasterScheduleResultCreateComponent,
      MasterScheduleTestComponent,
      MasterScheduleTestCreateComponent,
      MasterTestReferenceComponent,
      MasterTestReferenceCreateComponent,
      MasterTestComponent,
      MasterTestCreateComponent

    ],
    providers   : [
      MasterAgreementService,
      MasterAgreementCreateService,
      MasterCompanyService,
      MasterCompanyCreateService,
      MasterPhysicianService,
      MasterPhysicianCreateService,
      MasterPriceListService,
      MasterPriceListCreateService,
      MasterRewardService,
      MasterRewardCreateService,
      MasterScheduleService,
      MasterScheduleCreateService,
      MasterScheduleResultService,
      MasterScheduleResultCreateService,
      MasterScheduleTestService,
      MasterScheduleTestCreateService,
      MasterTestReferenceService,
      MasterTestReferenceCreateService,
      MasterTestService,
      MasterTestCreateService
    ]
})
export class MasterModule
{
}
