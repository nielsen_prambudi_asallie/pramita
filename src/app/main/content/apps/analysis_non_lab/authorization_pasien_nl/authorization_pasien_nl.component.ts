import { Component, ElementRef, OnInit, ViewChild, AfterViewInit  } from '@angular/core';
import { AuthorizationPasienNonLabService } from './authorization_pasien_nl.service';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import {ActivatedRoute, Router} from "@angular/router";
import { fuseAnimations } from '../../../../../core/animations';
import { MatPaginator, MatSort, PageEvent, MatTableDataSource } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import {debounceTime, distinctUntilChanged, startWith, tap, delay} from 'rxjs/operators';
import {merge} from "rxjs/observable/merge";
import {fromEvent} from 'rxjs/observable/fromEvent';
import { FuseUtils } from '../../../../../core/fuseUtils';
import {of} from "rxjs/observable/of";
import {catchError, finalize} from "rxjs/operators";

@Component({
    selector   : 'authorization_pasien_nl',
    templateUrl: './authorization_pasien_nl.component.html',
    styleUrls  : ['./authorization_pasien_nl.component.scss'],
    animations : fuseAnimations
})
export class AuthorizationPasienNonLabComponent implements OnInit, AfterViewInit
{
    dataSourcePasien: FilesDataSourcePasien;
    displayedColumnsPasien = ['no', 'regno', 'patientno', 'patientname', 'patientgender', 'patientage', 'patientdob', 'patientresult', 'regdate', 'company', 'cito', 'status'];
    selectedRowIndex: any;
    pasien: {regid: any, age: any}
    regid: any;
    age: any;
    length: number;
    pageEvent: PageEvent;
    pageIndex: any = this.pageEvent;
    pageSize: any = this.pageEvent;
    pageSizeOptions = [10, 25, 100];


    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private authorizationPasienNonLabService: AuthorizationPasienNonLabService,
        private route : ActivatedRoute,
        private router : Router
    )
    {
    }



    ngOnInit()
    {
        this.pasien = {
            regid: this.route.snapshot.params["regid"],
            age: this.route.snapshot.params['age']
        }
        this.regid = this.pasien['regid'];
        this.age = this.pasien['age'];
        console.log(this.regid)

        this.dataSourcePasien = new FilesDataSourcePasien(this.authorizationPasienNonLabService);

        this.dataSourcePasien.loadPasien(this.length, 0, 10, '');
    }

    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        fromEvent(this.filter.nativeElement,'keyup')
            .pipe(
                debounceTime(150),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;

                    this.loadPasienPage();
                })
            )
            .subscribe();

        // merge(this.sort.sortChange, this.paginator.page)
        this.paginator.page
        .pipe(
            tap(() => this.loadPasienPage())
        )
        .subscribe();

    }

    loadPasienPage() {
        this.dataSourcePasien.loadPasien(
            this.length,
            this.paginator.pageIndex,
            this.paginator.pageSize,
            this.filter.nativeElement.value);
    }

    onRowClicked(regid, age) {
        if (age !== null && age !== '') { 
            age = age
        } else {
            age = 0
        } 
      // this.router.navigate(['/apps/analysis/authorization_pasien/082EFAB8-0CF9-45E8-85FE-DD5D6A113658/650691C4-394A-4763-A1F6-68CB602BFD15'])
      this.router.navigate(['/apps/analysis_non_lab/authorization_pasien_nl/' + regid + '/' + age])
    }

}

export class FilesDataSourcePasien extends DataSource<any>
{
    private pasienSubject = new BehaviorSubject<any>([]);
    public totalData : any;

    private loadingPasien = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingPasien.asObservable();

    constructor(
        private authorizationPasienNonLabService: AuthorizationPasienNonLabService,
    )
    {
        super()

    }

    loadPasien(
        length: number,
        pageIndex:number,
        pageSize:number,
        filter:string) {

        this.loadingPasien.next(true);

        this.authorizationPasienNonLabService.getAuthPasien(pageIndex, pageSize, filter).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingPasien.next(false))
            )
            .subscribe(pasien => {
               this.pasienSubject.next(pasien.data)
               this.totalData = pasien.totalData
            });



        }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.pasienSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.pasienSubject.complete();
        this.loadingPasien.complete();
    }
}
