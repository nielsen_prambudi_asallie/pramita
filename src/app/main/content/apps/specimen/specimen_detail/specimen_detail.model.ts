import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatChipInputEvent } from '@angular/material';

export class Product
{
    regid: string;
    regno    : string;
    patientid: string;
    name: string;
    handle: string;
    gender          : string;
    age             : string;
    dob             : string;
    result          : string;
    regdate         : string;
    hs              : string;
    company         : string;
    cito            : string;
    status          : string;
    

    constructor(product?)
    {
        product = product || {};
        this.regid = product.regid || '';
        this.regno = product.regno || '';
        this.name = product.name || '';
        this.gender = product.gender || '';
        this.age    = product.age || '';
        this.dob    = product.dob || '';
        this.result = product.result || '';
        this.regdate = product.regdate || '';
        this.hs = product.hs || '';
        this.company = product.company || '';
        this.cito   = product.cito || '';
        this.status = product.status || '';

        
    }

    // addCategory(event: MatChipInputEvent): void
    // {
    //     const input = event.input;
    //     const value = event.value;

    //     // Add category
    //     if ( value )
    //     {
    //         this.categories.push(value);
    //     }

    //     // Reset the input value
    //     if ( input )
    //     {
    //         input.value = '';
    //     }
    // }

    // removeCategory(category)
    // {
    //     const index = this.categories.indexOf(category);

    //     if ( index >= 0 )
    //     {
    //         this.categories.splice(index, 1);
    //     }
    // }

    // addTag(event: MatChipInputEvent): void
    // {
    //     const input = event.input;
    //     const value = event.value;

    //     // Add tag
    //     if ( value )
    //     {
    //         this.tags.push(value);
    //     }

    //     // Reset the input value
    //     if ( input )
    //     {
    //         input.value = '';
    //     }
    // }

    // removeTag(tag)
    // {
    //     const index = this.tags.indexOf(tag);

    //     if ( index >= 0 )
    //     {
    //         this.tags.splice(index, 1);
    //     }
    // }
}
