import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpModule, Http, Headers} from '@angular/http';
import { HttpParams } from '@angular/common/http/src/params';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PARAMETERS } from '@angular/core/src/util/decorators';
import { map, catchError} from "rxjs/operators";
import { of } from 'rxjs/observable/of';

const SERVER_URL = 'https://202.146.232.86:4000'

@Injectable()
export class MasterPhysicianCreateService
{
    urlLink: string;

    private _options ={ headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text' as 'text' };
    constructor(private http:HttpClient) {

    }

    getAddressType(searchType: any, searchTypeserver: any): Observable<any[]> {
        return this.http.get<any>(SERVER_URL + '/Api/AddressType/' + searchTypeserver).pipe(
        catchError(() => of(({}))),
        map(res => res)
      )
    }
    
    getCity(searchCity: any, searchCityserver: any): Observable < any[] > {
        return this.http.get<any>(SERVER_URL + '/Api/City/' + searchCityserver).pipe(
          catchError(() => of(({}))),
          map(res => res)
        )
    }

    getContactType(searchType: any, searchTypeserver: any): Observable<any[]> {
        return this.http.get<any>(SERVER_URL + '/Api/contacttype/' + searchTypeserver).pipe(
          catchError(() => of(({}))),
          map(res => res)
        )
    }

    getSpecialities(searchSpecialities: any): Observable<any[]> {
        this.urlLink = SERVER_URL + '/Api/Doctor/Specialty/' + searchSpecialities ;
        console.log(this.urlLink);
        return this.http.get<any>(this.urlLink);
    }

    getSex(searchSex: any): Observable<any[]> {
        this.urlLink = SERVER_URL + '/Api/sex/' + searchSex ;
        console.log(this.urlLink);
        return this.http.get<any>(this.urlLink);
    }

    getMarketer(searchMarketer: any): Observable<any[]> {
        this.urlLink = SERVER_URL + '/Api/Marketer/' + searchMarketer ;
        console.log(this.urlLink);
        return this.http.get<any>(this.urlLink);
    }
    
    postNewDoctor(doctorData) {
        let body = JSON.stringify(
            doctorData
        );
        return this.http.post(SERVER_URL + '/Api/Dokter/submit', body, this._options)
    }
}
