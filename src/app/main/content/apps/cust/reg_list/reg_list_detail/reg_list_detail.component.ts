import { Component, OnDestroy, ElementRef, OnInit, ViewEncapsulation, ViewChild, EventEmitter, ChangeDetectorRef,
    ChangeDetectionStrategy } from '@angular/core';
import { RegistrationListDetailService } from './reg_list_detail.service';
import { fuseAnimations } from '../../../../../../core/animations';
import {CollectionViewer, DataSource } from '@angular/cdk/collections';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Product } from './reg_list_detail.model';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { FuseUtils } from '../../../../../../core/fuseUtils';
import { MatSnackBar } from '@angular/material';
import { Location } from '@angular/common';
import { MatPaginator, MatSort,  MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ActivatedRoute, Router } from '@angular/router';
import {of} from "rxjs/observable/of";
import {catchError, finalize, map, tap, startWith, switchMap, debounceTime, distinctUntilChanged, takeWhile,
    first} from "rxjs/operators";
import { MycurrencyPipe } from '../../pipe/mycurrency.pipe';

@Component({
    selector     : 'reg_list_detail',
    templateUrl  : './reg_list_detail.component.html',
    styleUrls    : ['./reg_list_detail.component.scss'],
    providers    : [ MycurrencyPipe ],  
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class RegistrationListDetailComponent implements OnInit
{
    registration_detail: {regid: number, regno: number}
    regid: any;
    regno: any;
    create: any;
    dataSourceAddress: FileDetailSourceAddress;
    dataSourceContact: FileDetailSourceContact;
    dataSourcePayment: FileDetailSourcePayment;
    dataSourceTest: FileDetailSourceTest;
    displayedColumnsAddress= ['addressloc', 'addressname', 'addresstype' ,'addresscity', 'postcode', 'phone', 'fax'];
    displayedColumnsContact = ['messangerType', 'messangerName', ];
    displayedColumnsTest= ['testname', 'testcategory'];
    displayedColumnsPayment= ['paymentto', 'paymentcard', 'paymentcardno', 'paymentapprovalcode', 'paymenttracecode', 'nominal', 'paidamount'];
    payment: FormGroup;
    totalPayment: number;
    typeaheadCard = new EventEmitter<any>();
    typeaheadBank = new EventEmitter<any>();
    TemppaymentTotal: any;
    paymentTotal: any;
    paymentSisa: any;
    paymentSisaTanggungan: any;
    paymentKelebihanBayar: any;
    paymentReg: any;
    createRegPayment: any = [];
    payBanksTemps = [];
    payBanks = [];
    payCards = [];
    data: any;
    summaryTest: any;
    selectedFile3: any;

    savelebih: any;
    savesisa: any;
    savenomor_kartu: string;
    savekode_approve: string;
    savenomor_mesin: string;
    savetrace_code: string;

    constructor(
      private route: ActivatedRoute,
      private registrationListDetailService: RegistrationListDetailService,
      private cpipe: MycurrencyPipe,
      private formBuilder: FormBuilder,
      private router: Router,
      private ref: ChangeDetectorRef) {}

      initPaymentTo() {
        return this.formBuilder.group({
          payToId: [''],
          payCardId: [''],
          paymentCardNO: [''],
          paymentApprovalCode: [''],
          paymentTraceCode: [''],
          paidAmount: [''],
          paymentNominal: ['']
        })
      }

    ngOnInit() {
        
      this.payment = this.formBuilder.group({
          registerId: [''],
          registerNo: [''],
          paymentViewModel: this.formBuilder.array([this.initPaymentTo()]),
          paymentNominal: [''],
          paymentSisaTanggungan: [''],
          paymentKelebihanBayar: [''],
          paymentTotal: [null],
          paymentNote: ['']
        });

        

        this.registration_detail = {
            regid: this.route.snapshot.params['regid'],
            regno: this.route.snapshot.params['regno']
        }
        // console.log(this.specimen_detail['regid'])
        this.regid = this.registration_detail['regid'];
        this.regno = this.registration_detail['regno'];

        this.dataSourceAddress = new FileDetailSourceAddress(this.registrationListDetailService);

        this.dataSourceAddress.loadDetailAddress(this.regid);

        this.dataSourceContact = new FileDetailSourceContact(this.registrationListDetailService);

        this.dataSourceContact.loadDetailContact(this.regid)

        this.dataSourcePayment = new FileDetailSourcePayment(this.registrationListDetailService);

        this.dataSourcePayment.loadDetailPayment(this.regid)

        this.dataSourceTest = new FileDetailSourceTest(this.registrationListDetailService);

        this.dataSourceTest.loadDetailTest(this.regid)

        this.registrationListDetailService.getRegListDetailPayment(this.regid).then(pay => {
          this.paymentReg = pay;
          this.payment.patchValue({
            registerId: pay.registerId,
            registerNo: pay.registerNo,
            paymentNominal: pay.paymentNominal,
            paymentSisaTanggungan: pay.paymentSisaTanggungan,
            paymentNote: pay.paymentNote,
            paymentTotal: pay.paymentTotal
          })
           this.TemppaymentTotal= pay.paymentTotal;
           let paymentawal = this.payment.controls.paymentViewModel['controls'][0].get('paidAmount')
           paymentawal.setValue(this.TemppaymentTotal);
           let nominalawal = this.payment.controls.paymentViewModel['controls'][0].get('paymentNominal')
           nominalawal.setValue(this.TemppaymentTotal);
        })
        console.log("payment", this.payment.value.paymentTotal)
        this.payment.get('paymentViewModel').valueChanges.subscribe(values => {
          this.totalPayment = 0;
          const totPay = <FormArray>this.payment.controls['paymentViewModel'];
          totPay.controls.forEach(x => {
            if (x.get('paymentNominal').value > 0) {
              let parsed = parseInt(x.get('paymentNominal').value)
              this.totalPayment += parsed
              this.ref.detectChanges
            }

          })
        });       
       
    }


    onBankSelected(event){
      console.log("event: ", event)
    }

    addPaymentValue() {
      const control = <FormArray>this.payment.controls['paymentViewModel'];
      control.push(this.initPaymentTo())

      let paymentawal = this.payment.controls.paymentViewModel['controls'][this.payment.controls.paymentViewModel['controls'].length - 1].get('paidAmount')
      paymentawal.setValue(this.payment.controls['paymentSisaTanggungan'].value);
      let paymentNominala = this.payment.controls.paymentViewModel['controls'][this.payment.controls.paymentViewModel['controls'].length - 1].get('paymentNominal')
      paymentNominala.setValue(this.payment.controls['paymentSisaTanggungan'].value);
      this.totalPayment = 0;
      const totPay = <FormArray>this.payment.controls['paymentViewModel'];
      totPay.controls.forEach(x => {
        if (x.get('paymentNominal').value > 0) {
          let parsed = parseInt(x.get('paymentNominal').value)
          this.totalPayment += parsed
          this.ref.detectChanges
        }

      })
      console.log("this.totalPayment", this.totalPayment)
      this.payment.patchValue({
        paymentNominal: this.payment.controls['paymentNominal'].value,
        paymentSisaTanggungan: ((this.payment.controls['paymentTotal'].value - this.totalPayment) < 0 ? 0 : (this.payment.controls['paymentTotal'].value - this.totalPayment)),
        paymentTotal: this.payment.controls['paymentTotal'].value
      })
      if (this.totalPayment - this.payment.controls['paymentTotal'].value > 0) {
        this.payment.patchValue({
          paymentKelebihanBayar: ((this.totalPayment - this.payment.controls['paymentTotal'].value) < 0 ? 0 : (this.totalPayment - this.payment.controls['paymentTotal'].value))
        })
      } else {
        this.payment.patchValue({
          paymentKelebihanBayar: 0
        })
      }
    }
  
    deletePaymentValue(index) {
      if(index > 0){
        const control = <FormArray>this.payment.controls['paymentViewModel'];
        control.removeAt(index);
        this.totalPayment = 0;
        const totPay = <FormArray>this.payment.controls['paymentViewModel'];
        totPay.controls.forEach(x => {
            if (x.get('paymentNominal').value > 0) {
            let parsed = parseInt(x.get('paymentNominal').value)
            this.totalPayment += parsed
            this.ref.detectChanges
            }

        })
        console.log("this.totalPayment", this.totalPayment)
        this.payment.patchValue({
            paymentNominal: this.payment.controls['paymentNominal'].value,
            paymentSisaTanggungan: ((this.payment.controls['paymentTotal'].value - this.totalPayment) < 0 ? 0 : (this.payment.controls['paymentTotal'].value - this.totalPayment)),
            paymentTotal: this.payment.controls['paymentTotal'].value
        })
        if (this.totalPayment - this.payment.controls['paymentTotal'].value > 0) {
            this.payment.patchValue({
            paymentKelebihanBayar: ((this.totalPayment - this.payment.controls['paymentTotal'].value) < 0 ? 0 : (this.totalPayment - this.payment.controls['paymentTotal'].value))
            })
        } else {
            this.payment.patchValue({
            paymentKelebihanBayar: 0
            })
        }
      }
    }

    onfilledBank(event) {
        this.typeaheadBank
        .pipe(
          startWith(''),
          debounceTime(200),
          switchMap(data => this.registrationListDetailService.getPaymentBank(data))
        )
        .subscribe(paybanks => {
          console.log("paybanks12312321: ", event)
          this.payBanks = paybanks;
          this.ref.markForCheck();
        }, (err) => {
          console.log('payBank error : ', err);
          this.payBanks = [];
          this.ref.markForCheck();
        })
    
    }

    onfilledCard(event) {
        this.typeaheadCard
          .pipe(
          startWith(''),
          debounceTime(200),
          switchMap(data => this.registrationListDetailService.getPaymentCard(data))
          )
          .subscribe(paycards => {
            console.log("paycards: ", paycards)
            this.payCards = paycards;
            this.ref.markForCheck();
          }, (err) => {
            console.log('payCard error : ', err);
            this.payBanks = [];
            this.ref.markForCheck();
          })
    
    }

    onChangeBanks(index,event){
      this.payBanksTemps[index] = event.payToId
        console.log('event bank ', this.payBanksTemps[index]);
        let paySelected = this.payBanksTemps[index];
    
    
        if (paySelected == "01" ) {
          // this.paymentHidden = !this.paymentHidden
          console.log("this.payment.controls['paymentViewModel'][index]", this.payment.controls.paymentViewModel['controls'][index].get('paymentBankID'));
    
          let paymentCardDisabled = this.payment.controls.paymentViewModel['controls'][index].get('payCardId')
          let paymentCardNo = this.payment.controls.paymentViewModel['controls'][index].get('paymentCardNO')
          let paymentAppCode = this.payment.controls.paymentViewModel['controls'][index].get('paymentApprovalCode')
          let paymentTraceCode = this.payment.controls.paymentViewModel['controls'][index].get('paymentTraceCode')
         
          paymentCardDisabled.setValue("");
          paymentCardNo.setValue("");
          paymentAppCode.setValue("");
          paymentTraceCode.setValue("");
        //   paymentCardNo.disable()
        //   paymentAppCode.disable()
        //   paymentTraceCode.disable()
    
    
          //})
          return;
        } else {
          let paymentCardDisabled = this.payment.controls.paymentViewModel['controls'][index].get('payCardId')
          let paymentCardNo = this.payment.controls.paymentViewModel['controls'][index].get('paymentCardNO')
          let paymentAppCode = this.payment.controls.paymentViewModel['controls'][index].get('paymentApprovalCode')
          let paymentTraceCode = this.payment.controls.paymentViewModel['controls'][index].get('paymentTraceCode')
          paymentCardNo.setValue("");
          paymentAppCode.setValue("");
          paymentTraceCode.setValue("");
        //   paymentCardNo.enable()
        //   paymentAppCode.enable()
        //   paymentTraceCode.enable()
    
          return;
        }
    }

    paymentcek(index, value, event) {
        let total = this.payment.controls['paymentTotal'].value
        console.log("total", this.payment)
        console.log("this.totalPayment", this.totalPayment)
        if ( value > total) {
          alert("Nominal tidak boleh lebih dari total dan nominal yang diinputkan")
          //lebih = this.totalPayment;
          var element = <HTMLInputElement>document.getElementById("finish");
          element.disabled = true;
          return;
        } 
      }

    calculatePayment(index,value,event){
        console.log("this.payBanks[index];: ", this.payBanksTemps[index])
        console.log("paymentBankID: ", this.payBanksTemps)
        var element = <HTMLInputElement>document.getElementById("finish");
        let TotTrans = <HTMLInputElement>document.getElementById("paymentTotal");
        let SisaTrans = <HTMLInputElement>document.getElementById("paymentSisaTanggungan");
        let total = this.payment.controls['paymentTotal'].value
        let kelebihan = this.payment.controls['paymentKelebihanBayar'].value
        let nominal = this.payment.controls['paymentNominal'].value
        let sisa = this.payment.controls['paymentSisaTanggungan'].value
        let lebih = this.payment.controls['paymentKelebihanBayar'].value
        const calculate = <FormArray>this.payment.controls['paymentViewModel'];
        calculate.controls.forEach(y => {
            let paymentNom = y.get('paidAmount')
            let paymentTot = y.get('paymentNominal')
        })

        this.payment.patchValue({
            paymentSisaTanggungan: ((total - this.totalPayment) < 0 ? 0 : (total - this.totalPayment)),
        })
        console.log("totalPayment: ", this.totalPayment)
        console.log("this.payBanksTemps[index].payBankId: ", this.payBanksTemps[index])
        let paySelected = this.payBanksTemps[index];
        if (paySelected == "01" ) {
            this.payment.patchValue({
              //paymentSisaTanggungan: ((total - this.totalPayment) < 0 ? 0 : (total - this.totalPayment)),
              //paymentSisaTanggungan: (total - value),
              paymentKelebihanBayar: ((this.totalPayment - total) < 0 ? 0 : (this.totalPayment - total))
            })
            console.log("ini 1")
            return;
          } else {
            
            if (this.totalPayment > total) {
              alert("Nominal tidak boleh lebih dari total")
              //lebih = this.totalPayment;
              console.log("ini bukan")
              var element = <HTMLInputElement>document.getElementById("finish");
              element.disabled = true;
              return;
            } else {
              console.log("ini bukan kurang")
            }
        }
        this.payment.patchValue({
      
            paymentNominal: this.payment.controls['paymentNominal'].value,
            paymentKelebihanBayar: this.payment.controls['paymentKelebihanBayar'].value,
            paymentSisaTanggungan: this.payment.controls['paymentSisaTanggungan'].value,
            paymentTotal: this.payment.controls['paymentTotal'].value
        })
      }

      submitRegPayment(data) {
        this.createRegPayment = {
          registerId: this.payment.value['registerId'],
          registerNo: this.payment.value['registerNo'],
          payment: this.payment.value['paymentViewModel'],
          paymentNominal :this.payment.value['paymentNominal'],
          paymentSisaTanggungan : this.payment.value['paymentSisaTanggungan'],
          paymentTotal : this.payment.value['paymentTotal'],
          paymentKelebihanBayar : this.payment.value['paymentKelebihanBayar'],
          paymentNote : this.payment.value['paymentNote']
        }
        console.log("createRegPayment: ", this.createRegPayment)
        data = this.createRegPayment
        this.registrationListDetailService.postPayment(data)
        alert('Data payment berhasil ditambahkan');
        this.router.navigate(['/apps/cust/reg_list']) ;
    }
}

export class FileDetailSourceAddress extends DataSource<any>
{
    private detailRegisterAddressSubject = new BehaviorSubject<any>([]);

    private loadingDetailRegisterAddress = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailRegisterAddress.asObservable();

    constructor(private registrationListDetailService: RegistrationListDetailService) {
        super()
    }

    loadDetailAddress(regid: any){
        this.loadingDetailRegisterAddress.next(true);

        this.registrationListDetailService.getRegListDetail(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailRegisterAddress.next(false))
        )
        .subscribe(detAdd => this.detailRegisterAddressSubject.next(detAdd.pasienAddress))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailRegisterAddressSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailRegisterAddressSubject.complete();
        this.loadingDetailRegisterAddress.complete();
    }
}

export class FileDetailSourceContact extends DataSource<any>
{
    private detailRegisterCont = new BehaviorSubject<any>([]);

    private loadingDetailRegCont = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailRegCont.asObservable();

    constructor(private registrationListDetailService: RegistrationListDetailService) {
        super()
    }

    loadDetailContact(regid: any){
        this.loadingDetailRegCont.next(true);

        this.registrationListDetailService.getRegListDetail(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailRegCont.next(false))
        )
          .subscribe(detCond => this.detailRegisterCont.next(detCond.patientContact))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailRegisterCont.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailRegisterCont.complete();
        this.loadingDetailRegCont.complete();
    }
}

export class FileDetailSourcePayment extends DataSource<any>
{
    private detailRegisterPay = new BehaviorSubject<any>([]);

    private loadingDetailRegPay = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailRegPay.asObservable();

    constructor(private registrationListDetailService: RegistrationListDetailService) {
        super()
    }

    loadDetailPayment(regid: any){
        this.loadingDetailRegPay.next(true);

        this.registrationListDetailService.getRegListDetail(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailRegPay.next(false))
        )
        .subscribe(detPay => {
          this.detailRegisterPay.next(detPay.payment)
          
        })
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailRegisterPay.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailRegisterPay.complete();
        this.loadingDetailRegPay.complete();
    }
}

export class FileDetailSourceTest extends DataSource<any>
{
    private detailRegisterTest = new BehaviorSubject<any>([]);

    private loadingDetailRegTest = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailRegTest.asObservable();

    constructor(private registrationListDetailService: RegistrationListDetailService) {
        super()
    }

    loadDetailTest(regid: any){
        this.loadingDetailRegTest.next(true);

        this.registrationListDetailService.getRegListDetail(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailRegTest.next(false))
        )
        .subscribe(detTest => this.detailRegisterTest.next(detTest.testPayment))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailRegisterTest.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailRegisterTest.complete();
        this.loadingDetailRegTest.complete();
    }
}

