import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  Observable
} from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import { Tes, Condition, Schedule, Price } from './tes';
import { RequestOptions } from '@angular/http/src/base_request_options';
import { HttpParams } from '@angular/common/http/src/params';
import { URLSearchParams } from '@angular/http/src/url_search_params';
import { Router, NavigationCancel } from '@angular/router';
import {HttpModule, Http, Headers} from '@angular/http';
import { HttpHeaders } from '@angular/common/http';
import {catchError, map, debounceTime, switchMap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';

const SERVER_URL = 'https://202.146.232.86:4000'


@Injectable()
export class TesService {

    urlLink: string;
    urlLinkAdd: string;
    param: string;
    searchParam: any;
    searchRef: any;
    register: any;

    private _options ={ headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text' as 'text'  };
    constructor(private http: HttpClient) {
    }

    getTest() {
      console.log("masuk sini");
    return this.http.get<any>('assets/registrasi/tess.json')
      .toPromise()
      .then(res => <Tes[]>res.data)
      .then(data => data);
    }

    getTreeTest(idTes, idPrice) {
      // idTes = '53020000,83840000';
      // idPrice = '0570';
      return this.http.get<any>(SERVER_URL + '/Api/test/'+ idTes +'/payments/'+ idPrice)
        .toPromise()
        .then(res => res);
    }

    // getPaymentBy(searchPayBy : any) {
    //   return this.http.get<any>(SERVER_URL + '/Api/PayBy/'+ searchPayBy)
    //   .toPromise()
    //   .then(res => res)
    // }

    //getPaymentBy(searchPayBy : any): Observable<any[]> {
    //  return this.http.get<any>(SERVER_URL + '/Api/PayBy/'+ searchPayBy).pipe(
    //    catchError(() => of(({}))),
    //    map(res => res)
    //  )

    //}
    getPasienContactType(searchType: any, searchTypeserver: any): Observable<any[]> {
      return this.http.get<any>(SERVER_URL + '/Api/contacttype/' + searchTypeserver).pipe(
        catchError(() => of(({}))),
        map(res => res)
      )

    }
    getPasienType(searchType: any, searchTypeserver: any): Observable<any[]> {
      return this.http.get<any>(SERVER_URL + '/Api/AddressType/' + searchTypeserver).pipe(
        catchError(() => of(({}))),
        map(res => res)
      )

    }
    getPasienCity(searchCity: any, searchCityserver: any): Observable < any[] > {
      return this.http.get<any>(SERVER_URL + '/Api/City/' + searchCityserver).pipe(
        catchError(() => of(({}))),
        map(res => res)
      )

    }
    getPaymentBank(searchPayBank : any): Observable<any[]> {
      return this.http.get<any>(SERVER_URL + '/Api/payto/'+ searchPayBank).pipe(
        catchError(() => of(({}))),
        map(res => res)
      )

    }

    getPaymentCard(searchPayCard : any): Observable<any[]> {
      return this.http.get<any>(SERVER_URL + '/Api/PayCard/'+ searchPayCard).pipe(
        catchError(() => of(({}))),
        map(res => res)
      )

    }

    // getTreeTest() {
    //   return this.http.get<any>('assets/registrasi/tess.json')
    //     .toPromise()
    //     .then(res => res);
    // }

    regCreate() {
      return this.http.get(SERVER_URL + '/Api/Register/create')
    }



    getRefs(searchRef: any): Observable<any[]> {
      // console.log(this.urlLink, {params: {searchRef}})
      // searchRef = 'umum'
      this.urlLink = SERVER_URL + '/Api/referee/' + searchRef ;
      // console.log(searchRef)
      console.log(this.urlLink)
      return this.http.get<any>(this.urlLink)
    }

    getAgrs(idRef: any, searchAgr: any): Observable<any[]> {
      // idRef = '03100001';
      // searchAgr = 'surabaya';
      this.urlLink = SERVER_URL + '/Api/referee/';
      this.urlLinkAdd = '/agreement/';
      console.log(this.urlLink + idRef + this.urlLinkAdd + searchAgr);
      return this.http.get<any>(this.urlLink + idRef + this.urlLinkAdd + searchAgr)
    }

    getTestList(idPrice: any, searchParam: any, test: any){
    // getTestList(){
    //   return this.http.get<any>('assets/registrasi/tes.json')
    //   .toPromise()
    //   .then(res => <Tes[]>res.TestGet)
    //   .then(TestGet => TestGet);
      // idPrice = '0570';
      // searchParam = 'a';
      test = ''
      this.urlLink = SERVER_URL + '/Api/Agreement/' + idPrice + '/test/' + searchParam + '/' + test;
      console.log(this.urlLink);
      return this.http.get<any>(this.urlLink)
      .toPromise()
      .then(res => res);
    }

    getTestFilter(idPrice: any, searchParam: any, test: any ): Observable<any[]> {
      // getTestList(){
      //   return this.http.get<any>('assets/registrasi/tes.json')
      //   .toPromise()
      //   .then(res => <Tes[]>res.TestGet)
      //   .then(TestGet => TestGet);
      // idPrice = '0570';
      // searchParam = 'a';
      this.urlLink = SERVER_URL + '/Api/Agreement/' + idPrice + '/test/' + searchParam + '/' + test;
      console.log(this.urlLink);
      return this.http.get<any>(this.urlLink)
    }

    getCondition(idtes: any){
      // return this.http.get<any>('assets/registrasi/condition.json')
      // .toPromise()
      // .then(res => <Condition[]>res.ConditionGet)
      // .then(ConditionGet => ConditionGet);
      //idtes = '12080500,12340000';//sengaja diganti gini biar muncul data sesuai API
      this.urlLink = SERVER_URL + '/Api/test/'+ idtes +'/question';
      console.log(this.urlLink);
      return this.http.get<any>(this.urlLink)
      .toPromise()
      .then(res => res);
    }

    getAnswer(idtes: any) {
      // return this.http.get<any>('assets/registrasi/condition.json')
      // .toPromise()
      // .then(res => <Condition[]>res.ConditionGet)
      // .then(ConditionGet => ConditionGet);
      //idtes = '03';//sengaja diganti gini biar muncul data sesuai API
      this.urlLink = SERVER_URL + '/Api/question/' + idtes + '/answer';
      console.log(this.urlLink);
      return this.http.get<any>(this.urlLink)
        .toPromise()
        .then(res => res);
    }

    getSchedule(idTes: any){
      // return this.http.get<any>('assets/registrasi/schedule.json')
      // .toPromise()
      // .then(res => <Schedule[]>res.ScheduleGet)
      // .then(ScheduleGet => ScheduleGet);
      //idTes = '13270200,81440200'; //sengaja diganti gini biar muncul data sesuai API
      // idTes = 11090000,11080000 // id lainnya
      // idTes = '11030000'
      this.urlLink = SERVER_URL + '/Api/test/'+ idTes +'/schedule';
      console.log(this.urlLink);
      return this.http.get<any>(this.urlLink)
      .toPromise()
      .then(res => res);
    }

    getPrice(idTes: any, idPrice: any){
      // return this.http.get<any>('assets/registrasi/price.json')
      // .toPromise()
      // .then(res => <Price[]>res.TestPriceGet)
      // .then(TestPriceGet => TestPriceGet);
      // idTes = '53020000,83840000';//sengaja diganti gini biar muncul data sesuai API
      // idPrice = '0570';//sengaja diganti gini biar muncul data sesuai API
      this.urlLink = SERVER_URL + '/Api/test/'+ idTes +'/Price/'+ idPrice;
      console.log(this.urlLink);
      return this.http.get<any>(this.urlLink)
      .pipe(
        map(res => res.testPrice)
      )
    }

    getSummaryPrice(idTes: any, idPrice: any){
      // return this.http.get<any>('assets/registrasi/price.json')
      // .toPromise()
      // .then(res => <Price[]>res.TestPriceGet)
      // .then(TestPriceGet => TestPriceGet);
      // idTes = '53020000,83840000';//sengaja diganti gini biar muncul data sesuai API
      // idPrice = '0570';//sengaja diganti gini biar muncul data sesuai API
      this.urlLink = SERVER_URL + '/Api/test/'+ idTes +'/Price/'+ idPrice;
      console.log(this.urlLink);
      return this.http.get<any>(this.urlLink)
      .map(res => res);
    }


    getPats(searchParam: string): Observable<any[]> {
      // searchParam = 't';
      console.log(searchParam)
      this.urlLink = SERVER_URL + '/Api/Pasien/' + searchParam;
      console.log(this.urlLink);
      return this.http.get<any>(this.urlLink)

      // return this.http.get<any>('https://202.146.232.86:4000/Api/Pasien/')
    }

    getDocs(searchParam): Observable<any[]> {
      // searchParam = 's';
      this.urlLink = SERVER_URL + '/Api/Dokter/' + searchParam;
      console.log(this.urlLink);
      return this.http.get<any>(this.urlLink)
    }

    getForms(searchParam): Observable<any[]> {
      // searchParam = 's';
      this.urlLink = SERVER_URL + '/Api/forms/' + searchParam;
      console.log(this.urlLink);
      return this.http.get<any>(this.urlLink)
    }

    getPatientId(idPatient: any){
      // idPatient = '2101100505751';
      this.urlLink = SERVER_URL + '/Api/PasienId/' + idPatient ;
      return this.http.get<any>(this.urlLink)
      .map(res => res);
    }
    
    //getPasienAddressType(): Observable<any[]> {
    //  this.urlLink = SERVER_URL + '/Api/AddressType/';
    //  return this.http.get<any>(this.urlLink)
    //}

    //getPasienCity(): Observable<any[]> {
    //  this.urlLink = SERVER_URL + '/Api/City/';
    //  return this.http.get<any>(this.urlLink)
    //}

    getPasienTitle(): Observable<any[]> {
      this.urlLink = SERVER_URL + '/Api/Title/';
      return this.http.get<any>(this.urlLink)
    }

    getPasienAgama(): Observable<any[]> {
      this.urlLink = SERVER_URL + '/Api/religion/	';
      return this.http.get<any>(this.urlLink)
    }

    getPasienPangkat(): Observable<any[]> {
      this.urlLink = SERVER_URL + '/Api/pangkat/		';
      return this.http.get<any>(this.urlLink)
    }
    
    getPasienSex(): Observable<any[]> {
      this.urlLink = SERVER_URL + '/Api/Sex/';
      return this.http.get<any>(this.urlLink)
    }

    getResAct(): Observable<any[]> {
      this.urlLink = SERVER_URL + '/Api/ResultAction/';
      return this.http.get<any>(this.urlLink)
    }

    getResFormat(): Observable<any[]> {
      this.urlLink = SERVER_URL + '/Api/Resultformat/';
      return this.http.get<any>(this.urlLink)
    }

    getResTo(): Observable<any[]> {
      this.urlLink = SERVER_URL + '/Api/ResultTo/';
      return this.http.get<any>(this.urlLink)
    }

    postReg(regData) {
      let body = JSON.stringify(
        regData
      );
      return this.http.post(SERVER_URL + '/Api/registration/submit', body, this._options)

    }

    private extractData(res: Response) {
      let body = res;
      return body || {};
    }
  
    handleError(error: Response | any) {
      console.log(error.message || error);
      return Observable.throw(error.message || error);
    }
  }
