import {
  Component,
  OnInit,
  Inject,
  Injectable,
  HostListener,
  Renderer,
  ViewChild,
  ElementRef,
  Output,
  forwardRef,
  EventEmitter,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  LOCALE_ID,
  AfterViewInit 
} from '@angular/core';
import {
  DOCUMENT
} from '@angular/platform-browser';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  FormGroupDirective,
  NgForm,
  ValidatorFn,
  AbstractControl,
  Validators,
  NG_VALUE_ACCESSOR,
  FormArray
} from '@angular/forms';
import {
  NativeDateAdapter,
  DateAdapter,
  MAT_DATE_FORMATS,
  MatStepper
} from '@angular/material';
import {
  Observable
} from 'rxjs/Observable';
import {
  HttpClient,
  HttpClientModule
} from '@angular/common/http';
import {
  Http,
  HttpModule
} from '@angular/http';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/do';
import {
  Subscription
} from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import { Tes, Condition, Schedule, Price, Answer } from './tes/tes';
import {
  TesService
} from './tes/tesservice';
import {
  fuseAnimations
} from '../../../../../core/animations';
import {
  ErrorStateMatcher
} from '@angular/material/core';
import {
  catchError,
  map,
  tap,
  startWith,
  switchMap,
  debounceTime,
  distinctUntilChanged,
  takeWhile,
  first,
  delay,
  finalize
} from 'rxjs/operators';
import { RequiredValidator } from '@angular/forms/src/directives/validators';
import { Response } from '@angular/http/src/static_response';
import { Summary } from '@angular/compiler';
import { LowerCasePipe } from '@angular/common/src/pipes';
import { CurrencyPipe } from '@angular/common';
import { DecimalPipe } from '@angular/common';
import { TreeNode } from 'primeng/primeng';
import { Router } from '@angular/router';
import { MycurrencyPipe } from '../pipe/mycurrency.pipe';
import { NgOption } from '@ng-select/ng-select';
import { ViewEncapsulation } from '@angular/core';
import { PickListModule, CalendarModule } from 'primeng/primeng';
import { locale } from '../../mail/i18n/tr';
import { localeData, locales } from 'moment';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort, PageEvent, MatTableDataSource } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {of} from "rxjs/observable/of";


interface AngSelectEvent {
  payByName: any;
  payById: any;
}

export interface Summary {
  summaryPrice?;
  summaryDiscount?;
  summaryPayable?;
}

export class AppDateAdapter extends NativeDateAdapter {
  parse(value: any): Date | null {
    if ((typeof value === 'string') && (value.indexOf('/') > -1)) {
      const str = value.split('/');
      const year = Number(str[2]);
      const month = Number(str[1]) - 1;
      const date = Number(str[0]);
      return new Date(year, month, date);
    }
    const timestamp = typeof value === 'number' ? value : Date.parse(value);
    return isNaN(timestamp) ? null : new Date(timestamp);
  }

  format(date: Date, displayFormat: Object): string {
    if (displayFormat == 'input') {
      const day = date.getDate();
      const month = date.getMonth() + 1;
      const year = date.getFullYear();
      return this._to2digit(day) + '/' + this._to2digit(month) + '/' + year;
    } else {
      return date.toDateString();
    }
  }

  private _to2digit(n: number) {
    return ('00' + n).slice(-2);
  }


}

export const APP_DATE_FORMATS = {
  parse: {
    dateInput: {
      month: 'short',
      year: 'numeric',
      day: 'numeric'
    }
  },
  display: {
    dateInput: 'input',
    monthYearLabel: {
      month: 'short',
      year: 'numeric',
      day: 'numeric'
    },
    dateA11yLabel: {
      year: 'numeric',
      month: 'long',
      day: 'numeric'
    },
    monthYearA11yLabel: {
      year: 'numeric',
      month: 'long'
    },
  }
};

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted || control.untouched));
  }
}


@Component({
  selector: 'reg_create',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './reg_create.component.html',
  styleUrls: ['./reg_create.component.scss'],
  providers: [{
      provide: DateAdapter,
      useClass: AppDateAdapter
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: APP_DATE_FORMATS
    },
    TesService, MycurrencyPipe
  ],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class RegistrationCreateComponent implements OnInit {
  public mask: Array<string | RegExp>
  emailPattern = "^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
  numberPattern = "[0-9]*"
  alphabetPattern = "[a-zA-Z][a-zA-Z ]+"
  checked = false;
  clicked = false;
  matcher = new MyErrorStateMatcher();

  dataSourcePrice: FilesDataSourcePrice;
  displayedColumnsPrice = [ 'no', 'test', 'amount', 'discount', 'status'];

  chooseRef = new FormControl();
  chooseAgr = new FormControl();
  choosePatient = new FormControl();

  chooseDoc = new FormControl();
  chooseForm = new FormControl();
  actionDoc = new FormControl();
  toDoc = new FormControl();
  formatDoc = new FormControl();
  patientId = new FormControl();
  public visibleOptionsSource: any[];
  filteredRefs: Observable<any[]>;
  filteredAgrs: Observable<any[]>;
  filteredPatient: Observable<any[]>;
  filteredDoctor: Observable<any[]>;
  filteredForm: Observable<any[]>;

  // filteredBy: any[];
  // bys: any[];
  
  errorMessage: boolean;
  successMessage: boolean;
  verifyok: boolean;
  TotalBruto: any;
  TotalDiskon: any;
  TotalNetto: any;
  paidAmount: any;
  payBanksTemps = [];
  payBys = [];
  payBanks = [];
  payCards = [];
  payById: any;
  paymentCardDisabled: any;
  paymentBankDisabled: any;
  paymentCardNo: any;
  paymentAppCode: any;
  paymentTraceCode: any;
  paymentTotal: any;
  paymentSisa: any;
  paymentSisaTanggungan: any;
  paymentKelebihanBayar: any;
  paymentNom: any;
  paymentTot: any;
  typeaheadBy = new EventEmitter<any>();
  typeaheadCard = new EventEmitter<any>();
  typeaheadBank = new EventEmitter<any>(); 
  typeaheadCity = new EventEmitter<any>();
  typeaheadType = new EventEmitter<any>();
  typeaheadJawaban = new EventEmitter<any>();
  typeaheadContactType = new EventEmitter<any>();
  typeaheadTempatLahir = new EventEmitter<any>();
  events: AngSelectEvent[] = [];
  savelebih: any;
  timeout: any;
  savesisa: any;
  moneycometome: any;
  moneygone: any;
  testerpakai: any;
  temptestconcate: any;
  savenomor_kartu: string;
  savekode_approve: string;
  savenomor_mesin: string;
  savetrace_code: string;
  breakLopFor: boolean;
  agrParam: string;
  searchRef: string;
  searchPat: string;
  idPriceParam: any;
  TesttVal: Tes[];
  sourceTes: Tes[];
  targetTes: Tes[];
  idTes: any = [];
  dummyTes: any = [];
  tempTest: any = [];
  idSummary: any = [];
  idPriceSummary: any = [];
  idPatient: any = [];
  idDocParam: any;
  idAddressDocParam: any;
  cond: Condition[];
  question: any;
  condJson: any = [{
    condition : '',
    answer : '',
    note : ''
  }];
  pasiencountADDAR: number;
  pasiencountContAR: number;
  pasienaddressAR: any = [];
  pasiencontactAR: any = [];
  conditionnoanswer: any = [];
  answersIN: any = [];
  answer: Answer[];
  price: Price[];
  createReg: any = [];
  summary: any;
  // patientData= [];
  hid_contact = [];
  hid_type = [];
  hid_address = [];
  patientData: any;
  jawabanarr = [];
  pasienTL = [];
  pasienContact = [];
  pasienType = [];
  pasienCity = [];
  pasienAddressType = [];
  PasienAgama = [];
  PasienPangkat = [];
  PasienTitle = [];
  pasienSex = [];
  summaryTest: Tes[];
  summaryChildTest: any = [];
  summaryPayment: any;
  doctorData= [];
  docAct = [];
  docTo = [];
  docFormat = [];
  data = {}
  tesJson: any = [];
  tesJsonDetail: any = [];
  tesPriceJson: any = [];
  agreeJson: any = [];
  regValue: any = [];
  selectedAct: any;
  selectedTo: any;
  selectedFormat: any;
  selectedActDoc: any = [];

  schedule: Schedule[];

  selection: any;
  selection2: any;
  selection3: any;
  selection4: any;
  refree: any;
  agreement: any;
  defFormat: any;
  paymentTreeWidth: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayFn(refree): any {
    return refree ? refree.refreeName : refree;
  }

  displayFn2(agreement): any {
    return agreement ? agreement.agreement : agreement;
  }

  displayFn3(patient): any {
    return patient ? `${patient.pasienID != null ? patient.pasienID : ""} ${patient.pasienNama != null ? patient.pasienNama : ""} ${patient.pasienTanggalLahir != null ? patient.pasienTanggalLahir : ""} ${patient.pasienAlamat != null ? patient.pasienAlamat : ""} ${patient.kota != null ? patient.kota : ""}` : patient;   
  }

  displayFn4(doctor): any {
    return doctor ? `${doctor.frontTitle != null ? doctor.frontTitle : ""} ${doctor.dokterNama != null ? doctor.dokterNama : ""} ${doctor.endTitle != null ? doctor.endTitle : ""} ${doctor.dokterLocation != null ? doctor.dokterLocation : ""} ${doctor.dokterAlamat != null ? doctor.dokterAlamat : ""}` : doctor;
  } 

  displayFn5(form): any {
    return form ? form.dokterForm : form;
  }


  filter(refVal: string): Observable<any[]> {
    return this.tesService.getRefs(refVal)
    .pipe(
      map(response => response.filter(option => {
        return (option.refreeName) != null ?
        option.refreeName :
        option.refreeId
      }))
    )

  }

  filterAgr(agrVal: string): Observable<any[]> {
    return this.tesService.getAgrs(this.agrParam, agrVal)
    .pipe(
      map(response => response.filter(option => {
        return (option.agreement) != null ?
        option.agreement :
        option.agreementID
      }))
    )
  }

  filterPat(patVal: string): Observable<any[]> {
    return this.tesService.getPats(patVal)
    .pipe(
      map(response => response.filter(option => {
        return (option.pasienNama) != null ?
        option.pasienNama :
        option.pasienID
      }))
    )
  }

  filterDoc(docVal: string): Observable<any[]> {
    return this.tesService.getDocs(docVal)
    .pipe(
      map(response => response.filter(option => {
        return (option.dokterNama) != null ?
        option.dokterNama :
        option.dokterID
      }))
    )
  }

  filterForm(formVal: string): Observable<any[]> {
    return this.tesService.getForms(formVal)
    .pipe(
      map(response => response.filter(option => {
        return (option.dokterFormName) != null ?
          option.dokterFormName :
          option.dokterFormID
      }))
    )
  }



  navIsFixed: boolean;
  form: FormGroup;
  formErrors: any;
  testRef: FormGroup;
  totalPayment: number;
  totalPaymentStr: String;
  thousands_separator = ",";

  options = [];

  // Horizontal Stepper
  horizontalStepperStepTest: FormGroup;
  horizontalStepperStepRegister: FormGroup;
  horizontalStepperStepDoctor: FormGroup;
  horizontalStepperStepPayment: FormGroup;
  horizontalStepperStepTestErrors: any;
  horizontalStepperStepRegisterErrors: any;
  horizontalStepperStepDoctorErrors: any;
  horizontalStepperStepPaymentErrors: any;

  // Vertical Stepper
  verticalStepperStepTest: FormGroup;
  verticalStepperStepCondition: FormGroup;
  verticalStepperStepSchedule: FormGroup;
  verticalStepperStepPrice: FormGroup;
  verticalStepperStepTestErrors: any;
  verticalStepperStepConditionErrors: any;
  verticalStepperStepScheduleErrors: any;
  verticalStepperStepPriceErrors: any;
  schData: any;

  selectedFile3: Tes;

  urlBuktiPemeriksaan = "http://202.146.232.86:8081/jasperserver/flow.html?_flowId=viewReportFlow&ParentFolderUri=/reports/Pramita&reportUnit=/reports/Pramita/BuktiPemeriksaan&standAlone=false&&j_username=joeuser&j_password=joeuser&RegNo=";
  urlBuktiTagihan = "http://202.146.232.86:8081/jasperserver/flow.html?_flowId=viewReportFlow&ParentFolderUri=/reports/Pramita&reportUnit=/reports/Pramita/BuktiTagihan&standAlone=false&&j_username=joeuser&j_password=joeuser&RegNo=";
  urlKartuKontrol = "http://202.146.232.86:8081/jasperserver/flow.html?_flowId=viewReportFlow&ParentFolderUri=/reports/Pramita&reportUnit=/reports/Pramita/KartuKontrolPelayanan&standAlone=false&&j_username=joeuser&j_password=joeuser&RegNo=";
  urlKwitansi = "http://202.146.232.86:8081/jasperserver/flow.html?_flowId=viewReportFlow&ParentFolderUri=/reports/Pramita&reportUnit=/reports/Pramita/Kwitansi&standAlone=false&&j_username=joeuser&j_password=joeuser&RegNo=";
  urlNotaPembayaran = "http://202.146.232.86:8081/jasperserver/flow.html?_flowId=viewReportFlow&ParentFolderUri=/reports/Pramita&reportUnit=/reports/Pramita/NotaPembayaran&standAlone=false&&j_username=joeuser&j_password=joeuser&RegNo=";
  urlTandaTerima = "http://202.146.232.86:8081/jasperserver/flow.html?_flowId=viewReportFlow&ParentFolderUri=/reports/Pramita&reportUnit=/reports/Pramita/TandaTerimaPembayaran&standAlone=false&&j_username=joeuser&j_password=joeuser&RegNo=";
  regNo : any;

  @ViewChild('expandingTree')
  expandingTree: Tes;
  @ViewChild('autoclick') autoclick: ElementRef;
  @ViewChild('autoclickcond') autoclickcond: ElementRef;
  @ViewChild('autoloadtest') autoloadtest: ElementRef; 
  @ViewChild('autoloadsch') autoloadsch: ElementRef;
  @ViewChild('autoloadprice') autoloadprice: ElementRef;
  // public pasienAddress: Array < any > = [];
  public contactArray: Array < any > = [];
  public newAttribute: any = {};
  public conditionArray: Array < any > = [];
  public scheduleArray: Array < any > = [];
  public actionArray: Array < any > = [];
  public myArray: any = {};
  

  textChanged(x){
    console.log("x")
    console.log(x)
    this.filter(x)

  }

  validateInput(event){
    console.log("validateeeeee: ", event)
  }

  initAddresss(data): FormGroup {
    //this.tesService.getPasienCity().subscribe(pasienCity => this.pasienCity = pasienCity);
    //this.tesService.getPasienAddressType().subscribe(pasienAddressType => this.pasienAddressType = pasienAddressType);
    return this.formBuilder.group({
      patientAddressId: data.patientAddressId,
      addressLocation: data.addressLocation,
      addressName: data.addressName,
      addressType: data.addressType,
      addressTypeId: data.addressTypeId,
      addressCity: data.addressCity,
      addressCityId: data.addressCityId,
      addressPostCode: [data.addressPostCode, [Validators.required, Validators.pattern]],
      addressPhone: data.addressPhone,
      addressFax: data.addressFax
    });
  }

  initAddress(): FormGroup {
    //this.tesService.getPasienCity().subscribe(pasienCity => this.pasienCity = pasienCity);
    //this.tesService.getPasienAddressType().subscribe(pasienAddressType => this.pasienAddressType = pasienAddressType);
    return this.formBuilder.group({
      patientAddressId: [''],
      addressLocation: [''],
      addressName: [''],
      addressType: [''],
      addressTypeId: [''],
      addressCity: [''],
      addressCityId: [''],
      addressPostCode: ["", [Validators.required, Validators.pattern]],
      addressPhone: [''],
      addressFax: ['']
    });
  }

  initContacts(data) : FormGroup {
    return this.formBuilder.group({
      pasienContactID: data.pasienContactID,
      messangerTypeId: data.messangerTypeId,
      patientName: data.patientName,
      patientId: data.patientId,
      messangerType: data.messangerType,
      messangerName: data.messangerName
    });
  }
  initContact(): FormGroup {
    return this.formBuilder.group({
      pasienContactID: [''],
      messangerTypeId: [''],
      patientName: [''],
      patientId: [''],
      messangerType: [''],
      messangerName: ['']
    });
  }

  initResultTo() {
    
    this.tesService.getResAct().subscribe(docAct => this.docAct = docAct);
    this.tesService.getResTo().subscribe(docTo => this.docTo = docTo);
    this.tesService.getResFormat().subscribe(docFormat => {
      this.docFormat = docFormat;
      // this.defFormat = docFormat[0].resultActionID;
    });
    return this.formBuilder.group({
      resultActionID: [''],
      resultToID: [''],
      resultFormatID: ['11'],
      note: ['']
    })
  }

  initPaymentTo() {
    return this.formBuilder.group({
      payToId: [''],
      payCardId: [''],
      paidAmount: [''],
      paymentCardNO: [''],
      paymentApprovalCode: [''],
      paymentTraceCode: [''],
      paymentNominal: ['']
    })
  }

  initCondition() {
    const group = this.formBuilder.group({
      questionID: '',
      question: '',
      answerID: '',
      answers: '',
      note: ''
    })
    return group;
  }

  initConditionPopulate(sch) {
    const group = this.formBuilder.group({
      questionID: sch.questionID,
      question: sch.question,
      answerID: null,
      answers: (sch.answers != null ? sch.answers : []),
      note: sch.note
    })
    return group;
  }

  initSchedulePopulate(sch) {
    const group = this.formBuilder.group({
      scheduleTimeId: sch.scheduleTimeId,
      scheduleName: sch.scheduleName,
      date: sch.date || "",
      time: sch.time || ""
    })
    return group;
  }

  addAddressValue() {
    const control = <FormArray>this.horizontalStepperStepRegister.controls['pasienAddress'];
    control.push(this.initAddress())
  }

  deleteAddressValue(index) {
    if(index > 0){
      const control = <FormArray>this.horizontalStepperStepRegister.controls['pasienAddress'];
      control.removeAt(index);
    }
  }

  addConditionValue(targetTes, stepKondisi) {
    this.resetCondition()
    console.log("iniiiiitttt: ", this.initCondition)
    this.tesService.getCondition(this.idTes).then(cond => {
      this.cond = cond;
      console.log("cond; ", cond)
      const control = <FormArray>this.verticalStepperStepCondition.controls['condition'];
      cond.forEach(y => {
        // console.log("yeeeeeeee : ", y)
        // console.log("yeeeeeeee222222 : ", y.answers)
        this.conditionnoanswer.push({
          "questionID": y.questionID,
          "question": y.question,
          "answerID": y.answerID,
          "answers": null,
          "note": y.note
        })
        control.push(this.initConditionPopulate(y))
      })

      this.loadcondelement();
    });

    this.tesService.getTreeTest(this.idTes, this.idPriceParam).then(tes => {

      this.summaryTest = [{
        children: tes.test

      }];

      console.log("summaryTest")
      console.log(this.summaryTest)
      //console.log("paymentTotal")
      //console.log(tes.paymentTotal)
      //console.log("tes lengkap: ", tes)


      this.summaryTest.forEach(summaryTest => {
        this.expandRecursive(summaryTest, true);
      });
    });
    this.tesService.getTreeTest(this.idTes, this.idPriceParam).then(tes => {
      this.summaryPayment = tes


    })
    stepKondisi.selectedIndex = 0;
  }

  passIndexValue(index) {
    this.answersIN = [];
    console.log(index);//clicked index
    this.tesService.getAnswer(index).then(answer => {
      this.answer = answer;
      console.log("answer; ", answer)
      answer.forEach(y => {
        this.answersIN.push(y)
        console.log("say hi", y);

      })

    });
  }

  passValue(index, valuex, valuey, action) {
    if (valuex != null) {
      this.verticalStepperStepCondition.value.condition[index].answers = null;
      this.conditionnoanswer[index].answerID = action.controls.answerID.value
      console.log("action", this.verticalStepperStepCondition.controls.condition['controls'][index].controls["answers"].value["answerName"]);
      console.log("action", this.verticalStepperStepCondition.controls.condition['controls'].values);
      let pick = this.verticalStepperStepCondition.controls.condition['controls'][index].controls["answers"];
      pick.patchValue({
        answerID: valuex,
        answerName: valuey
      });
      let pickid = this.verticalStepperStepCondition.controls.condition['controls'][index].controls["answerID"];
      pickid.setValue(valuex);
      //action.controls.answers.value.answerName = valuey;
     // action.controls.answers.value= null;
      // const control = <FormArray>this.initCondition.controls['answerID'];
      // control.at(index).patchValue(valuex);
      // // this.answerID.at(index).patchValue({
      // //   answerID : valuex
      // // })
      console.log("action", action);//clicked index

      // this.verticalStepperStepCondition.controls['answerID'].patchValue(valuex);
    }
  }

  passValueNote(index, action) {


      this.conditionnoanswer[index].note = action.controls.note.value


  }

  deleteConditionValue(index) {
    this.conditionArray.splice(index, 1);
  }

  addScheduleValue(stepSchedule) {
    console.log("answerID isinyaa: ", this.verticalStepperStepCondition.value["condition"])
    this.resetSchedule();
    const control = <FormArray>this.verticalStepperStepSchedule.controls['scheduleTEST'];
    this.tesService.getSchedule(this.idTes).then(sch => {
      this.schedule = sch
      sch.forEach(x => {
        control.push(this.initSchedulePopulate(x))
        // console.log("xmen",x)
      })
      this.loadelementsch();
    });
    stepSchedule.selectedIndex = 0;
  }

  deleteScheduleValue(index) {
    this.scheduleArray.splice(index, 1);
  }

  addPriceValue(stepPrice) {
    this.clicked = false;
    this.dataSourcePrice = new FilesDataSourcePrice(this.tesService)
    this.dataSourcePrice.loadPrice(this.idTes, this.idPriceParam)

    this.tesService.getSummaryPrice(this.idTes, this.idPriceParam).subscribe(data => {
      this.summary = data
      this.loadelementprice();
      console.log("summary payment: ", this.horizontalStepperStepPayment.controls['paymentViewModel'].value[0].paidAmount)
      this.horizontalStepperStepPayment.patchValue({
        // paymentBankID: this.summaryPayment['paymentBankID'],
        // paymentCardID: this.summaryPayment['paymentCardID'],
        // paymentCardNO: this.summaryPayment['paymentCardNO'],
        // paymentApprovalCode: this.summaryPayment['paymentApprovalCode'],
        // paymentMachineNO: this.summaryPayment['paymentMachineNO'],
        // paymentTraceCode: this.summaryPayment['paymentTraceCode'],
        // paymentKelebihanBayar: this.summaryPayment['paymentKelebihanBayar'],

        paymentSubTotal: this.summary.summaryPrice,
        paymentDiscount: this.summary.summaryDiscount,
        paymentNominal: this.summaryPayment['paymentNominal'],
        paymentSisaTanggungan: this.summaryPayment['paymentSisaTanggungan'],
        paymentTotal: this.summaryPayment['paymentTotal'] - this.summary.summaryDiscount,
        paymentNote: this.summaryPayment['paymentNote']
      })
      let paymentawal = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][0].get('paidAmount')
      paymentawal.setValue(this.summaryPayment['paymentTotal'] - this.summary.summaryDiscount);
      let nominalawal = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][0].get('paymentNominal')
      nominalawal.setValue(this.summaryPayment['paymentTotal'] - this.summary.summaryDiscount);
      console.log(" payment: ", this.horizontalStepperStepPayment.controls['paymentViewModel'].value[0])
      this.moneycometome = this.summaryPayment['paymentTotal'] - this.summary.summaryDiscount;
    })
    
    
    this.savesisa = this.horizontalStepperStepPayment.controls['paymentSisaTanggungan'].value;
    // this.savenomor_kartu = this.horizontalStepperStepPayment.controls['paymentCardNO'].value
    // this.savekode_approve = this.horizontalStepperStepPayment.controls['paymentApprovalCode'].value
    // this.savenomor_mesin = this.horizontalStepperStepPayment.controls['paymentMachineNO'].value
    // this.savetrace_code = this.horizontalStepperStepPayment.controls['paymentTraceCode'].value

    stepPrice.selectedIndex = 0;
    
  }


  addContactValue() {
    const control = <FormArray>this.horizontalStepperStepRegister.controls['pasienContact'];
    control.push(this.initContact())
  }

  deleteContactValue(index) {
    if(index > 0){
      const control = <FormArray>this.horizontalStepperStepRegister.controls['pasienContact'];
      control.removeAt(index);
    }
  }

  addActionValue() {
    const control = <FormArray>this.horizontalStepperStepDoctor.controls['registerResultTO'];
    control.push(this.initResultTo())
  }

  deleteActionValue(index) {
    if(index > 0){
      // this.actionArray.splice(index, 1);
      const control = <FormArray>this.horizontalStepperStepDoctor.controls['registerResultTO'];
      control.removeAt(index);
    }
  }

  addPaymentValue() {
    this.clicked = true;
    const control = <FormArray>this.horizontalStepperStepPayment.controls['paymentViewModel'];
    control.push(this.initPaymentTo())
    //this.moneycometome = this.horizontalStepperStepPayment.controls['paymentTotal'].value - this.moneygone;
    console.log("length",this.horizontalStepperStepPayment.controls.paymentViewModel['controls'].length);
    console.log("log", this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][this.horizontalStepperStepPayment.controls.paymentViewModel['controls'].length - 2]);
    let paymentawal = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][this.horizontalStepperStepPayment.controls.paymentViewModel['controls'].length-1].get('paidAmount')
    paymentawal.setValue(this.horizontalStepperStepPayment.value['paymentSisaTanggungan']);
    let paymentNominala = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][this.horizontalStepperStepPayment.controls.paymentViewModel['controls'].length - 1].get('paymentNominal')
    paymentNominala.setValue(this.horizontalStepperStepPayment.value['paymentSisaTanggungan']);

    this.totalPayment = 0;
    this.totalPaymentStr = "0";
    //this.moneygone = 0;
    const totPay = <FormArray>this.horizontalStepperStepPayment.controls['paymentViewModel'];
    totPay.controls.forEach(x => {
      //this.moneygone += parseInt(x.get('paidAmount').value)
      if (parseInt(x.get('paymentNominal').value) > 0) {
        //let parsed = 0;
        //if(x.get('paymentNominal').value){
        //  if(x.get('paymentNominal').value.indexOf(this.thousands_separator) > -1)
        //    parsed = +(x.get('paymentNominal').value.replace(this.thousands_separator,""))
        //    else parsed = +(x.get('paymentNominal').value);
        //  }
        //  else parsed = 0;         
        let parsed = parseInt(x.get('paymentNominal').value)
        this.totalPayment += parsed;
        //this.totalPaymentStr = this.cpipe.transform(this.totalPayment.toString());
        this.ref.detectChanges();
      }

    })
    this.horizontalStepperStepPayment.patchValue({
      paymentNominal: this.horizontalStepperStepPayment.controls['paymentNominal'].value,
      paymentSisaTanggungan: this.horizontalStepperStepPayment.controls['paymentTotal'].value - this.totalPayment < 0 ? 0 : this.horizontalStepperStepPayment.controls['paymentTotal'].value - this.totalPayment,
      paymentTotal: this.horizontalStepperStepPayment.controls['paymentTotal'].value
    })
    if (this.totalPayment - this.horizontalStepperStepPayment.controls['paymentTotal'].value > 0) {
      this.horizontalStepperStepPayment.patchValue({
        paymentKelebihanBayar: this.totalPayment - this.horizontalStepperStepPayment.controls['paymentTotal'].value < 0 ? 0 : this.totalPayment - this.horizontalStepperStepPayment.controls['paymentTotal'].value
      })
    } else {
      this.horizontalStepperStepPayment.patchValue({
        paymentKelebihanBayar: 0
      })
    }
  }

  deletePaymentValue(index) {
    if (index > 0) {
          
      const control = <FormArray>this.horizontalStepperStepPayment.controls['paymentViewModel'];
      control.removeAt(index);
      this.totalPayment = 0;
      this.totalPaymentStr = "0";
      //this.moneygone = 0;
      const totPay = <FormArray>this.horizontalStepperStepPayment.controls['paymentViewModel'];
      totPay.controls.forEach(x => {
        //this.moneygone += parseInt(x.get('paidAmount').value)
        if (parseInt(x.get('paymentNominal').value) > 0) {
          //let parsed = 0;
          //if(x.get('paymentNominal').value){
          //  if(x.get('paymentNominal').value.indexOf(this.thousands_separator) > -1)
          //    parsed = +(x.get('paymentNominal').value.replace(this.thousands_separator,""))
          //    else parsed = +(x.get('paymentNominal').value);
          //  }
          //  else parsed = 0;         
          let parsed = parseInt(x.get('paymentNominal').value)
          this.totalPayment += parsed
          //this.totalPaymentStr = this.cpipe.transform(this.totalPayment.toString());
          this.ref.detectChanges
        }

      })
     // console.log("index -1", this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index - 1].get('paidAmount').value)
     // this.moneycometome = parseInt(this.horizontalStepperStepPayment.controls['paymentTotal'].value) - this.moneygone;
      //console.log("moneycometome", this.moneycometome)

      console.log("this.totalPayment", this.totalPayment)
      this.horizontalStepperStepPayment.patchValue({
        paymentNominal: this.horizontalStepperStepPayment.controls['paymentNominal'].value,
        paymentSisaTanggungan: this.horizontalStepperStepPayment.controls['paymentTotal'].value - this.totalPayment < 0 ? 0 : this.horizontalStepperStepPayment.controls['paymentTotal'].value - this.totalPayment,
        paymentTotal: this.horizontalStepperStepPayment.controls['paymentTotal'].value
      })
      if (this.totalPayment - this.horizontalStepperStepPayment.controls['paymentTotal'].value > 0) {
        this.horizontalStepperStepPayment.patchValue({
          paymentKelebihanBayar: this.totalPayment - this.horizontalStepperStepPayment.controls['paymentTotal'].value < 0 ? 0 : this.totalPayment - this.horizontalStepperStepPayment.controls['paymentTotal'].value
        })
      } else {
        this.horizontalStepperStepPayment.patchValue({
          paymentKelebihanBayar: 0
        })
      }
    }
  }

  addJsonCreateValue(){
    // var pasienData : any;
    // pasienData = this.horizontalStepperStepRegister.value
    this.createReg = {
      registerNo : "",
      registerId : "",
      refreeID : this.agrParam,
      agreementID : (this.agreeJson["agreementId"] != null ?
        this.agreeJson["agreementId"] : "0"),      
      test : this.tesJson,
      condition: this.conditionnoanswer,
      // condition : this.verticalStepperStepCondition.value["condition"],
      schedule: this.verticalStepperStepSchedule.value["scheduleTEST"],
      testPrice : this.tesPriceJson,
      pasienID : this.horizontalStepperStepRegister.value["pasienID"],
      pasienData: this.horizontalStepperStepRegister.value,
      doctorAddressId: this.idAddressDocParam,
      dokterID : this.idDocParam,
      formId: this.horizontalStepperStepDoctor.value["chooseForm"],
      companyNote: (this.agrParam["refreeNote"] != null ?
        this.agrParam["refreeNote"] : ""),   
      agreementNote : this.agreeJson["agreementNote"],
      PatientNote: this.horizontalStepperStepRegister.value["pasienNote"],
      diagnoseNote : this.horizontalStepperStepDoctor.value["diagnoseNote"],
      paymentNote : this.horizontalStepperStepPayment.value['paymentNote'],
      registerResultTO : this.horizontalStepperStepDoctor.value["registerResultTO"],
      // paymentBankID : this.horizontalStepperStepPayment.value['paymentBankID'],
      // paymentCardID : this.horizontalStepperStepPayment.value['paymentCardID'],
      // paymentCardNO : this.horizontalStepperStepPayment.value['paymentCardNO'],
      // paymentApprovalCode : this.horizontalStepperStepPayment.value['paymentApprovalCode'],
      // paymentTraceCode : this.horizontalStepperStepPayment.value['paymentTraceCode'],
      paymentViewModel: this.horizontalStepperStepPayment.value['paymentViewModel'],
      paymentSubTotal: this.horizontalStepperStepPayment.value['paymentSubTotal'],
      paymentDiscount: this.horizontalStepperStepPayment.value['paymentDiscount'],
      paymentNominal :this.totalPayment,
      paymentSisaTanggungan : this.horizontalStepperStepPayment.value['paymentSisaTanggungan'],
      paymentTotal : this.horizontalStepperStepPayment.value['paymentTotal']
    }
  }

  onGenderChg(event, sex){ 
    if(event.isUserInput){
      this.horizontalStepperStepRegister.patchValue({
        pasienGender: sex.sexName
      })
    }
  }

  onAgamaChg(event, agama){ 
    if(event.isUserInput){
      this.horizontalStepperStepRegister.patchValue({
        pasienAgama: agama.religionName
      })
    }
  }

  onPangkatChg(event, pangkat){ 
    if(event.isUserInput){
      this.horizontalStepperStepRegister.patchValue({
        pasienPangkat: pangkat.pangkatName
      })
    }
  }

  onRefreeSelected(refree){
    if(refree !== null){
      console.log("refree",refree);
      this.agrParam = refree.refreeId;
      console.log("agrParam: " + this.agrParam);
      this.filteredAgrs = this.chooseAgr.valueChanges
      .pipe(
        startWith(null),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(valAgr => {
          // console.log("val: ")
          // console.log(valAgr)
          return this.filterAgr(valAgr || '')
        })
      );
      this.horizontalStepperStepTest.patchValue({
        chooseRef : refree.refreeId
      })
    }
  }

  onAgreeSelected(agreement){
     this.tempTest = [];
    if(agreement !== null){
      console.log("agreement",agreement)
      this.idPriceParam = agreement.priceId;
      this.agreeJson = agreement;
      this.tesService.getTestList(this.idPriceParam, null, null).then(tes => {
        this.sourceTes = tes
        this.loadelementtest();
      });
      this.targetTes = [];
      this.horizontalStepperStepTest.patchValue({
        chooseAgr : agreement.agreementId
      })
    } else{
      this.horizontalStepperStepRegister.reset()
    }
  }

  onPasienSelected(patient: any){
    if (patient != null){
      console.log("patient");
      this.horizontalStepperStepRegister.reset();
      this.pasiencountADDAR = 0;
      this.pasiencountContAR = 0;
      this.pasienaddressAR = [];
      this.pasiencontactAR = [];
      this.hid_contact = [];
      this.hid_contact.push(false);
      this.hid_type = [];
      this.hid_type.push(false);
      this.hid_address = [];
      this.hid_address.push(false);
      this.pasienContact = [];
      this.pasienType = [];
      this.pasienCity = [];
      this.idPatient = patient.pasienID;
      //console.log("reg1", patient)
      //console.log("reg2", this.horizontalStepperStepRegister.value.pasienAddress[0])
      
      this.tesService.getPatientId(this.idPatient).subscribe(data => {
        this.patientData = data
        console.log("pasien: ", data.pasienContact.length)

        const control = <FormArray>this.horizontalStepperStepRegister.controls['pasienAddress'];
        console.log("control pasien: ", control.length)
        for (var z = control.length; z >= 0; z--) {
          control.removeAt(z);
          console.log("delete pasien: z", z)
        }

        if (data.pasienAddress.length > 0) {
          
          data.pasienAddress.forEach(y => {
            control.push(this.initAddresss(y))
          })
        } else {
         
          this.addAddressValue();
        }

        const control1 = <FormArray>this.horizontalStepperStepRegister.controls['pasienContact'];

        console.log("control1 pasien: ", control1.length)
        for (var y = control1.length; y >= 0; y--) {
          control1.removeAt(y);
          console.log("contact del pasien: y", y)
        }
      
        if (data.pasienContact.length > 0) {
          
          data.pasienContact.forEach(y => {
            control1.push(this.initContacts(y))
          })
        } else {
         
          this.addContactValue();
        }
        
        this.horizontalStepperStepRegister.patchValue({
          pasienID: data.pasienID,
          pasienNama: data.pasienNama,
          pasienTempatLahir: data.pasienTempatLahir,
          pasienTanggalLahir: data.pasienTanggalLahir,
          pasienUmur: data.pasienUmur,
          // pasienGender: {
          //   'sexId' : data.pasienGenderId ? ((data.pasienGenderId == "F") ? "P" : "L") : null, 
          //   'sexName' : data.pasienGender ? ((data.pasienGenderId == "F") ? "Female" : "Male") : null},
          pasienGender: data.pasienGender,
          pasienGenderId: data.pasienGenderId ? ((data.pasienGenderId == "F") ? "P" : "L") : null ,
          pasienAgama: data.pasienAgama,
          pasienAgamaId: data.pasienAgamaId,
          pasienPangkatId: data.pasienPangkatId,
          pasienPangkat: data.pasienPangkat,
          pasienAlive: data.pasienAlive,
          //pasienDOB : data.pasienDOB,
          pasienRealDOB: data.pasienRealDOB,
          pasienTitleId: data.pasienTitleId,
          pasienTitle: data.pasienTitle,
          nik: data.nik,
          nip: data.nip,
          department: data.department,
          jobTitle: data.jobTitle,
          pasienNote: data.pasienNote
        })
       
      });
    }
    this.regValue = this.horizontalStepperStepRegister.value
    //let address = this.horizontalStepperStepRegister.value.pasienAddress[0]
    //address.setValue("");
  }


  onDokterSelected(doctor: any){
    this.idDocParam = doctor.dokterID;
    this.idAddressDocParam = doctor.doctorAddressId;
    this.horizontalStepperStepDoctor.patchValue({
      chooseDoc : doctor.dokterID
    })
  }

  onFormSelected(form){
    this.horizontalStepperStepDoctor.patchValue({
      chooseForm : form.dokterFormID
    })
  }

  onDiagnoseNote(event){
    // console.log("event diagnose note: ", $event)
    this.horizontalStepperStepDoctor.patchValue({
      diagnoseNote : event
    })
  }


  constructor(
    private renderer: Renderer,
    private cpipe: MycurrencyPipe,
    private formBuilder: FormBuilder,
    private http: Http,
    private tesService: TesService,
    @Inject(DOCUMENT) private document: Document,
    private router: Router,
    private ref: ChangeDetectorRef
    ) {
    this.constructAll()

    

  }
  
  //onfilledby(event) {
  //  this.typeaheadBy
  //    .pipe(
  //    startWith(''),
  //    debounceTime(200),
  //    switchMap(data => this.tesService.getPaymentBy(data))
  //    )
  //    .subscribe(paybys => {
  //      console.log("paybys: ", paybys)
  //      this.payBys = paybys;
  //      this.ref.markForCheck();
  //    }, (err) => {
  //      console.log('payBy error : ', err);
  //      this.payBys = [];
  //      this.ref.markForCheck();
  //    })


  //  this.payment = this.onAdd(event)
  //  console.log(this.payment)
  //}

  //onAdd(event) {
  //  if(event != null){
  //    console.log("on add $event : ", event)
  //    this.events.push({payByName: event.payById, payById: event});
  //    console.log("this.events : ", this.events)
  //  }
  //}

  //onChangeBys(index,event, payments) {
  //  console.log("on change $event : ", event)
  //  this.payment = event.payById;
  //  // this.events.push({payByName: '(add)', payById: $event});
  //  console.log("this.payment : ", this.payment)

  //  this.typeaheadBank
  //  .pipe(
  //    startWith(''),
  //    debounceTime(200),
  //    switchMap(data => this.tesService.getPaymentBank(data))
  //  )
  //  .subscribe(paybanks => {
  //    console.log("paybanks: ", paybanks)
  //    this.payBanks = paybanks;
  //    this.ref.markForCheck();
  //  }, (err) => {
  //    console.log('payBank error : ', err);
  //    this.payBanks = [];
  //    this.ref.markForCheck();
  //  })

  //  let paySelected = this.payment;
    

  //  if (paySelected == "01" || paySelected == "15") {
  //    // this.paymentHidden = !this.paymentHidden
  //    console.log("this.horizontalStepperStepPayment.controls['paymentViewModel'][index]", this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentBankID'));
  //    let paymentBankDisabled = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentBankID')
  //    let paymentCardDisabled = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentCardID')
  //    let paymentCardNo = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentCardNO')
  //    let paymentAppCode = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentApprovalCode')
  //    let paymentTraceCode = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentTraceCode')
  //    paymentBankDisabled.setValue("");
  //    paymentCardDisabled.setValue("");
  //    paymentBankDisabled.disable()
  //    paymentCardDisabled.disable()
  //    paymentCardNo.disable()
  //    paymentAppCode.disable()
  //    paymentTraceCode.disable()
  //    //const disableSel = <FormArray>this.horizontalStepperStepPayment.controls['paymentViewModel'];
  //    //disableSel.controls.forEach(y => {
  //    //  console.log("aloha",y);
        

  //    //})
  //    return;
  //  } else {
  //    let paymentBankDisabled = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentBankID')
  //    let paymentCardDisabled = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentCardID')
  //    let paymentCardNo = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentCardNO')
  //    let paymentAppCode = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentApprovalCode')
  //    let paymentTraceCode = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentTraceCode')
  //    paymentBankDisabled.enable()
  //    paymentCardDisabled.enable()
  //    paymentCardNo.enable()
  //    paymentAppCode.enable()
  //    paymentTraceCode.enable()
      
  //    return;
  //  }
  //}

  inputjawaban(event,index,action) {
    console.log('event inputcity : ', this.verticalStepperStepCondition);
    if (event != null) {
      this.verticalStepperStepCondition.value.condition[index].answers = null;
      this.conditionnoanswer[index].answerID = action.controls.answerID.value
      //console.log("action", this.verticalStepperStepCondition.controls.condition['controls'][index].controls["answers"].value["answerName"]);
      //console.log("action", this.verticalStepperStepCondition.controls.condition['controls'].values);
      //let pick = this.verticalStepperStepCondition.controls.condition['controls'][index].controls["answers"];
      //pick.patchValue({
      //  answerID: valuex,
      //  answerName: valuey
      //});
      //let pickid = this.verticalStepperStepCondition.controls.condition['controls'][index].controls["answerID"];
      //pickid.setValue(valuex);
      //action.controls.answers.value.answerName = valuey;
      // action.controls.answers.value= null;
      // const control = <FormArray>this.initCondition.controls['answerID'];
      // control.at(index).patchValue(valuex);
      // // this.answerID.at(index).patchValue({
      // //   answerID : valuex
      // // })
      console.log("action", action);//clicked index

      // this.verticalStepperStepCondition.controls['answerID'].patchValue(valuex);
    }
  } 

  inputTL(event) {
    console.log('event inputcity : ', event);
    this.typeaheadTempatLahir
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.tesService.getPasienCity(data, event.target.value))
      )
      .subscribe(Data => {
        console.log("Type: ", Data)
        this.pasienTL = Data;
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienType error : ', err);
        this.pasienTL = [];
        this.ref.markForCheck();
      })
  }

  onfilledTL(event) {
    this.typeaheadTempatLahir
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.tesService.getPasienCity(data, ""))
      )
      .subscribe(Data => {
        console.log("Type: ", Data)
        this.pasienTL = Data;
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienType error : ', err);
        this.pasienTL = [];
        this.ref.markForCheck();
      })

  }

  onchangeTL(event) {
    console.log('ContactTipe set', this.horizontalStepperStepRegister.controls["pasienTempatLahir"]);
    let TL = this.horizontalStepperStepRegister.controls["pasienTempatLahir"];
    TL.setValue(event.cityName);
    console.log('TL', TL);
   
  }

  onchangeContactTipe(event, index) {
    console.log('ContactTipe set', event);
    let contactTipe = this.horizontalStepperStepRegister.controls.pasienContact['controls'][index].controls["messangerType"];
    let contactTipeid = this.horizontalStepperStepRegister.controls.pasienContact['controls'][index].controls["messangerTypeId"];
    contactTipeid.setValue(event.messengerTypeId);
    contactTipe.setValue(event.messangerType);
    console.log('contactTipeid', contactTipeid);
      console.log('contactTipe', contactTipe);
  }

  onfilledContactType(event) {
    this.typeaheadContactType
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.tesService.getPasienContactType(data, ""))
      )
      .subscribe(Type => {
        console.log("Type: ", Type)
        this.pasienContact = Type;
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienType error : ', err);
        this.pasienContact = [];
        this.ref.markForCheck();
      })

  }

  inputContactType(event) {
    console.log('event inputcity : ', event);
    this.typeaheadContactType
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.tesService.getPasienContactType(data, event.target.value))
      )
      .subscribe(Type => {
        console.log("Type: ", Type)
        this.pasienContact = Type;
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienType error : ', err);
        this.pasienContact = [];
        this.ref.markForCheck();
      })
  }

  onfilledjawaban(event,index) {
    this.typeaheadJawaban
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.tesService.getAnswer(index))
      )
      .subscribe(jawaban => {
        console.log("jawaban: ", jawaban)
        this.jawabanarr = jawaban;
        this.ref.markForCheck();
      }, (err) => {
        console.log('jawaban error : ', err);
        this.jawabanarr = [];
        this.ref.markForCheck();
      })

  }

  inputcity(event) {
    console.log('event inputcity : ', event);
    this.typeaheadCity
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.tesService.getPasienCity(data, event.target.value))
      )
      .subscribe(city => {
        console.log("city: ", city)
        this.pasienCity = city;
        console.log("this.pasienCity : ", this.pasienCity )
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienCity error : ', err);
        this.pasienCity = [];
        this.ref.markForCheck();
      })
  }

  onchangeCity(event, index) {
    let city = this.horizontalStepperStepRegister.controls.pasienAddress['controls'][index].controls["addressCity"];
    let cityid = this.horizontalStepperStepRegister.controls.pasienAddress['controls'][index].controls["addressCityId"];
    cityid.setValue(event.cityId);
    city.setValue(event.cityName);
    console.log('city set',event);
  }
  onfilledCity(event) {
    this.typeaheadCity
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.tesService.getPasienCity(data,""))
      )
      .subscribe(city => {
        console.log("city: ", city)
        this.pasienCity = city;
        console.log("this.pasienCity : ", this.pasienCity)
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienCity error : ', err);
        this.pasienCity = [];
        this.ref.markForCheck();
      })

  }

  onchangeTipe(event, index) {
    console.log('Tipe set', this.horizontalStepperStepRegister.controls.pasienAddress['controls'][index]);
    let Tipe = this.horizontalStepperStepRegister.controls.pasienAddress['controls'][index].controls["addressType"];
    let Tipeid = this.horizontalStepperStepRegister.controls.pasienAddress['controls'][index].controls["addressTypeId"];
    Tipeid.setValue(event.addressTypeId);
    Tipe.setValue(event.addressType);
    //console.log('Tipe set', this.horizontalStepperStepRegister.controls.pasienAddress['controls'][index]);
  }

  onfilledType(event) {
    this.typeaheadType
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.tesService.getPasienType(data, ""))
      )
      .subscribe(Type => {
        console.log("Type: ", Type)
        this.pasienType = Type;
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienType error : ', err);
        this.pasienType = [];
        this.ref.markForCheck();
      })

  }

  inputType(event) {
    console.log('event inputcity : ', event);
    this.typeaheadType
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.tesService.getPasienType(data, event.target.value))
      )
      .subscribe(Type => {
        console.log("Type: ", Type)
        this.pasienType = Type;
        this.ref.markForCheck();
      }, (err) => {
        console.log('pasienType error : ', err);
        this.pasienType = [];
        this.ref.markForCheck();
      })
  }

  onfilledBank(event) {
    this.clicked = true;
    this.typeaheadBank
    .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.tesService.getPaymentBank(data))
    )
    .subscribe(paybanks => {
      console.log("paybanks: ", paybanks)
      this.payBanks = paybanks;
      this.ref.markForCheck();
    }, (err) => {
      console.log('payBank error : ', err);
      this.payBanks = [];
      this.ref.markForCheck();
    })

  }

  onfilledCard(event) {
    this.clicked = true;
    this.typeaheadCard
      .pipe(
      startWith(''),
      debounceTime(200),
      switchMap(data => this.tesService.getPaymentCard(data))
      )
      .subscribe(paycards => {
        console.log("paycards: ", paycards)        
        this.payCards = paycards;
        this.ref.markForCheck();
      }, (err) => {
        console.log('payCard error : ', err);
        this.payBanks = [];
        this.ref.markForCheck();
      })

  }

  onChangeBanks(index,event){
    this.payBanksTemps[index] = event.payToId
    console.log('event bank ', this.payBanksTemps[index]);
    let paySelected = this.payBanksTemps[index];


    if (paySelected == "01" ) {
      // this.paymentHidden = !this.paymentHidden
      console.log("this.horizontalStepperStepPayment.controls['paymentViewModel'][index]", this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('payToId'));

      let paymentCardDisabled = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('payCardId')
      let paymentCardNo = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentCardNO')
      let paymentAppCode = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentApprovalCode')
      let paymentTraceCode = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentTraceCode')
      //paymentCardNo.disable()
      //paymentAppCode.disable()
      //paymentTraceCode.disable()
      paymentCardDisabled.setValue("");
      paymentCardNo.setValue("");
      paymentAppCode.setValue("");
      paymentTraceCode.setValue("");
      
      //const disableSel = <FormArray>this.horizontalStepperStepPayment.controls['paymentViewModel'];
      //disableSel.controls.forEach(y => {
      //  console.log("aloha",y);


      //})
      return;
    } else {
      let paymentCardDisabled = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('payCardId')
      let paymentCardNo = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentCardNO')
      let paymentAppCode = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentApprovalCode')
      let paymentTraceCode = this.horizontalStepperStepPayment.controls.paymentViewModel['controls'][index].get('paymentTraceCode')
      paymentCardNo.setValue("");
      paymentAppCode.setValue("");
      paymentTraceCode.setValue("");
      //paymentCardNo.enable()
      //paymentAppCode.enable()
      //paymentTraceCode.enable()

      return;
    }
    // let paySelected = payments.controls.paymentBankID.value
    //let paySelected = this.payment

    //if (paySelected == "01" || paySelected == "14" || paySelected == "15") {
     
    //  this.horizontalStepperStepPayment.patchValue({
    //    paymentNominal: 0,
    //    paymentSisaTanggungan: 0
    //  })
    //  return;
    //  // this.resetPayment();
    //} else {
    //  this.horizontalStepperStepPayment.patchValue({
    //    paymentNominal: 0,
    //    paymentSisaTanggungan: this.savesisa
    //  })

    //}
  }

  constructAll(){
    this.mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]
    this.agrParam = "";
    this.idDocParam = "";
    this.idAddressDocParam = "";
    this.filteredRefs = this.chooseRef.valueChanges
      .pipe(
        startWith(null),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(valref => {
          // console.log("valref: ", valref)
          return this.filter(valref || '')
        })
      );


      this.filteredPatient = this.choosePatient.valueChanges
      .pipe(
        startWith(null),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(val => {
          return this.filterPat(val || '')
        })
      );

      this.filteredDoctor = this.chooseDoc.valueChanges
      .pipe(
        startWith(null),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(val => {
          return this.filterDoc(val || '')
        })
      );

      this.filteredForm = this.chooseForm.valueChanges
      .pipe(
        startWith(null),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(val => {
          return this.filterForm(val || '')
        })
      );



    this.horizontalStepperStepTestErrors = {
      chooseRef: {},
      chooseAgr: {}
    };

    // Horizontal Stepper form error
    this.horizontalStepperStepRegisterErrors = {
      pasienID: {},
      pasienNama: {},
      chooseDate: {},
      pasienUmur: {},
      city: {},
      gender: {},
      religion: {},
      pasienPangkat: {},
      addressLocationID: {},
      addressLocation: {},
      addressName: {},
      addressType: {},
      addressCity: {},
      addressPostCode: {},
      addressPhone: {},
      addressFax: {}
    };

    this.horizontalStepperStepDoctorErrors = {
        chooseDoc : {}
    };

    this.horizontalStepperStepPaymentErrors = {
      payToId: {},
      payCardId: {},
      paymentCardNO: {},
      paymentApprovalCode: {},
      paymentMachineNO: {},
      paymentTraceCode: {},
      paymentKelebihanBayar: {},
      paymentNominal: {},
      paymentSisaTanggungan: {},
      paymentTotal: {},
      paymentNote: {}
    };

    // Vertical Stepper form error
    this.verticalStepperStepTestErrors = {
      targetTes: [{}]
    };

    this.verticalStepperStepConditionErrors = {
      question: {},
      answer: {},
      noteCond: {}
    };

    this.verticalStepperStepScheduleErrors = {
      scheduleName: {},
      date: {},
      time: {}
    };

    this.verticalStepperStepPriceErrors = {
      tesPrc: {},
      amountPrice: {},
      discount: {},
      payStatus: {}
    };

  }

  ngOnInit() {
    this.verifyok = false;
    this.TotalBruto = 0;
    this.TotalDiskon = 0;
    this.TotalNetto = 0;
    this.hid_contact.push(false);
    this.hid_type.push(false);
    this.hid_address.push(false);
    this.paymentTreeWidth = innerWidth - 400 + 'px';
    this.resetAll()
    this.tesService.getPasienPangkat().subscribe(PasienPangkat => this.PasienPangkat = PasienPangkat);
    this.tesService.getPasienAgama().subscribe(PasienAgama => this.PasienAgama = PasienAgama);
    this.tesService.getPasienSex().subscribe(pasienSex => this.pasienSex = pasienSex);
    this.horizontalStepperStepPayment.get('paymentViewModel').valueChanges.subscribe(values => {
      this.totalPayment = 0;
      this.totalPaymentStr = "0";
      const totPay = <FormArray>this.horizontalStepperStepPayment.controls['paymentViewModel'];
      totPay.controls.forEach(x => {
        if (parseInt(x.get('paymentNominal').value)>0){
            //let parsed = 0;
            //if(x.get('paymentNominal').value){
            //  if(x.get('paymentNominal').value.indexOf(this.thousands_separator) > -1)
            //    parsed = +(x.get('paymentNominal').value.replace(this.thousands_separator,""))
            //    else parsed = +(x.get('paymentNominal').value);
            //  }
            //  else parsed = 0;         
            let parsed = parseInt(x.get('paymentNominal').value)
            this.totalPayment += parsed
            //this.totalPaymentStr = this.cpipe.transform(this.totalPayment.toString());
            this.ref.detectChanges
          }
         
      })
    })
  }

  resetAll(){

    this.getRegister()

    this.sourceTes = [];
    this.targetTes = [];
    this.question = '';


    this.myArray = {
      question: '',
      answer: '',
      noteCond: ''
    };
    this.conditionArray.push(this.myArray);

    this.myArray = {
      scheduleID: '',
      date: '',
      time: ''
    };
    this.scheduleArray.push(this.myArray);


    // Reactive Form
    this.resetTes();
    this.resetPasien();
    this.resetDoctor();
    this.resetPayment();

    this.horizontalStepperStepTest.valueChanges.subscribe(() => {
      this.onFormValuesChanged();
    })

    this.horizontalStepperStepRegister.valueChanges.subscribe(() => {
      this.onFormValuesChanged();
    });

    this.horizontalStepperStepDoctor.valueChanges.subscribe(() => {
      this.onFormValuesChanged();
    });

    this.horizontalStepperStepPayment.valueChanges.subscribe(() => {
      this.onFormValuesChanged();
    });

    // Vertical Stepper form stepper
    this.resetCondition();
    this.resetSchedule();
    this.resetPrice();

    this.verticalStepperStepTest.valueChanges.subscribe(() => {
      this.onFormValuesChanged();
    });

    this.verticalStepperStepCondition.valueChanges.subscribe(() => {
      this.onFormValuesChanged();
    });

    this.verticalStepperStepSchedule.valueChanges.subscribe(() => {
      this.onFormValuesChanged();
    });

    this.verticalStepperStepPrice.valueChanges.subscribe(() => {
      this.onFormValuesChanged();
    });
  }

  resetTes(){
    this.horizontalStepperStepTest = this.formBuilder.group({
      chooseRef: ['', Validators.required],
      chooseAgr: ['', Validators.required]
    });

    this.verticalStepperStepTest = this.formBuilder.group({
      targetTes: [[], Validators.required]
    });

  }

  resetCondition(){
    this.verticalStepperStepCondition = this.formBuilder.group({
      condition : this.formBuilder.array([]),
      answers : this.formBuilder.array([])
    });
  }

  resetSchedule(){
    this.verticalStepperStepSchedule = this.formBuilder.group({
      // scheduleTEST: this.formBuilder.array([], Validators.required)
      scheduleTEST: this.formBuilder.array([])
    });
  }

  resetPrice(){
    this.verticalStepperStepPrice = this.formBuilder.group({
      tesPrc: ['', Validators.required],
      amountPrice: ['', Validators.required],
      discount: ['', Validators.required],
      payStatus: ['', Validators.required]
      // tesPrc: [''],
      // amountPrice: [''],
      // discount: [''],
      // payStatus: ['']
    });
  }

  resetPasien(){
    this.horizontalStepperStepRegister = this.formBuilder.group({
      pasienID: [''],
      pasienNama: [''],
      pasienTanggalLahir: [''],
      pasienUmur: [''],
      pasienTempatLahir: [''],
      pasienGender: [''],
      pasienGenderId: [''],
      pasienAgama: [''],
      pasienAgamaId: [''],
      pasienPangkatId: [''],
      pasienPangkat: [''],
      pasienRealDOB: [''],
      pasienAlive: [''],
      pasienNote: [''],
      pasienTitleId: [''],
      pasienTitle: [''],
      nik: [''],
      nip: [''],
      department: [''],
      jobTitle: [''],
      pasienAddress: this.formBuilder.array([this.initAddress()]),
      pasienContact: this.formBuilder.array([this.initContact()])
    });
  }

  resetDoctor(){
    this.horizontalStepperStepDoctor = this.formBuilder.group({
      chooseDoc : ['', Validators.required],
      chooseForm : ['', Validators.required],
      diagnoseNote : ['', Validators.required],
      registerResultTO : this.formBuilder.array([this.initResultTo()])
    });
  }

  resetPayment(){
    this.horizontalStepperStepPayment = this.formBuilder.group({
      paymentViewModel: this.formBuilder.array([this.initPaymentTo()]),
      paymentSubTotal: [''],
      paymentDiscount: [''],
      paymentNominal: [''],
      paymentSisaTanggungan: [''],
      paymentKelebihanBayar: [''],
      paymentTotal: [null],
      paymentNote: ['']
    });
  }

  changePicklist(){
    //console.log("target tes change")
    console.log(this.targetTes.length)
  }

  onTargetFilled(event, isiTes: any = []){
    this.TotalBruto = 0;
    this.TotalDiskon = 0;
    this.TotalNetto = 0;
    let evenItem = event.items[0];
    this.breakLopFor = false;
    console.log("isi", event)
    
    if (event.items.length == 0 && this.sourceTes.length > 0) {
      for (var i = 0; i < this.sourceTes.length; i++) {
        isiTes.push(this.sourceTes[i]);
        console.log("event", this.sourceTes[i])

      }
      for (var i = 0; i < this.sourceTes.length; i++) {
        this.sourceTes.splice(0, this.sourceTes.length)


      }

    } 
    this.tempTest.push(event.items);
    console.log("tempTest", this.tempTest)
    // console.log(evenItem)
    // console.log("target tessssssssssssssssssssss")
    // console.log(isiTes)
    // console.log(isiTes.length)
    // console.log(isiTes.testId)
    // console.log(isiTes.testName)
    this.tesJson = [];
    this.idTes = [];
    this.tesPriceJson = [];
    this.verticalStepperStepTest.reset();
    this.dummyTes = [];
    this.targetTes = [];
    // if(isiTes.length > 0){
    //   for(var i=0; i < isiTes.length; i++){
    //     if(isiTes[i]["testGroupSingle"] == false){
    //       if(isiTes[i]['structuredItemDetail'].length > 0 ){
    //         for(var y=0; y < isiTes[i]['structuredItemDetail'].length; y++){
    //           isiTes.push(isiTes[i]['structuredItemDetail'][y])
    //         }
    //       }
    //     }
    //   }
    // }
    this.dummyTes=isiTes;
    for (var i = 0; i < isiTes.length; i++) {
      console.log("isiTes waaaa", isiTes)
      if (isiTes[i]['testGroupSingle'] == false && isiTes[i]['structuredItemDetail'].length > 0) {
        this.dummyTes.push({
          "realtestId": isiTes[i]['testId'],
          "testId": isiTes[i]['testId'],
          "testName": isiTes[i]['testName'],
          "testTypeName": isiTes[i]['testTypeName'],
          "testGroupName": isiTes[i]['testGroupName'],
          "testScheduleName": isiTes[i]['testScheduleName'],
          "testScheduleId": isiTes[i]['testScheduleId'],
          "testGroupSingle": isiTes[i]['testGroupSingle'],
          "testMasterDetailType": isiTes[i]['testMasterDetailType'],
          "testPrice": isiTes[i]['testPrice'],
          "testDiscount": isiTes[i]['testDiscount'],
          "testPayable": isiTes[i]['testPayable'],
          "structuredItemDetail": []
        });
        for (var y = 0; y < isiTes[i]['structuredItemDetail'].length; y++) {
          this.dummyTes.push({
            "realtestId": isiTes[i]['testId'],
            "testId": isiTes[i]['structuredItemDetail'][y]["testDetailId"],
            "testName": isiTes[i]['structuredItemDetail'][y]["testDetailName"],
            "testTypeName": "Single",
            "testGroupName": isiTes[i]['testGroupName'],
            "testScheduleName": isiTes[i]['structuredItemDetail'][y]["scheduleName"],
            "testScheduleId": isiTes[i]['structuredItemDetail'][y]["scheduleTestId"],
            "testGroupSingle": isiTes[i]['testGroupSingle'],
            "testMasterDetailType": isiTes[i]['structuredItemDetail'][y]["testMasterDetailType"],
            "testPrice": isiTes[i]['structuredItemDetail'][y]["testPrice"],
            "testDiscount": isiTes[i]['structuredItemDetail'][y]["testDiscount"],
            "testPayable": isiTes[i]['structuredItemDetail'][y]["testPayable"],
            "structuredItemDetail":[]
          });


        }
        console.log("waaaaaa", this.dummyTes)
       // isiTes.splice(i, 1)
        this.dummyTes.splice(i, 1)
        console.log("looooooo", this.dummyTes)
        isiTes=this.dummyTes


        console.log("dummyTes", this.dummyTes)        
      }
      this.TotalBruto += isiTes[i]['testPrice'];
      this.TotalDiskon += isiTes[i]['testDiscount'];
      this.TotalNetto += isiTes[i]['testPayable'];
    }
    console.log("isiTes", isiTes)
    for (var value of isiTes) {
      this.targetTes.push(value)
    } console.log("testerpakai", this.targetTes)

    this.testerpakai = "";
    for (var xx = 0; xx < this.targetTes.length; xx++) {
      this.testerpakai+=this.targetTes[xx]['testId'] ;
      if (xx != this.targetTes.length - 1) {
        this.testerpakai += ",";
      }
      
    }
    console.log("testerpakai", this.testerpakai)
    if (isiTes.length > 1) {
      for (var i = 0; i < isiTes.length; i++) {
        //if (this.breakLopFor ) {
        //  break;
        //}
        console.log("isi array waaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", isiTes)
        if ((isiTes[i]['structuredItemDetail'] != null || isiTes[i]['structuredItemDetail'] != undefined) && isiTes[i]['structuredItemDetail'].length > 0) { //ini untuk array
          console.log("isi error   i", i)
          for (var y = 0; y < isiTes[i]['structuredItemDetail'].length; y++) {
            //if (this.breakLopFor) {
            //  break;
            //}
            console.log("isi error   y", y)
            for (var x = 0; x < isiTes.length; x++) {
              //if (this.breakLopFor) {
              //  break;
              //}

              if (i != x) {
                console.log("isi error  xxxxxxxxxxxxxxxxx", isiTes[x])
                if ((isiTes[x]['structuredItemDetail'] != null || isiTes[x]['structuredItemDetail'] != undefined) && isiTes[x]['structuredItemDetail'].length > 0 ) { //ini untuk array yang dihapus bila kembar
                  console.log("isi error  x", isiTes[x]['structuredItemDetail'])

                  for (var z = 0; z < isiTes[x]['structuredItemDetail'].length; z++) {
                    console.log("isi error  z", z)
                    //if (this.breakLopFor) {
                    //  break;
                    //}
                    //console.log("isi array kembar", isiTes[x]['structuredItemDetail'][z])
                    if (isiTes[i]['structuredItemDetail'][y]['testDetailId'] == isiTes[x]['structuredItemDetail'][z]['testDetailId']) {
                      alert("kembar array dengan array")
                      console.log("kembar array dengan array")
                      this.sourceTes.push(isiTes[x])
                      this.targetTes.splice(x, 1)
                      isiTes.splice(x, 1)
                      //this.breakLopFor = true;
                      break;
                    }
                  }

                } else //ini untuk single yang dihapus bila kembar
                {
                  console.log("i, y, x")
                  console.log(i, y, x)
                  if (isiTes[i]['structuredItemDetail'][y]['testDetailId'] == isiTes[x]['testId']) {
                    alert("kembar array dengan single")

                    this.sourceTes.push(isiTes[x])
                    this.targetTes.splice(x, 1)
                    isiTes.splice(x, 1)
                    console.log("kembar array dengan single targetTes", this.targetTes)
                    break;
                  }
                }

              }


            }

          }
        } else //ini untuk single
        {

          for (var x = 0; x < isiTes.length; x++) {

            if (i != x) {
              if (isiTes[x]['structuredItemDetail'].length > 0) { //ini untuk array yang dihapus bila kembar

                for (var z = 0; z < isiTes[x]['structuredItemDetail'].length; z++) {
                
                  if (isiTes[i]['testId'] == isiTes[x]['structuredItemDetail'][z]['testDetailId']) {
                    alert("kembar single dengan array")
                    console.log("kembar single dengan array jum i", isiTes[i])
                    console.log("kembar single dengan array jum x", isiTes[x])
                    console.log("kembar single dengan array", isiTes[x])
                    this.sourceTes.push(isiTes[x])
                    this.targetTes.splice(x, 1)
                    isiTes.splice(x, 1)
                    //this.breakLopFor = true;
                    break;
                  }
                }

              } else //ini untuk single yang dihapus bila kembar
              {
                if (isiTes[i]['testId'] == isiTes[x]['testId']) {
                  alert("kembar single dengan single")
                  console.log("kembar single dengan single")
                  this.sourceTes.push(isiTes[x])
                  this.targetTes.splice(x, 1)
                  isiTes.splice(x, 1)
                  break;
                }
              }
            }


          }

        }

      }
    }
    console.log("isiTes ",isiTes)
    for (var value of isiTes) {
      this.tesJsonDetail = [];
      for (var value1 of value.structuredItemDetail) {
        // console.log("value", value1.testMasterId);
        this.tesJsonDetail.push({
         // "testMasterId": value1.testMasterId,
          "testID": value1.testDetailId,
          "scheduleTestId": value1.testScheduleId,
          "testType ": value1.testMasterDetailType
          //"testDetailName": value1.testDetailName,
          //"testPrice": value1.testPrice,
          //"testDiscount": value1.testDiscount,
          //"testPayable": value1.testPayable,
          //"scheduleName": value1.scheduleName,
          //"scheduleTestId": value1.scheduleTestId,
          //"structuredItemDetail": ""
        });
      }
      this.tesJson.push({
        "testID": value.testId,
        "testType ": value.testMasterDetailType,
        //"testName": value.testName,
        //"testTypeName": value.testTypeName,
        //"testGroupName": value.testGroupName,
        //"testScheduleName": value.testScheduleName,
        "scheduleTestId": value.testScheduleId,
        //"testGroupSingle": value.testGroupSingle,
        //"testPrice": value.testPrice,
        //"testDiscount": value.testDiscount,
        //"testPayable": value.testPayable,
        "structuredItemDetail": this.tesJsonDetail
      });
      console.log("tesJson", this.tesJson);
      this.idTes.push(value.testId);
      // console.log(this.idTes);
      this.tesPriceJson.push({
        "testID": value.testId,
        "priceID": this.idPriceParam,
        "testPriceId": this.idPriceParam,
        "price": value.testPrice,
        "discount": value.testDiscount,
        "payable": value.testPayable
      })
      this.verticalStepperStepTest.patchValue({
        targetTes: value
      })
    }


  }
  

  loadcondelement() {
   
      this.autoclickcond.nativeElement.click()
      console.log("autoclickcond");
   
  }

  loadelementtest() {

    this.autoloadtest.nativeElement.click()
    console.log("autoloadtest");

  }

  loadelementsch() {

    this.autoloadsch.nativeElement.click()
    console.log("autoloadsch");

  }

  loadelementprice() {

    this.autoloadprice.nativeElement.click()
    console.log("autoloadprice");

  }
  

  loadelement() {
    var timer = null;
    this.autoclick.nativeElement.click()
     timer = setTimeout(() => {
        for (var i = 0; i < document.getElementsByClassName("ui-picklist-item ng-star-inserted").length; i++) {
        
          var element = document.getElementsByClassName("ui-picklist-item ng-star-inserted")[i];
          //console.log("element", document.getElementsByClassName("ui-picklist-item ng-star-inserted").length);
          element.setAttribute("style", "display: block");
        }
     }, 50);
    
  }
 
  loaddatafilter(event) {
  
    this.sourceTes = [];
    var tar = null;
    if (event.target.value !="")
    { tar = event.target.value; }
    this.tesService.getTestFilter(this.idPriceParam, tar, this.testerpakai).subscribe(tes => {
      this.sourceTes = tes;
      console.log("source value1", this.sourceTes);
      this.loadelement();
      console.log("source value", event.target.value);
      //event.target.value = "";
    });


  }

  myTimer(event) {
    clearTimeout(this.timeout);
    console.log("mulai");
    this.timeout = setTimeout(() => {
      console.log("selesai");
      if (event.target.placeholder === "Pencarian berdasarkan nama") {
        console.log("source", event);
        this.loaddatafilter(event);

      } else {
        console.log("target");
      }



    }, 50);
  }
  cleartime() {
    clearTimeout(this.timeout);
    console.log("reset");
    
  }

  testt(event) {
    console.log("event", event);
    this.myTimer(event);
       
  }
 
  

  onTargetOuted(event, isiTes: any = []) {
    this.tesJson = [];
    this.idTes = [];
    this.tesPriceJson = [];
    this.TotalBruto = 0;
    this.TotalDiskon = 0;
    this.TotalNetto = 0;
    console.log("tempTest", this.tempTest);
    console.log("event.items", event.items);
    for (var i = 0; i < event.items.length; i++) {
      for (var j = 0; j < this.tempTest.length; j++) {
        for (var k = 0; k < this.tempTest[j].length; k++) {
          //for (var l = 0; l < this.tempTest[j][k]['structuredItemDetail'].length; l++) {
          //  if (event.items[i]["realtestId"] == this.tempTest[j][k]["structuredItemDetail"][l]['testMasterId']) {
          //    //console.log("hapus 1", );
          //    for (var y = 0; y < isiTes.length; y++) {
          //      console.log("isiTes", isiTes[y]);
          //      if (isiTes[y]["realtestId"] == event.items[i]["realtestId"]) {
          //        //console.log("hapus 2 ", y);

          //        isiTes.splice(y, 1);

          //      }
          //    }

          //  }
          //}
          if (event.items[i]["realtestId"] == this.tempTest[j][k]['testId']) {
            this.sourceTes.push(this.tempTest[j][k]);
          }
        }
      }
    }
    for (var i = 0; i < event.items.length; i++) {
      for (var j = 0; j < this.sourceTes.length; j++) {
        if (this.sourceTes[j]['realtestId'] != null) {
          //console.log("this.sourceTes ", this.sourceTes[j]['realtestId']);
          if (event.items[i]["realtestId"] == this.sourceTes[j]['realtestId']) {
            this.sourceTes.splice(j, 1);
          }
        }
      }
    }
    for (var s = 0; s < this.tempTest.length; s++){
      for (var t = 0; t < this.tempTest[s].length; t++) {
        for (var i = 0; i < this.sourceTes.length; i++) {
          console.log("load i ", i);
          for (var j = 0; j < this.sourceTes.length; j++) {
            console.log("load j ", j);
            if (i != j) {
              if (this.sourceTes[i] != null && this.sourceTes[j]['testId'] != null) {
                if (this.sourceTes[i]["testId"] == this.sourceTes[j]['testId']) {
                  console.log("hapus ", j);
                  this.sourceTes.splice(j, 1);
                }
              }
            }
          }
        }
      }
    }
    console.log("this.sourceTes ", this.sourceTes);
    this.testerpakai = "";
    for (var xx = 0; xx < this.targetTes.length; xx++) {
      this.testerpakai += this.targetTes[xx]['testId'];
      if (xx != this.targetTes.length - 1) {
        this.testerpakai += ",";
      }

    }
    console.log("testerpakai", this.testerpakai)
    this.verticalStepperStepTest.reset();
    console.log("event", event);
    for (var value of isiTes) {
      this.tesJsonDetail = [];
      for (var value1 of value.structuredItemDetail) {
        // console.log("value", value1.testMasterId);
        this.tesJsonDetail.push({
        //  "testMasterId": value1.testMasterId,
          "testID": value1.testDetailId,
          "scheduleTestId": value1.testScheduleId,
          "testType": value1.testMasterDetailType
          //"testDetailName": value1.testDetailName,
          //"testPrice": value1.testPrice,
          //"testDiscount": value1.testDiscount,
          //"testPayable": value1.testPayable,
          //"scheduleName": value1.scheduleName,
          //"scheduleTestId": value1.scheduleTestId,
          //"structuredItemDetail": ""
        });
      }
      this.tesJson.push({
        "testID": value.testId,
        "testType": value.testMasterDetailType,
        //"testName": value.testName,
        //"testTypeName": value.testTypeName,
        //"testGroupName": value.testGroupName,
        //"testScheduleName": value.testScheduleName,
        "scheduleTestId": value.testScheduleId,
        //"testGroupSingle": value.testGroupSingle,
        //"testPrice": value.testPrice,
        //"testDiscount": value.testDiscount,
        //"testPayable": value.testPayable,
        "structuredItemDetail": this.tesJsonDetail
      });
      this.idTes.push(value.testId);
      this.tesPriceJson.push({
        "testID": value.testId,
        "priceID": this.idPriceParam,
        "testPriceId": this.idPriceParam,
        "price": value.testPrice,
        "discount": value.testDiscount,
        "payable": value.testPayable
      })
      this.verticalStepperStepTest.patchValue({
        targetTes : value
      })
      this.TotalBruto += value.testPrice;
      this.TotalDiskon += value.testDiscount;
      this.TotalNetto += value.testPayable;
    }
    this.loadelement();
  }

  onValidateInputTest(){
    if(this.chooseRef.valid && this.chooseAgr.valid){

    }
  }

  getRegister() {
    this.tesService.regCreate().subscribe(data => {
      this.createReg = data
      console.log("this.createReg")
      console.log(this.createReg)
    })
  }

  Nextverify(event) {
    
    if (event.checked === true) {
      this.verifyok = true;
    } else { this.verifyok = false;}
  }

  onNewInput(event){
    // console.log("cek cek : ", event.checked)
    // if(event.checked === true){
      this.hid_contact = [];
      this.hid_contact.push(true);
      this.hid_address = [];
      this.hid_address.push(true);
      this.hid_type = [];
      this.hid_type.push(true);
      this.pasienContact = [];
      this.pasienType = [];
      this.pasienCity = [];
      this.patientData = [];
      this.tesService.getPasienTitle().subscribe(pasienTitle => this.PasienTitle = pasienTitle);
      this.choosePatient.reset()
      this.horizontalStepperStepRegister.reset()
      this.resetPasien()
      //this.patientData.patchValue({

      //  pasienID: "",
      //  pasienNama: "",
      //  pasienTempatLahir: "",
      //  pasienTanggalLahir: "",
      //  pasienUmur: "",
      //  pasienGender: "",
      //  pasienAgama: "",
      //  pasienPangkat: "",
      //  pasienAlive: "",
      //  //pasienDOB : data.pasienDOB,
      //  pasienRealDOB: "",
      //  pasienTitle: "",
      //  nik: "",
      //  nip: "",
      //  department: "",
      //  jobTitle: "",
      //  pasienAddress: "",
      //  pasienContact: "",
      //  pasienNote: ""

      //});
      this.horizontalStepperStepRegister.clearValidators();
      this.horizontalStepperStepRegister.clearAsyncValidators();
      console.log("cek cek : ", this.patientData)
    // }
  }

  paymentcek(index, value, event) {
    let total = this.horizontalStepperStepPayment.controls['paymentTotal'].value
    console.log("total", total)
    console.log("this.totalPayment", this.totalPayment)
    if ( value > total) {
      alert("Nominal tidak boleh lebih dari total dan nominal yang diinputkan")
      //lebih = this.totalPayment;
      var element = <HTMLInputElement>document.getElementById("finish");
      element.disabled = true;
      return;
    } 
  }

  clickchange() {
    this.clicked = true;
  }


  calculatePayment(index,value,event) {
    console.log("this.payBanks[index];: ", this.payBanksTemps[index])
    
   
    console.log("payToId: ", this.payBanksTemps)
    var element = <HTMLInputElement>document.getElementById("finish");
    let TotTrans = <HTMLInputElement>document.getElementById("paymentTotal");
    let SisaTrans = <HTMLInputElement>document.getElementById("paymentSisaTanggungan");
    let total,kelebihan,nominal,sisa,lebih;
    if(this.horizontalStepperStepPayment.controls['paymentTotal'].value){
      if(this.horizontalStepperStepPayment.controls['paymentTotal'].value > 0)
        total = this.horizontalStepperStepPayment.controls['paymentTotal'].value;
      else total = this.horizontalStepperStepPayment.controls['paymentTotal'].value;
      }
        else total = 0;
    if(this.horizontalStepperStepPayment.controls['paymentKelebihanBayar'].value){
    if(this.horizontalStepperStepPayment.controls['paymentKelebihanBayar'].value > 0)
    kelebihan = this.horizontalStepperStepPayment.controls['paymentKelebihanBayar'].value
    else kelebihan = this.horizontalStepperStepPayment.controls['paymentKelebihanBayar'].value;
    }else kelebihan = 0;
    if(this.horizontalStepperStepPayment.controls['paymentNominal'].value){
      if(this.horizontalStepperStepPayment.controls['paymentNominal'].value > 0)
        nominal = this.horizontalStepperStepPayment.controls['paymentNominal'].value
        else nominal = +this.horizontalStepperStepPayment.controls['paymentNominal'].value
    }else nominal = 0;
    if(this.horizontalStepperStepPayment.controls['paymentSisaTanggungan'].value){
      if(this.horizontalStepperStepPayment.controls['paymentSisaTanggungan'].value > 0)
    sisa = this.horizontalStepperStepPayment.controls['paymentSisaTanggungan'].value
    else sisa = +this.horizontalStepperStepPayment.controls['paymentSisaTanggungan'].value
    }else sisa = 0;
    if(this.horizontalStepperStepPayment.controls['paymentKelebihanBayar'].value){
      if(this.horizontalStepperStepPayment.controls['paymentKelebihanBayar'].value > 0)
    lebih = this.horizontalStepperStepPayment.controls['paymentKelebihanBayar'].value
    else lebih = +this.horizontalStepperStepPayment.controls['paymentKelebihanBayar'].value
    }else lebih = 0;
    const calculate = <FormArray>this.horizontalStepperStepPayment.controls['paymentViewModel'];
      calculate.controls.forEach(y => {
        let paymentNom, paymentTot;
        paymentNom = y.get('paidAmount')
        paymentTot = y.get('paymentNominal')
        //if (paymentTot > paymentNom) {
        //  alert('Nilai tidak boleh lebih dari total pembayaran')
        //  return;
        //}
        //paymentNom == sisa
        
      })

    //this.horizontalStepperStepPayment.value.paymentNominal = this.cpipe.transform(Nominal.value)
    this.horizontalStepperStepPayment.patchValue({
        paymentSisaTanggungan: total - this.totalPayment < 0 ? 0 : (total - this.totalPayment),
      //paymentSisaTanggungan: (total - value),
     // paymentKelebihanBayar: ((sisa - total) < 0 ? 0 : (sisa - total))
    })
    //if(paySelected == "01" || paySelected == "14" || paySelected == "15") {
    //  // this.paymentHidden = !this.paymentHidden
    //  const calculate = <FormArray>this.horizontalStepperStepPayment.controls['paymentViewModel'];
    //  calculate.controls.forEach(y => {
    //    let paymentNom = y.get('payment')
    //    let paymentTot = y.get('paymentNominal')
    //    if (paymentTot > paymentNom) {
    //      alert('Nilai tidak boleh lebih dari total pembayaran')
    //      return;
    //    }
    //    paymentNom == sisa
        
    //  })
    //}
    console.log("totalPayment: ", this.totalPayment)
    console.log("this.payBanksTemps[index].payBankId: ", this.payBanksTemps[index])
    let paySelected = this.payBanksTemps[index];
    if (paySelected == "01" ) {
      this.horizontalStepperStepPayment.patchValue({
        //paymentSisaTanggungan: ((total - this.totalPayment) < 0 ? 0 : (total - this.totalPayment)),
        //paymentSisaTanggungan: (total - value),
        paymentKelebihanBayar: this.totalPayment - total < 0 ? 0 : (this.totalPayment - total)
      })
      console.log("ini 1")
      return;
    } else {
      
      if (this.totalPayment > total) {
        alert("Nominal tidak boleh lebih dari total")
        //lebih = this.totalPayment;
        console.log("ini bukan")
        var element = <HTMLInputElement>document.getElementById("finish");
        element.disabled = true;
        return;
      } else {
        console.log("ini bukan kurang")
      }
    }
    
    //TotTrans.value = this.cpipe.transform(total.toString())
    //SisaTrans.value = this.cpipe.transform(this.horizontalStepperStepPayment.controls['paymentSisaTanggungan'].value)

    // else if (paySelected == "14" || paySelected == "15") {
    //   if (this.horizontalStepperStepPayment.controls['paymentNominal'].value > 0 && this.horizontalStepperStepPayment.controls['paymentNote'].value == !""){
    //     element.disabled = false;
    //     if (this.horizontalStepperStepPayment.controls['paymentNominal'].value > total) {
    //       alert("Nominal tidak boleh lebih dari total")
    //       element.disabled = true;

    //       // LebihTrans.value = this.cpipe.transform(this.horizontalStepperStepPayment.controls['paymentKelebihanBayar'].value)
    //       return;
    //       // this.horizontalStepperStepPayment.controls['paymentNominal'].value == 0;
    //     }
    //   }
    // } else if (paySelected == "01") {
    //   if (this.horizontalStepperStepPayment.controls['paymentNominal'].value > 0 && this.horizontalStepperStepPayment.controls['paymentNote'].value == !"" ) {
    //     element.disabled = false;
    //     return;
    //     // this.horizontalStepperStepPayment.controls['paymentNominal'].value == 0;
    //   }
    // }

    

    //console.log("paymentNominal", this.cpipe.transform(this.horizontalStepperStepPayment.controls['paymentNominal'].value))
    this.horizontalStepperStepPayment.patchValue({
      //paymentNominal: this.cpipe.transform(this.horizontalStepperStepPayment.controls['paymentNominal'].value),
      //paymentKelebihanBayar: this.cpipe.transform(this.horizontalStepperStepPayment.controls['paymentKelebihanBayar'].value),
      //paymentSisaTanggungan: this.cpipe.transform(this.horizontalStepperStepPayment.controls['paymentSisaTanggungan'].value),
      //paymentTotal: this.cpipe.transform(this.horizontalStepperStepPayment.controls['paymentTotal'].value)

      paymentNominal: this.totalPayment,
      paymentKelebihanBayar: this.horizontalStepperStepPayment.controls['paymentKelebihanBayar'].value > 0 ? this.horizontalStepperStepPayment.controls['paymentKelebihanBayar'].value : "0",
      paymentSisaTanggungan: this.horizontalStepperStepPayment.controls['paymentSisaTanggungan'].value,
      paymentTotal: this.horizontalStepperStepPayment.controls['paymentTotal'].value
    })
    // nominal.setValue(this.totalPayment);
  }

  // getSelected(payments) {


  //   let paySelected = payments.controls.paymentBankID.value

  //   if (paySelected == "01" || paySelected == "14" || paySelected == "15") {
  //     this.horizontalStepperStepPayment.patchValue({
  //       paymentNominal: 0,
  //       paymentSisaTanggungan: 0
  //     })
  //     // this.resetPayment();
  //   } else {
  //     this.horizontalStepperStepPayment.patchValue({
  //       paymentNominal: 0,
  //       paymentSisaTanggungan: this.savesisa
  //     })

  //   }
  // }

  private expandRecursive(summaryTest, isExpand: boolean) {
    summaryTest.expanded = isExpand;
    if (summaryTest.children) {
      summaryTest.children.forEach(summaryTest => {
        this.expandRecursive(summaryTest, isExpand);
      });
    }
  }



  onFormValuesChanged() {
    for (const field in this.formErrors) {
      if (!this.formErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.formErrors[field] = {};

      // Get the control
      const control = this.form.get(field);

      if (control && control.dirty && !control.valid) {
        this.formErrors[field] = control.errors;
      }
    }
  }

  hidKota(event, action, index) {
    console.log("action hidKota", action[index].value.addressCity)
    this.hid_address[index]=true;
  }

  hidtype(event, action, index) {
    console.log("action hidtype", action[index].value.addressType)
    this.hid_type[index] = true;
  }
  
  hidContacttype(event, action, index) {
    //console.log("action hidtype", action[index].value.addressType)
    this.hid_contact[index] = true;
  }

  calculateAge(e:any){
    var birthdate:Date;
    if(e.target.value.indexOf('y') == -1 && this.horizontalStepperStepRegister.value.pasienTanggalLahir){
      birthdate = this.horizontalStepperStepRegister.value.pasienTanggalLahir;
      birthdate = new Date(this.horizontalStepperStepRegister.value.pasienTanggalLahir.substr(6,4)+'-'+this.horizontalStepperStepRegister.value.pasienTanggalLahir.substr(3,2)+'-'+this.horizontalStepperStepRegister.value.pasienTanggalLahir.substr(0,2)+'T00:00:00+01:00');
      var ageDifMs = Math.floor(Date.now() - birthdate.getTime());
      var ageDate = new Date(ageDifMs); // miliseconds from epoch
      var message = Math.abs(ageDate.getUTCFullYear() - 1970) + " tahun "
      message += Math.abs(ageDate.getUTCMonth()) + " bulan " 
      message += Math.abs(ageDate.getUTCDate() - 1) + " hari"
      this.horizontalStepperStepRegister.controls['pasienUmur'].setValue(message);
    }
  }

  finishHorizontalStepper(data, stepper, Vstepper) {
    this.horizontalStepperStepPayment.controls['paymentNominal'].value == parseInt(this.horizontalStepperStepPayment.controls['paymentNominal'].value, 10)
    this.addJsonCreateValue()
    data = this.createReg
    console.log(data)
    this.tesService.postReg(data).subscribe(res => {
      if(res){
          // this.successMessage = true;
          // setTimeout(function () {
          //         this.successMessage = false;
          //         this.router.navigate(['/apps/cust/reg_list']) ;
          //     }.bind(this), 2000)

          alert('Registrasi Berhasil!');
          var element = <HTMLInputElement>document.getElementById("finish");
          element.disabled = true;
          this.regNo = res;
          // this.router.navigate(['/apps/cust/reg_list']) ;
      }
      // else{
          // this.errorMessage = true;
          // setTimeout(function (){
          //     this.errorMessage = false;
          // }.bind(this), 2000)
      //     alert('Registrasi Gagal!')
      // }
  },error => {
      // this.errorMessage = true;
      // setTimeout(function (){
      //     this.errorMessage = false;
      // }.bind(this), 2000)
      alert('Registrasi Gagal!')
  });
    // this.sourceTes = [];
    // this.targetTes = [];
    // this.chooseAgr.reset();
    // this.chooseRef.reset();
    // this.choosePatient.reset();
    // this.chooseDoc.reset();
    // this.chooseForm.reset();
    // this.horizontalStepperStepTest.reset();
    // this.verticalStepperStepTest.reset();
    // this.verticalStepperStepCondition.reset();
    // this.verticalStepperStepSchedule.reset();
    // this.verticalStepperStepPrice.reset();
    // this.horizontalStepperStepRegister.reset();
    // this.horizontalStepperStepDoctor.reset();
    // this.horizontalStepperStepPayment.reset();
    // this.resetAll()
    // stepper.selectedIndex = 0;
    // Vstepper.selectedIndex = 0;
    
  }

  finishVerticalStepper() {
    alert('You have finished the vertical stepper!');
  }

  gotoBuktiPemeriksaan(){
    window.open(this.urlBuktiPemeriksaan + this.regNo);
  }

  gotoBuktiTagihan(){
    window.open(this.urlBuktiTagihan + this.regNo);
  }

  gotoKartuKontrol(){
    window.open(this.urlKartuKontrol + this.regNo);
  }

  gotoKwitansi(){
    window.open(this.urlKwitansi + this.regNo);
  }

  gotoNotaPembayaran(){
    window.open(this.urlNotaPembayaran + this.regNo);
  }

  gotoTandaTerima(){
    window.open(this.urlTandaTerima + this.regNo);
  }
}

export class FilesDataSourcePrice extends DataSource<any>
{
    private detailPriceSubject = new BehaviorSubject<any>([]);

    private loadingDetailPrice = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailPrice.asObservable();

    constructor(private tesService: TesService) {
        super()
    }

    loadPrice(idtes: any, idpriceparams: any){
        this.loadingDetailPrice.next(true);

        this.tesService.getPrice(idtes, idpriceparams).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailPrice.next(false))
        )
        .subscribe(price => this.detailPriceSubject.next(price))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailPriceSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailPriceSubject.complete();
        this.loadingDetailPrice.complete();
    }
}
