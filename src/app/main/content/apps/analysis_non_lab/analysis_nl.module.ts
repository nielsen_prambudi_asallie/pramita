import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { ProcessSampleInstrumentNonLabComponent } from './process_sample_instrument_nl/process_sample_instrument_nl.component';
import { ProcessSampleInstrumentNonLabService } from './process_sample_instrument_nl/process_sample_instrument_nl.service';
import { ProcessSamplePasienNonLabComponent } from './process_sample_pasien_nl/process_sample_pasien_nl.component';
import { ProcessSamplePasienNonLabService } from './process_sample_pasien_nl/process_sample_pasien_nl.service';
import { ProcessSampleDetailNonLabComponent } from './process_sample_detail_nl/process_sample_detail_nl.component';
import { ProcessSampleDetailNonLabService } from './process_sample_detail_nl/process_sample_detail_nl.service';
import { ResultInstrumentNonLabComponent } from './result_instrument_nl/result_instrument_nl.component';
import { ResultPasienNonLabComponent } from './result_pasien_nl/result_pasien_nl.component';
import { ResultInstrumentNonLabService } from './result_instrument_nl/result_instrument_nl.service';
import { ResultPasienNonLabService } from './result_pasien_nl/result_pasien_nl.service';
import { ResultDetailNonLabComponent } from './result_detail_nl/result_detail_nl.component';
import { ResultDetailNonLabService } from './result_detail_nl/result_detail_nl.service';
import { VerificationPasienNonLabComponent } from './verification_pasien_nl/verification_pasien_nl.component';
import { VerificationPasienNonLabService } from './verification_pasien_nl/verification_pasien_nl.service';
import { VerificationDetailNonLabComponent } from './verification_detail_nl/verification_detail_nl.component';
import { VerificationDetailNonLabService } from './verification_detail_nl/verification_detail_nl.service';
import { AuthorizationPasienNonLabComponent } from './authorization_pasien_nl/authorization_pasien_nl.component';
import { AuthorizationPasienNonLabService } from './authorization_pasien_nl/authorization_pasien_nl.service';
import { AuthorizationDetailNonLabComponent } from './authorization_detail_nl/authorization_detail_nl.component';
import { AuthorizationDetailNonLabService } from './authorization_detail_nl/authorization_detail_nl.service';
import { AgmCoreModule } from '@agm/core';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { CKEditorModule } from 'ng2-ckeditor';


const routes: Routes = [
    {
        path     : 'process_sample_instrument_nl',
        component: ProcessSampleInstrumentNonLabComponent
    },
    {
        path     : 'process_sample_instrument_nl/:insid/:name',
        component: ProcessSamplePasienNonLabComponent
    },
    {
        path     : 'process_sample_pasien_nl/:insid/:name/:pasid/:age',
        component: ProcessSampleDetailNonLabComponent
    },
    {
        path     : 'result_instrument_nl',
        component: ResultInstrumentNonLabComponent
    },
    {
        path     : 'result_instrument_nl/:insid/:methodId/:insname',
        component: ResultPasienNonLabComponent
    },
    {
        path     : 'result_pasien_nl/:insid/:methodId/:insname/:age/:pasid',
        component: ResultDetailNonLabComponent
    },
    {
        path     : 'verification_pasien_nl',
        component: VerificationPasienNonLabComponent
    },
    {
        path     : 'verification_pasien_nl/:regid/:age',
        component: VerificationDetailNonLabComponent
    },
    {
        path     : 'authorization_pasien_nl',
        component: AuthorizationPasienNonLabComponent
    },
    {
        path     : 'authorization_pasien_nl/:regid/:age',
        component: AuthorizationDetailNonLabComponent
    }
    
    

];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
        CKEditorModule,
        FuseWidgetModule,
        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    declarations: [
        ProcessSampleInstrumentNonLabComponent,
        ProcessSamplePasienNonLabComponent,
        ProcessSampleDetailNonLabComponent,
        ResultInstrumentNonLabComponent,
        ResultPasienNonLabComponent,
        ResultDetailNonLabComponent,
        VerificationPasienNonLabComponent,
        VerificationDetailNonLabComponent,
        AuthorizationDetailNonLabComponent,
        AuthorizationPasienNonLabComponent 
    ],
    providers   : [
        ProcessSampleInstrumentNonLabService,
        ProcessSamplePasienNonLabService,
        ProcessSampleDetailNonLabService,
        ResultInstrumentNonLabService,
        ResultPasienNonLabService,
        ResultDetailNonLabService,
        VerificationPasienNonLabService,
        VerificationDetailNonLabService,
        AuthorizationPasienNonLabService,
        AuthorizationDetailNonLabService
    ]
})
export class AnalysisNonLabModule
{
}
