import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PARAMETERS } from '@angular/core/src/util/decorators';
import {map} from "rxjs/operators";

const SERVER_URL = 'https://202.146.232.86:4000'

@Injectable()
export class AuthorizationPasienService
{

    constructor(
        private http: HttpClient
    )
    {
    }

    getAuthPasien(pageIndex, pageSize, filter):  Observable<any> {

        return this.http.get(SERVER_URL + '/Api/Register/AuthAnalyst/' + pageIndex + '/' + pageSize + '/' + filter)
        .pipe(
            map(res =>  res)
            // console.log(res['regNo'])
        );
    }
}
