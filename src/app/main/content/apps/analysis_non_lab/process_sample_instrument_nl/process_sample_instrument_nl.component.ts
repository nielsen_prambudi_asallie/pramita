import { Component, ElementRef, OnInit, ViewChild, AfterViewInit  } from '@angular/core';
import { ProcessSampleInstrumentNonLabService } from './process_sample_instrument_nl.service';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import {ActivatedRoute} from "@angular/router";
import { fuseAnimations } from '../../../../../core/animations';
import { MatPaginator, MatSort, PageEvent, MatTableDataSource } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import {debounceTime, distinctUntilChanged, startWith, tap, delay} from 'rxjs/operators';
import {merge} from "rxjs/observable/merge";
import {fromEvent} from 'rxjs/observable/fromEvent';
import { FuseUtils } from '../../../../../core/fuseUtils';
import {of} from "rxjs/observable/of";
import {catchError, finalize} from "rxjs/operators";

@Component({
    selector   : 'process_sample_instrument_nl',
    templateUrl: './process_sample_instrument_nl.component.html',
    styleUrls  : ['./process_sample_instrument_nl.component.scss'],
    animations : fuseAnimations
})
export class ProcessSampleInstrumentNonLabComponent implements OnInit, AfterViewInit
{
    dataSource: FilesDataSource;
    displayedColumns = [ 'instrumentName', 'instrumentCategoryName'];
    selectedRowIndex: any;

    length: number;
    pageEvent: PageEvent;
    pageIndex: any = this.pageEvent;
    pageSize: any = this.pageEvent;
    pageSizeOptions = [10, 25, 100];


    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private processSampleInstrumentNonLabService: ProcessSampleInstrumentNonLabService,
        private route : ActivatedRoute
    )
    {
    }



    ngOnInit()
    {
        // this.course = this.route.snapshot.data["course"];

        this.dataSource = new FilesDataSource(this.processSampleInstrumentNonLabService);

        this.dataSource.loadInstruments(this.length, 0, 10, '');
    }

    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        fromEvent(this.filter.nativeElement,'keyup')
            .pipe(
                debounceTime(150),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;

                    this.loadInstrumentsPage();
                })
            )
            .subscribe();

        // merge(this.sort.sortChange, this.paginator.page)
        this.paginator.page
        .pipe(
            tap(() => this.loadInstrumentsPage())
        )
        .subscribe();

    }

    loadInstrumentsPage() {
        this.dataSource.loadInstruments(
            this.length,
            this.paginator.pageIndex,
            this.paginator.pageSize,
            this.filter.nativeElement.value);
    }

   onRowClicked(dataSource) {
    console.log('Row clicked: ', dataSource);
    console.log('Row clicked id', dataSource.regid);
    }
}

export class FilesDataSource extends DataSource<any>
{
    private instrumentsSubject = new BehaviorSubject<any>([]);
    public totalData : any;

    private loadingInstruments = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingInstruments.asObservable();

    constructor(
        private processSampleInstrumentNonLabService: ProcessSampleInstrumentNonLabService,
    )
    {
        super()

    }

    loadInstruments(
        length: number,
        pageIndex:number,
        pageSize:number,
        filter:string) {

        this.loadingInstruments.next(true);

        this.processSampleInstrumentNonLabService.getProcessSampleNonLab(pageIndex, pageSize, filter).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingInstruments.next(false))
            )
            .subscribe(instruments => {
               this.instrumentsSubject.next(instruments.data)
               this.totalData = instruments.totalData
            });



        }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.instrumentsSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.instrumentsSubject.complete();
        this.loadingInstruments.complete();
    }
}
