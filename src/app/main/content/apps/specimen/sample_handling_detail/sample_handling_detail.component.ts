import { Component, OnDestroy, ElementRef, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { SampleHandlingDetailService } from './sample_handling_detail.service';
import { fuseAnimations } from '../../../../../core/animations';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar } from '@angular/material';
import { Location } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from "rxjs/observable/of";
import { catchError, finalize } from "rxjs/operators";
import { Options } from 'selenium-webdriver';

export interface Detail {
  nama?;
  no?;
  estVol?;
  // vol?;
  conditionId?;
  condition?;
  note?;
  process?: boolean;
  processDate?: "yyyy-MM-dd";
  processUser?;
  received?: boolean;
  receiveDate?: "yyyy-MM-dd";
  receiveUser?;
  sampleHandlingId?;
}

export interface Detail2 {
  registerTestId?;
  testName?;
  cito?: boolean;
  scheduleName?;
}
@Component({
    selector     : 'sample_handling_detail',
    templateUrl  : './sample_handling_detail.component.html',
    styleUrls    : ['./sample_handling_detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class SampleHandlingDetailComponent implements OnInit {
  specimen_handling_detail: { regid: number, regno: any, regdate: any, name: any }
  regid: any;
  regno: any;
  regdate: any;
  name: any;
  user: any;
  errorMessage: boolean;
  successMessage: boolean;
  tampungTestCito: any = [];
  createData: any = [];
  data: any;
  createSpec: any = [];
  detail: Detail[];
  detail2: Detail2[];
  processSpec: FormGroup;
  patientNoteGroup: FormGroup;
  physicianNoteGroup: FormGroup;
  referralNoteGroup: FormGroup;
  labNoteGroup: FormGroup;
  manyTest: FormGroup;
  physicianGroup : FormGroup;
  dataSource: FileDetailSource;
  dataSourceSch: FileDetailSourceSch;
  // dataSourcePhy : FileDetailSourcePhy;
  dataSourceSpechand: FileDetailSourceSpechand;
  dataSourceSamhand: FileDetailSourceSamhand;
  dataSourceLabNote: FileDetailSourceLabNote;
  dataSourceItem : FileDetailSourceItem;
  dataSourceSpc : FileDetailSourceSpc;
  drawer: string;
  clickProcess = [];
  clickCito: boolean = false;
  specStat: any;
  defStat: any;

  displayedColumns = ['question', 'answer', 'note'];

  displayedColumns2 = ['schedule', 'test', 'day', 'time'];

  // displayedColumns3 = ['nama', 'estVol', 'no', 'bodySite', 'loCSite', 'drawSite', 'drawUser', 'draw', 'note', 'received', 'receiveDate', 'receiveUser'];
  displayedColumns3 = ['nama', 'no', 'estVol', 'condition','note', 'receive', 'receiveDate', 'receiveUser', 'send', 'sendDate', 'sendUser'];

  displayedColumns4 = ['nama', 'no', 'estVol', 'condition', 'note', 'send', 'sendDate', 'sendUser', 'process', 'processDate', 'processUser'];
  displayedColumnsNoteLab = ['no', 'note', 'user', 'date'];
  displayedColumnsItem= ['no', 'testName', 'cito', 'scheduleName'];
  displayedColumnsSpc = ['nama', 'no', 'estVol', 'received', 'process', 'verification', 'condition', 
    'note', 'receiveDate', 'receiveUser', 'processDate', 'processUser'];

  // displayedColumnsPhy= ['doctorName', 'addressName', 'addressLocation', 'cityName',
  // 'formTypeName', 'specialtyName'];

  initProc(dra) {
    const group = this.formBuilder.group({
      nama: dra.nama,
      no: dra.no,
      estVol: dra.estVol,
      vol: dra.vol,
      conditionId: dra.condition,
      condition: dra.condition,
      note: dra.note,
      process: dra.process,
      processDate: dra.processDate,
      processUser: (dra.process == true ? "admin" : ""),
      sampleHandlingId: dra.sampleHandlingId,
      registerSpecimenId: dra.registerSpecimenId
    })
    return group;
  }


  //getClickDraw(dr) {
  //  if (this.clickDraw = !this.clickDraw) {
  //    dr.controls.drawUser.value = "admin";
  //    // dr.controls.condition.value = "berhasil";
  //  } else {
  //    dr.controls.drawUser.value = "";
  //    // dr.controls.condition.value = "";
  //  }
  //}

  getClickProc(dr, index) {
    console.log("dr", dr)
    if (this.clickProcess[index] != true) {
      dr.controls.processUser.value = "admin";
      this.clickProcess[index] = true;
    } else {
      dr.controls.processUser.value = "";
      this.clickProcess[index] = false;
    }
  }

  initTest(tes) {
    const group = this.formBuilder.group({
      registerTestId: tes.registerTestId,
      testName: tes.testName,
      cito: tes.cito,
      scheduleName: tes.scheduleName
    })
    //if (tes.cito = 0) {
    //  tes.cito = true
    //} else {
    //  tes.cito = false
    //}
    return group;

  }



  passcito(index, value) {
    this.tampungTestCito[index].cito = value.controls.cito.value, value.controls.registerTestId.value;
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sampleHandlingDetailService: SampleHandlingDetailService,
    private formBuilder: FormBuilder) {

      this.processSpec = this.formBuilder.group({
        processSpecArray: this.formBuilder.array([])
      })

    // console.log(this.drawReceive.value['drawUser'] = "admin")

    this.patientNoteGroup = this.formBuilder.group({
        patientNote: [''],
        patientName: [''],
        regDate: [''],
        patientId: ['']
    })

    this.physicianNoteGroup = this.formBuilder.group({
      physicianNote: [''],
      physicianName: [''],
      regDate: ['']
    })

    this.referralNoteGroup = this.formBuilder.group({
      referralNote: [''],
      companyName: [''],
      regDate: ['']
    })

    this.labNoteGroup = this.formBuilder.group({
      // labNote: ['']
      labNotes: this.formBuilder.array([this.initNote()])
    })

    this.manyTest = this.formBuilder.group({
      testArray: this.formBuilder.array([])
    })

    this.physicianGroup = this.formBuilder.group({
      frontTitle    : [''],
      doctorName : [''],
      endTitle : [''],
      addressName : [''],
      addressLocation : [''],
      cityName : [''],
      formTypeName : [''],
      specialtyName : ['']
    })
  }

  ngOnInit() {
    // private loadingSpecimensDetail = new BehaviorSubject<boolean>(false);
    // private specimensDetailSubject = new BehaviorSubject<any>([]);

    this.specimen_handling_detail = {
      regid: this.route.snapshot.params['regid'],
      regno: this.route.snapshot.params['regno'],
      regdate: this.route.snapshot.params['regdate'],
      name: this.route.snapshot.params['name']
    }
    this.regid = this.specimen_handling_detail['regid'];
    this.regno = this.specimen_handling_detail['regno'];
    this.regdate = this.specimen_handling_detail['regdate'];
    this.name = this.specimen_handling_detail['name'];

    this.sampleHandlingDetailService.createSampleHandle(this.regid).subscribe(create => {
      this.createData = create;
      console.log("create: ", create)
      this.physicianNoteGroup.patchValue({
        physicianNote: create[0].doctornote,
        physicianName: create[0].doctorName,
        regDate: create[0].regDate
       });
      this.patientNoteGroup.patchValue({
        patientNote: create[0].patientNote,
        patientName: create[0].patientName,
        regDate: create[0].regDate,
        patientId: create[0].patientID
       });
      this.referralNoteGroup.patchValue({
        referralNote: create[0].referalNote,
        companyName: create[0].referalCompanyName,
        regDate: create[0].regDate
      });
    })
    this.sampleHandlingDetailService.getSpecimensStatus().subscribe(
      specStat => {this.specStat = specStat; this.defStat = specStat[0].specimenStatusId});

    //const control = <FormArray>this.processSpec.controls['processSpecArray'];
    //this.sampleHandlingDetailService.getSampleHandleDetail(this.regid).then(dra => {
    //  this.detail = dra
    //  dra.forEach(x => {
    //    this.clickProcess.push(x.process)
    //    control.push(this.initProc(x))
    //    console.log("init Dra re",x)
    //  });
    //})


    const control1 = <FormArray>this.manyTest.controls['testArray'];
    this.sampleHandlingDetailService.getSampleHandleDetailTest(this.regid).then(tes => {
      this.detail2 = tes
      tes.forEach(y => {
        control1.push(this.initTest(y))
        console.log("init test", y)
        this.tampungTestCito.push({
          "testId": y.registerTestId,
          "testName": y.testName,
          "cito": y.cito,
          "scheduleName": y.scheduleName
        });
      });
    })

    this.sampleHandlingDetailService.getSampleHandleDetailPhy(this.regid)
      .then(phys => {
        this.physicianGroup.patchValue({
          frontTitle: phys.frontTitle,
          doctorName : phys.doctorName,
          endTitle: phys.endTitle,
          addressName : phys.addressName,
          addressLocation : phys.addressLocation,
          cityName : phys.cityName,
          formTypeName : phys.formTypeName,
          specialtyName : phys.specialtyName
        })
      });

    this.dataSource = new FileDetailSource(this.sampleHandlingDetailService);
    this.dataSource.loadDetail(this.regid)

    this.dataSourceSch = new FileDetailSourceSch(this.sampleHandlingDetailService);
    this.dataSourceSch.loadDetailSch(this.regid)

    this.dataSourceSpechand = new FileDetailSourceSpechand(this.sampleHandlingDetailService);
    this.dataSourceSpechand.loadDetailSpechand(this.regid)

    this.dataSourceSamhand = new FileDetailSourceSamhand(this.sampleHandlingDetailService);
    this.dataSourceSamhand.loadDetailSamhand(this.regid)

    this.dataSourceLabNote = new FileDetailSourceLabNote(this.sampleHandlingDetailService);
    this.dataSourceLabNote.loadDetailLabNote(this.regid)

    // this.dataSourcePhy = new FileDetailSourcePhy(this.sampleHandlingDetailService);
    // this.dataSourcePhy.loadDetailPhy(this.regid)

    this.dataSourceItem = new FileDetailSourceItem(this.sampleHandlingDetailService);
    this.dataSourceItem.loadDetailItem(this.regid)

    this.dataSourceSpc = new FileDetailSourceSpc(this.sampleHandlingDetailService);
    this.dataSourceSpc.loadSpc(this.regid)
  }

  initNote() {
    return this.formBuilder.group({
      LabNote: [''],
      user: 'admin',
      // date: [''],
      RegisterId: this.route.snapshot.params['regid']
    })
  }

  addNoteValue() {
      const control = <FormArray>this.labNoteGroup.controls['labNotes'];
      control.push(this.initNote())
  }

  deleteNoteValue(index) {
      if(index > 0){
          const control = <FormArray>this.labNoteGroup.controls['labNotes'];
          control.removeAt(index);
      }
  }

  // sendData() {
  //     console.log(this.drawReceive.value["drawReceiveArray"])
  //     console.log(this.regid);
  //     // console.log(this.dataSource['value'])
  // }

  clickprocessed(event) {
    this.dataSourceSpc.ProcessAll(event);
  }
  clickreceived(event) {
    this.dataSourceSpc.ReceiveAll(event);
  }

  cito(event, index) {
    this.dataSourceItem.saveCito(event, index)
  }
  process(event, index){
    // console.log("process: ", event)
    this.dataSourceSpc.saveProcess(event, index)
  }
  receive(event, index){
    this.dataSourceSpc.saveReceive(event, index)
  }
  note(event, index){
    this.dataSourceSpc.saveNote(event, index)
  }
  cond(event, index){
    // console.log("cond: ", event)
    this.dataSourceSpc.saveCondID(event, index)
  }
  stat(event, index){
    // console.log("cond: ", event)
    this.dataSourceSpc.saveStatID(event, index)
  }

  submitSpec(data) {
    this.createSpec = {
      registerId: this.regid,
      patientNote : this.patientNoteGroup.value['patientNote'],
      referalNote : this.referralNoteGroup.value['referralNote'],
      doctornote : this.physicianNoteGroup.value['physicianNote'],
      regNo: this.regno,
      regDate: this.regdate,
      labNote : this.labNoteGroup.value['labNotes'],
      // cito: this.tampungTestCito,
      cito: this.dataSourceItem.dataItem(),
      specimenCollectionForm: null,
      specimenHandlingForm: null,
      // sampleHandlingForm: this.processSpec.value['processSpecArray']
      sampleHandlingForm : this.dataSourceSpc.dataSpc(),
    }
    // console.log(this.createSpec)
    data = this.createSpec
    this.sampleHandlingDetailService.postSampleHandle(data).subscribe(res => {
      if(res == 'Submited'){
          this.successMessage = true;
          setTimeout(function () {
                  this.successMessage = false;
                  this.router.navigate(['/apps/specimen/sample_handling']) ;
              }.bind(this), 2000)

          // alert('Result Berhasil!');
      }else{
          this.errorMessage = true;
          setTimeout(function (){
              this.errorMessage = false;
          }.bind(this), 2000)
      }
  },error => {
      this.errorMessage = true;
      setTimeout(function (){
          this.errorMessage = false;
      }.bind(this), 2000)
  });
    
  }
}

export class FileDetailSource extends DataSource<any>
{
  private detailSampleSubject = new BehaviorSubject<any>([]);

  private loadingDetailSample = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingDetailSample.asObservable();

  constructor(private sampleHandlingDetailService: SampleHandlingDetailService) {
    super()
  }

  loadDetail(regid: any) {
    this.loadingDetailSample.next(true);

    this.sampleHandlingDetailService.getSampleHandleDetailCond(regid).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingDetailSample.next(false))
    )
      .subscribe(detspec => this.detailSampleSubject.next(detspec))
  }

  connect(collectionViewer: CollectionViewer): Observable<any> {
    console.log("Connecting data source");
    return this.detailSampleSubject.asObservable();
  }


  disconnect(collectionViewer: CollectionViewer): void {
    this.detailSampleSubject.complete();
    this.loadingDetailSample.complete();
  }
}

export class FileDetailSourceSch extends DataSource<any>
{
  private detailSampleSubjectSch = new BehaviorSubject<any>([]);

  private loadingDetailSampleSch = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingDetailSampleSch.asObservable();

  constructor(private sampleHandlingDetailService: SampleHandlingDetailService) {
    super()
  }

  loadDetailSch(regid: any) {
    this.loadingDetailSampleSch.next(true);

    this.sampleHandlingDetailService.getSampleHandleDetailSch(regid).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingDetailSampleSch.next(false))
    )
      .subscribe(detspec => this.detailSampleSubjectSch.next(detspec))
  }

  connect(collectionViewer: CollectionViewer): Observable<any> {
    console.log("Connecting data source");
    return this.detailSampleSubjectSch.asObservable();
  }


  disconnect(collectionViewer: CollectionViewer): void {
    this.detailSampleSubjectSch.complete();
    this.loadingDetailSampleSch.complete();
  }
}

export class FileDetailSourceLabNote extends DataSource<any>
{
    private detailLabNoteSubject = new BehaviorSubject<any>([]);

    private loadingDetailLabNote = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailLabNote.asObservable();

    constructor(private sampleHandlingDetailService: SampleHandlingDetailService) {
        super()
    }

    loadDetailLabNote(regid: any){
        this.loadingDetailLabNote.next(true);

        this.sampleHandlingDetailService.getLabNote(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailLabNote.next(false))
        )
        .subscribe(test => this.detailLabNoteSubject.next(test))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailLabNoteSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailLabNoteSubject.complete();
        this.loadingDetailLabNote.complete();
    }
}

// export class FileDetailSourcePhy extends DataSource<any>
// {
//     private detailSpecimensSubjectPhy = new BehaviorSubject<any>([]);

//     private loadingDetailSpecimensPhy = new BehaviorSubject<boolean>(false);

//     public loading$ = this.loadingDetailSpecimensPhy.asObservable();

//     constructor(private sampleHandlingDetailService: SampleHandlingDetailService) {
//         super()
//     }

//     loadDetailPhy(regid: any){
//         this.loadingDetailSpecimensPhy.next(true);

//         this.sampleHandlingDetailService.getSampleHandleDetailPhy(regid).pipe(
//              catchError(() => of([])),
//              finalize(() => this.loadingDetailSpecimensPhy.next(false))
//         )
//         .subscribe(detspec => this.detailSpecimensSubjectPhy.next(detspec))
//     }

//     connect(collectionViewer: CollectionViewer): Observable<any>
//     {
//         console.log("Connecting data source");
//         return this.detailSpecimensSubjectPhy.asObservable();
//     }


//     disconnect(collectionViewer: CollectionViewer): void
//     {
//         this.detailSpecimensSubjectPhy.complete();
//         this.loadingDetailSpecimensPhy.complete();
//     }
// }

export class FileDetailSourceSpechand extends DataSource<any>
{
  private detailSampleSubjectSpechand = new BehaviorSubject<any>([]);

  private loadingDetailSampleSpechand = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingDetailSampleSpechand.asObservable();

  constructor(private sampleHandlingDetailService: SampleHandlingDetailService) {
    super()
  }

  loadDetailSpechand(regid: any) {
    this.loadingDetailSampleSpechand.next(true);

    this.sampleHandlingDetailService.getSampleHandleDetailSpecCol(regid).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingDetailSampleSpechand.next(false))
    )
      .subscribe(detspec => this.detailSampleSubjectSpechand.next(detspec))
  }

  connect(collectionViewer: CollectionViewer): Observable<any> {
    console.log("Connecting data source");
    return this.detailSampleSubjectSpechand.asObservable();
  }


  disconnect(collectionViewer: CollectionViewer): void {
    this.detailSampleSubjectSpechand.complete();
    this.loadingDetailSampleSpechand.complete();
  }
}

export class FileDetailSourceSamhand extends DataSource<any>
{
  private detailSampleSubjectSamhand = new BehaviorSubject<any>([]);

  private loadingDetailSampleSamhand = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingDetailSampleSamhand.asObservable();

  constructor(private sampleHandlingDetailService: SampleHandlingDetailService) {
    super()
  }

  loadDetailSamhand(regid: any) {
    this.loadingDetailSampleSamhand.next(true);

    this.sampleHandlingDetailService.getSampleHandleDetailSpecHandle(regid).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingDetailSampleSamhand.next(false))
    )
      .subscribe(detspec => this.detailSampleSubjectSamhand.next(detspec))
  }

  connect(collectionViewer: CollectionViewer): Observable<any> {
    console.log("Connecting data source");
    return this.detailSampleSubjectSamhand.asObservable();
  }


  disconnect(collectionViewer: CollectionViewer): void {
    this.detailSampleSubjectSamhand.complete();
    this.loadingDetailSampleSamhand.complete();
  }
}

export class FileDetailSourceItem extends DataSource<any>
{
    private detailItem = new BehaviorSubject<any>([]);

    private loadingItem = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingItem.asObservable();

    constructor(private sampleHandlingDetailService: SampleHandlingDetailService) {
        super()
    }

    saveCito(cito: any, index) {
      // console.log(this.detailItem.value[index])
        this.detailItem.value[index].cito = cito;
        return this.detailItem.value;
    }

    dataItem(){
      return this.detailItem.value;
    }

    loadDetailItem(regid: any){
        this.loadingItem.next(true);

        this.sampleHandlingDetailService.getSpecimensItem(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingItem.next(false))
        )
        .subscribe(detItem => {
          // console.log("detItem: ", detItem)
           this.detailItem.next(detItem)
        })
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailItem.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailItem.complete();
        this.loadingItem.complete();
    }
}

export class FileDetailSourceSpc extends DataSource<any>
{
    private detailSpc = new BehaviorSubject<any>([]);

    private loadingSpc = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSpc.asObservable();

    public clickproses: boolean;
    public clickreceived: boolean;
    specCond: any;

    constructor(private sampleHandlingDetailService: SampleHandlingDetailService) {
      super()
      this.clickproses = false;
      this.clickreceived = false;
    }

    ReceiveAll(receive:any) {
      // console.log(this.detailItem.value[index])
      // if (this.clickreceived) { this.clickreceived = false; } else { this.clickreceived = true; }
      for (var i = 0; i < this.detailSpc.value.length; i++) {
        this.detailSpc.value[i].received = receive;
        if (this.detailSpc.value[i].received == true) {
          this.detailSpc.value[i].receiveUser = 'admin';
        } else {
          this.detailSpc.value[i].receiveUser = '';
        }
      }
      return this.detailSpc.value;
    }

    ProcessAll(process:any) {
      // console.log(this.detailItem.value[index])
      // if (this.clickproses) { this.clickproses = false; } else { this.clickproses = true; }
      for (var i = 0; i < this.detailSpc.value.length; i++) {
        this.detailSpc.value[i].process = process;
        if (this.detailSpc.value[i].process == true) {
          this.detailSpc.value[i].processUser = 'admin';
        } else {
          this.detailSpc.value[i].processUser = '';
        }
      }
      return this.detailSpc.value;
    }

    saveProcess(process:any, index) {
      // console.log(this.detailItem.value[index])
        this.detailSpc.value[index].process = process;
        if (this.detailSpc.value[index].process == true) {
          this.detailSpc.value[index].processUser = 'admin';
        } else {
          this.detailSpc.value[index].processUser = '';
        }
        return this.detailSpc.value;
    }

    saveReceive(rec: any, index) {
      // console.log(this.detailItem.value[index])
        this.detailSpc.value[index].received = rec;
        if (this.detailSpc.value[index].received == true) {
          this.detailSpc.value[index].receiveUser = 'admin';
        } else {
          this.detailSpc.value[index].receiveUser = '';
        }
        return this.detailSpc.value;
    }

    saveCondID(condId: any, index) {
      // console.log(this.detailItem.value[index])
        this.detailSpc.value[index].conditionId = condId;
        return this.detailSpc.value;
    }
    
    saveStatID(statId: any, index) {
      // console.log(this.detailItem.value[index])
        this.detailSpc.value[index].specimenStatusId = statId;
        return this.detailSpc.value;
    }

    saveNote(note: any, index) {
      // console.log(this.detailItem.value[index])
        this.detailSpc.value[index].note = note;
        return this.detailSpc.value;
    }

    dataSpc(){
      return this.detailSpc.value;
    }

    loadSpc(regid: any){
        this.loadingSpc.next(true);

        this.sampleHandlingDetailService.getSpecimensCondition().subscribe(
          specCond => this.specCond = specCond);
    
        this.sampleHandlingDetailService.getSpecimens(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingSpc.next(false))
        )
        .subscribe(detSpc => {
          // console.log("detSpc: ", detSpc)
          for(var i = 0; i < detSpc[0].sampleHandlingForm.length; i++) {
            if(detSpc[0].sampleHandlingForm[i].conditionId){
              let cond = this.specCond.find(x => x.specimenConditionId == detSpc[0].sampleHandlingForm[i].conditionId);
              detSpc[0].sampleHandlingForm[i].condition = cond.specimenConditionName;
            }
            detSpc[0].sampleHandlingForm[i].specimenStatusId = null;
          }
           this.detailSpc.next(detSpc[0].sampleHandlingForm)
        })
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpc.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpc.complete();
        this.loadingSpc.complete();
    }
}
