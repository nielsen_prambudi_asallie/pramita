import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {map} from "rxjs/operators";
import { RequestOptions } from '@angular/http/src/base_request_options';
import {HttpModule, Http, Headers} from '@angular/http';
import { HttpHeaders } from '@angular/common/http';

const SERVER_URL = 'https://202.146.232.86:4000'

@Injectable()
export class ResultDetailNonLabService 
{
    private _options ={ headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text' as 'text' };
    constructor(private http:HttpClient) {

    }

    createResultDetailNonLab(insId, testMethodId, pasienAge, regId) {
        // regId = '014011D3-F2C4-4CF2-BED4-E056EB674E32';
        // insId = '082EFAB8-0CF9-45E8-85FE-DD5D6A113658';
        // testMethodId = '117';
        // pasienAge = '17';

        //return this.http.get<any>(SERVER_URL + '/Api/Register/' + insId + '/' + testMethodId + '/' +
        //pasienAge + '/' + regId + '/AnalysisResult/Non/Create')
      return this.http.get<any>(SERVER_URL + '/Api/Register/' + insId + '/' + testMethodId + '/' + '/' + regId + '/AnalysisResult/Non/Create')
    }

    // getAnalysisDetailSample(regId) {

    //     return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/Question')
    //     .pipe(
    //         map(res => res)
    //     );
    // }

    getLabNote(regId) {
        // regId = '014011D3-F2C4-4CF2-BED4-E056EB674E32';
        
        return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/labnote')
        .pipe(
            map(res => res)
        );
        // .toPromise()
        // .then(res => res);
    }

    getItemNonLab(insId, testMethodId, pasienAge, regId) {
        // regId = '014011D3-F2C4-4CF2-BED4-E056EB674E32';
        // insId = '082EFAB8-0CF9-45E8-85FE-DD5D6A113658';
        // testMethodId = '117';
        // pasienAge = '17';

        //return this.http.get<any>(SERVER_URL + '/Api/Register/' + insId + '/' + testMethodId + '/' +
        //pasienAge + '/' + regId + '/AnalysisResult/Non/Create')
        //.pipe(
        //  map(res => res.analysisResultItem)
        //)
      return this.http.get<any>(SERVER_URL + '/Api/Register/' + insId + '/' + testMethodId + '/' + '/' + regId + '/AnalysisResult/Non/Create')
        .pipe(
        map(res => res.analysisResultItem)
        )
        
    }

    getSpecimensDetailTest(regId) {
        // regId = '014011D3-F2C4-4CF2-BED4-E056EB674E32';
        return this.http.get<any>(SERVER_URL + '/Api/Register/Non/' + regId + '/test')
        .toPromise()
        .then(res => res);
        
    }

    getResultStatusNonLab(insId, testMethodId, pasienAge, regId) {
        // regId = '014011D3-F2C4-4CF2-BED4-E056EB674E32';
        // insId = '082EFAB8-0CF9-45E8-85FE-DD5D6A113658';
        // testMethodId = '117';
        // pasienAge = '17';
  
        //return this.http.get<any>(SERVER_URL + '/Api/Register/' + insId + '/' + testMethodId + '/' +
        //pasienAge + '/' + regId + '/AnalysisResult/Non/Create')
        //  .pipe(
        //  map(res => res.analysisResultStatus)
        //  );

      return this.http.get<any>(SERVER_URL + '/Api/Register/' + insId + '/' + testMethodId + '/' + '/' + regId + '/AnalysisResult/Non/Create')
        .pipe(
        map(res => res.analysisResultStatus)
        );
        // .toPromise()
        // .then(res => res);
      }

    getSpecimensDetailCond(regId) {
        // regId = '014011D3-F2C4-4CF2-BED4-E056EB674E32';
        
        return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/Question')
        .pipe(
            map(res => res)
        );
        // .toPromise()
        // .then(res => res);
    }

    getDoctor(regId) {
        // regId = '014011D3-F2C4-4CF2-BED4-E056EB674E32';
        
        return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/Doctor')
        // .pipe(
        //     map(res => res)
        // );
        .toPromise()
        .then(res => res);
    }

    getTestNonLab(insId, testMethodId, pasienAge, regId) {
        // regId = '014011D3-F2C4-4CF2-BED4-E056EB674E32';
        // insId = '082EFAB8-0CF9-45E8-85FE-DD5D6A113658';
        // testMethodId = '117';
        // pasienAge = '17';

      //return this.http.get<any>(SERVER_URL + '/Api/Register/' + insId + '/' + testMethodId + '/' +
      //pasienAge + '/' + regId + '/AnalysisResult/Non/Create')
      //  .pipe(
      //  map(res => res.analysisResultTest)
      //  );
      return this.http.get<any>(SERVER_URL + '/Api/Register/' + insId + '/' + testMethodId + '/' + '/' + regId + '/AnalysisResult/Non/Create')
        .pipe(
        map(res => res.analysisResultTest)
        );
        // .toPromise()
        // .then(res => res);
    }

    getSampleNonLab(insId, testMethodId, pasienAge, regId) {
        // regId = '014011D3-F2C4-4CF2-BED4-E056EB674E32';
        // insId = '082EFAB8-0CF9-45E8-85FE-DD5D6A113658';
        // testMethodId = '117';
        // pasienAge = '17';

      //return this.http.get<any>(SERVER_URL + '/Api/Register/' + insId + '/' + testMethodId + '/' +
      //pasienAge + '/' + regId + '/AnalysisResult/Non/Create')
      //  .pipe(
      //     map(res => res.analysisResultForm)
      //);
      return this.http.get<any>(SERVER_URL + '/Api/Register/' + insId + '/' + testMethodId + '/' + '/' + regId + '/AnalysisResult/Non/Create')
        .pipe(
        map(res => res.analysisResultForm)
        );
        //  .toPromise()
        // .then(res => res.analysisResultForm);
    }

    getstatustestNonLab(insId, testMethodId, pasienAge, regId) {
        // regId = '014011D3-F2C4-4CF2-BED4-E056EB674E32';
        // insId = '082EFAB8-0CF9-45E8-85FE-DD5D6A113658';
        // testMethodId = '117';
        // pasienAge = '17';
  
        //return this.http.get<any>(SERVER_URL + '/Api/Register/' + insId + '/' + testMethodId + '/' +
        //pasienAge + '/' + regId + '/AnalysisResult/Non/Create')
        //  .pipe(
        //     map(res => res.analysisResultStatusTest)
        //  );
      return this.http.get<any>(SERVER_URL + '/Api/Register/' + insId + '/' + testMethodId + '/' + '/' + regId + '/AnalysisResult/Non/Create')
        .pipe(
        map(res => res.analysisResultStatusTest)
        );
         
      }

    getSampleHandling(regId) {
        // regId = '014011D3-F2C4-4CF2-BED4-E056EB674E32';
        
        return this.http.get<any>(SERVER_URL + '/Api/SpecimenHandling/Non/' + regId + '/View')
        .pipe(
            map(res => res)
        );
        // .toPromise()
        // .then(res => res);
    }

    getSpecimenHandling(regId) {
        // regId = '014011D3-F2C4-4CF2-BED4-E056EB674E32';
        
        return this.http.get<any>(SERVER_URL + '/Api/SpecimenHandling/Non/' + regId + '/View')
        .pipe(
            map(res => res)
        );
        // .toPromise()
        // .then(res => res);
    }

    getSpecimenCollection(regId) {
        // regId = '014011D3-F2C4-4CF2-BED4-E056EB674E32';
        
        return this.http.get<any>(SERVER_URL + '/Api/SpecimenCollection/Non/' + regId + '/View')
        .pipe(
            map(res => res)
        );
        // .toPromise()
        // .then(res => res);
    }

    getSpecimensDetailSch(regId) {
        // regId = '014011D3-F2C4-4CF2-BED4-E056EB674E32';
        
        return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/schedule')
        .pipe(
            map(res => res)
        );
        // .toPromise()
        // .then(res => res);
    }

    getSpecimensDetail(regId) {
        // regId = '014011D3-F2C4-4CF2-BED4-E056EB674E32';
        
        return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/SpecimenCollection/Non')
        // .pipe(
        //     map(res => res)
        // );
        .toPromise()
        .then(res => res);
    }

    postResNonLab(resData) {
        let body = JSON.stringify(
          resData
        );
        return this.http.post(SERVER_URL + '/Api/AnalysisResult/Non/submit', body, this._options)
  
      }
  
      private extractData(res: Response) {
        let body = res;
        return body || {};
    }

    handleError(error: Response | any) {
      console.log(error.message || error);
      return Observable.throw(error.message || error);
    }
}
