import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MasterPhysicianService } from './mphysician.service';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import {ActivatedRoute} from "@angular/router";
import { fuseAnimations } from '../../../../../core/animations';
import { MatPaginator, MatSort, PageEvent, MatTableDataSource } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import {debounceTime, distinctUntilChanged, startWith, tap, delay} from 'rxjs/operators';
import {merge} from "rxjs/observable/merge";
import {fromEvent} from 'rxjs/observable/fromEvent';
import { FuseUtils } from '../../../../../core/fuseUtils';
import {of} from "rxjs/observable/of";
import {catchError, finalize} from "rxjs/operators";

@Component({
    selector   : 'mphysician',
    templateUrl: './mphysician.component.html',
    styleUrls  : ['./mphysician.component.scss'],
    animations : fuseAnimations
})
export class MasterPhysicianComponent implements OnInit, AfterViewInit
{
  dataSource: FilesDataSource;
  displayedColumns = [ 'no', 'doctorId', 'doctorName', 'sex', 'dob', 'nip', 'specialtyName', 'marketer'];
  selectedRowIndex: any;


  pageEvent: PageEvent;
  pageIndex: any = this.pageEvent;
  pageSize: any = this.pageEvent;
  length: number;
  pageSizeOptions = [10, 25, 100];


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('filter') filter: ElementRef;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
      private masterPhysicianService: MasterPhysicianService,
      private route : ActivatedRoute
  )
  {
  }

  ngOnInit()
  {
    // this.course = this.route.snapshot.data["course"];

    this.dataSource = new FilesDataSource(this.masterPhysicianService);
    this.dataSource.loadDoctors(this.length, 0, 10, '');

  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    fromEvent(this.filter.nativeElement,'keyup')
        .pipe(
            debounceTime(150),
            distinctUntilChanged(),
            tap(() => {
                this.paginator.pageIndex = 0;

                this.loadDoctorsPage();
            })
        )
        .subscribe();

    // merge(this.sort.sortChange, this.paginator.page)
    this.paginator.page
    .pipe(
        tap(() => this.loadDoctorsPage())
    )
    .subscribe();

  }

    loadDoctorsPage() {
        this.dataSource.loadDoctors(
            this.length,
            this.paginator.pageIndex,
            this.paginator.pageSize,
            this.filter.nativeElement.value);
    }

   onRowClicked(dataSource) {
    console.log('Row clicked: ', dataSource);
    console.log('Row clicked id', dataSource.regid);
}


}

export class FilesDataSource extends DataSource<any>
{
    private doctorsSubject = new BehaviorSubject<any>([]);
    public totalData : any;

    private loadingDoctors = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDoctors.asObservable();

    constructor(
        private masterPhysicianService: MasterPhysicianService,
    )
    {
        super()

    }

    loadDoctors(
        length: number,
        pageIndex:number,
        pageSize:number,
        filter:string) {

        this.loadingDoctors.next(true);

        this.masterPhysicianService.getPhysicians(pageIndex, pageSize, filter).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingDoctors.next(false))
            )
            .subscribe(doctors => {
              console.log("doctor: ", doctors)
               this.doctorsSubject.next(doctors.data);
               this.totalData = doctors.totalData
              });



        }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.doctorsSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.doctorsSubject.complete();
        this.loadingDoctors.complete();
    }
}
