import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from "@angular/forms"
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { ResultHandlingPasienComponent } from './result_handling_pasien/result_handling_pasien.component';
import { ResultHandlingPasienService } from './result_handling_pasien/result_handling_pasien.service';
import { ResultHandlingDetailComponent } from './result_handling_detail/result_handling_detail.component';
import { ResultHandlingDetailService } from './result_handling_detail/result_handling_detail.service';
import { AgmCoreModule } from '@agm/core';
import { AccordionModule } from 'primeng/components/accordion/accordion';

const routes: Routes = [

    {
        path     : 'result_handling_pasien',
        component: ResultHandlingPasienComponent,
    },
    {
        path     : 'result_handling_pasien/:regid/:age',
        component: ResultHandlingDetailComponent,
    }
    


];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        FormsModule,
        AccordionModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    declarations: [
        ResultHandlingPasienComponent,
        ResultHandlingDetailComponent
    ],
    providers   : [
        ResultHandlingPasienService,
        ResultHandlingDetailService
    ]
})
export class ResultModule
{
}
