import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from "@angular/forms"
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { SpecimenCollectionComponent } from './specimen_collection/specimen_collection.component';
import { SpecimenCollectionService } from './specimen_collection/specimen_collection.service';
import { SpecimenDetailComponent } from './specimen_detail/specimen_detail.component';
import { SpecimenDetailService } from './specimen_detail/specimen_detail.service';
import { SpecimenHandlingComponent } from './specimen_handling/specimen_handling.component';
import { SpecimenHandlingService } from './specimen_handling/specimen_handling.service';
import { SpecimenHandlingDetailComponent } from './specimen_handling_detail/specimen_handling_detail.component';
import { SpecimenHandlingDetailService } from './specimen_handling_detail/specimen_handling_detail.service';
import { SampleHandlingComponent } from './sample_handling/sample_handling.component';
import { SampleHandlingService } from './sample_handling/sample_handling.service';
import { SampleHandlingDetailComponent } from './sample_handling_detail/sample_handling_detail.component';
import { SampleHandlingDetailService } from './sample_handling_detail/sample_handling_detail.service';
import { AgmCoreModule } from '@agm/core';
import { AccordionModule } from 'primeng/components/accordion/accordion';

const routes: Routes = [

    {
        path     : 'specimen_collection',
        component: SpecimenCollectionComponent,
    },
    {
        path     : 'specimen_collection/:regid/:regno/:regdate/:name',
        component: SpecimenDetailComponent,
    },
    {
        path     : 'specimen_handling',
        component: SpecimenHandlingComponent,

    },
    {
        path: 'specimen_handling/:regid/:regno/:regdate/:name',
        component: SpecimenHandlingDetailComponent,

    },
    {
        path     : 'sample_handling',
        component: SampleHandlingComponent,

    },
    {
        path: 'sample_handling/:regid/:regno/:regdate/:name',
        component: SampleHandlingDetailComponent,

    },


];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        FormsModule,
        AccordionModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    declarations: [
        SpecimenCollectionComponent,
        SpecimenDetailComponent,
        SpecimenHandlingComponent,
        SpecimenHandlingDetailComponent,
        SampleHandlingComponent,
        SampleHandlingDetailComponent,
    ],
    providers   : [
        SpecimenCollectionService,
        SpecimenDetailService,
        SpecimenHandlingService,
        SpecimenHandlingDetailService,
        SampleHandlingDetailService,
        SampleHandlingService,
    ]
})
export class SpecimenModule
{
}
