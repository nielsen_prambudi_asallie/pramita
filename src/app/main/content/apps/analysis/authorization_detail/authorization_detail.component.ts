import { Component, OnDestroy, ElementRef, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { AuthorizationDetailService } from './authorization_detail.service';
import { fuseAnimations } from '../../../../../core/animations';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar } from '@angular/material';
import { Location } from '@angular/common';
import { MatPaginator, MatSort,  MatTableDataSource  } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ActivatedRoute, Router } from '@angular/router';
import {of} from "rxjs/observable/of";
import {catchError, finalize} from "rxjs/operators";
import { Options } from 'selenium-webdriver';

export interface Detail {
    nama?;
    estVol?;
    no?;
    bodySite?;
    loCSite?;
    drawSite?;
    drawUser?;
    draw?: boolean;
    condition?;
    specimenId?;
    note?;
    received?: boolean;
    receiveDate?: "yyyy-MM-dd";
    receiveUser?: "yyyy-MM-dd";
}

export interface Detail2 {
    registerTestId?;
    testName?;
    cito?: boolean;
    scheduleName?;
}

export interface Detail3 {
    id?;
    name?;
    result?;
    result_comment?;
    auth?: boolean;
    authDate?;
    regno?;
    regdate?;
}

export interface Detail4 {
    doctor_id?;
    n?;
    action?;
    to?;
    format?;
    rh_id?;
    rh_name?;
    submitted?;
    received?;
    doctor?;
    submittedDate?;
    submittedUser?;
    receivedDate?;
    receivedUser?;
}

export interface Detail5 {
    msg?;
    msgAttn?;
    msgTo?;
    msgReady?;
    msgToName?;
    msgDelivered?;
    msgReadyDate?;
    msgSendingDate?;
    msgDeliveredDate?;
    msgQ?;
    mds?;
    regno?;
}

@Component({
    selector     : 'authorization_detail',
    templateUrl  : './authorization_detail.component.html',
    styleUrls    : ['./authorization_detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AuthorizationDetailComponent implements OnInit
{
    auth_detail: {regid: any, age: any }
    regid: any;
    age: any;
    regdate: any;
    user: any;
    doctor: any;
    errorMessage: boolean;
    successMessage: boolean;
    tampungItemAuth: any = [];
    tampungSampleProcess: any = [];
    tampungSampleReceived: any = [];
    tampungResultAuth: any = [];
    createData : any = [];
    data: any;
    createAuth: any = [];
    detail: Detail[];
    detail2: Detail2[];
    detail3: Detail3[];
    detail4: Detail4[];
    detail5: Detail5[];
    drawReceive: FormGroup;
    patientNoteGroup: FormGroup;
    physicianNoteGroup: FormGroup;
    referralNoteGroup: FormGroup;
    labNoteGroup: FormGroup;
    statusTest: FormGroup;
    manyItem: FormGroup;
    manyRh: FormGroup;
    manyMsg: FormGroup;
    samples: FormGroup;
    dataSourceTest: FileDetailSourceTest;
    //dataSourceSample: FileDetailSourceSample;
    dataSourceSampleHandling: FileDetailSourceSampleHandling;
    dataSourceSpecimenHandling: FileDetailSourceSpecimenHandling;
    dataSourceSpecimenCollection: FileDetailSourceSpecimenCollection;
    dataSourceLabNote: FileDetailSourceLabNote;
    dataSource: FileDetailSource;
    dataSourceSch : FileDetailSourceSch;
    dataSourceStatus: FileDetailSourceStatus;
    dataSourceRun: FileDetailSourceRun;
    dataSourceConcList: FileDetailSourceConcList;
    dataSourceConc: FileDetailSourceConc;
    dataSourceRec: FileDetailSourceRec;
    dataSourceSug: FileDetailSourceSug;
    dataSourceResultNote: FileDetailSourceResultNote;
    dataSourceItem: FileDetailSourceItem;
    drawer: string;
    clickDraw: boolean = false;
    clickRe: boolean = false;
    clickCito: boolean = false;

    displayedColumnsItem = ['testname', 'flag', 'interfaceFlag', 'result', 'unit', 'resultComment','reference', 'auth', 'last1', 'last2', 'verification','INP','PRN', 'testMethod','process', 'cito', 'schedule', 'scheduleDateTime', 'authDate', 'authUser', 'verificationDate', 'verificationUser', 'resultINPDate', 'resultINPUser','processDate', 'processUser', 'resultSI', 'formatSI','instrumentName','interfacingId', 'interfacingResult', 'interfacingLisDate', 'interfacingRawResult', 'interfacingRawInterpretation', 'interfacingRawUnit', 'interfacingRawFlag', 'interfacingRawDate', 'testid',  'microbiology'];
    displayedColumnsConcList = ['conclusion', 'accepted', 'byuser', 'testconcid', 'testconcsubid'];
    displayedColumnsConc = ['id', 'name', 'print'];
    displayedColumnsResultNote = ['regno', 'lang', 'note', 'updateuser', 'updatedate'];
    displayedColumnsRec = ['id', 'name'];
    displayedColumnsSug = ['id', 'name']
    displayedColumns = ['question', 'answer', 'note']
    displayedColumnsNoteLab = ['no', 'note', 'user', 'date'];
    displayedColumns2= ['schedule', 'test', 'day', 'time'];
    displayedColumnsRun = ['id', 'name', 'result', 'result_comment', 'status', 'analyzer'];
    displayedColumnsTest = ['testid', 'testname', 'testmethod'];
    displayedColumnsStatus = ['id', 'name', 'result', 'result_comment', 'auth', 'auth_date'];
    displayedColumnsSampleHandling = ['name', 'estvol', 'condition', 'note', 'received', 'receiveddate', 'receiveduser','process', 'processdate', 'processuser', 'samplehandlingid'];
    displayedColumnsSpecimenHandling = ['nama', 'estvol', 'conditionid', 'condition', 'note', 'send', 'senddate', 'process', 'processdate', 'senduser', 'processuser'];
    displayedColumnsSample = ['specimenName', 'no', 'estvol', 'condition', 'note', , 'process', 'processdate','send', 'senddate'];
    displayedColumnsSpecimenCollection = ['nama', 'estvol',  'receiveuser','receive','specimenid', 'conditionid', 'note', 'send', 'senddate', 'senduser'];


    initMsg (msg) {
        const group = this.formBuilder.group({
            msg: msg.msg,
            msgAttn: msg.msgAttn,
            msgTo: msg.msgTo,
            msgReady: msg.msgReady,
            msgToName: msg.msgToName,
            msgDelivered: msg.msgDelivered,
            msgReadyDate: msg.msgReadyDate,
            msgSendingDate: msg.msgSendingDate,
            msgDeliveredDate: msg.msgDeliveredDate,
            msgQ: msg.msgQ,
            mds: msg.mds,
            regno: msg.regno
        })
        return group;
    }

    initRh (rh) {
        const group = this.formBuilder.group({
            doctor_id: rh.doctor_id,
            n: rh.n,
            action: rh.action,
            to: rh.to,
            format: rh.format,
            rh_id: rh.rh_id,
            rh_name: rh.rh_name,
            submitted: rh.submitted,
            received: rh.received,
            doctor: rh.doctor,
            submittedDate: rh.submittedDate,
            submittedUser: rh.submittedUser,
            receivedDate: rh.receivedDate,
            receivedUser: rh.receivedUser
        })
        return group;
    }

    initStatusTest (stat) {
        const group = this.formBuilder.group({
            id: stat.id,
            name: stat.name,
            result: stat.result,
            result_comment: stat.result_comment,
            auth: stat.auth,
            authDate: stat.authDate
        })
        return group;
    }

    initSample(sample) {
      const group = this.formBuilder.group({
        specimenId: sample.specimenId,
        specimenName: sample.specimenName,
        no: sample.no,
        estvol: sample.estvol,
        // vol: sample.vol,
        condition: sample.condition,
        note: sample.note,
        process: sample.process,
        processDate: sample.processDate,
        received: sample.received,
        receivedDate: sample.receiveDate
      })
      return group;
    }


    initNote() {
        return this.formBuilder.group({
            LabNote: [''],
            user: 'admin'
            // date: [''],
            // RegisterId: this.route.snapshot.params['regid']
        })
    }

    addNoteValue() {
        const control = <FormArray>this.labNoteGroup.controls['labNotes'];
        control.push(this.initNote())
    }

    deleteNoteValue(index) {
        if(index > 0){
            const control = <FormArray>this.labNoteGroup.controls['labNotes'];
            control.removeAt(index);
        }
    }

    passAuth(index, value) {
      this.tampungItemAuth[index].inp = value.controls.auth.value;
  }



    // passprocesssample(index, value) {
    //     this.tampungSampleProcess[index].process = value.controls.process.value, value.controls.specimenId.value;
    // }

    // passreceived(index, value) {
    //     this.tampungSampleReceived[index].received = value.controls.received.value, value.controls.specimenId.value;
    // }

    passauth(index, value) {
        this.tampungResultAuth[index].auth = value.controls.auth.value, value.controls.id.value;
    }

    constructor(private route: ActivatedRoute, private authorizationDetailService: AuthorizationDetailService, private formBuilder: FormBuilder, private router: Router) {
        this.manyMsg = this.formBuilder.group({
            msgArray: this.formBuilder.array([])
        })

        this.manyRh = this.formBuilder.group({
            rhArray: this.formBuilder.array([])
        })

        this.samples = this.formBuilder.group({
            sampleArray: this.formBuilder.array([])
        })

        this.statusTest = this.formBuilder.group({
            statusTestArray: this.formBuilder.array([])
        })

        // console.log(this.drawReceive.value['drawUser'] = "admin")

        this.labNoteGroup = this.formBuilder.group({
            labNotes: this.formBuilder.array([this.initNote()])
        })

        this.manyItem = this.formBuilder.group({
            itemArray: this.formBuilder.array([])
        })


    }

    ngOnInit() {
        // private loadingSpecimensDetail = new BehaviorSubject<boolean>(false);
        // private specimensDetailSubject = new BehaviorSubject<any>([]);



        this.auth_detail = {
            regid: this.route.snapshot.params['regid'],
            age: this.route.snapshot.params['age']
        }
        this.regid = this.auth_detail['regid'];
        this.age = this.auth_detail['age'];

        this.authorizationDetailService.createAuthorizeDetail(this.regid).subscribe(create => {
            this.createData = create;

        })

        this.authorizationDetailService.getDoctor(this.regid).then(doc => this.doctor = doc)

        const control4 = <FormArray>this.manyMsg.controls['msgArray'];
        // this.resultDetailService.getSample(this.pasid).then(msg => {
        //     this.detail4 = msg
        //     msg.forEach(b => {
        //         control.push(this.initStatusTest(b))
        //         console.log("init Status Text", b)
        //         this.tampungResultAuth.push({
        //             "id": b.id,
        //             "auth": b.auth
        //         })
        //     });
        // })

        const control3 = <FormArray>this.manyRh.controls['rhArray'];
        // this.resultDetailService.getSample(this.pasid).then(rh => {
        //     this.detail4 = rh
        //     rh.forEach(b => {
        //         control.push(this.initStatusTest(b))
        //         console.log("init Status Text", b)
        //         this.tampungResultAuth.push({
        //             "id": b.id,
        //             "auth": b.auth
        //         })
        //     });
        // })

        const control2 = <FormArray>this.statusTest.controls['statusTestArray'];
        // this.authorizationDetailService.getstatustest(this.regid).then(statTest => {
        //   this.detail3 = statTest
        //   statTest.forEach(b => {
        //     control2.push(this.initStatusTest(b))
        //     console.log("init Status Text", b)
        //     this.tampungResultAuth.push({
        //       "id": b.id,
        //       "auth": b.auth
        //     })
        //   });
        // })

        const control = <FormArray>this.samples.controls['sampleArray'];
        // this.authorizationDetailService.getSample(this.insid, this.pasid).then(sample => {
        //   this.detail = sample
        //   sample.forEach(x => {
        //     control.push(this.initSample(x))
        //     console.log("init Dra re", x)
        //     this.tampungSampleProcess.push({
        //       "specimenId": x.specimenId,
        //       "process": x.process
        //     })
        //     this.tampungSampleReceived.push({
        //       "specimenId": x.specimenId,
        //       "received": x.received
        //     })
        //   });
        // })


        // const control1 = <FormArray>this.manyItem.controls['itemArray'];
        // this.authorizationDetailService.getItem(this.regid).then(item => {
        //   this.detail2 = item
        //   console.log("item item", item)
        //   item.forEach(y => {
        //     control1.push(this.initItem(y))
        //     console.log("init item", y)
        //     this.tampungItemAuth.push({
        //       "auth": y.auth
        //     });
        //   });
        // })

        this.dataSourceItem = new FileDetailSourceItem(this.authorizationDetailService)

        this.dataSourceItem.loadDetailItem(this.regid)
        // this.dataSourceResultNote = new FileDetailSourceResultNote(this.authorizationDetailService)

        // this.dataSourceResultNote.loadDetailResultNote(this.regid)

        // this.dataSourceSug = new FileDetailSourceSug(this.authorizationDetailService)

        // this.dataSourceSug.loadDetailSug(this.regid)

        // this.dataSourceRec = new FileDetailSourceRec(this.authorizationDetailService)

        // this.dataSourceRec.loadDetailRec(this.regid)

        // this.dataSourceConc = new FileDetailSourceConc(this.authorizationDetailService)

        // this.dataSourceConc.loadDetailConc(this.regid)

        // this.dataSourceConcList = new FileDetailSourceConcList(this.authorizationDetailService)

        // this.dataSourceConcList.loadDetailConcList(this.regid)

        // this.dataSourceTest = new FileDetailSourceTest(this.authorizationDetailService)

        // this.dataSourceTest.loadDetailTest(this.regid)

        // console.log(this.dataSourceTest.loadDetailTest(this.regid))

        // this.dataSourceRun = new FileDetailSourceRun(this.authorizationDetailService)

        // this.dataSourceRun.loadDetailRun(this.regid)

        // this.dataSourceStatus = new FileDetailSourceStatus(this.authorizationDetailService)

        // this.dataSourceStatus.loadDetailStatus(this.regid)

        // this.dataSourceSample = new FileDetailSourceSample(this.authorizationDetailService)

        // this.dataSourceSample.loadDetailSample(this.pasid)

        this.dataSourceSampleHandling = new FileDetailSourceSampleHandling(this.authorizationDetailService)

        this.dataSourceSampleHandling.loadDetailSampleHandling(this.regid)

        this.dataSourceSpecimenHandling = new FileDetailSourceSpecimenHandling(this.authorizationDetailService)

        this.dataSourceSpecimenHandling.loadDetailSpecimenHandling(this.regid)

        this.dataSourceSpecimenCollection = new FileDetailSourceSpecimenCollection(this.authorizationDetailService)

        this.dataSourceSpecimenCollection.loadDetailSpecimenCollection(this.regid)

        this.dataSource = new FileDetailSource(this.authorizationDetailService);

        this.dataSource.loadDetail(this.regid)

        this.dataSourceSch = new FileDetailSourceSch(this.authorizationDetailService);

        this.dataSourceSch.loadDetailSch(this.regid)

        this.dataSourceLabNote = new FileDetailSourceLabNote(this.authorizationDetailService);

        this.dataSourceLabNote.loadDetailLabNote(this.regid)



    }

    // sendData() {
    //     console.log(this.drawReceive.value["drawReceiveArray"])
    //     console.log(this.regid);
    //     // console.log(this.dataSource['value'])
    // }

    clickAllVerify(event){
        this.dataSourceItem.allVerify(event);
    }
    
    verify(event, index) {
        this.dataSourceItem.saveVerify(event, index);
    }

    comment(event, index) {
        this.dataSourceItem.saveComment(event, index);
    }

    result(event, index) {
        this.dataSourceItem.saveResult(event, index);
    }

    clickAllPrn(event) {
        this.dataSourceItem.allPrn(event);
    }

    prn(event, index) {
        this.dataSourceItem.savePrn(event, index);
    }

    clickAllAuth(event) {
        this.dataSourceItem.allAuth(event);
    }

    auth(event, index) {
        this.dataSourceItem.saveAuth(event, index)
    }

    submitAuth(data) {
        this.createAuth = {
          registerId : this.regid,
          patientNote : this.createData.patientNote,
          referalNote: this.createData.referalNote,
          doctornote : this.createData.doctornote,
          regNo : this.createData.regNo,
          regDate : this.createData.regDate,
          verificationAnalystForm : null,
          verificationAnalystItem : this.dataSourceItem.loadItem(),

            // process: this.tampungItemAuth,
            // specimenCollectionForm : this.drawReceive.value['drawReceiveArray'],
            // specimenHandlingForm : null,
            // sampleHandlingForm : null,
            labNote : this.labNoteGroup.value['labNote']
        }
        console.log(this.createAuth)
        data = this.createAuth
        this.authorizationDetailService.postAuth(data).subscribe(res => {
            if(res == 'Submited'){
                this.successMessage = true;
                this.router.navigate(['apps/analysis/authorization_pasien'])
                setTimeout(function () {
                        this.successMessage = false;
                    
                    }.bind(this), 2000)
            }else{
                this.errorMessage = true;
                setTimeout(function (){
                    this.errorMessage = false;
                }.bind(this), 2000)
            }
        },error => {
            this.errorMessage = true;
            setTimeout(function (){
                this.errorMessage = false;
            }.bind(this), 2000)
        });
        console.log(data);
    }
}

export class FileDetailSourceItem extends DataSource<any>
{
    private detailItemSubject = new BehaviorSubject<any>([]);

    private loadingDetailItem = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailItem.asObservable();

    public clickauth: boolean;

    public clickverify: boolean;

    public clickprn: boolean;

    constructor(private authorizationDetailService: AuthorizationDetailService) {
        super()
    }

    allVerify(verify:any) {
        // if (this.clickverify) { this.clickverify = false; } else { this.clickverify = true; }
      for (var i = 0; i < this.detailItemSubject.value.length; i++) {
        this.detailItemSubject.value[i].verification = verify;
      }
      return this.detailItemSubject.value;
    }

    saveVerify(verify: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailItemSubject.value[index].verification = verify;
        return this.detailItemSubject.value;
    }

    saveComment(resComment: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailItemSubject.value[index].resultComment = resComment;
        return this.detailItemSubject.value;
    }

    saveResult(res: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailItemSubject.value[index].result = res;
        return this.detailItemSubject.value;
    }

    allPrn(prn:any) {
        // if (this.clickprn) { this.clickprn = false; } else { this.clickprn = true; }
      for (var i = 0; i < this.detailItemSubject.value.length; i++) {
        this.detailItemSubject.value[i].prn = prn;
      }
      return this.detailItemSubject.value;
    }

    savePrn(prn: any, index) {
        // console.log(this.detailItemSubject.value[index])
      this.detailItemSubject.value[index].prn = prn;
        return this.detailItemSubject.value;
    }

    allAuth(auth:any) {
        // if (this.clickauth) { this.clickauth = false; } else { this.clickauth = true; }
      for (var i = 0; i < this.detailItemSubject.value.length; i++) {
        this.detailItemSubject.value[i].auth = auth;
      }
      return this.detailItemSubject.value;
    }

    saveAuth(auth: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailItemSubject.value[index].auth = auth;
        return this.detailItemSubject.value;
    }

    loadItem(){
        return this.detailItemSubject.value;
    }

    loadDetailItem(regid: any){
        this.loadingDetailItem.next(true);

        this.authorizationDetailService.getItem(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailItem.next(false))
        )
        .subscribe(item => this.detailItemSubject.next(item))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailItemSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailItemSubject.complete();
        this.loadingDetailItem.complete();
    }
}

export class FileDetailSourceResultNote extends DataSource<any>
{
    private detailResultNoteSubject = new BehaviorSubject<any>([]);

    private loadingDetailResultNote = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailResultNote.asObservable();

    constructor(private authorizationDetailService: AuthorizationDetailService) {
        super()
    }

    // loadDetailResultNote(pasid: any){
    //     this.loadingDetailResultNote.next(true);

    //     this.authorizationDetailService.getTest(pasid).pipe(
    //          catchError(() => of([])),
    //          finalize(() => this.loadingDetailResultNote.next(false))
    //     )
    //     .subscribe(renote => this.detailResultNoteSubject.next(renote))
    // }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailResultNoteSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailResultNoteSubject.complete();
        this.loadingDetailResultNote.complete();
    }
}

export class FileDetailSourceRec extends DataSource<any>
{
    private detailRecSubject = new BehaviorSubject<any>([]);

    private loadingDetailRec = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailRec.asObservable();

    constructor(private authorizationDetailService: AuthorizationDetailService) {
        super()
    }

    // loadDetailRec(pasid: any){
    //     this.loadingDetailRec.next(true);

    //     this.authorizationDetailService.getTest(pasid).pipe(
    //          catchError(() => of([])),
    //          finalize(() => this.loadingDetailRec.next(false))
    //     )
    //     .subscribe(test => this.detailRecSubject.next(test))
    // }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailRecSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailRecSubject.complete();
        this.loadingDetailRec.complete();
    }
}

export class FileDetailSourceSug extends DataSource<any>
{
    private detailSugSubject = new BehaviorSubject<any>([]);

    private loadingDetailSug = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSug.asObservable();

    constructor(private authorizationDetailService: AuthorizationDetailService) {
        super()
    }

    // loadDetailSug(pasid: any){
    //     this.loadingDetailSug.next(true);

    //     this.authorizationDetailService.getTest(pasid).pipe(
    //          catchError(() => of([])),
    //          finalize(() => this.loadingDetailSug.next(false))
    //     )
    //     .subscribe(test => this.detailSugSubject.next(test))
    // }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSugSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSugSubject.complete();
        this.loadingDetailSug.complete();
    }
}

export class FileDetailSourceConc extends DataSource<any>
{
    private detailConcSubject = new BehaviorSubject<any>([]);

    private loadingDetailConc = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailConc.asObservable();

    constructor(private authorizationDetailService: AuthorizationDetailService) {
        super()
    }

    // loadDetailConc(pasid: any){
    //     this.loadingDetailConc.next(true);

    //     this.authorizationDetailService.getTest(pasid).pipe(
    //          catchError(() => of([])),
    //          finalize(() => this.loadingDetailConc.next(false))
    //     )
    //     .subscribe(test => this.detailConcSubject.next(test))
    // }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailConcSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailConcSubject.complete();
        this.loadingDetailConc.complete();
    }
}

export class FileDetailSourceConcList extends DataSource<any>
{
    private detailConcListSubject = new BehaviorSubject<any>([]);

    private loadingDetailConcList = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailConcList.asObservable();

    constructor(private authorizationDetailService: AuthorizationDetailService) {
        super()
    }

    // loadDetailConcList(pasid: any){
    //     this.loadingDetailConcList.next(true);

    //     this.authorizationDetailService.getTest(pasid).pipe(
    //          catchError(() => of([])),
    //          finalize(() => this.loadingDetailConcList.next(false))
    //     )
    //     .subscribe(test => this.detailConcListSubject.next(test))
    // }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailConcListSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailConcListSubject.complete();
        this.loadingDetailConcList.complete();
    }
}

export class FileDetailSourceLabNote extends DataSource<any>
{
    private detailLabNoteSubject = new BehaviorSubject<any>([]);

    private loadingDetailLabNote = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailLabNote.asObservable();

    constructor(private authorizationDetailService: AuthorizationDetailService) {
        super()
    }

    loadDetailLabNote(pasid: any){
        this.loadingDetailLabNote.next(true);

        this.authorizationDetailService.getLabNote(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailLabNote.next(false))
        )
        .subscribe(test => this.detailLabNoteSubject.next(test))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailLabNoteSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailLabNoteSubject.complete();
        this.loadingDetailLabNote.complete();
    }
}

export class FileDetailSourceTest extends DataSource<any>
{
    private detailTestSubject = new BehaviorSubject<any>([]);

    private loadingDetailTest = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailTest.asObservable();

    constructor(private authorizationDetailService: AuthorizationDetailService) {
        super()
    }

    // loadDetailTest(pasid: any){
    //     this.loadingDetailTest.next(true);

    //     this.authorizationDetailService.getTest(pasid).pipe(
    //          catchError(() => of([])),
    //          finalize(() => this.loadingDetailTest.next(false))
    //     )
    //     .subscribe(test => this.detailTestSubject.next(test))
    // }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailTestSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailTestSubject.complete();
        this.loadingDetailTest.complete();
    }
}

export class FileDetailSourceRun extends DataSource<any>
{
    private detailRunSubject = new BehaviorSubject<any>([]);

    private loadingDetailRun = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailRun.asObservable();

    constructor(private authorizationDetailService: AuthorizationDetailService) {
        super()
    }

    // loadDetailRun(pasid: any){
    //     this.loadingDetailRun.next(true);

    //     this.authorizationDetailService.getTest(pasid).pipe(
    //          catchError(() => of([])),
    //          finalize(() => this.loadingDetailRun.next(false))
    //     )
    //     .subscribe(run => this.detailRunSubject.next(run))
    // }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailRunSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailRunSubject.complete();
        this.loadingDetailRun.complete();
    }
}

export class FileDetailSourceStatus extends DataSource<any>
{
    private detailStatusSubject = new BehaviorSubject<any>([]);

    private loadingDetailStatus = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailStatus.asObservable();

    constructor(private authorizationDetailService: AuthorizationDetailService) {
        super()
    }

    // loadDetailStatus(insid: any, pasid: any) {
    //     this.loadingDetailStatus.next(true);

    //     this.authorizationDetailService.getResultStatus(insid, pasid).pipe(
    //          catchError(() => of([])),
    //          finalize(() => this.loadingDetailStatus.next(false))
    //     )
    //     .subscribe(status => this.detailStatusSubject.next(status))
    // }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailStatusSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailStatusSubject.complete();
        this.loadingDetailStatus.complete();
    }
}

//export class FileDetailSourceSample extends DataSource<any>
//{
//    private detailSampleSubject = new BehaviorSubject<any>([]);

//    private loadingDetailSample = new BehaviorSubject<boolean>(false);

//    public loading$ = this.loadingDetailSample.asObservable();

//    constructor(private authorizationDetailService: AuthorizationDetailService) {
//        super()
//    }

//    loadDetailSample(pasid: any){
//        this.loadingDetailSample.next(true);

//        this.authorizationDetailService.getSample(pasid).pipe(
//             catchError(() => of([])),
//             finalize(() => this.loadingDetailSample.next(false))
//        )
//        .subscribe(sample => this.detailSampleSubject.next(sample))
//    }

//    connect(collectionViewer: CollectionViewer): Observable<any>
//    {
//        console.log("Connecting data source");
//        return this.detailSampleSubject.asObservable();
//    }


//    disconnect(collectionViewer: CollectionViewer): void
//    {
//        this.detailSampleSubject.complete();
//        this.loadingDetailSample.complete();
//    }
//}

export class FileDetailSourceSampleHandling extends DataSource<any>
{
    private detailSampleHandlingSubject = new BehaviorSubject<any>([]);

    private loadingDetailSampleHandling = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSampleHandling.asObservable();

    constructor(private authorizationDetailService: AuthorizationDetailService) {
        super()
    }

    loadDetailSampleHandling(regid: any){
        this.loadingDetailSampleHandling.next(true);

        this.authorizationDetailService.getSampleHandling(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSampleHandling.next(false))
        )
        .subscribe(sample => this.detailSampleHandlingSubject.next(sample))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSampleHandlingSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSampleHandlingSubject.complete();
        this.loadingDetailSampleHandling.complete();
    }
}

export class FileDetailSourceSpecimenHandling extends DataSource<any>
{
    private detailSpecimenHandlingSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimenHandling = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimenHandling.asObservable();

    constructor(private authorizationDetailService: AuthorizationDetailService) {
        super()
    }

    loadDetailSpecimenHandling(pasid: any){
        this.loadingDetailSpecimenHandling.next(true);

        this.authorizationDetailService.getSpecimenHandling(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimenHandling.next(false))
        )
        .subscribe(specimen => this.detailSpecimenHandlingSubject.next(specimen))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimenHandlingSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimenHandlingSubject.complete();
        this.loadingDetailSpecimenHandling.complete();
    }
}

export class FileDetailSourceSpecimenCollection extends DataSource<any>
{
    private detailSpecimenCollectionSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimenCollection = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimenCollection.asObservable();

    constructor(private authorizationDetailService: AuthorizationDetailService) {
        super()
    }

    loadDetailSpecimenCollection(pasid: any){
        this.loadingDetailSpecimenCollection.next(true);

        this.authorizationDetailService.getSpecimenCollection(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimenCollection.next(false))
        )
        .subscribe(specimen => this.detailSpecimenCollectionSubject.next(specimen))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimenCollectionSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimenCollectionSubject.complete();
        this.loadingDetailSpecimenCollection.complete();
    }
}

export class FileDetailSource extends DataSource<any>
{
    private detailSpecimensSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimens = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimens.asObservable();

    constructor(private authorizationDetailService: AuthorizationDetailService) {
        super()
    }

    loadDetail(regid: any){
        this.loadingDetailSpecimens.next(true);

        this.authorizationDetailService.getSpecimensDetailCond(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimens.next(false))
        )
        .subscribe(detspec => this.detailSpecimensSubject.next(detspec))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimensSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimensSubject.complete();
        this.loadingDetailSpecimens.complete();
    }
}



export class FileDetailSourceSch extends DataSource<any>
{
    private detailSpecimensSubjectSch = new BehaviorSubject<any>([]);

    private loadingDetailSpecimensSch = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimensSch.asObservable();

    constructor(private authorizationDetailService: AuthorizationDetailService) {
        super()
    }

    loadDetailSch(regid: any){
        this.loadingDetailSpecimensSch.next(true);

        this.authorizationDetailService.getSpecimensDetailSch(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimensSch.next(false))
        )
        .subscribe(detspec => this.detailSpecimensSubjectSch.next(detspec))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimensSubjectSch.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimensSubjectSch.complete();
        this.loadingDetailSpecimensSch.complete();
    }
}

// export class FileDetailSourceSample extends DataSource<any>
// {
//     private detailAnalysisSubjectSample = new BehaviorSubject<any>([]);

//     private loadingDetailAnalysisSample = new BehaviorSubject<boolean>(false);

//     public loading$ = this.loadingDetailAnalysisSample.asObservable();

//     constructor(private processSampleDetailService: ProcessSampleDetailService) {
//         super()
//     }

//     loadDetailSample(regid: any){
//         this.loadingDetailAnalysisSample.next(true);

//         this.processSampleDetailService.getAnalysisDetailSample(regid).pipe(
//              catchError(() => of([])),
//              finalize(() => this.loadingDetailAnalysisSample.next(false))
//         )
//         .subscribe(detspec => this.detailAnalysisSubjectSample.next(detspec))
//     }

//     connect(collectionViewer: CollectionViewer): Observable<any>
//     {
//         console.log("Connecting data source");
//         return this.detailAnalysisSubjectSample.asObservable();
//     }


//     disconnect(collectionViewer: CollectionViewer): void
//     {
//         this.detailAnalysisSubjectSample.complete();
//         this.loadingDetailAnalysisSample.complete();
//     }
// }
