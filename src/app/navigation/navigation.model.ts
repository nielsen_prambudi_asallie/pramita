import { FuseNavigationModelInterface } from '../core/components/navigation/navigation.model';

export class FuseNavigationModel implements FuseNavigationModelInterface {
  public model: any[];

  constructor() {
    this.model = [{
      'id': 'applications',
      'title': 'Applications',
      'translate': 'NAV.APPLICATIONS',
      'type': 'group',
      'icon': 'apps',
      'children': [{
          'id': 'dashboards',
          'title': 'Dashboards',
          'translate': 'NAV.DASHBOARDS',
          'type': 'collapse',
          'icon': 'dashboard',
          'children': [{
            'id': 'project',
            'title': 'Project',
            'type': 'item',
            'url': '/apps/dashboards/project'
          }]
        },
        {
          'id': 'master',
          'title': 'Master',
          'translate': 'NAV.MASTER',
          'type': 'collapse',
          'icon': 'library_add',
          'children': [{
              'id': 'magreement',
              'title': 'Agreement',
              'translate': 'NAV.AGREEMENT',
              'type': 'item',
              'icon': 'library_add',
              'url': '/apps/master/magreement',
              'exactMatch': true
            },
            {
              'id': 'mcompany',
              'title': 'Company',
              'translate': 'NAV.COMPANY',
              'type': 'item',
              'icon': 'library_add',
              'url': '/apps/master/mcompany',
              'exactMatch': true
            },
            {
              'id': 'mphysician',
              'title': 'Physician',
              'translate': 'NAV.PHYSICIAN',
              'type': 'item',
              'icon': 'library_add',
              'url': '/apps/master/mphysician',
              'exactMatch': true
            },
            {
              'id': 'mprice_list',
              'title': 'Price List',
              'translate': 'NAV.PRICE_LIST',
              'type': 'item',
              'icon': 'library_add',
              'url': '/apps/master/mprice_list',
              'exactMatch': true
            },
            {
              'id': 'mreward',
              'title': 'Reward',
              'translate': 'NAV.REWARD',
              'type': 'item',
              'icon': 'library_add',
              'url': '/apps/master/mreward',
              'exactMatch': true
            },
            {
              'id': 'mschedule',
              'title': 'Schedule',
              'translate': 'NAV.SCHEDULE',
              'type': 'item',
              'icon': 'library_add',
              'url': '/apps/master/mschedule',
              'exactMatch': true
            },
            {
              'id': 'mschedule_result',
              'title': 'Schedule Result',
              'translate': 'NAV.SCHEDULE_RESULT',
              'type': 'item',
              'icon': 'library_add',
              'url': '/apps/master/mschedule_result',
              'exactMatch': true
            },
            {
              'id': 'mschedule_test',
              'title': 'Schedule Test',
              'translate': 'NAV.SCHEDULE_TEST',
              'type': 'item',
              'icon': 'library_add',
              'url': '/apps/master/mschedule_test',
              'exactMatch': true
            },
            {
              'id': 'mtest',
              'title': 'Test',
              'translate': 'NAV.TEST',
              'type': 'item',
              'icon': 'library_add',
              'url': '/apps/master/mtest',
              'exactMatch': true
            },
            {
              'id': 'mtest_reference',
              'title': 'Test Reference',
              'translate': 'NAV.TEST_REFERENCE',
              'type': 'item',
              'icon': 'library_add',
              'url': '/apps/master/mtest_reference',
              'exactMatch': true
            }
          ]
        },
        {
          'id': 'cust',
          'title': 'Customer Relationship',
          'translate': 'NAV.CUSTOMER_RELATIONSHIP',
          'type': 'collapse',
          'icon': 'web_asset',
          'children': [{
              'id': 'reg_create',
              'title': 'Registration',
              'translate': 'NAV.REGISTRATIONS',
              'type': 'item',
              'icon': 'web_asset',
              'url': '/apps/cust/reg_create',
              'exactMatch': true
            },
            {
              'id': 'reg_list',
              'title': 'Registration List',
              'translate': 'NAV.REGISTRATIONS_LIST',
              'type': 'item',
              'icon': 'web_asset',
              'url': '/apps/cust/reg_list',
              'exactMatch': true
            }
          ]
        },
        {
          'id': 'specimen',
          'title': 'Specimen Lab',
          'translate': 'NAV.SPECIMEN_LAB',
          'type': 'collapse',
          'icon': 'assignment',
          'children': [{
              'id': 'specimen_collection',
              'title': 'Specimen Collection',
              'type': 'item',
              'url': '/apps/specimen/specimen_collection',
              'exactMatch': true
            },
            {
              'id': 'specimen_handling',
              'title': 'Specimen Handling',
              'type': 'item',
              'url': '/apps/specimen/specimen_handling',
              'exactMatch': true
            },
            {
              'id': 'sample_handling',
              'title': 'Sample Handling',
              'type': 'item',
              'url': '/apps/specimen/sample_handling',
              'exactMatch': true
            }
          ]
        },
        {
          'id': 'specimen_non_lab',
          'title': 'Specimen Non Lab',
          'translate': 'NAV.SPECIMEN_NON_LAB',
          'type': 'collapse',
          'icon': 'assignment',
          'children': [{
              'id': 'specimen_collection_nl',
              'title': 'Specimen Collection Non Lab',
              'type': 'item',
              'url': '/apps/specimen_non_lab/specimen_collection_nl',
              'exactMatch': true
            },
            {
              'id': 'specimen_handling_nl',
              'title': 'Specimen Handling Non Lab',
              'type': 'item',
              'url': '/apps/specimen_non_lab/specimen_handling_nl',
              'exactMatch': true
            },
            {
              'id': 'sample_handling_nl',
              'title': 'Sample Handling Non Lab',
              'type': 'item',
              'url': '/apps/specimen_non_lab/sample_handling_nl',
              'exactMatch': true
            }
          ]
        },
        {
          'id': 'analysis',
          'title': 'Analysis Lab',
          'translate': 'NAV.ANALYSIS',
          'type': 'collapse',
          'icon': 'youtube_searched_for',
          'children': [{
              'id': 'process_sample_instrument',
              'title': 'Process Sample',
              'type': 'item',
              'url': '/apps/analysis/process_sample_instrument',
              'exactMatch': true
            },
            {
              'id': 'result_instrument',
              'title': 'Result',
              'type': 'item',
              'url': '/apps/analysis/result_instrument',
              'exactMatch': true
            },
            {
              'id': 'verification_pasien',
              'title': 'Verification',
              'type': 'item',
              'url': '/apps/analysis/verification_pasien',
              'exactMatch': true
            },
            {
              'id': 'authorization_pasien',
              'title': 'Authorization',
              'type': 'item',
              'url': '/apps/analysis/authorization_pasien',
              'exactMatch': true
            }
          ]
        },
        {
          'id': 'analysis_non_lab',
          'title': 'Analysis Non Lab',
          'translate': 'NAV.ANALYSIS_NON_LAB',
          'type': 'collapse',
          'icon': 'youtube_searched_for',
          'children': [
            {
              'id': 'process_sample_instrument_nl',
              'title': 'Process Sample Non Lab',
              'type': 'item',
              'url': '/apps/analysis_non_lab/process_sample_instrument_nl',
              'exactMatch': true
            },
            {
              'id': 'result_instrument_nl',
              'title': 'Result Non Lab',
              'type': 'item',
              'url': '/apps/analysis_non_lab/result_instrument_nl',
              'exactMatch': true
            },
            {
              'id': 'verification_pasien_nl',
              'title': 'Verification Non Lab',
              'type': 'item',
              'url': '/apps/analysis_non_lab/verification_pasien_nl',
              'exactMatch': true
            },
            {
              'id': 'authorization_pasien_nl',
              'title': 'Authorization Non Lab',
              'type': 'item',
              'url': '/apps/analysis_non_lab/authorization_pasien_nl',
              'exactMatch': true
            }
          ]
        },
        {
          'id': 'result',
          'title': 'Result',
          'translate': 'NAV.RESULT',
          'type': 'collapse',
          'icon': 'assessment',
          'children': [{
            'id': 'result_handling_pasien',
            'title': 'Result Handling',
            'type': 'item',
            'url': '/apps/result/result_handling_pasien',
            'exactMatch': true
          }]
        }
      ]
    }];
  }
}
