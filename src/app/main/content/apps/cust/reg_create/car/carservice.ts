import 'rxjs/add/operator/toPromise';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Car } from './car';
import { Tes } from './tes';

@Injectable()
export class CarService {

    constructor(private http: HttpClient) { }

    getCarsSmall() {
    return this.http.get<any>('assets/cars-small.json')
      .toPromise()
      .then(res => <Car[]>res.data)
      .then(data => data);
    }

    getCarsMedium() {
    return this.http.get<any>('assets/cars-medium.json')
      .toPromise()
      .then(res => <Car[]>res.data)
      .then(data => data);
    }

    getTest() {
      console.log("masuk sini");
    return this.http.get<any>('assets/registrasi/tess.json')
      .toPromise()
      .then(res => <Tes[]>res.data)
      .then(data => data);
    }

    getTestList(){
      return this.http.get<any>('assets/registrasi/tes.json')
      .toPromise()
      .then(res => <Tes[]>res.TestGet)
      .then(TestGet => TestGet);
    }

  //   getCarsLarge() {
  //   return this.http.get<any>('assets/showcase/data/cars-large.json')
  //     .toPromise()
  //     .then(res => <Car[]>res.data)
  //     .then(data => { return data; });
  //   }

  // getCarsHuge() {
  //   return this.http.get<any>('assets/showcase/data/cars-huge.json')
  //     .toPromise()
  //     .then(res => <Car[]>res.data)
  //     .then(data => { return data; });
  // }
}
