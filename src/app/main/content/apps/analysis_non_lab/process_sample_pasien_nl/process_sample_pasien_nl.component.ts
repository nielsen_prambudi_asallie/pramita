import { Component, ElementRef, OnInit, ViewChild, AfterViewInit  } from '@angular/core';
import { ProcessSamplePasienNonLabService } from './process_sample_pasien_nl.service';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import {ActivatedRoute, Router} from "@angular/router";
import { fuseAnimations } from '../../../../../core/animations';
import { MatPaginator, MatSort, PageEvent, MatTableDataSource } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import {debounceTime, distinctUntilChanged, startWith, tap, delay} from 'rxjs/operators';
import {merge} from "rxjs/observable/merge";
import {fromEvent} from 'rxjs/observable/fromEvent';
import { FuseUtils } from '../../../../../core/fuseUtils';
import {of} from "rxjs/observable/of";
import {catchError, finalize} from "rxjs/operators";

@Component({
    selector   : 'process_sample_pasien_nl',
    templateUrl: './process_sample_pasien_nl.component.html',
    styleUrls  : ['./process_sample_pasien_nl.component.scss'],
    animations : fuseAnimations
})
export class ProcessSamplePasienNonLabComponent implements OnInit, AfterViewInit
{
    dataSourcePasien: FilesDataSourcePasien;
    displayedColumnsPasien = ['no', 'regno', 'patientno', 'patientname', 'patientgender', 'patientage', 'patientdob', 'patientresult', 'regdate', 'company', 'cito', 'status'];
    selectedRowIndex: any;
    pasien: {insid: any, name: any}
    name: any;
    insid: any;
    pasid: any;
    pasname: any;
    age: any;
    length: number;
    pageEvent: PageEvent;
    pageIndex: any = this.pageEvent;
    pageSize: any = this.pageEvent;
    pageSizeOptions = [10, 25, 100];


    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private processSamplePasienNonLabService: ProcessSamplePasienNonLabService,
        private route : ActivatedRoute,
        private router : Router
    )
    {
    }



    ngOnInit()
    {
        this.pasien = {
            insid: this.route.snapshot.params["insid"],
            name: this.route.snapshot.params["name"]
        }
        this.insid = this.pasien['insid'];
        this.name = this.pasien['name']
        console.log(this.insid)

        this.dataSourcePasien = new FilesDataSourcePasien(this.processSamplePasienNonLabService);

        this.dataSourcePasien.loadPasien(this.length, this.insid, 0, 10, '');
    }

    ngAfterViewInit() {

        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        fromEvent(this.filter.nativeElement,'keyup')
            .pipe(
                debounceTime(150),
                distinctUntilChanged(),
                tap(() => {
                    this.paginator.pageIndex = 0;

                    this.loadPasienPage();
                })
            )
            .subscribe();

        // merge(this.sort.sortChange, this.paginator.page)
        this.paginator.page
        .pipe(
            tap(() => this.loadPasienPage())
        )
        .subscribe();

    }

    loadPasienPage() {
        this.dataSourcePasien.loadPasien(
            this.length,
            this.insid,
            this.paginator.pageIndex,
            this.paginator.pageSize,
            this.filter.nativeElement.value);
    }

    onRowClicked(dataSourcePasien, insid, name) {
        this.pasid = dataSourcePasien.regid;
        this.pasname = dataSourcePasien.patientName;
        if (dataSourcePasien.patientAge !== null && dataSourcePasien.patientAge !== '') {
            this.age = dataSourcePasien.patientAge
        } else {
            this.age = 0
        }
        this.router.navigate(['/apps/analysis_non_lab/process_sample_pasien_nl/' + this.insid + '/' + this.name + '/'  + this.pasid + '/' + this.age])
    }
}

export class FilesDataSourcePasien extends DataSource<any>
{
    private pasienSubject = new BehaviorSubject<any>([]);
    public totalData : any;

    private loadingPasien = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingPasien.asObservable();

    constructor(
        private processSamplePasienNonLabService: ProcessSamplePasienNonLabService,
    )
    {
        super()

    }

    loadPasien(
        length: number,
        insid: any,
        pageIndex:number,
        pageSize:number,
        filter:string) {

        this.loadingPasien.next(true);

        this.processSamplePasienNonLabService.getProcessSamplePasienNonLab(insid, pageIndex, pageSize, filter).pipe(
                catchError(() => of([])),
                finalize(() => this.loadingPasien.next(false))
            )
            .subscribe(pasien => {
               this.pasienSubject.next(pasien.data)
               this.totalData = pasien.totalData
            });



        }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.pasienSubject.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.pasienSubject.complete();
        this.loadingPasien.complete();
    }
}
