import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { map } from "rxjs/operators";
import { RequestOptions } from '@angular/http/src/base_request_options';
import { HttpModule, Http, Headers } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';

const SERVER_URL = 'https://202.146.232.86:4000'

@Injectable()
export class SampleHandlingDetailNonLabService
{
  private _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text' as 'text' };
  constructor(private http: HttpClient) {

  }

  createSampleHandle(regId) {
    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/SampleHandling/NON/Create')
  }

  getSpecimensItem(regId) {
    // regId = '12312312312312';
    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/test/NON')
  }

  getSampleHandleDetailTest(regId) {
    //regId = '12312312312312';
    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/test/NON')
      .toPromise()
      .then(res => res);

  }

  getSampleHandleDetailCond(regId) {
    //regId = '12312312312312';

    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/Question')
      .pipe(
      map(res => res)
      );
    // .toPromise()
    // .then(res => res);
  }

  getSampleHandleDetailSch(regId) {
    //regId = '12312312312312';

    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/schedule')
      .pipe(
      map(res => res)
      );
    // .toPromise()
    // .then(res => res);
  }

  getSampleHandleDetailPhy(regId) {
    //regId = '12312312312312';

    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/Doctor')
      // .pipe(
      //     map(res => res)
      // );
      .toPromise()
      .then(res => res);
  }

  getSpecimensCondition() {
    // regId = '12312312312312';

    return this.http.get<any>(SERVER_URL + '/Api/SpecimenCondition')
  }

  getLabNote(regId) {
    // regId='4D256DB7-6983-41B9-904D-E28A5E898BF4'
    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/LabNote')
      .pipe(
      map(res => res)
      );
  }

  getSampleHandleDetailSpecCol(regId) {
    // regId = '12312312312312';
    //return this.http.get<any>(SERVER_URL + '/Api/SampleHandling/' + regId + '/View')
    //  .pipe(
    //  map(res => res)
    //  );

    return this.http.get<any>(SERVER_URL + '/Api/SpecimenCollection/NON/' + regId + '/View')
      .pipe(
      map(res => res)
      );
    // .toPromise()
    // .then(res => res);
  }

  getSampleHandleDetailSpecHandle(regId) {
    // regId = '12312312312312';
    //return this.http.get<any>(SERVER_URL + '/Api/SampleHandling/' + regId + '/View')
    //  .pipe(
    //  map(res => res)
    //  );

    return this.http.get<any>(SERVER_URL + '/Api/SpecimenHandling/NON/' + regId + '/View')
      .pipe(
      map(res => res)
      );
    // .toPromise()
    // .then(res => res);
  }

  getSampleHandleDetail(regId) {
    // regId = '12312312312312';

    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/SampleHandling/NON')
      // .pipe(
      //     map(res => res)
      // );
      .toPromise()
      .then(res => res);
  }

  getSpecimens(regId) {
    // regId = '12312312312312';

    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/SampleHandling/NON')
  }

  postSampleHandle(sampleData) {
    let body = JSON.stringify(
      sampleData
    );
    return this.http.post(SERVER_URL + '/Api/SampleHandling/NON/submit', body, this._options)
      

  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  handleError(error: Response | any) {
    console.log(error.message || error);
    return Observable.throw(error.message || error);
  }
}
