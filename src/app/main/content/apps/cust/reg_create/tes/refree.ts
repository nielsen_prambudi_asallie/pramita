export interface Refree {
  RefreeId?: 0;
  RefreeCode?: string;
  RefreeName?: string;
  RefreeNote?: string;
}

export interface Agreement {
  AgreementID?;
  AgreementName?: string;
  AgreementNote?: string;
}
