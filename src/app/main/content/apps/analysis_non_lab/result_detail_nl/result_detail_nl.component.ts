import { Component, OnDestroy, ElementRef, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { ResultDetailNonLabService } from './result_detail_nl.service';
import { fuseAnimations } from '../../../../../core/animations';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Product } from './result_detail_nl.model';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar } from '@angular/material';
import { Location } from '@angular/common';
import { MatPaginator, MatSort,  MatTableDataSource  } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ActivatedRoute, Router } from '@angular/router';
import {of} from "rxjs/observable/of";
import {catchError, finalize} from "rxjs/operators";
import { Options } from 'selenium-webdriver';

declare var $ :any;

export interface Detail {
    nama?;
    estVol?;
    no?;
    bodySite?;
    loCSite?;
    drawSite?;
    drawUser?;
    draw?: boolean;
    condition?;
    specimenId?;
    note?;
    received?: boolean;
    receiveDate?: "yyyy-MM-dd";
    receiveUser?: "yyyy-MM-dd";
}

export interface Detail2 {
    registerTestId?;
    testName?;
    cito?: boolean;
    scheduleName?;
}

export interface Detail3 {
    id?;
    name?;
    result?; 
    result_comment?;
    auth?: boolean;
    authDate?;
    regno?;
    regdate?;
}

@Component({
    selector     : 'result_detail_nl',
    templateUrl  : './result_detail_nl.component.html',
    styleUrls    : ['./result_detail_nl.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ResultDetailNonLabComponent implements OnInit
{
    bgColor: string;
    largerValue: any = [];
    lessValue: any = [];
    process_sample_detail: {insid: any, methodId:any, insname: any, age:any, pasid: any }
    insname: any;
    insid: any;
    pasid: any;
    methodId: any;
    age: any;
    regdate: any;
    user: any;
    doctor: any;
    testing: any;
    
    errorMessage: boolean;
    successMessage: boolean;
    tampungItemProcess: any = [];
    tampungSampleProcess: any = [];
    tampungSampleReceived: any = [];
    tampungResultAuth: any = [];
    createData : any = [];
    data: any;
    createRes: any = [];
    detail: Detail[];
    detail2: Detail2[];
    detail3: Detail3[];
    drawReceive: FormGroup;
    patientNoteGroup: FormGroup;
    physicianNoteGroup: FormGroup;
    referralNoteGroup: FormGroup;
    labNoteGroup: FormGroup;
    editorGroup: FormGroup;
    manyItem: FormGroup;
    dataSourceItem: FileDetailSourceItem;
    dataSourceTest: FileDetailSourceTest;
    dataSourceSample: FileDetailSourceSample;
    dataSourceSampleHandling: FileDetailSourceSampleHandling;
    dataSourceSpecimenHandling: FileDetailSourceSpecimenHandling;
    dataSourceSpecimenCollection: FileDetailSourceSpecimenCollection;
    dataSourceLabNote: FileDetailSourceLabNote;
    dataSource: FileDetailSource;
    dataSourceSch : FileDetailSourceSch;
    dataSourceStatus: FileDetailSourceStatus;
    dataSourceStatusTest: FileDetailSourceStatusTest;
    dataSourceList: FileDetailSourceList;
    dataSourceImg: FileDetailSourceImg;
    dataSourceRun: FileDetailSourceRun;
    drawer: string;
    clickDraw: boolean = false;
    clickRe: boolean = false;
    clickCito: boolean = false;

    displayedColumns = ['question', 'answer', 'note']
    displayedColumnsNoteLab = ['no', 'note', 'user', 'date'];
    displayedColumns2= ['schedule', 'test', 'day', 'time'];
    displayedColumnsItem = ['testName', 'divider' , 'format', 'resultComment', 'divider1', 'result', 'unit', 'INP', 'reference', 'printGroup', 'rawResult', 'PRN', 'process', 'inputDate', 'procDate', 'cito', 'schedule', 'scheduleDateTime', 'resultSI', 'interfacingFlag', 'testid'];
    displayedColumnsRun = ['id', 'name', 'result', 'result_comment', 'status', 'analyzer'];
    displayedColumnsTest = ['testid', 'testname', 'testmethod'];
    displayedColumnsImg = ['regno', 'id', 'name'];
    displayedColumnsStatus = ['id', 'name', 'result', 'result_comment', 'auth', 'auth_date'];
    displayedColumnsStatusTest = [ 'name', 'result', 'result_comment', 'auth', 'authDate'];
    displayedColumnsList = ['regno', 'result', 'resultinputdate', 'p', 'interfacingresult', 'interfacingdate'];
    displayedColumnsSampleHandling = ['name', 'estvol', 'condition', 'note', 'process', 'processdate', 'processuser', 'samplehandlingid'];
    displayedColumnsSpecimenHandling = ['nama', 'estvol', 'conditionid', 'condition', 'note', 'send', 'senddate', 'process', 'processdate', 'senduser', 'processuser'];
    displayedColumnsSample = ['specimenName','no', 'estvol', 'condition', 'note', , 'process', 'processdate','send', 'senddate'];
    displayedColumnsSpecimenCollection = ['nama', 'estvol', 'receiveuser','receive','specimenid', 'conditionid', 'note', 'send', 'senddate', 'senduser'];

    selectedFormat: any;
    resComment: any;

    ckEditorConfig: {}= {
        "toolbarGroups": [
            {"name": "insert"},
            {"name": "tools"},
            { "name": "forms", "groups": [ "forms" ] }
        ],
        "removeButtons":"Source,Save,Templates,Find,Replace,Scayt,SelectAll"
    }



    initNote() {
        return this.formBuilder.group({
          LabNote: [''],
          user: 'admin'
          // date: [''],
        //   RegisterId: this.route.snapshot.params['pasid']
        })
    }

    addNoteValue() {
        const control = <FormArray>this.labNoteGroup.controls['labNotes'];
        control.push(this.initNote())
    }
    
    deleteNoteValue(index) {
        if(index > 0){
            const control = <FormArray>this.labNoteGroup.controls['labNotes'];
            control.removeAt(index);
        }
    }

    passinp(index, value) {
      this.tampungItemProcess[index].resultINP = value.controls.resultINP.value, value.controls.testId.value;
    }



    passreceived(index, value) {
        this.tampungSampleReceived[index].received = value.controls.received.value, value.controls.specimenId.value;
    }

    passauth(index, value) {
        this.tampungResultAuth[index].auth = value.controls.auth.value, value.controls.id.value;
    }

    constructor(private route: ActivatedRoute, private resultDetailNonLabService: ResultDetailNonLabService, private formBuilder: FormBuilder, private router: Router) {
        this.bgColor = 'FFFFFF';
        

        // console.log(this.drawReceive.value['drawUser'] = "admin")

        this.labNoteGroup = this.formBuilder.group({
            labNotes: this.formBuilder.array([this.initNote()])
        })

        // this.manyItem = this.formBuilder.group({
        //     itemArray: this.formBuilder.array([])
        // })

        
    }

    ngOnInit() {
        // private loadingSpecimensDetail = new BehaviorSubject<boolean>(false);
        // private specimensDetailSubject = new BehaviorSubject<any>([]);

        $.FroalaEditor.DefineIcon();

        this.process_sample_detail = {
            insid: this.route.snapshot.params['insid'],
            methodId: this.route.snapshot.params['methodId'],
            insname: this.route.snapshot.params['insname'],
            age: this.route.snapshot.params['age'],
            pasid: this.route.snapshot.params['pasid']
        }
        this.insid = this.process_sample_detail['insid'];
        this.methodId = this.process_sample_detail['methodId'];
        this.age = this.process_sample_detail['age'];
        this.pasid = this.process_sample_detail['pasid'];
        this.insname = this.process_sample_detail['insname'];


        this.resultDetailNonLabService.createResultDetailNonLab(
          this.insid, this.methodId, this.age, this.pasid).subscribe(create => {
            this.createData = create;

        })

        this.resultDetailNonLabService.getDoctor(this.pasid).then(doc => this.doctor = doc)


        // const control2 = <FormArray>this.statusTest.controls['statusTestArray'];
        // this.resultDetailNonLabService.getstatustestNonLab(this.insid, this.methodId, this.age, this.pasid).then(statTest => {
        //      this.detail3 = statTest
        //      statTest.forEach(b => {
        //        control2.push(this.initStatusTest(b))
        //          console.log("init Status Text", b)
        //          this.tampungResultAuth.push({
        //              "id": b.id,
        //              "auth": b.auth
        //          })
        //      });
        //  })


        // const control1 = <FormArray>this.manyItem.controls['itemArray'];
        // this.resultDetailNonLabService.getItem(this.pasid).then(item => {
        //     this.detail2 = item
        //     item.forEach(y => {
        //         control1.push(this.initItem(y))
        //         console.log("init item", y)
        //         this.tampungItemProcess.push({
        //             "testId" : y.testId,
        //             "inp": y.inp
        //         });
        //     });
        // })

        this.dataSourceItem = new FileDetailSourceItem(this.resultDetailNonLabService)

        this.dataSourceItem.loadDetailItem(this.insid, this.methodId, this.age, this.pasid)

        this.dataSourceTest = new FileDetailSourceTest(this.resultDetailNonLabService)

        this.dataSourceTest.loadDetailTest(this.insid, this.methodId, this.age, this.pasid)

        console.log(this.dataSourceTest.loadDetailTest(this.insid, this.methodId, this.age, this.pasid))

        this.dataSourceRun = new FileDetailSourceRun(this.resultDetailNonLabService)

        this.dataSourceRun.loadDetailRun(this.insid, this.methodId, this.age, this.pasid)

        this.dataSourceImg = new FileDetailSourceImg(this.resultDetailNonLabService)

        this.dataSourceImg.loadDetailImg(this.pasid)

        this.dataSourceStatus = new FileDetailSourceStatus(this.resultDetailNonLabService)

        this.dataSourceStatus.loadDetailStatus(this.insid, this.methodId, this.age, this.pasid)

        this.dataSourceList = new FileDetailSourceList(this.resultDetailNonLabService)

        this.dataSourceList.loadDetailList(this.pasid)

        this.dataSourceStatusTest = new FileDetailSourceStatusTest(this.resultDetailNonLabService)

        this.dataSourceStatusTest.loadDetailStatusTest(this.insid, this.methodId, this.age, this.pasid)

        this.dataSourceSample = new FileDetailSourceSample(this.resultDetailNonLabService)

        this.dataSourceSample.loadDetailSamples(this.insid, this.methodId, this.age, this.pasid)

        this.dataSourceSampleHandling = new FileDetailSourceSampleHandling(this.resultDetailNonLabService)

        this.dataSourceSampleHandling.loadDetailSampleHandling(this.pasid)

        this.dataSourceSpecimenHandling = new FileDetailSourceSpecimenHandling(this.resultDetailNonLabService)

        this.dataSourceSpecimenHandling.loadDetailSpecimenHandling(this.pasid)

        this.dataSourceSpecimenCollection = new FileDetailSourceSpecimenCollection(this.resultDetailNonLabService)

        this.dataSourceSpecimenCollection.loadDetailSpecimenCollection(this.pasid)

        this.dataSource = new FileDetailSource(this.resultDetailNonLabService);

        this.dataSource.loadDetail(this.pasid)

        this.dataSourceSch = new FileDetailSourceSch(this.resultDetailNonLabService);

        this.dataSourceSch.loadDetailSch(this.pasid)

        this.dataSourceLabNote = new FileDetailSourceLabNote(this.resultDetailNonLabService);

        this.dataSourceLabNote.loadDetailLabNote(this.pasid)

        // this.dataSourceSample = new FileDetailSourceSample(this.processSampleDetailService);

        // this.dataSourceSample.loadDetailSample(this.regid)
        

    }

    backToPasien(insid, testMethodId){
        this.router.navigate(['/apps/analysis_non_lab/result_instrument_nl/' + this.insid+ '/' + this.methodId + '/' + this.insname])
    }

    public options: Object = {
        charCounterCount: true,
        toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'paragraphFormat','specialCharacters'],
        toolbarButtonsXS: ['bold', 'italic', 'underline', 'paragraphFormat','alert'],
        toolbarButtonsSM: ['bold', 'italic', 'underline', 'paragraphFormat','alert'],
        toolbarButtonsMD: ['bold', 'italic', 'underline', 'paragraphFormat','alert'],
        events : {
            'froalaEditor.input': function(e, editor){
                this.testing = editor.selection.get().focusNode.data
                console.log(this.testing)
                // console.log("index: ", index)

            }
        }
    };

    format(event, index) {
        console.log(event)
        // this.comment(event, index);
        this.resComment = event;
        console.log(this.resComment)
    }

    procSamp(event, index) {
        this.dataSourceSample.saveProc(event, index)
    }

    receiveSample(event, index){
        this.dataSourceSample.saveRe(event, index)
    }

    comment(event, index) {
        this.dataSourceItem.saveComment(event, index)
    }

    result(event, index) {
        this.dataSourceItem.saveResult(event, index)
    }

    clickAllPrn(event) {
        this.dataSourceItem.allPrn(event)
    }

    clickAllInp(event) {
      this.dataSourceItem.AllInp(event)
    }

    prn(event, index) {
        this.dataSourceItem.savePrn(event, index)
    }

    inp(event, index) {
      this.dataSourceItem.saveInp(event, index)
    }

    auth(event, index) {
        this.dataSourceStatusTest.saveAuth(event, index)
    }

    recom(event, index) {
        this.dataSourceStatusTest.saveRecom(event, index)
        this.editorGroup = this.formBuilder.group({
            resultComment: ['']
        })
    }

    clickAllProc(event) {
        this.dataSourceSample.procAll(event)
    }

    clickAllRe(event) {
        this.dataSourceSample.reAll(event)
    }

    clickAllAuth(event) {
        this.dataSourceStatusTest.authAll(event);
    }


    cekColor(x, value){
      // console.log("x: ", x)
      // console.log("value x: ", value)
      // console.log("largerValue x: ", this.largerValue[x])
      // console.log("lessValue x: ", this.lessValue[x])
      //var valueInt = +value;
      //if(valueInt < this.lessValue[x]){
      //  this.bgColor = '24CBE5'
      //} else if(valueInt > this.largerValue[x]){
      //  this.bgColor = 'FF0000'
      //} else {
      //  this.bgColor = 'FFFFFF'
      //}
    }

    // sendData() {
    //     console.log(this.drawReceive.value["drawReceiveArray"])
    //     console.log(this.regid);
    //     // console.log(this.dataSource['value'])
    // }

    submitRes(data) {
        this.createRes = {
            registerId: this.createData.registerId,
            patientNote: this.createData.patientNote,
            referalNote: this.createData.referalNote,
            doctornote: this.createData.doctornote,
            regNo: this.createData.regNo,
            regDate: this.createData.regDate,
            labNote: this.labNoteGroup.value['labNotes'],
            // process: this.tampungItemProcess,
            // specimenCollectionForm : this.drawReceive.value['drawReceiveArray'],
            // specimenHandlingForm : null,
            // sampleHandlingForm : null,
            // labNote : this.labNoteGroup.value['labNote']
            analysisResultTest : this.createData.analysisResultTest,
            analysisResultForm : this.dataSourceSample.loadSamples(),
            analysisResultItem : this.dataSourceItem.loadItem(),
            analysisResultStatus : this.dataSourceStatusTest.loadStatTest()
        }
        console.log(this.createRes)
        data = this.createRes
        this.resultDetailNonLabService.postResNonLab(data).subscribe(res => {
            if(res == 'Submited'){
                this.successMessage = true;
                setTimeout(function () {
                        this.successMessage = false;
                        this.router.navigate(['apps/analysis_non_lab/result_instrument_nl'])
                    }.bind(this), 2000)

                // alert('Result Berhasil!');
            }else{
                this.errorMessage = true;
                setTimeout(function (){
                    this.errorMessage = false;
                }.bind(this), 2000)
            }
        },error => {
            this.errorMessage = true;
            setTimeout(function (){
                this.errorMessage = false;
            }.bind(this), 2000)
        });
        console.log(data);
       
    }
}

export class FileDetailSourceItem extends DataSource<any> 
{
    private detailItemSubject = new BehaviorSubject<any>([]);

    private loadingDetailItem = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailItem.asObservable();

    public clickprn: boolean;

    constructor(private resultDetailNonLabService: ResultDetailNonLabService) {
        super()
    }

    saveComment(resComment: any, index) {
        console.log(this.detailItemSubject.value[index].resultComment)
        this.detailItemSubject.value[index].resultComment = resComment;
        return this.detailItemSubject.value;
    }

    saveResult(res: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailItemSubject.value[index].result = res;
        return this.detailItemSubject.value;
    }

    allPrn(prn:any) {
        // if (this.clickprn) {this.clickprn = false;}else {this.clickprn = true;}
        for (var i = 0; i < this.detailItemSubject.value.length; i++) {
          this.detailItemSubject.value[i].prn = prn;
        }
        return this.detailItemSubject.value; 
    }

    AllInp(inp: any) {
      // if (this.clickinp) { this.clickinp = false; } else { this.clickinp = true; }
      for (var i = 0; i < this.detailItemSubject.value.length; i++) {
        this.detailItemSubject.value[i].resultINP = inp;
      }
      return this.detailItemSubject.value;
    }

    saveInp(inp: any, index) {
      // console.log(this.detailItemSubject.value[index])
      this.detailItemSubject.value[index].resultINP = inp;
      return this.detailItemSubject.value;
    }

    savePrn(prn: any, index) {
        // console.log(this.detailItemSubject.value[index])
      this.detailItemSubject.value[index].prn = prn;
        return this.detailItemSubject.value;
    }

    loadItem(){
        return this.detailItemSubject.value;
    }

    loadDetailItem(insid: any, methodId: any, age: any, pasid: any){
        this.loadingDetailItem.next(true);

        this.resultDetailNonLabService.getItemNonLab(insid, methodId, age, pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailItem.next(false))
        )
        .subscribe(item => this.detailItemSubject.next(item))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailItemSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailItemSubject.complete();
        this.loadingDetailItem.complete();
    }
}

export class FileDetailSourceSample extends DataSource<any> 
{
    private detailSamplesSubject = new BehaviorSubject<any>([]);

    private loadingDetailSamples = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSamples.asObservable();

    public procbol: boolean;

    public rebol: boolean;

    constructor(private resultDetailNonLabService: ResultDetailNonLabService) {
        super()
    }

    saveProc(proc: any, index){
        console.log(this.detailSamplesSubject.value)
        this.detailSamplesSubject.value[index].process = proc;
        return this.detailSamplesSubject.value;
    }

    saveRe(re: any, index){
        console.log(this.detailSamplesSubject.value)
        this.detailSamplesSubject.value[index].received = re;
        return this.detailSamplesSubject.value;
    }

    reAll(re:any) {
        // console.log(this.detailStatusTestSubject.value)
        // if (this.rebol) {this.rebol = false;}else {this.rebol = true;}
        for (var i = 0; i < this.detailSamplesSubject.value.length; i++) {
            this.detailSamplesSubject.value[i].received = re;
        }
        return this.detailSamplesSubject.value; 
    }

    procAll(proc:any) {
        // console.log(this.detailStatusTestSubject.value)
        // if (this.procbol) {this.procbol = false;}else {this.procbol = true;}
        for (var i = 0; i < this.detailSamplesSubject.value.length; i++) {
            this.detailSamplesSubject.value[i].process = proc;
        }
        return this.detailSamplesSubject.value; 
    }

    loadSamples(){
        return this.detailSamplesSubject.value;
    }

    loadDetailSamples(insid: any, methodId: any, age: any, pasid: any){
        this.loadingDetailSamples.next(true);

        this.resultDetailNonLabService.getSampleNonLab(insid, methodId, age, pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSamples.next(false))
        )
        .subscribe(samples => this.detailSamplesSubject.next(samples))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSamplesSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSamplesSubject.complete();
        this.loadingDetailSamples.complete();
    }
}

export class FileDetailSourceStatusTest extends DataSource<any> 
{
    private detailStatusTestSubject = new BehaviorSubject<any>([]);

    private loadingDetailStatusTest = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailStatusTest.asObservable();

    public authbol: boolean;

    constructor(private resultDetailNonLabService: ResultDetailNonLabService) {
        super()
    }

    saveRecom(recom: any, index){
        console.log(this.detailStatusTestSubject.value)
        // recom = "<p>hello world!</p>";
        this.detailStatusTestSubject.value[index].resultComment = recom;
        return this.detailStatusTestSubject.value;
    }

    authAll(auth:any) {
        // console.log(this.detailStatusTestSubject.value)
        // if (this.authbol) {this.authbol = false;}else {this.authbol = true;}
        for (var i = 0; i < this.detailStatusTestSubject.value.length; i++) {
            this.detailStatusTestSubject.value[i].auth = auth;
        }
        return this.detailStatusTestSubject.value; 
    }

    saveAuth(auth: any, index) {
        // console.log(this.detailItemSubject.value[index])
        this.detailStatusTestSubject.value[index].auth = auth;
        return this.detailStatusTestSubject.value;
    }

    loadStatTest(){
        return this.detailStatusTestSubject.value;
    }

    loadDetailStatusTest(insid: any, methodId: any, age: any, pasid: any){
        this.loadingDetailStatusTest.next(true);

        this.resultDetailNonLabService.getstatustestNonLab(insid, methodId, age, pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailStatusTest.next(false))
        )
        .subscribe(statusTest => this.detailStatusTestSubject.next(statusTest))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailStatusTestSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailStatusTestSubject.complete();
        this.loadingDetailStatusTest.complete();
    }
}

export class FileDetailSourceLabNote extends DataSource<any> 
{
    private detailLabNoteSubject = new BehaviorSubject<any>([]);

    private loadingDetailLabNote = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailLabNote.asObservable();

    constructor(private resultDetailNonLabService: ResultDetailNonLabService) {
        super()
    }

    loadDetailLabNote(pasid: any){
        this.loadingDetailLabNote.next(true);

        this.resultDetailNonLabService.getLabNote(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailLabNote.next(false))
        )
        .subscribe(test => this.detailLabNoteSubject.next(test))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailLabNoteSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailLabNoteSubject.complete();
        this.loadingDetailLabNote.complete();
    }
}

export class FileDetailSourceTest extends DataSource<any> 
{
    private detailTestSubject = new BehaviorSubject<any>([]);

    private loadingDetailTest = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailTest.asObservable();

    constructor(private resultDetailNonLabService: ResultDetailNonLabService) {
        super()
    }

    loadDetailTest(insid: any, testMethodId: any, pasienAge:any, pasid: any){
        this.loadingDetailTest.next(true);

        this.resultDetailNonLabService.getTestNonLab(insid, testMethodId, pasienAge, pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailTest.next(false))
        )
        .subscribe(test => this.detailTestSubject.next(test))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailTestSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailTestSubject.complete();
        this.loadingDetailTest.complete();
    }
}

export class FileDetailSourceRun extends DataSource<any> 
{
    private detailRunSubject = new BehaviorSubject<any>([]);

    private loadingDetailRun = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailRun.asObservable();

    constructor(private resultDetailNonLabService: ResultDetailNonLabService) {
        super()
    }

    loadDetailRun(insid: any, testMethodId: any, pasienAge:any, pasid: any){
        this.loadingDetailRun.next(true);

        this.resultDetailNonLabService.getTestNonLab(insid, testMethodId, pasienAge, pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailRun.next(false))
        )
        .subscribe(run => this.detailRunSubject.next(run))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailRunSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailRunSubject.complete();
        this.loadingDetailRun.complete();
    }
}

export class FileDetailSourceStatus extends DataSource<any> 
{
    private detailStatusSubject = new BehaviorSubject<any>([]);

    private loadingDetailStatus = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailStatus.asObservable();

    constructor(private resultDetailNonLabService: ResultDetailNonLabService) {
        super()
    }

    loadDetailStatus(insid: any, testMethodId: any, pasienAge:any, pasid: any){
        this.loadingDetailStatus.next(true);

        this.resultDetailNonLabService.getResultStatusNonLab(insid, testMethodId, pasienAge, pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailStatus.next(false))
        )
        .subscribe(status => this.detailStatusSubject.next(status))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailStatusSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailStatusSubject.complete();
        this.loadingDetailStatus.complete();
    }
}

export class FileDetailSourceList extends DataSource<any> 
{
    private detailListSubject = new BehaviorSubject<any>([]);

    private loadingDetailList = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailList.asObservable();

    constructor(private resultDetailNonLabService: ResultDetailNonLabService) {
        super()
    }

    loadDetailList(pasid: any){
        this.loadingDetailList.next(true);

        this.resultDetailNonLabService.getSpecimensDetailCond(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailList.next(false))
        )
        .subscribe(status => this.detailListSubject.next(status))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailListSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailListSubject.complete();
        this.loadingDetailList.complete();
    }
}

export class FileDetailSourceImg extends DataSource<any> 
{
    private detailImgSubject = new BehaviorSubject<any>([]);

    private loadingDetailImg = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailImg.asObservable();

    constructor(private resultDetailNonLabService: ResultDetailNonLabService) {
        super()
    }

    loadDetailImg(pasid: any){
        this.loadingDetailImg.next(true);

        this.resultDetailNonLabService.getSpecimensDetailCond(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailImg.next(false))
        )
        .subscribe(status => this.detailImgSubject.next(status))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailImgSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailImgSubject.complete();
        this.loadingDetailImg.complete();
    }
}

// export class FileDetailSourceSample extends DataSource<any> 
// {
//     private detailSampleSubject = new BehaviorSubject<any>([]);

//     private loadingDetailSample = new BehaviorSubject<boolean>(false);

//     public loading$ = this.loadingDetailSample.asObservable();

//     constructor(private resultDetailNonLabService: ResultDetailNonLabService) {
//         super()
//     }

//     loadDetailSample(pasid: any){
//         this.loadingDetailSample.next(true);

//         this.resultDetailNonLabService.getSample(pasid).pipe(
//              catchError(() => of([])),
//              finalize(() => this.loadingDetailSample.next(false))
//         )
//         .subscribe(sample => this.detailSampleSubject.next(sample))
//     }

//     connect(collectionViewer: CollectionViewer): Observable<any>
//     {
//         console.log("Connecting data source");
//         return this.detailSampleSubject.asObservable();   
//     }


//     disconnect(collectionViewer: CollectionViewer): void
//     {
//         this.detailSampleSubject.complete();
//         this.loadingDetailSample.complete();
//     }
// }

export class FileDetailSourceSampleHandling extends DataSource<any> 
{
    private detailSampleHandlingSubject = new BehaviorSubject<any>([]);

    private loadingDetailSampleHandling = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSampleHandling.asObservable();

    constructor(private resultDetailNonLabService: ResultDetailNonLabService) {
        super()
    }

    loadDetailSampleHandling(pasid: any){
        this.loadingDetailSampleHandling.next(true);

        this.resultDetailNonLabService.getSampleHandling(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSampleHandling.next(false))
        )
        .subscribe(sample => this.detailSampleHandlingSubject.next(sample))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSampleHandlingSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSampleHandlingSubject.complete();
        this.loadingDetailSampleHandling.complete();
    }
}

export class FileDetailSourceSpecimenHandling extends DataSource<any> 
{
    private detailSpecimenHandlingSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimenHandling = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimenHandling.asObservable();

    constructor(private resultDetailNonLabService: ResultDetailNonLabService) {
        super()
    }

    loadDetailSpecimenHandling(pasid: any){
        this.loadingDetailSpecimenHandling.next(true);

        this.resultDetailNonLabService.getSpecimenHandling(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimenHandling.next(false))
        )
        .subscribe(specimen => this.detailSpecimenHandlingSubject.next(specimen))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimenHandlingSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimenHandlingSubject.complete();
        this.loadingDetailSpecimenHandling.complete();
    }
}

export class FileDetailSourceSpecimenCollection extends DataSource<any> 
{
    private detailSpecimenCollectionSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimenCollection = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimenCollection.asObservable();

    constructor(private resultDetailNonLabService: ResultDetailNonLabService) {
        super()
    }

    loadDetailSpecimenCollection(pasid: any){
        this.loadingDetailSpecimenCollection.next(true);

        this.resultDetailNonLabService.getSpecimenCollection(pasid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimenCollection.next(false))
        )
        .subscribe(specimen => this.detailSpecimenCollectionSubject.next(specimen))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimenCollectionSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimenCollectionSubject.complete();
        this.loadingDetailSpecimenCollection.complete();
    }
}

export class FileDetailSource extends DataSource<any> 
{
    private detailSpecimensSubject = new BehaviorSubject<any>([]);

    private loadingDetailSpecimens = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimens.asObservable();

    constructor(private resultDetailNonLabService: ResultDetailNonLabService) {
        super()
    }

    loadDetail(regid: any){
        this.loadingDetailSpecimens.next(true);

        this.resultDetailNonLabService.getSpecimensDetailCond(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimens.next(false))
        )
        .subscribe(detspec => this.detailSpecimensSubject.next(detspec))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimensSubject.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimensSubject.complete();
        this.loadingDetailSpecimens.complete();
    }
}



export class FileDetailSourceSch extends DataSource<any> 
{
    private detailSpecimensSubjectSch = new BehaviorSubject<any>([]);

    private loadingDetailSpecimensSch = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingDetailSpecimensSch.asObservable();

    constructor(private resultDetailNonLabService: ResultDetailNonLabService) {
        super()
    }

    loadDetailSch(regid: any){
        this.loadingDetailSpecimensSch.next(true);

        this.resultDetailNonLabService.getSpecimensDetailSch(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingDetailSpecimensSch.next(false))
        )
        .subscribe(detspec => this.detailSpecimensSubjectSch.next(detspec))
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpecimensSubjectSch.asObservable();   
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpecimensSubjectSch.complete();
        this.loadingDetailSpecimensSch.complete();
    }
}

// export class FileDetailSourceSample extends DataSource<any> 
// {
//     private detailAnalysisSubjectSample = new BehaviorSubject<any>([]);

//     private loadingDetailAnalysisSample = new BehaviorSubject<boolean>(false);

//     public loading$ = this.loadingDetailAnalysisSample.asObservable();

//     constructor(private processSampleDetailService: ProcessSampleDetailService) {
//         super()
//     }

//     loadDetailSample(regid: any){
//         this.loadingDetailAnalysisSample.next(true);

//         this.processSampleDetailService.getAnalysisDetailSample(regid).pipe(
//              catchError(() => of([])),
//              finalize(() => this.loadingDetailAnalysisSample.next(false))
//         )
//         .subscribe(detspec => this.detailAnalysisSubjectSample.next(detspec))
//     }

//     connect(collectionViewer: CollectionViewer): Observable<any>
//     {
//         console.log("Connecting data source");
//         return this.detailAnalysisSubjectSample.asObservable();   
//     }


//     disconnect(collectionViewer: CollectionViewer): void
//     {
//         this.detailAnalysisSubjectSample.complete();
//         this.loadingDetailAnalysisSample.complete();
//     }
// }
