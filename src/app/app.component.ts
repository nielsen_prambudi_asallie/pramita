import { Component, OnInit, OnDestroy } from "@angular/core";
import { FuseSplashScreenService } from "./core/services/splash-screen.service";
import { TranslateService } from "@ngx-translate/core";
import { FuseTranslationLoaderService } from "./core/services/translation-loader.service";
import { AccordionModule } from "primeng/components/accordion/accordion";
import { MenuItem } from "primeng/components/common/api";
import { FuseNavigationService } from "./core/components/navigation/navigation.service";
import { FuseNavigationModel } from "./navigation/navigation.model";
import { locale as navigationEnglish } from "./navigation/i18n/en";
import { locale as navigationTurkish } from "./navigation/i18n/tr";
import { locale as navigationIndonesian } from "./navigation/i18n/id";
import { AuthService } from "./services/auth.service";



@Component({
  selector: 'fuse-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  loggedIn: Boolean;

  constructor(
    private fuseNavigationService: FuseNavigationService,
    private fuseSplashScreen: FuseSplashScreenService,
    private translate: TranslateService,
    private translationLoader: FuseTranslationLoaderService,
    private authService: AuthService
  ) {




    // Add languages
    this.translate.addLangs(["en", "id"]);

    // Set the default language
    this.translate.setDefaultLang('en');

    // Use a language
    this.translate.use('en');

    // Set the navigation model
    this.fuseNavigationService.setNavigationModel(new FuseNavigationModel());

    // Set the navigation translations
    this.translationLoader.loadTranslations(
      navigationEnglish,
      navigationTurkish,
      navigationIndonesian
    );

    this.authService.isLoggedInObs()
      .subscribe(flag => {
        this.loggedIn = flag;
        if (!flag) {
          this.authService.startSigninMainWindow();
        }
      });

  }


  login() {
    this.authService.startSigninMainWindow();
  }

  logout() {
    this.authService.startSignoutMainWindow();
  }
}
