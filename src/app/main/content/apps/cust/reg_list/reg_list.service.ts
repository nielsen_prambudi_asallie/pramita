import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PARAMETERS } from '@angular/core/src/util/decorators';
import {map} from "rxjs/operators";

@Injectable()
export class RegistrationListService
{
    constructor(private http:HttpClient) {

    }

    getSpecimens(pageIndex, pageSize, filter):  Observable<any> {
        
        return this.http.get<any>('https://202.146.232.86:4000/Api/Register/' + pageIndex + '/' + pageSize + '/' + filter)
        .pipe(
            map(res =>  res)
            // console.log(res['regNo'])
        );
    }

    


}
