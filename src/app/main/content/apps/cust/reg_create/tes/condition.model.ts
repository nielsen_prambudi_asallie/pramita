import {
  MatChipInputEvent
} from '@angular/material';

export class ConditionModel {
  QuestionID: number;
  QuestionName: string;
  AnswerID: number;
  AnswerName: string;
  Note: string;

  constructor(condition ? ) {
    condition = condition || {};
    this.QuestionID = condition.QuestionID || 0;
    this.QuestionName = condition.QuestionName || '';
    this.AnswerID = condition.AnswerID || '';
    this.AnswerName = condition.AnswerName || '';
    this.Note = condition.Note || '';
  }
}
