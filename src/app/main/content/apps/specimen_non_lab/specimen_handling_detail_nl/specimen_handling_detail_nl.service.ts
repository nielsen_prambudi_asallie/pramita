import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { map } from "rxjs/operators";
import { RequestOptions } from '@angular/http/src/base_request_options';
import { HttpModule, Http, Headers } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';

const SERVER_URL = 'https://202.146.232.86:4000'

@Injectable()
export class SpecimenHandlingDetailNonLabService
{
  private _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text' as 'text' };
  constructor(private http: HttpClient) {

  }

  createSpecimenHandle(regId) {
    // regId = '12312312312312';
    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/SpecimenHandling/NON/Create')
  }

  getSpecimensItem(regId) {
    // regId = '12312312312312';
    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/test/NON')
  }

  getSpecimensHandleDetailTest(regId) {
    // regId = '12312312312312';
    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/test/NON')
      .toPromise()
      .then(res => res);

  }

  getSpecimensHandleDetailCond(regId) {
    // regId = '12312312312312';

    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/Question')
      .pipe(
      map(res => res)
      );
    // .toPromise()
    // .then(res => res);
  }

  getSpecimensHandleDetailSch(regId) {
    //  regId = '12312312312312';

    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/schedule')
      .pipe(
      map(res => res)
      );
    // .toPromise()
    // .then(res => res);
  }

  getSpecimensHandleDetailPhy(regId) {
    // regId = '12312312312312';

    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/Doctor')
      // .pipe(
      //     map(res => res)
      // );
      .toPromise()
      .then(res => res);
  }

  getSpecimensCondition() {
    // regId = '12312312312312';

    return this.http.get<any>(SERVER_URL + '/Api/SpecimenCondition')
  }

  getSpecimensDetailSpechand(regId) {
    // regId = '12312312312312';
    //return this.http.get<any>(SERVER_URL + '/Api/SpecimenCollection/' + regId + '/View')
    //  .pipe(
    //  map(res => res)
    //  );

    return this.http.get<any>(SERVER_URL + '/Api/SpecimenCollection/NON/' + regId + '/View')
      .pipe(
      map(res => res)
      );
    // .toPromise()
    // .then(res => res);
  }

  getSpecimensHandleDetail(regId) {
    //  regId = '12312312312312';

    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/SpecimenHandling/NON')
      // .pipe(
      //     map(res => res)
      // );
      .toPromise()
      .then(res => res);
  }

  getSpecimens(regId) {
    // regId = '12312312312312';

    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/SpecimenHandling/NON')
  }

  postSpec(specData) {
    let body = JSON.stringify(
      specData
    );
    return this.http.post(SERVER_URL + '/Api/SpecimenHandling/NON/submit', body, this._options)
      
  }

  getLabNote(regId) {
    // regId = '0D46B0EE-7D90-490E-A2F9-F613953781EC';

    return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/labnote')
      .pipe(
      map(res => res)
      );
    // .toPromise()
    // .then(res => res);
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  handleError(error: Response | any) {
    console.log(error.message || error);
    return Observable.throw(error.message || error);
  }
}
