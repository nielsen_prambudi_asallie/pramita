import { Component, OnDestroy, ElementRef, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { SpecimenHandlingDetailNonLabService } from './specimen_handling_detail_nl.service';
import { fuseAnimations } from '../../../../../core/animations';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar } from '@angular/material';
import { Location } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from "rxjs/observable/of";
import { catchError, finalize } from "rxjs/operators";
import { Options } from 'selenium-webdriver';

export interface Detail {
  nama?;
  no?;
  estVol?;
  vol?;
  conditionId?;
  condition?;
  note?;
  received?: boolean;
  receivedDate?: "yyyy-MM-dd";
  process?: boolean;
  processDate?: "yyyy-MM-dd";
  receiveUser?;
  processUser?;
}

export interface Detail2 {
  registerTestId?;
  testName?;
  cito?: boolean;
  scheduleName?;
}

@Component({
    selector     : 'specimen_handling_detail_nl',
    templateUrl  : './specimen_handling_detail_nl.component.html',
    styleUrls    : ['./specimen_handling_detail_nl.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class SpecimenHandlingDetailNonLabComponent implements OnInit
{
  specimen_handling_detail: { regid: number, regno: any, regdate: any, name: any }
  regid: any;
  regno: any;
  regdate: any;
  name: any;
  user: any;
  errorMessage: boolean;
  successMessage: boolean;
  tampungTestCito: any = [];
  createData: any = [];
  data: any;
  createSpec: any = [];
  detail: Detail[];
  detail2: Detail2[];
  recProcess: FormGroup;
  patientNoteGroup: FormGroup;
  physicianNoteGroup: FormGroup;
  referralNoteGroup: FormGroup;
  labNoteGroup: FormGroup;
  manyTest: FormGroup;
  physicianGroup: FormGroup;
  dataSource: FileDetailSource;
  dataSourceSch: FileDetailSourceSch;
  // dataSourcePhy : FileDetailSourcePhy;
  dataSourceSpechand: FileDetailSourceSpechand;
  dataSourceLabNote: FileDetailSourceLabNote;
  dataSourceItem : FileDetailSourceItem;
  dataSourceSpc : FileDetailSourceSpc;
  drawer: string;
  clickReceive = [];
  clickProcess = [];
  clickCito: boolean = false;
  specCond: any;

  displayedColumns = ['question', 'answer', 'note'];

  displayedColumns2 = ['schedule', 'test', 'day', 'time'];

  // displayedColumns3 = ['nama', 'estVol', 'no', 'bodySite', 'loCSite', 'drawSite', 'drawUser', 'draw', 'note', 'received', 'receiveDate', 'receiveUser'];
  displayedColumns3 = ['nama', 'estVol', 'receive', 'receiveDate', 'receiveUser', 'note', 'send', 'sendDate', 'sendUser'];

  displayedColumnsNoteLab = ['no', 'labNote', 'user', 'date'];
  displayedColumnsItem= ['no', 'testName', 'cito', 'scheduleName'];
  displayedColumnsSpc = ['nama', 'estVol', 'send', 'process', 'condition',
    'note', 'sendDate', 'sendUser', 'processDate', 'processUser'];

  // displayedColumnsPhy= ['doctorName', 'addressName', 'addressLocation', 'cityName',
  // 'formTypeName', 'specialtyName'];

  initRecPros(dra) {
    const group = this.formBuilder.group({
      nama: dra.nama,
      no: dra.no,
      estVol: dra.estVol,
      vol: dra.vol,
      conditionId: dra.conditionId,
      condition: dra.condition,
      note: dra.note,
      received: dra.received,
      receivedDate: dra.receivedDate,
      process: dra.process,
      processDate: dra.processDate,
      receiveUser: (dra.received == true ? "admin" : ""),
      processUser: (dra.process == true ? "admin" : ""),
      registerSpecimenId: dra.registerSpecimenId
    })
    return group;
  }


  getClickReceive(dr, index) {
    if (this.clickReceive[index] != true) {
      dr.controls.receiveUser.value = "admin";
      this.clickReceive[index] = true
      // dr.controls.condition.value = "berhasil";
    } else {
      dr.controls.receiveUser.value = "";
      this.clickReceive[index] = false
      // dr.controls.condition.value = "";
    }
  }

  getClickProcess(dr, index) {
    if (this.clickProcess[index] != true) {
      dr.controls.processUser.value = "admin";
      this.clickProcess[index] = true
    } else {
      dr.controls.processUser.value = "";
      this.clickProcess[index] = false
    }
  }

  initTest(tes) {
    const group = this.formBuilder.group({
      registerTestId: tes.registerTestId,
      testName: tes.testName,
      cito: tes.cito,
      scheduleName: tes.scheduleName
    })
    //if (tes.cito = 0) {
    //  tes.cito = true
    //} else {
    //  tes.cito = false
    //}
    return group;

  }

  initNote() {
    return this.formBuilder.group({
      LabNote: [''],
      user: "admin",
      RegisterId: this.route.snapshot.params['regid']
    })
  }

  addNoteValue() {
    const control = <FormArray>this.labNoteGroup.controls['labNotes'];
    control.push(this.initNote())
  }

  deleteNoteValue(index) {
    if (index > 0) {
      const control = <FormArray>this.labNoteGroup.controls['labNotes'];
      control.removeAt(index);
    }
  }

  passcito(index, value) {
    this.tampungTestCito[index].cito = value.controls.cito.value, value.controls.registerTestId.value;
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private specimenHandlingDetailNonLabService: SpecimenHandlingDetailNonLabService,
    private formBuilder: FormBuilder) {

    this.recProcess = this.formBuilder.group({
      recProcessArray: this.formBuilder.array([])
    })

    // console.log(this.recProcess.value['drawUser'] = "admin")

    this.patientNoteGroup = this.formBuilder.group({
      patientNote: [''],
      patientName: [''],
      regDate: [''],
      patientId: ['']
    })

    this.physicianNoteGroup = this.formBuilder.group({
      physicianNote: [''],
      physicianName: [''],
      regDate: ['']
    })

    this.referralNoteGroup = this.formBuilder.group({
      referralNote: [''],
      companyName: [''],
      regDate: ['']
    })

    this.labNoteGroup = this.formBuilder.group({
      // labNote: ['']
      labNotes: this.formBuilder.array([this.initNote()])
    })

    this.manyTest = this.formBuilder.group({
      testArray: this.formBuilder.array([])
    })

    this.physicianGroup = this.formBuilder.group({
      frontTitle: [''],
      doctorName: [''],
      endTitle: [''],
      addressName: [''],
      addressLocation: [''],
      cityName: [''],
      formTypeName: [''],
      specialtyName: ['']
    })
  }

  ngOnInit() {
    // private loadingSpecimensDetail = new BehaviorSubject<boolean>(false);
    // private specimensDetailSubject = new BehaviorSubject<any>([]);

    this.specimen_handling_detail = {
      regid: this.route.snapshot.params['regid'],
      regno: this.route.snapshot.params['regno'],
      regdate: this.route.snapshot.params['regdate'],
      name: this.route.snapshot.params['name']
    }
    this.regid = this.specimen_handling_detail['regid'];
    this.regno = this.specimen_handling_detail['regno'];
    this.regdate = this.specimen_handling_detail['regdate'];
    this.name = this.specimen_handling_detail['name'];

    this.specimenHandlingDetailNonLabService.createSpecimenHandle(this.regid).subscribe(create => {
      this.createData = create;
      console.log("create: ", create)
      this.physicianNoteGroup.patchValue({
        physicianNote: create[0].doctornote,
        physicianName: create[0].doctorName,
        regDate: create[0].regDate
      });
      this.patientNoteGroup.patchValue({
        patientNote: create[0].patientNote,
        patientName: create[0].patientName,
        regDate: create[0].regDate,
        patientId: create[0].patientID
      });
      this.referralNoteGroup.patchValue({
        referralNote: create[0].referalNote,
        companyName: create[0].referalCompanyName,
        regDate: create[0].regDate
      });
    })

    this.specimenHandlingDetailNonLabService.getSpecimensCondition().subscribe(
      specCond => this.specCond = specCond);

    //const control = <FormArray>this.recProcess.controls['recProcessArray'];
    //this.specimenHandlingDetailNonLabService.getSpecimensHandleDetail(this.regid).then(dra => {
    //  this.detail = dra
    //  dra.forEach(x => {
    //    // x.forEach(y => {
    //      this.clickProcess.push(false)
    //      this.clickReceive.push(x.received)
    //      control.push(this.initRecPros(x))
    //      console.log("init Dra re", x)
    //    // });
    //  });
    //})


    const control1 = <FormArray>this.manyTest.controls['testArray'];
    this.specimenHandlingDetailNonLabService.getSpecimensHandleDetailTest(this.regid).then(tes => {
      this.detail2 = tes
      tes.forEach(y => {
        control1.push(this.initTest(y))
        console.log("init test", y)
        this.tampungTestCito.push({
          "testId": y.registerTestId,
          "testName": y.testName,
          "cito": y.cito,
          "scheduleName": y.scheduleName
        });
      });
    })

    this.specimenHandlingDetailNonLabService.getSpecimensHandleDetailPhy(this.regid)
      .then(phys => {
        this.physicianGroup.patchValue({
          frontTitle: phys.frontTitle,
          doctorName: phys.doctorName,
          endTitle: phys.endTitle,
          addressName: phys.addressName,
          addressLocation: phys.addressLocation,
          cityName: phys.cityName,
          formTypeName: phys.formTypeName,
          specialtyName: phys.specialtyName
        })
      });

    this.dataSource = new FileDetailSource(this.specimenHandlingDetailNonLabService);

    this.dataSource.loadDetail(this.regid)

    this.dataSourceSch = new FileDetailSourceSch(this.specimenHandlingDetailNonLabService);

    this.dataSourceSch.loadDetailSch(this.regid)

    this.dataSourceSpechand = new FileDetailSourceSpechand(this.specimenHandlingDetailNonLabService);

    this.dataSourceSpechand.loadDetailSpechand(this.regid)

    this.dataSourceLabNote = new FileDetailSourceLabNote(this.specimenHandlingDetailNonLabService);

    this.dataSourceLabNote.loadDetailLabNote(this.regid)

    // this.dataSourcePhy = new FileDetailSourcePhy(this.specimenHandlingDetailService);
    // this.dataSourcePhy.loadDetailPhy(this.regid)

    this.dataSourceItem = new FileDetailSourceItem(this.specimenHandlingDetailNonLabService);
    this.dataSourceItem.loadDetailItem(this.regid)

    this.dataSourceSpc = new FileDetailSourceSpc(this.specimenHandlingDetailNonLabService);
    this.dataSourceSpc.loadSpc(this.regid)
  }

  // sendData() {
  //     console.log(this.drawReceive.value["drawReceiveArray"])
  //     console.log(this.regid);
  //     // console.log(this.dataSource['value'])
  // }
  
  clickprocessed(event) {
    this.dataSourceSpc.ProcessAll(event);
  }

  clickrec(event) {
    this.dataSourceSpc.ReceiveAll(event);
  }

  cito(event, index) {
    this.dataSourceItem.saveCito(event, index)
  }
  process(event, index){
    // console.log("process: ", event)
    this.dataSourceSpc.saveProcess(event, index)
  }
  receive(event, index){
    this.dataSourceSpc.saveReceive(event, index)
  }
  note(event, index){
    this.dataSourceSpc.saveNote(event, index)
  }
  cond(event, index){
    // console.log("cond: ", event)
    this.dataSourceSpc.saveCondID(event, index)
  }

  submitSpecHandle(data) {
    this.createSpec = {
      registerId: this.regid,
      patientNote: this.patientNoteGroup.value['patientNote'],
      referalNote: this.referralNoteGroup.value['referralNote'],
      doctornote: this.physicianNoteGroup.value['physicianNote'],
      registerNo: this.regno,
      regDate: this.regdate,
      labNote: this.labNoteGroup.value['labNotes'],
      // cito: this.tampungTestCito,
      cito: this.dataSourceItem.dataItem(),
      specimenCollectionForm: null,
      // specimenHandlingForm: this.recProcess.value['recProcessArray'],
      specimenHandlingForm : this.dataSourceSpc.dataSpc(),
      sampleHandlingForm: null
    }
    // console.log(this.createSpec)
    data = this.createSpec
    this.specimenHandlingDetailNonLabService.postSpec(data).subscribe(res => {
      if(res == 'Submited'){
          this.successMessage = true;
          setTimeout(function () {
                  this.successMessage = false;
                  this.router.navigate(['/apps/specimen_non_lab/specimen_handling_nl']);
              }.bind(this), 2000)

          // alert('Result Berhasil!');
      }else{
          this.errorMessage = true;
          setTimeout(function (){
              this.errorMessage = false;
          }.bind(this), 2000)
      }
  },error => {
      this.errorMessage = true;
      setTimeout(function (){
          this.errorMessage = false;
      }.bind(this), 2000)
  });
    
  }
}

export class FileDetailSource extends DataSource<any>
{
  private detailSpecimensSubject = new BehaviorSubject<any>([]);

  private loadingDetailSpecimens = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingDetailSpecimens.asObservable();

  constructor(private specimenHandlingDetailNonLabService: SpecimenHandlingDetailNonLabService) {
    super()
  }

  loadDetail(regid: any) {
    this.loadingDetailSpecimens.next(true);

    this.specimenHandlingDetailNonLabService.getSpecimensHandleDetailCond(regid).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingDetailSpecimens.next(false))
    )
      .subscribe(detspec => this.detailSpecimensSubject.next(detspec))
  }

  connect(collectionViewer: CollectionViewer): Observable<any> {
    console.log("Connecting data source");
    return this.detailSpecimensSubject.asObservable();
  }


  disconnect(collectionViewer: CollectionViewer): void {
    this.detailSpecimensSubject.complete();
    this.loadingDetailSpecimens.complete();
  }
}

export class FileDetailSourceSch extends DataSource<any>
{
  private detailSpecimensSubjectSch = new BehaviorSubject<any>([]);

  private loadingDetailSpecimensSch = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingDetailSpecimensSch.asObservable();

  constructor(private specimenHandlingDetailNonLabService: SpecimenHandlingDetailNonLabService) {
    super()
  }

  loadDetailSch(regid: any) {
    this.loadingDetailSpecimensSch.next(true);

    this.specimenHandlingDetailNonLabService.getSpecimensHandleDetailSch(regid).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingDetailSpecimensSch.next(false))
    )
      .subscribe(detspec => this.detailSpecimensSubjectSch.next(detspec))
  }

  connect(collectionViewer: CollectionViewer): Observable<any> {
    console.log("Connecting data source");
    return this.detailSpecimensSubjectSch.asObservable();
  }


  disconnect(collectionViewer: CollectionViewer): void {
    this.detailSpecimensSubjectSch.complete();
    this.loadingDetailSpecimensSch.complete();
  }
}

// export class FileDetailSourcePhy extends DataSource<any>
// {
//     private detailSpecimensSubjectPhy = new BehaviorSubject<any>([]);

//     private loadingDetailSpecimensPhy = new BehaviorSubject<boolean>(false);

//     public loading$ = this.loadingDetailSpecimensPhy.asObservable();

//     constructor(private specimenHandlingDetailService: SpecimenHandlingDetailService) {
//         super()
//     }

//     loadDetailPhy(regid: any){
//         this.loadingDetailSpecimensPhy.next(true);

//         this.specimenHandlingDetailService.getSpecimensHandleDetailPhy(regid).pipe(
//              catchError(() => of([])),
//              finalize(() => this.loadingDetailSpecimensPhy.next(false))
//         )
//         .subscribe(detspec => this.detailSpecimensSubjectPhy.next(detspec))
//     }

//     connect(collectionViewer: CollectionViewer): Observable<any>
//     {
//         console.log("Connecting data source");
//         return this.detailSpecimensSubjectPhy.asObservable();
//     }


//     disconnect(collectionViewer: CollectionViewer): void
//     {
//         this.detailSpecimensSubjectPhy.complete();
//         this.loadingDetailSpecimensPhy.complete();
//     }
// }

export class FileDetailSourceLabNote extends DataSource<any>
{
  private detailLabNoteSubject = new BehaviorSubject<any>([]);

  private loadingDetailLabNote = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingDetailLabNote.asObservable();

  constructor(private specimenHandlingDetailNonLabService: SpecimenHandlingDetailNonLabService) {
    super()
  }

  loadDetailLabNote(pasid: any) {
    this.loadingDetailLabNote.next(true);

    this.specimenHandlingDetailNonLabService.getLabNote(pasid).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingDetailLabNote.next(false))
    )
      .subscribe(test => this.detailLabNoteSubject.next(test))
  }

  connect(collectionViewer: CollectionViewer): Observable<any> {
    console.log("Connecting data source");
    return this.detailLabNoteSubject.asObservable();
  }


  disconnect(collectionViewer: CollectionViewer): void {
    this.detailLabNoteSubject.complete();
    this.loadingDetailLabNote.complete();
  }
}

export class FileDetailSourceSpechand extends DataSource<any>
{
  private detailSpecimensSubjectSpechand = new BehaviorSubject<any>([]);

  private loadingDetailSpecimensSpechand = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingDetailSpecimensSpechand.asObservable();

  constructor(private specimenHandlingDetailNonLabService: SpecimenHandlingDetailNonLabService) {
    super()
  }

  loadDetailSpechand(regid: any) {
    this.loadingDetailSpecimensSpechand.next(true);

    this.specimenHandlingDetailNonLabService.getSpecimensDetailSpechand(regid).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingDetailSpecimensSpechand.next(false))
    )
      .subscribe(detspec => this.detailSpecimensSubjectSpechand.next(detspec))
  }

  connect(collectionViewer: CollectionViewer): Observable<any> {
    console.log("Connecting data source");
    return this.detailSpecimensSubjectSpechand.asObservable();
  }


  disconnect(collectionViewer: CollectionViewer): void {
    this.detailSpecimensSubjectSpechand.complete();
    this.loadingDetailSpecimensSpechand.complete();
  }
}

export class FileDetailSourceItem extends DataSource<any>
{
    private detailItem = new BehaviorSubject<any>([]);

    private loadingItem = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingItem.asObservable();

    constructor(private specimenHandlingDetailNonLabService: SpecimenHandlingDetailNonLabService) {
        super()
    }

    saveCito(cito: any, index) {
      // console.log(this.detailItem.value[index])
        this.detailItem.value[index].cito = cito;
        return this.detailItem.value;
    }

    dataItem(){
      return this.detailItem.value;
    }

    loadDetailItem(regid: any){
        this.loadingItem.next(true);

        this.specimenHandlingDetailNonLabService.getSpecimensItem(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingItem.next(false))
        )
        .subscribe(detItem => {
          // console.log("detItem: ", detItem)
           this.detailItem.next(detItem)
        })
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailItem.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailItem.complete();
        this.loadingItem.complete();
    }
}

export class FileDetailSourceSpc extends DataSource<any>
{
    private detailSpc = new BehaviorSubject<any>([]);

    private loadingSpc = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSpc.asObservable();

    public clickproses: boolean;

    public clickrec: boolean;

    constructor(private specimenHandlingDetailNonLabService: SpecimenHandlingDetailNonLabService) {
        super()
        this.clickproses = false;
        this.clickrec = false;
    }

    ProcessAll(process:any) {
      // console.log(this.detailItem.value[index])
      // if (this.clickproses) { this.clickproses = false; } else { this.clickproses = true; }
      for (var i = 0; i < this.detailSpc.value.length; i++) {
        this.detailSpc.value[i].process = process;
        if (this.detailSpc.value[i].process == true) {
          this.detailSpc.value[i].processUser = 'admin';
        } else {
          this.detailSpc.value[i].processUser = '';
        }
      }
      return this.detailSpc.value;
    }

    ReceiveAll(receive:any) {
      // console.log(this.detailItem.value[index])
      // if (this.clickrec) { this.clickrec = false; } else { this.clickrec = true; }
      for (var i = 0; i < this.detailSpc.value.length; i++) {
        this.detailSpc.value[i].received = receive;
        if (this.detailSpc.value[i].received == true) {
          this.detailSpc.value[i].receiveUser = 'admin';
        } else {
          this.detailSpc.value[i].receiveUser = '';
        }
      }
      return this.detailSpc.value;
    }

    saveProcess(process:any, index) {
      // console.log(this.detailItem.value[index])
        this.detailSpc.value[index].process = process;
        if (this.detailSpc.value[index].process == true) {
          this.detailSpc.value[index].processUser = 'admin';
        } else {
          this.detailSpc.value[index].processUser = '';
        }
        return this.detailSpc.value;
    }

    saveReceive(rec: any, index) {
      // console.log(this.detailItem.value[index])
        this.detailSpc.value[index].received = rec;
        if (this.detailSpc.value[index].received == true) {
          this.detailSpc.value[index].receiveUser = 'admin';
        } else {
          this.detailSpc.value[index].receiveUser = '';
        }
        return this.detailSpc.value;
    }

    saveCondID(condId: any, index) {
      // console.log(this.detailItem.value[index])
        this.detailSpc.value[index].conditionId = condId;
        return this.detailSpc.value;
    }

    saveNote(note: any, index) {
      // console.log(this.detailItem.value[index])
        this.detailSpc.value[index].note = note;
        return this.detailSpc.value;
    }

    dataSpc(){
      return this.detailSpc.value;
    }

    loadSpc(regid: any){
        this.loadingSpc.next(true);

        this.specimenHandlingDetailNonLabService.getSpecimens(regid).pipe(
             catchError(() => of([])),
             finalize(() => this.loadingSpc.next(false))
        )
        .subscribe(detSpc => {
          // console.log("detSpc: ", detSpc)
           this.detailSpc.next(detSpc)
        })
    }

    connect(collectionViewer: CollectionViewer): Observable<any>
    {
        console.log("Connecting data source");
        return this.detailSpc.asObservable();
    }


    disconnect(collectionViewer: CollectionViewer): void
    {
        this.detailSpc.complete();
        this.loadingSpc.complete();
    }
}
