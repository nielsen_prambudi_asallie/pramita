import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from "@angular/forms"
import { SharedModule } from '../../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { MatInputModule, MatSelectModule, MatFormFieldModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { PickListModule, CalendarModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/components/accordion/accordion';
import { MenuItem } from 'primeng/components/common/api';
import { TreeModule } from 'primeng/primeng';
import { MessagesModule } from 'primeng/primeng';
import { MessageModule } from 'primeng/primeng';
import { RegistrationCreateComponent } from './reg_create/reg_create.component';
import { RegistrationListComponent } from './reg_list/reg_list.component';
import { RegistrationListService } from './reg_list/reg_list.service';
import { RegistrationListDetailComponent } from './reg_list/reg_list_detail/reg_list_detail.component';
import { RegistrationListDetailService } from './reg_list/reg_list_detail/reg_list_detail.service';
import { InputMaskModule } from 'primeng/inputmask';
import { MycurrencyPipe } from './pipe/mycurrency.pipe';
import { MycurrencyDirective } from './directive/mycurrency.directive';
import { NgSelectModule } from '@ng-select/ng-select';
import {AutoCompleteModule} from 'primeng/autocomplete';
import { CurrencyMaskModule } from "ng2-currency-mask";
// import { Inputmask } from "inputmask";


declare const $: any;

const routes: Routes = [
  {
    path     : 'reg_create',
    component: RegistrationCreateComponent
  },
  {
    path     : 'reg_list',
    component: RegistrationListComponent
  },
  {
    path     : 'reg_list/:regid/:regno',
    component: RegistrationListDetailComponent
  }
];

@NgModule({
  imports     : [
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    AccordionModule,
    PickListModule,
    InputMaskModule,
    CalendarModule,
    NgSelectModule,
    MessageModule,
    MessagesModule,
    TreeModule,
    AutoCompleteModule,
    ReactiveFormsModule,
    CurrencyMaskModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    RegistrationCreateComponent,
    RegistrationListComponent,
    RegistrationListDetailComponent,
    MycurrencyPipe,
    MycurrencyDirective
  ],
  providers   : [
    RegistrationListService,
    RegistrationListDetailService
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class RegistrationModule
{
}
