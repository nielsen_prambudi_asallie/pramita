import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { SampleHandlingService } from './sample_handling.service';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from "@angular/router";
import { fuseAnimations } from '../../../../../core/animations';
import { MatPaginator, MatSort, PageEvent, MatTableDataSource } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { debounceTime, distinctUntilChanged, startWith, tap, delay } from 'rxjs/operators';
import { merge } from "rxjs/observable/merge";
import { fromEvent } from 'rxjs/observable/fromEvent';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { of } from "rxjs/observable/of";
import { catchError, finalize } from "rxjs/operators";

@Component({
    selector: 'sample_handling',
    templateUrl: './sample_handling.component.html',
    styleUrls  : ['./sample_handling.component.scss'],
    animations : fuseAnimations
})
export class SampleHandlingComponent implements OnInit, AfterViewInit
{
  dataSource: FilesDataSource;
  displayedColumns = [ 'no', 'regno', 'patientno', 'patientname', 'patientgender', 'patientage', 'patientdob', 'patientresult', 'regdate', 'company', 'cito', 'status'];
  selectedRowIndex: any;


  pageEvent: PageEvent;
  pageIndex: any = this.pageEvent;
  pageSize: any = this.pageEvent;
  length: number;
  pageSizeOptions = [10, 25, 100];


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('filter') filter: ElementRef;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private sampleHandlingService: SampleHandlingService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    // this.course = this.route.snapshot.data["course"];

    this.dataSource = new FilesDataSource(this.sampleHandlingService);
    this.dataSource.loadSample(this.length, 0, 10, '');

  }


  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(
      debounceTime(150),
      distinctUntilChanged(),
      tap(() => {
        this.paginator.pageIndex = 0;
        this.loadSamplePage();
      })
      )
      .subscribe();

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
      tap(() => this.loadSamplePage())
      )
      .subscribe();

  }

  loadSamplePage() {
    this.dataSource.loadSample(
      this.length,
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.filter.nativeElement.value);
  }

  onRowClicked(dataSource) {
    console.log('Row clicked: ', dataSource);
    console.log('Row clicked id', dataSource.regid);
  }
}

export class FilesDataSource extends DataSource<any>
{
  private specimensSubject = new BehaviorSubject<any>([]);
  public totalData : any;

  private loadingSpecimens = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSpecimens.asObservable();

  constructor(
    private sampleHandlingService: SampleHandlingService,
  ) {
    super()

  }

  loadSample(
    length: number,
    pageIndex: number,
    pageSize: number,
    filter: string) {

    this.loadingSpecimens.next(true);

    this.sampleHandlingService.getSpecimens(pageIndex, pageSize, filter).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSpecimens.next(false))
    )
    .subscribe(specimens => {
      this.specimensSubject.next(specimens.data)
      this.totalData = specimens.totalData
    });

  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(collectionViewer: CollectionViewer): Observable<any> {
    console.log("Connecting data source");
    return this.specimensSubject.asObservable();
  }


  disconnect(collectionViewer: CollectionViewer): void {
    this.specimensSubject.complete();
    this.loadingSpecimens.complete();
  }
}
