import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {map} from "rxjs/operators";
import { RequestOptions } from '@angular/http/src/base_request_options';
import {HttpModule, Http, Headers} from '@angular/http';
import { HttpHeaders } from '@angular/common/http';

const SERVER_URL = 'https://202.146.232.86:4000'

@Injectable()
export class VerificationDetailService
{
    private _options ={ headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text' as 'text'};
    constructor(private http:HttpClient) {

    }

    // createProcessSampleDetail(insId, regId) {
    //   //regId="5B5CA7A6-35E7-421B-A4E8-92A9DA132EDD"
    //   return this.http.get<any>(SERVER_URL + '/Api/Register/' + insId + '/' + regId + '/AnalysisResult/Create')
    // }

    createVerificationDetail(regId) {
      //regId="5B5CA7A6-35E7-421B-A4E8-92A9DA132EDD"
      return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/VerificationAnalyst/Create')
    }

    // getAnalysisDetailSample(regId) {

    //     return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/Question')
    //     .pipe(
    //         map(res => res)
    //     );
    // }

    getLabNote(regId) {
        //regId = '0D46B0EE-7D90-490E-A2F9-F613953781EC';

        return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/labnote')
        .pipe(
            map(res => res)
        );
        // .toPromise()
        // .then(res => res);
    }

    getItem(regId) {
      //regId = '5B5CA7A6-35E7-421B-A4E8-92A9DA132EDD';
      return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/VerificationAnalyst/Create')
        .toPromise()
        .then(res => res.verificationAnalystItem);

    }

    // getResultStatus(regId) {
    //   //regId = '12312312312312';

    //   return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/VerificationAnalyst/Create')
    //     .pipe(
    //     map(res => res.analysisResultStatus)
    //     );
    //   // .toPromise()
    //   // .then(res => res);
    // }

    getSpecimensDetailTest(regId) {
        //regId = '12312312312312';
        return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/test')
        .toPromise()
        .then(res => res);

    }

    // getstatustest(regId) {
    //   //regId = '5B5CA7A6-35E7-421B-A4E8-92A9DA132EDD';

    //   return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/VerificationAnalyst/Create')
    //     //.pipe(
    //     //    map(res => res.processSampleForm)
    //     //);
    //     .toPromise()
    //     .then(res => res.analysisResultStatusTest);
    // }

    getSpecimensDetailCond(regId) {
       // regId = '12312312312312';

        return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/Question')
        .pipe(
            map(res => res)
        );
        // .toPromise()
        // .then(res => res);
    }

    getDoctor(regId) {
        //regId = '12312312312312';

        return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/Doctor')
        // .pipe(
        //     map(res => res)
        // );
        .toPromise()
        .then(res => res);
    }

    // getTest(regId) {
    //   //regId = '5B5CA7A6-35E7-421B-A4E8-92A9DA132EDD';

    //   return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/VerificationAnalyst/Create')
    //     .pipe(
    //     map(res => res.analysisResultTest)
    //     );
    //   // .toPromise()
    //   // .then(res => res);
    // }

    // getRun(insId, regId) {
    //   //regId = '5B5CA7A6-35E7-421B-A4E8-92A9DA132EDD';

    //   return this.http.get<any>(SERVER_URL + '/Api/Register/' + insId + '/' + regId + '/AnalysisResult/Create')
    //     .pipe(
    //     map(res => res.analysisResultTest)
    //     );
    //   // .toPromise()
    //   // .then(res => res);
    // }

    // getSample(insId, regId) {
    //   //regId = '5B5CA7A6-35E7-421B-A4E8-92A9DA132EDD';

    //   return this.http.get<any>(SERVER_URL + '/Api/Register/' + insId + '/' + regId + '/AnalysisResult/Create')
    //     //.pipe(
    //     //    map(res => res.processSampleForm)
    //     //);
    //     .toPromise()
    //     .then(res => res.analysisResultForm);
    // }

    getSampleHandling(regId) {
        //regId = '12312312312312';

        return this.http.get<any>(SERVER_URL + '/Api/SpecimenHandling/' + regId + '/View')
        .pipe(
            map(res => res)
        );
        // .toPromise()
        // .then(res => res);
    }

    getSpecimenHandling(regId) {
        //regId = '12312312312312';

        return this.http.get<any>(SERVER_URL + '/Api/SpecimenHandling/' + regId + '/View')
        .pipe(
            map(res => res)
        );
        // .toPromise()
        // .then(res => res);
    }

    getSpecimenCollection(regId) {
       // regId = '12312312312312';

        return this.http.get<any>(SERVER_URL + '/Api/SpecimenCollection/' + regId + '/View')
        .pipe(
            map(res => res)
        );
        // .toPromise()
        // .then(res => res);
    }

    getSpecimensDetailSch(regId) {
       // regId = '12312312312312';

        return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/schedule')
        .pipe(
            map(res => res)
        );
        // .toPromise()
        // .then(res => res);
    }

    getSpecimensDetail(regId) {
       // regId = '12312312312312';

        return this.http.get<any>(SERVER_URL + '/Api/Register/' + regId + '/SpecimenCollection')
        // .pipe(
        //     map(res => res)
        // );
        .toPromise()
        .then(res => res);
    }

    postVer(specData) {
        let body = JSON.stringify(
          specData
        );
        return this.http.post(SERVER_URL + '/Api/VerificationAnalyst/submit', body, this._options)
          
    }

    private extractData(res: Response) {
        let body = res;
        return body || {};
    }

    handleError(error: Response | any) {
      console.log(error.message || error);
      return Observable.throw(error.message || error);
    }
}
