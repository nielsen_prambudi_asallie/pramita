function printZPL() {
    var config = qz.configs.create("Printer Name");
 
    var data = [
       '^XA\n',
       '^FO50,50^ADN,36,20^FDPRINTED USING QZ TRAY PLUGIN\n',
       { 
         type: 'raw', format: 'image', data: 'assets/img/image_sample_bw.png', 
         options: { language: "ZPL" } 
       }, 
       '^FS\n',
       '^XZ\n'
    ];
 
    qz.print(config, data).catch(function(e) { console.error(e); });
 }