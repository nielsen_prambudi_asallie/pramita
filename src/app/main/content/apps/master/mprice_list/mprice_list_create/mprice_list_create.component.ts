import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MasterPriceListCreateService } from './mprice_list_create.service';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import {ActivatedRoute} from "@angular/router";
import { fuseAnimations } from '../../../../../../core/animations';
import { MatPaginator, MatSort, PageEvent, MatTableDataSource } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import {debounceTime, distinctUntilChanged, startWith, tap, delay} from 'rxjs/operators';
import {merge} from "rxjs/observable/merge";
import {fromEvent} from 'rxjs/observable/fromEvent';
import { FuseUtils } from '../../../../../../core/fuseUtils';
import {of} from "rxjs/observable/of";
import {catchError, finalize} from "rxjs/operators";

@Component({
    selector   : 'mprice_list_create',
    templateUrl: './mprice_list_create.component.html',
    styleUrls  : ['./mprice_list_create.component.scss'],
    animations : fuseAnimations
})
export class MasterPriceListCreateComponent implements OnInit, AfterViewInit
{

  constructor(
      private masterPriceListCreateService: MasterPriceListCreateService,
      private route : ActivatedRoute
    )
  {
  }

  ngOnInit()
  {


  }

  ngAfterViewInit() {

  }


}

