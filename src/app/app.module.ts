import { NgModule, APP_INITIALIZER } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule, Routes } from "@angular/router";
import { InMemoryWebApiModule } from "angular-in-memory-web-api";
import "hammerjs";
import { SharedModule } from "./core/modules/shared.module";
import { AppComponent } from "./app.component";
import { ProjectModule } from "./main/content/apps/dashboards/project/project.module";
import { FuseFakeDbService } from "./fuse-fake-db/fuse-fake-db.service";
import { FuseMainModule } from "./main/main.module";
import { PagesModule } from "./main/content/pages/pages.module";
import { UIModule } from "./main/content/ui/ui.module";
import { ComponentsModule } from "./main/content/components/components.module";
import { FuseSplashScreenService } from "./core/services/splash-screen.service";
import { FuseConfigService } from "./core/services/config.service";
import { FuseNavigationService } from "./core/components/navigation/navigation.service";
import { ComponentsThirdPartyModule } from "./main/content/components-third-party/components-third-party.module";
import { ServicesModule } from "./main/content/services/services.module";
import { FuseAngularMaterialModule } from "./main/content/components/angular-material/angular-material.module";
import { MarkdownModule } from "angular2-markdown";
import { TranslateModule } from "@ngx-translate/core";
import { AppStoreModule } from "./store/store.module";
import { AccordionModule } from "primeng/primeng";
import { MenuItem } from "primeng/primeng";
import { TextMaskModule } from "angular2-text-mask";
import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { AuthService } from "./services/auth.service";
import { AuthGuardService } from "./guards/auth.guard";



const appRoutes: Routes = [
  {
    path: "apps/master",
    loadChildren:
      "./main/content/apps/master/master.module#MasterModule"
  },
  {
    path: "apps/specimen",
    loadChildren:
      "./main/content/apps/specimen/specimen.module#SpecimenModule"
  },
  {
    path: "apps/specimen_non_lab",
    loadChildren:
      "./main/content/apps/specimen_non_lab/specimen_nl.module#SpecimenNonLabModule"
  },
  {
    path: "apps/analysis",
    loadChildren:
      "./main/content/apps/analysis/analysis.module#AnalysisModule"
  },
  {
    path: "apps/analysis_non_lab",
    loadChildren:
      "./main/content/apps/analysis_non_lab/analysis_nl.module#AnalysisNonLabModule"
  },
  {
    path: "apps/result",
    loadChildren:
      "./main/content/apps/result/result.module#ResultModule"
  },
  {
    path: "apps/cust",
    loadChildren:
      "./main/content/apps/cust/registration.module#RegistrationModule"
  },
  {
    path: "**",
    redirectTo: "apps/dashboards/project"
  }
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    SharedModule,
    MarkdownModule.forRoot(),
    TranslateModule.forRoot(),
    InMemoryWebApiModule.forRoot(FuseFakeDbService, {
      delay: 0,
      passThruUnknownUrl: true
    }),
    AppStoreModule,
    FuseMainModule,
    ProjectModule,
    PagesModule,
    UIModule,
    ServicesModule,
    ComponentsModule,
    FuseAngularMaterialModule,
    ComponentsThirdPartyModule,
    HttpModule,
    TextMaskModule
  ],
  providers: [
    FuseSplashScreenService,
    FuseConfigService,
    FuseNavigationService,
    AuthGuardService,
    AuthService,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(

  ) {

  }
}
