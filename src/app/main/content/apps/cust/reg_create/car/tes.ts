export interface Tes {
  TestID?;
  TestName?: string;
  TestGroup?: string;
  TestPrice?;
  schedule_name?: string;
  children?: Tes[];
  expanded?: boolean;
}

